grammar IOMonkey2;

options
{
	language=Java;
	output=AST;
}


tokens 
{
	DOCUMENT;
	PACKAGE;
	IMPORT;
	
	OPTIONS;
	OPTION;
	
	DOTNAME;
	
	VALUE_STRING;
	VALUE_INT;
	VALUE_FLOAT;
	VALUE_ENUM_CONST;
	VALUE_NULL;
	VALUE_BOOL;
	VALUE_ENUM_CONST_REF;
	
	ENUM;
	ENUM_CONST;
	
	STRUCT;
	STRUCT_SUPER;
	STRUCT_ATTR;
	STRUCT_ATTR_SLOT;
	
	SLOT_REQUIRED;
	SLOT_OPTIONAL;
	
	IMPLICIT_VALUE;
	
	TYPE_INT;
	TYPE_SHORT;
	TYPE_BYTE;
	TYPE_LONG;
	TYPE_STRING;
	TYPE_FLOAT;
	TYPE_BOOL;
	TYPE_ID;
	TYPE_FIXED_ARRAY;
	TYPE_DYNAMIC_ARRAY;

	SIZE_INT;
	SIZE_SYMBOL_REF;
	
	ANNOTATIONS;
	ANNOTATION;
	
	ABSTRACT_STRUCT;
	
	DOC_COMMENT;
}

@parser::header
{
package net.bartipan.iomonkey2.grammar;

}

@lexer::header
{
package net.bartipan.iomonkey2.grammar;
}


document 
	:	packageDecl headerOp* elementDefs -> ^(DOCUMENT packageDecl headerOp* elementDefs)
	;
	
packageDecl 
	:	docComment? 'package' dotName ';' -> ^(PACKAGE dotName docComment?)
	;
	

dotName
	:	ID ( '.' ID )* -> ^(DOTNAME ID ID*)
	;

headerOp
	:	importStat
	|	optionsStat
	;
	
importStat
	:	'import' dotName ';' -> ^(IMPORT dotName)
	;
	
optionsStat
	:	'options' '{' ( optionStat ';' ) + '}' ';' ? -> ^(OPTIONS optionStat+)
	;
	
optionStat
	:	dotName '=' optionValue -> ^(OPTION dotName optionValue)
	;
	
optionValue
	:	stringValue | intValue 
	;
		
elementDefs
	:	elementDef*
	;
	
elementDef
	:	enumDef
	|	structDef
	;
	
enumDef	:	docComment? 'enum' ID '{' enumConst ( ',' enumConst ) * ','? '}' -> ^(ENUM ID docComment? enumConst enumConst*) 
	;
	
enumConst
	:	docComment? ID '=' intValue -> ^(ENUM_CONST ID intValue docComment?)
	;
	
structDef
	:	 docComment? annotations? structHeaderDef ID structSuperDef? '{' structItem * '}' ';'? 
	-> ^(structHeaderDef ID structSuperDef? annotations? docComment? structItem*)
	;
	
structHeaderDef
	:	structHeader
	|	abstractStructHeader
	;
	
structHeader 
	:	'struct' -> ^(STRUCT)
	;
	
abstractStructHeader
	:	'abstract' 'struct' -> ^(ABSTRACT_STRUCT)
	;
	
structSuperDef
	:	':' ID -> ^(STRUCT_SUPER ID)
	;
	
structItem
	:	attribute ';'!
	;
	
attribute
	:	docComment? annotations? attributeSlot? attributeType ID attributeDefaultValueSet? 
		-> ^(STRUCT_ATTR ID attributeType attributeSlot? attributeDefaultValueSet? annotations? docComment?)
	;
	
attributeType
	:	scalarType
	|	arrayType
	;
	
scalarType
	:	intType
	|	shortType
	|	byteType
	|	longType
	|	stringType
	|	floatType
	|	boolType
	|	idType
	;
	
intType	:	'int' -> TYPE_INT
	;
	
shortType	
	:	'short' -> TYPE_SHORT
	;

byteType	
	:	'byte' -> TYPE_BYTE
	;
	
longType:	'long' -> TYPE_LONG
	;

stringType
	:	'string' -> TYPE_STRING
	;
	
floatType
	:	'float' -> TYPE_FLOAT
	;
	
boolType
	:	'bool' -> TYPE_BOOL
	;
	
idType	:	ID	-> ^(TYPE_ID ID)
	;


arrayType
	:	fixedSizeArrayType
	|	dynamicArrayType
	;
	
fixedSizeArrayType
	:	scalarType '[' fixedArraySize ']' -> ^(TYPE_FIXED_ARRAY scalarType fixedArraySize)
	;

fixedArraySize
    :   NUM -> ^(SIZE_INT NUM)
    |   ID '.' ID -> ^(SIZE_SYMBOL_REF ID ID)
    ;
	
dynamicArrayType
	:	scalarType '[' ']' -> ^(TYPE_DYNAMIC_ARRAY scalarType)
	;
	
attributeSlot
	:	attributeSlotType ( '(' NUM ')' )? -> ^(attributeSlotType NUM?)
	;
	
attributeSlotType
	:	'required' -> SLOT_REQUIRED
	|	'optional' -> SLOT_OPTIONAL
	;
	
attributeDefaultValueSet
	:	'=' attributeValue -> ^(IMPLICIT_VALUE attributeValue)
	;
	
attributeValue
	:	intValue | floatValue | stringValue | enumConstValue | nullValue | boolValue
	;

enumConstValue
	:	ID -> ^(VALUE_ENUM_CONST ID)
	;
	
enumConstRefValue
	:	ID '.' ID -> ^(VALUE_ENUM_CONST_REF ID ID)
	;
	
stringValue 
	:	STRING -> ^(VALUE_STRING STRING)
	; 	

floatValue
	:	FLOAT -> ^(VALUE_FLOAT FLOAT)
	;

boolValue 
	:	BOOL_ -> ^(VALUE_BOOL BOOL_)
	;

intValue
	:	decimalIntValue     -> ^(VALUE_INT decimalIntValue)
	|	hexadecimalIntValue -> ^(VALUE_INT hexadecimalIntValue)
	;
	
decimalIntValue 
	:	('+' | '-')? NUM
	;
	
hexadecimalIntValue
	:	('+' | '-')? HEX_NUM
	;
	
nullValue
	:	NULL	-> ^(VALUE_NULL)
	;
		
	
annotations
	:	annotationDef+ -> ^(ANNOTATIONS annotationDef+)
	;
annotationDef
	:	'@' ID ( '(' annotationParam (',' annotationParam ) *  ')' ) ? -> ^(ID annotationParam*)
	;
	
annotationParam
	:	intValue
	|	floatValue
	|	stringValue
	|	enumConstRefValue
	;

docComment 
	:	docCommentSlashBlock
	|	docCommentStartBlock
	;
	
docCommentSlashBlock
	:	DOC_COMMENT_SLASH+ -> ^(DOC_COMMENT DOC_COMMENT_SLASH+)
	;
	
docCommentStartBlock
	:	DOC_COMMENT_STAR -> ^(DOC_COMMENT DOC_COMMENT_STAR)
	;
	
BOOL_:	'true'
	|	'false'
	;
		
NULL 	:	'null'
	;

ID  :	('a'..'z'|'A'..'Z'|'_') ('a'..'z'|'A'..'Z'|'0'..'9'|'_')*
    ;

NUM :	'0'..'9'+
    ;
    
HEX_NUM	:	'0x' HEX_DIGIT+
	;

FLOAT
    :   ('0'..'9')+ '.' ('0'..'9')* EXPONENT?
    |   '.' ('0'..'9')+ EXPONENT?
    |   ('0'..'9')+ EXPONENT
    ;
    

SPLITTER_COMMENT
	:	'////' ~('\n'|'\r')* '\r'? '\n' {$channel=HIDDEN;}
	;

DOC_COMMENT_SLASH
	: '///' ~('\n'|'\r')* '\r'? '\n' /*{$channel=HIDDEN;}*/
	;
DOC_COMMENT_STAR	
	:
	|	'/**' ( options {greedy=false;} : . )* '*/' /*{$channel=HIDDEN;}*/
	;

COMMENT
    :   '//' ~('\n'|'\r')* '\r'? '\n' {$channel=HIDDEN;}
    |   '/*' ( options {greedy=false;} : . )* '*/' {$channel=HIDDEN;}
    ;

WS  :   ( ' '
        | '\t'
        | '\r'
        | '\n'
        ) {$channel=HIDDEN;}
    ;

STRING
    :  '"' ( ESC_SEQ | ~('\\'|'"') )* '"'
    ;

fragment
EXPONENT : ('e'|'E') ('+'|'-')? ('0'..'9')+ ;

fragment
HEX_DIGIT : ('0'..'9'|'a'..'f'|'A'..'F') ;

fragment
ESC_SEQ
    :   '\\' ('b'|'t'|'n'|'f'|'r'|'\"'|'\''|'\\')
    |   UNICODE_ESC
    |   OCTAL_ESC
    ;

fragment
OCTAL_ESC
    :   '\\' ('0'..'3') ('0'..'7') ('0'..'7')
    |   '\\' ('0'..'7') ('0'..'7')
    |   '\\' ('0'..'7')
    ;

fragment
UNICODE_ESC
    :   '\\' 'u' HEX_DIGIT HEX_DIGIT HEX_DIGIT HEX_DIGIT
    ;
