# ioMonkey2

## What is ioMonkey2

_ioMonkey2_ is a tool for generating loader/saver classes for various programming languages (C#, Java, and C++ are 
supported at the moment). This generated code is based on model description written in domain specific language stored in files with extension `.iom2`.

In many aspects ioMonkey2 is very similar to Google's Protocol Buffers. However ioMonkey2 brings couple fundamental 
improvements to this concept:

- structure inheritance and load-time type identification,
- support for annotations to pass custom meta data to code-gen backend (see supported annotations for more details).


## `iom2` file syntax


### Basic structure

```
package test.config

import test.objects

struct Config
{
    Item[] items;
}

struct Item
{
    string name;
    string value;
    optional(1) Scope scope = DEFAULT;
}

enum Scope
{
    DEFAULT = 0,
    SYSTEM = 1,
    USER = 2
}
```

### Supported data-types

Simple data-types:

* `byte` 8-bit integer
* `short` 16-bit integer
* `int` 32-bit integer
* `long` 64-bit integer
* `float` 32-bit floating point number
* `bool` 8-bit boolean
* `string` UTF-8 string


User defined:

* enum
* struct


Arrays:
```
byte[4] magic; // fixed size
byte[] data;   // variable size
```

### struct syntax

#### attribute syntax

```
byte x;             // required(1)
required byte y;    // required(2)
optional byte z;    // optional(1)
optional(2) byte w; // optional(2)
```

### enum syntax

```
enum Bool {
  YES = 1,
  NO  = 2
}
```

### Doc Comments

IoMonkey has built-in support for JavaDoc-like comments. All comments starting with tripple slashes
are treated as _Documentaiton Comments_ for next element.
Following elements can be commented:
- _package_,
- _struct_,
- _struct attribute_,
- _enum_,
- and _enum constant_.

Doc-comments can be used in two ways:
- native language generated comments (JavaDoc for java, XML Document comments for C#, and Doxygen for C++),
- reference doc generated with `doc` code-gen engine.

See following example:

```
/// Office data model package
package hyperoffice.document.mondel;

/// This is documenation for Document struct.
struct Document
{
  /// This is comment for attribute docType
  DocType docType;
  
  /// Comments can span
  /// multiple lines
  /// all these lines are treated as documenation
  /// for attribute attr2.
  int attr2;
}

/// Documentation for enum DocType
enum DocType
{
    /// Documentation for DocType.TEXT constant
    TEXT = 1,
    /// Documentation for DocType.SHEET
    SHEET = 2
}
```

### Complex sample

```
package iomonkey2.test;

import iomonkey2.base;
import iomonkey2.io;

options
{
	version = "2.0";
}

struct TestDocument
{
	Header header;
	Element root;
}

struct Header
{
	byte[4] magic;
	byte major;
	byte minor;
	optional(1) string[] comments;
}

struct Element
{
	string name;
	Element[] children;
}

struct TransformElement : Element
{
	float3 translation;
	quaternion rotation;
	float3 scale;
}

struct ModelElement : TransformElement
{
	string modelName;
	optional(1) string[] materialOverride;
}
```

## Supported Annotations

### `@Fixed`

`@Fixed` annotation enforces a specific value of field when loading. This is useful to check format header magic and versions, and eventually quit loading when these fields mistmatch their fixed values.

```
struct Header
{
	@Fixed("Doc")
	string magic;
	@Fixed(0x0101)
	int version;
}
```


### `@MapKeyName`

`@MapKeyName` annotation allows to change a dynamic array to be a dictionary/map with key of specified struct field.


```
struct Parameter
{
	string name;
	string value;
}

string Config
{
	@MapKeyName("name")
	Parameter[] parameters;
}
```

### `@ValueType`

`@ValueType` annotation allows you to mark a struct element that will be generates as value type one instead of reference type.
This is useful only for C# now (however C++ may use it too in future). By default each iom2 struct is generated as C# `class`.
But sometimes if you're handling big arrays of structs it may be more efficient to use C# `struct` instead. If you mark
iom2 struct with `@ValueType` annotation, it gets generated as C# `struct`.
Note that this annotation doesn't affect any other languages besides C# (Java, C++).

### `@MaxArraySize`

`@MaxArraySize(n)` annotation is used for defining maximal count of array elements. This limit is useful to prevent big allocations
for corrupted binary streams, or otherwise malformed data. The number `n` defines maximal allowed item count.

## Supported Options

### `packagePrefix.java`

`packagePrefix.java` options allow define common package prefix, which modifies full qualified names of generated Java classes. Consider following example:

```
package io;

options
{
	packagePrefix.java = "com.mydomain.app";
}

struct Document
{

}
```

This code generates Java class with following full qualified name: `com.mydomain.app.io.Document` and C# class with `Io.Document` full qualified name. 

## Global settings file

You can create `global.properties` file in project root (i.e. directory that is used as root for all packages). This file defines project-wide
settings. At this point these settings can be set there:
- `maxArraySize`---maximal size of arrays, see `@MaxArraySize` above for more details.
