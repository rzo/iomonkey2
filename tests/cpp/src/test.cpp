#include <iostream>
#include <cassert>
#include <fstream>
#include <memory>

#include "tests/basic/TextDocument.h"
#include "tests/basic/ParagraphElement.h"
#include "tests/basic/ImageElement.h"
#include "tests/extended/YouTubeElement.h"
using namespace tests::basic;
using namespace tests::extended;


struct App
{
    App()
        : _doc(new TextDocument)
    {
        generateDoc();
    }




    void run()
    {
        //TextDocument* vanila = (TextDocument*) _doc->clone();

        save("test.dat");

        long fs = fileSize("test.dat");
        int cs =_doc->calculateSize();
        std::cout << fs << std::endl << cs << std::endl;

        assert(fs == cs);

        load("test.dat");
    }

private:
    long fileSize(const std::string& fileName)
    {
        FILE* fp = fopen(fileName.c_str(), "rb");
        fseek(fp, 0L, SEEK_END);
        long ret = ftell(fp);
        return ret;
    }

    ParagraphElement* newText(const std::string& text, const std::string& font, float fontSize, ParagraphAlign align) const
    {
        ParagraphElement* ret = new ParagraphElement();
        ret->text = text;
        ret->style = new TextStyle();
        ret->style->font = font;
        ret->style->align = align;
        ret->style->size = fontSize;
        return ret;
    }

    ParagraphElement* newText(const std::string& text) const
    {
        ParagraphElement* ret = new ParagraphElement();
        ret->text = text;
        return ret;
    }

    ImageElement* newImage(const std::string& url) const
    {
        ImageElement* ret = new ImageElement();
        ret->url = url;
        return ret;
    }

    YouTubeElement* newYouTube(const std::string& url) const
    {
        YouTubeElement* ret = new YouTubeElement();
        ret->clipUrl = url;
        return ret;
    }

    void generateDoc()
    {
        _doc->root->children.push_back(newText("Hello world", "Helvetica", 18, ParagraphAlign::ALIGN_CENTER));
        _doc->root->children.push_back(newText("Lorem ipsum"));
        _doc->root->children.push_back(newImage("http://some.com/img.png"));
        _doc->root->children.push_back(newText("She sells seashells"));
        _doc->root->children.push_back(newYouTube("http://youtube.com/3212932"));

        _doc->header->grantedPerms[0] = nullptr;
        _doc->header->grantedPerms[1] = new PermGroup();
    }

    void load(const std::string& fileName)
    {
        std::fstream ifs(fileName.c_str(), std::ios::in | std::ios::binary );
        _doc->load(ifs);
    }

    void save(const std::string& fileName) const
    {
        std::fstream ofs(fileName.c_str(), std::ios::out | std::ios::binary );
        _doc->save(ofs);
    }

private:
    std::unique_ptr<TextDocument> _doc;

} app;

int main(int argc, char** argv)
{
    app.run();
    return 0;
}
