using System.IO;
using Tests.Basic;
using Tests.Extended;

public class App
{
	private TextDocument _doc;

	App()
	{
		_doc = new TextDocument();
	}

	void Load(string filePath)
	{
		using (FileStream fs = new FileStream(filePath, FileMode.Open))
		using (BinaryReader r = new BinaryReader(fs))
		{
			_doc.Load(r);
		}
	}

	void Save(string filePath)
	{
		using (FileStream fs = new FileStream(filePath, FileMode.Create))
		using (BinaryWriter w = new BinaryWriter(fs))
		{
			_doc.Save(w);
		}
	}

    ParagraphElement NewText(string text, string font, float fontSize, ParagraphAlign align)
    {
        ParagraphElement ret = new ParagraphElement();
        ret.Text = text;
        ret.Style = new TextStyle();
        ret.Style.Font = font;
        ret.Style.Align = align;
        ret.Style.Size = fontSize;
        return ret;
    }

    ParagraphElement NewText(string text)
    {
        ParagraphElement ret = new ParagraphElement();
        ret.Text = text;
        return ret;
    }

    ImageElement NewImage(string url)
    {
        ImageElement ret = new ImageElement();
        ret.Url = url;
        return ret;
    }

    YouTubeElement NewYouTube(string url)
    {
        YouTubeElement ret = new YouTubeElement(){ ClipUrl = url };
        return ret;
    }

	void GenerateDoc()
	{
        _doc.Root.Children.Add(NewText("Hello world", "Helvetica", 18, ParagraphAlign.AlignCenter));
        _doc.Root.Children.Add(NewText("Lorem ipsum"));
        _doc.Root.Children.Add(NewImage("http://some.com/img.png"));
        _doc.Root.Children.Add(NewText("She sells seashells"));
        _doc.Root.Children.Add(NewYouTube("http://youtube.com/3212932"));

        _doc.Header.GrantedPerms[0] = null;
        _doc.Header.GrantedPerms[1] = new PermGroup();
	}

	void Assert(bool expr)
	{
	    if (!expr)
	        throw new System.ArgumentException("Assertion failed");
	}

    void CompareElement(Element a, Element b)
    {
        if (a is ParagraphElement)
        {
            Assert(((ParagraphElement)a).Text == ((ParagraphElement)b).Text);
        }
        else if (a is ImageElement)
        {
            Assert(((ImageElement)a).Url == ((ImageElement)b).Url);
        }
        else if (a is GroupElement)
        {
            GroupElement ga = (GroupElement)a;
            GroupElement gb = (GroupElement)b;

            for (int i = 0; i < ga.Children.Count; i++) {
                CompareElement(ga.Children[i], gb.Children[i]);
            }
        }
    }

	void Run()
	{
		GenerateDoc();
		TextDocument vanila = (TextDocument)_doc.Clone();

		Save("test.data");
		var fi = new System.IO.FileInfo("test.data");
		System.Console.WriteLine(fi.Length);
		System.Console.WriteLine(_doc.CalculateSize());
		Assert(fi.Length == _doc.CalculateSize());

		_doc = new TextDocument();

		Load("test.data");
        CompareElement(_doc.Root, vanila.Root);
	}

	public static void Main(string[] args)
	{
        Tests.Registrator.RegisterReaders();
		App app = new App();
		app.Run();
	}
}
