import java.io.*;
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.Exception;

import net.bartipan.iomonkey2.utils.*;

import tests.basic.*;
import tests.extended.*;

public class Main {
    private TextDocument _doc = new TextDocument();

    private Main() {

    }

    private ParagraphElement newText(String text, String font, float fontSize, ParagraphAlign align) {
        ParagraphElement ret = new ParagraphElement();
        ret.text = text;
        ret.style = new TextStyle();
        ret.style.font = font;
        ret.style.align = align;
        ret.style.size = fontSize;
        return ret;
    }

    private ParagraphElement newText(String text) {
        ParagraphElement ret = new ParagraphElement();
        ret.text = text;
        return ret;
    }

    private ImageElement newImage(String url) {
        ImageElement ret = new ImageElement();
        ret.url = url;
        return ret;
    }

    private YouTubeElement newYouTube(String url) {
        YouTubeElement ret = new YouTubeElement();
        ret.clipUrl = url;
        return ret;
    }

    private void generateDoc() {
        _doc.header.mainPermissions[0] = PermissionType.READ;
        _doc.root.children.add(newText("Hello world", "Helvetica", 18, ParagraphAlign.ALIGN_CENTER));
        _doc.root.children.add(newText("Lorem ipsum"));
        _doc.root.children.add(newImage("http://some.com/img.png"));
        _doc.root.children.add(newText("She sells seashells"));
        _doc.root.children.add(newYouTube("http://youtube.com/3212932"));

        _doc.header.grantedPerms[0] = null;
        _doc.header.grantedPerms[1] = new PermGroup();
    }

    private void load(File file) throws IOException {
        FileInputStream fs = new FileInputStream(file);
        LittleEndianDataInputStream dis = new LittleEndianDataInputStream(fs);
        _doc.load(dis);
        dis.close();
    }

    private void save(File file) throws IOException {
        FileOutputStream fs = new FileOutputStream(file);
        LittleEndianDataOutputStream dos = new LittleEndianDataOutputStream(fs);
        _doc.save(dos);
        dos.close();
    }

    private void compareElement(Element a, Element b) {
        if (a instanceof ParagraphElement) {
            check(((ParagraphElement)a).text.equals(((ParagraphElement)b).text));
        } else if (a instanceof ImageElement) {
            check(((ImageElement)a).url.equals(((ImageElement)b).url));
        } else if (a instanceof GroupElement) {
            GroupElement ga = (GroupElement)a;
            GroupElement gb = (GroupElement)b;

            for (int i = 0; i < ga.children.size(); i++) {
                compareElement(ga.children.get(i), gb.children.get(i));
            }
        }
    }

    void check(boolean expr) {
        if (!expr)
            throw new IllegalArgumentException("assertion failed");
    }


    private void run() {

        try {
            generateDoc();
            TextDocument vanila = (TextDocument)_doc.clone();

            File file = new File("test.data");
            save(file);
            System.out.println(file.length());
            System.out.println(_doc.calculateSize());
            check(file.length() == _doc.calculateSize());

            load(file);
            compareElement(_doc.root, vanila.root);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        tests.Registrator.registerReaders();

        Main main = new Main();
        main.run();

    }
}