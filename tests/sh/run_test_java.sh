#!/bin/sh
DIR=$(cd $(dirname $0); pwd)
JAVA=$DIR/../java
JAVA_OUT=$DIR/../java_out
cp -r $JAVA/ $JAVA_OUT
$DIR/iom2 java $(find $DIR/../iom2 -name '*.iom2') -o $JAVA_OUT -v 1 -r $DIR/../iom2/tests:tests

cd $JAVA_OUT
javac $(find $JAVA_OUT -name '*.java')
java Main
cd $DIR
