#!/bin/sh
DIR=$(cd $(dirname $0); pwd)
CS=$DIR/../cs
CS_OUT=$DIR/../cs_out
cp -r $CS/ $CS_OUT
$DIR/iom2 cs $(find $DIR/../iom2 -name '*.iom2') -o $CS_OUT -v 1 -r $DIR/../iom2/tests:tests

cd $CS_OUT
mcs -out:app.exe $(find $CS_OUT -name '*.cs')
mono app.exe
cd $DIR
