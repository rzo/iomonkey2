#!/bin/sh
DIR=$(cd $(dirname $0); pwd)
IOM_JAR=$DIR/../../target/iomonkey2-1.0-SNAPSHOT-jar-with-dependencies.jar
mkdir -p $DIR/../jar
cp $IOM_JAR $DIR/../jar/iomonkey2.jar

$DIR/install_test_cs.sh
$DIR/install_test_java.sh
$DIR/install_test_cpp.sh
