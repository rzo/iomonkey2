#!/bin/sh
DIR=$(cd $(dirname $0); pwd)
CPP=$DIR/../cpp
CPP_OUT=$DIR/../cpp_out
cp -r $CPP/ $CPP_OUT
$DIR/iom2 cpp $(find $DIR/../iom2 -name '*.iom2') -o $CPP_OUT -v 1 -r $DIR/../iom2/tests:tests

cd $CPP_OUT
mkdir -p bin
cd bin
cmake -DCMAKE_BUILD_TYPE=Debug ..
make
./iomtest
cd $DIR
