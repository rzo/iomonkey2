#!/bin/sh
DIR=$(cd $(dirname $0); pwd)
TMP=$DIR/../tmp
JAVA=$DIR/../java
mkdir -p $JAVA $TMP
cd $TMP
git clone https://bitbucket.org/rzo/iomonkey2-utils-java.git
cp -r iomonkey2-utils-java/src/main/java/net $JAVA
cd $DIR
