#!/bin/sh
DIR=$(cd $(dirname $0); pwd)
TMP=$DIR/../tmp
CPP_DIR=$DIR/../cpp
mkdir -p $CS_DIR $TMP
cd $TMP
git clone https://bitbucket.org/rzo/iomonkey2-utils-cpp.git
mkdir -p $CPP_DIR/iom2
rm -rf $CPP_DIR/iom2/include $CPP_DIR/iom2/src
cp -r iomonkey2-utils-cpp/include $CPP_DIR/iom2/include
cp -r iomonkey2-utils-cpp/src $CPP_DIR/iom2/src
cd $DIR

