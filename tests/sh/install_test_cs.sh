#!/bin/sh
DIR=$(cd $(dirname $0); pwd)
TMP=$DIR/../tmp
CS_DIR=$DIR/../cs
mkdir -p $CS_DIR $TMP
cd $TMP
git clone https://bitbucket.org/rzo/iomonkey2-utils-cs.git
cp -r iomonkey2-utils-cs/src/IoMonkey $CS_DIR
cd $DIR

