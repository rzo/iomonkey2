package net.bartipan.iomonkey2.parser;

import net.bartipan.iomonkey2.model.SourceLocation;
import org.antlr.runtime.tree.CommonTree;

/**
 * Created by jbartipan on 30.10.14.
 */
public class NameConflictException extends SourceException {
    private static String formatMessage(String element, String name) {
        return String.format("There's already defined %s with name '%s'", element, name);
    }

    private static String formatMessage(String element, String name, SourceLocation prev) {
        return String.format("There's already defined %s with name '%s' (defined in %s)", element, name, prev.toString());
    }

    public NameConflictException(String fileName, int lineNo, int columnNo, String element, String name) {
        super(fileName, lineNo, columnNo, formatMessage(element, name));
    }

    public NameConflictException(String fileName, CommonTree node, String element, String name) {
        super(fileName, node, formatMessage(element, name));
    }

    public NameConflictException(String fileName, int lineNo, int columnNo, String element, String name, SourceLocation prevDef) {
        super(fileName, lineNo, columnNo, formatMessage(element, name, prevDef));
    }

    public NameConflictException(String fileName, CommonTree node, String element, String name, SourceLocation prevDef) {
        super(fileName, node, formatMessage(element, name, prevDef));
    }

    public NameConflictException(SourceLocation loc, String element, String name) {
        super(loc.fileName, loc.lineNumber, loc.columnNumber, formatMessage(element, name));
    }

    public NameConflictException(SourceLocation loc, String element, String name, SourceLocation prevDef) {
        super(loc.fileName, loc.lineNumber, loc.columnNumber, formatMessage(element, name, prevDef));
    }

}
