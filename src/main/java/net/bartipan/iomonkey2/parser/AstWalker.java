package net.bartipan.iomonkey2.parser;

import net.bartipan.iomonkey2.grammar.IOMonkey2Lexer;
import org.antlr.runtime.tree.CommonTree;

/**
 * Created by honza on 9/16/14.
 */
public class AstWalker {

    public static void walk(String fileName, CommonTree root, IAstVisitor visitor) {
        final int cn = root.getChildCount();
        for (int i = 0; i < cn; i++) {
            CommonTree node = (CommonTree) root.getChild(i);

            switch (node.getToken().getType()) {
                case IOMonkey2Lexer.PACKAGE:
                    visitor.visitPackage(node);
                    break;

                case IOMonkey2Lexer.OPTIONS:
                    visitor.visitOptions(node);
                    break;

                case IOMonkey2Lexer.IMPORT:
                    visitor.visitImport(node);
                    break;

                case IOMonkey2Lexer.STRUCT:
                case IOMonkey2Lexer.ABSTRACT_STRUCT:
                    visitor.visitStruct(node);
                    break;

                case IOMonkey2Lexer.ENUM:
                    visitor.visitEnum(node);
                    break;

                default:
                    throw new UnexpectedNodeException(node, fileName, "package, options, import, struct, or enum");

            }
        }
    }
}
