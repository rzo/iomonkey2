package net.bartipan.iomonkey2.parser;

import net.bartipan.iomonkey2.Main;
import net.bartipan.iomonkey2.grammar.IOMonkey2Lexer;
import net.bartipan.iomonkey2.model.*;
import net.bartipan.iomonkey2.model.Enum;
import net.bartipan.iomonkey2.utils.TextUtils;
import net.bartipan.iomonkey2.utils.VariableProvider;
import org.antlr.runtime.tree.CommonTree;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by honza on 9/16/14.
 */
public class ParserUtils {
    public static AttributeType parseScalarAttributeType(String fileName, CommonTree node) {
        AttributeType ret = new AttributeType();

        switch (node.getToken().getType()) {
            case IOMonkey2Lexer.TYPE_BYTE:
                ret.scalarType = ScalarType.Byte;
                break;

            case IOMonkey2Lexer.TYPE_SHORT:
                ret.scalarType = ScalarType.Short;
                break;

            case IOMonkey2Lexer.TYPE_INT:
                ret.scalarType = ScalarType.Int;
                break;

            case IOMonkey2Lexer.TYPE_LONG:
                ret.scalarType = ScalarType.Long;
                break;


            case IOMonkey2Lexer.TYPE_FLOAT:
                ret.scalarType = ScalarType.Float;
                break;

            case IOMonkey2Lexer.TYPE_BOOL:
                ret.scalarType = ScalarType.Bool;
                break;

            case IOMonkey2Lexer.TYPE_STRING:
                ret.scalarType = ScalarType.String;
                break;

            case IOMonkey2Lexer.TYPE_ID:
                ret.scalarType = ScalarType.TypeId;
                ret.typeId = node.getChild(0).getText();
                break;

            default:
                throw new UnexpectedNodeException(node, fileName, "byte, short, int, long, float, bool, string or ID");

        }

        return ret;
    }

    public static AttributeType parseAttributeType(String fileName, CommonTree node) {
        AttributeType ret = null;
        switch (node.getToken().getType()) {
            case IOMonkey2Lexer.TYPE_DYNAMIC_ARRAY:
                ret = parseScalarAttributeType(fileName, (CommonTree) node.getChild(0));
                ret.arraySize = 0;
                break;

            case IOMonkey2Lexer.TYPE_FIXED_ARRAY:
                CommonTree ch0 = (CommonTree) node.getChild(0);
                ret = parseScalarAttributeType(fileName, ch0);
                CommonTree ch1 = (CommonTree) node.getChild(1);
                parseFixedArraySize(fileName, ch1, ret);
                break;

            default:
                ret = parseScalarAttributeType(fileName, node);


        }
        return  ret;
    }

    private static void parseFixedArraySize(String fileName, CommonTree node, AttributeType at) {
        switch (node.getToken().getType()) {
            case IOMonkey2Lexer.SIZE_INT:
                at.arraySize = Integer.parseInt(node.getChild(0).getText());
                break;

            case IOMonkey2Lexer.SIZE_SYMBOL_REF:
                at.arraySize = -2;
                at.arraySizeRef = new String[node.getChildCount()];
                for (int i = 0; i < at.arraySizeRef.length; i++) {
                    at.arraySizeRef[i] = node.getChild(i).getText();
                }
                break;

            default:
                throw new UnexpectedNodeException(node, fileName, "SIZE_INT (e.g. 123) or SIZE_SYMBOL_REF (e.g. MyEnum.count)");

        }
    }

    public static String[] getChildrenAsStrings(CommonTree node) {
        final int n = node.getChildCount();
        String[] ret = new String[n];
        for (int i = 0; i < n; i++)
            ret[i] = node.getChild(i).getText();
        return ret;
    }

    public static DotName parseDotName(String fileName, CommonTree node) {
        UnexpectedNodeException.expect(fileName, node, "dot-name", IOMonkey2Lexer.DOTNAME);

        return new DotName(getChildrenAsStrings(node));
    }

    private static Value parseValue(String fileName, CommonTree valTypeNode) {

        String value = "";
        String sep = valTypeNode.getToken().getType() == IOMonkey2Lexer.VALUE_ENUM_CONST_REF ? "." : "";

        for (int i = 0; i < valTypeNode.getChildCount(); i++) {
            value = (i > 0 ? value + sep : value) + (CommonTree)valTypeNode.getChild(i);
        }

        ValueType type;
        type = getValueType(fileName, valTypeNode);

        return new Value(type, value);
    }

    private static Annotation parseAnnotation(String fileName, CommonTree n) {
        List<Value> parameters = new ArrayList<Value>();
        for (int i = 0; i < n.getChildCount(); i++) {
            parameters.add(parseValue(fileName, (CommonTree)n.getChild(i)));
        }

        return new Annotation(n.getText(), parameters);
    }

    private static List<Annotation> parseAnnotations(String fileName, CommonTree n) {
        UnexpectedNodeException.expect(fileName, n, "annotations", IOMonkey2Lexer.ANNOTATIONS);
        List<Annotation> ret = new ArrayList<Annotation>();
        for (int i = 0; i < n.getChildCount(); i++) {
            ret.add(parseAnnotation(fileName, (CommonTree)n.getChild(i)));
        }

        return ret;
    }

    private static ValueType getValueType(String fileName, CommonTree valTypeNode) {
        ValueType type;
        switch (valTypeNode.getToken().getType()) {
            case IOMonkey2Lexer.VALUE_INT:
                type = ValueType.Int;
                break;

            case IOMonkey2Lexer.VALUE_FLOAT:
                type = ValueType.Float;
                break;

            case IOMonkey2Lexer.VALUE_STRING:
                type = ValueType.String;
                break;

            case IOMonkey2Lexer.VALUE_ENUM_CONST:
                type = ValueType.EnumConst;
                break;

            case IOMonkey2Lexer.VALUE_ENUM_CONST_REF:
                type = ValueType.EnumConstRef;
                break;

            case IOMonkey2Lexer.VALUE_NULL:
                type = ValueType.Null;
                break;

            case IOMonkey2Lexer.VALUE_BOOL:
                type = ValueType.Bool;
                break;

            default:
                throw new UnexpectedNodeException(valTypeNode, fileName, "expected int, float, string or enum-const value");
        }
        return type;
    }

    public static Attribute parseAttribute(String fileName, CommonTree node) {
        //
        // <attribute> -+- <name>
        //              +- <type>...
        //              +- ? <slot>...
        //              +- ? <implicit value>...
        //              +- ? <annotations>
        Attribute ret = new Attribute();
        ret.sourceLocation = new SourceLocation(fileName, node);
        ret.name = node.getChild(0).getText();
        ret.type = parseAttributeType(fileName, (CommonTree)node.getChild(1));

        int idx = 2;
        if (idx < node.getChildCount()) {
            CommonTree n = (CommonTree)node.getChild(idx);
            int ntt = n.getToken().getType();
            if (ntt == IOMonkey2Lexer.SLOT_REQUIRED || ntt == IOMonkey2Lexer.SLOT_OPTIONAL) {
                idx++;

                ret.slot = new AttributeSlot(ntt == IOMonkey2Lexer.SLOT_REQUIRED, -1);
                if (n.getChildCount() > 0) {
                    ret.slot.slot = Integer.parseInt(n.getChild(0).getText());
                }

                n = idx < node.getChildCount() ? (CommonTree)node.getChild(idx) : null;
            }

            if (n != null && n.getToken().getType() == IOMonkey2Lexer.IMPLICIT_VALUE) {
                n = (CommonTree) node.getChild(idx);
                ret.implicitValue = parseValue(fileName, (CommonTree)n.getChild(0));
                idx++;
                n = idx < node.getChildCount() ? (CommonTree)node.getChild(idx) : null;
            }

            if (n != null && n.getToken().getType() == IOMonkey2Lexer.ANNOTATIONS) {
                ret.annotations = parseAnnotations(fileName, n);
                idx++;
                n = idx < node.getChildCount() ? (CommonTree)node.getChild(idx) : null;
            }

            if (n != null) {
                if (n.getToken().getType() != IOMonkey2Lexer.DOC_COMMENT) {
                    throw new UnexpectedNodeException(n, fileName, "doc comment or annotation");
                }

                ret.docComment = parseDocComment(fileName, n);
            }
        }

        return ret;
    }

    public static Option parseOption(String fileName, CommonTree node) {
        Option ret = new Option();
        final CommonTree nameNode = (CommonTree)node.getChild(0);
        final CommonTree valueNode = (CommonTree)node.getChild(1);

        ret.name = ParserUtils.parseDotName(fileName, nameNode);
        ret.type = getValueType(fileName, valueNode);
        ret.value = valueNode.getChild(0).getText();
        if (ret.type == ValueType.String) {
            // strip string quotes
            ret.value = ret.value.substring(1, ret.value.length() - 1);
            ret.value = TextUtils.replaceVariables(ret.value, Main.getInstance().getPropertiesValueProvider());
        }

        return ret;
    }

    public static Enum parseEnum(String fileName, CommonTree node) {
        UnexpectedNodeException.expect(fileName, node, "enum", IOMonkey2Lexer.ENUM);

        Enum ret = new Enum();
        ret.sourceLocation = new SourceLocation(fileName, node);
        ret.name = node.getChild(0).getText();
        final int n = node.getChildCount();

        int offset = 1;
        CommonTree nd = (CommonTree)node.getChild(offset);
        if (nd.getToken().getType() == IOMonkey2Lexer.DOC_COMMENT) {
            offset++;
            ret.docComment = parseDocComment(fileName, nd);
        }

        for (int i = offset; i < n; i++) {
            EnumConst ec = parseEnumConst(fileName, (CommonTree)node.getChild(i));

            // Check for potential name conflict
            EnumConst potentialConflict = ret.findEnumConst(ec.name);
            if (potentialConflict != null) {
                throw new NameConflictException(ec.sourceLocation, "enum constant",
                        ec.name, potentialConflict.sourceLocation);

            }
            ret.constants.add(ec);
        }

        return ret;
    }

    private static int parseInt(String fileName, CommonTree node) {
        if (node.getToken().getType() == IOMonkey2Lexer.VALUE_INT) {
            int base = 10;
            String t = joinChildrenNodes(node);
            if (t.startsWith("0x")) {
                t = t.substring(2);
                base = 16;
            }
            return Integer.parseInt(t, base);
        }
        throw new UnexpectedNodeException(node, fileName, "Value int");
    }

    private static String joinChildrenNodes(CommonTree parent) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < parent.getChildCount(); i++) {
            sb.append(parent.getChild(i).getText());
        }

        return sb.toString();
    }

    private static EnumConst parseEnumConst(String fileName, CommonTree node) {
        UnexpectedNodeException.expect(fileName, node, "enum-const", IOMonkey2Lexer.ENUM_CONST);
        EnumConst ret = new EnumConst(node.getChild(0).getText(), parseInt(fileName, (CommonTree)node.getChild(1)));

        if (node.getChildCount() == 3)
            ret.docComment = parseDocComment(fileName, (CommonTree)node.getChild(2));

        ret.sourceLocation = new SourceLocation(fileName, node);
        return ret;
    }


    public static Struct parseStruct(String fileName, CommonTree node) {
        UnexpectedNodeException.expect(fileName, node, "struct", IOMonkey2Lexer.STRUCT, IOMonkey2Lexer.ABSTRACT_STRUCT);

        Struct ret = new Struct();
        ret.abstractStruct = node.getToken().getType() == IOMonkey2Lexer.ABSTRACT_STRUCT;
        ret.sourceLocation = new SourceLocation(fileName, node);
        ret.name = node.getChild(0).getText();

        final int n = node.getChildCount();

        if (n > 1) {
            int offset = 1;

            CommonTree nextNode = (CommonTree)node.getChild(1);

            if (nextNode.getToken().getType() == IOMonkey2Lexer.STRUCT_SUPER) {
                ret.superClassName = nextNode.getChild(0).getText();
                offset++;
                nextNode = offset < n ? (CommonTree)node.getChild(offset) : null;
            }

            if (nextNode != null && nextNode.getToken().getType() == IOMonkey2Lexer.ANNOTATIONS) {
                ret.annotations = parseAnnotations(fileName, nextNode);
                offset++;
            }

            if (nextNode != null && nextNode.getToken().getType() == IOMonkey2Lexer.DOC_COMMENT) {
                ret.docComment = parseDocComment(fileName, nextNode);
                offset++;
            }

            for (int i = offset; i < n; i++) {
                final Attribute attr = ParserUtils.parseAttribute(fileName, (CommonTree)node.getChild(i));

                // check for potential conflict
                Attribute potentialConflict = ret.findAttribute(attr.name);
                if (potentialConflict != null) {
                    throw new NameConflictException(attr.sourceLocation, "struct attribute",
                            attr.name, potentialConflict.sourceLocation);
                }

                ret.attributes.add(attr);
                attr.struct = ret;
            }
        }

        return ret;
    }

    public static DocComment parseDocComment(String fileName, CommonTree node) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < node.getChildCount(); i++)
        {
            String line = node.getChild(i).getText();
            if (line.startsWith("/// ")) {
                line = line.substring(4);
            } else if (line.startsWith("///")) {
                line = line.substring(3);
            }

            sb.append(line);
        }

        return new DocComment(sb.toString());
    }
}
