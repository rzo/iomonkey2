package net.bartipan.iomonkey2.parser;

/**
 * Created by stetinapet on 6.2.15.
 */
public class InvalidMapKeyException extends RuntimeException {

    public InvalidMapKeyException(String message) {
        super(message);
    }
}
