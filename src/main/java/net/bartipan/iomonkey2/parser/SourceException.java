package net.bartipan.iomonkey2.parser;

import net.bartipan.iomonkey2.model.SourceLocation;
import org.antlr.runtime.tree.CommonTree;

/**
 * Created by jbartipan on 30.10.14.
 */
public class SourceException extends RuntimeException {
    private String _fileName;
    private int _lineNumber;
    private int _columnNumber;
    private String _description;

    public SourceException(String fileName, int lineNo, int columnNo, String description) {
        _fileName = fileName;
        _lineNumber = lineNo;
        _columnNumber = columnNo;
        _description = description;
    }

    public SourceException(String fileName, CommonTree node, String description) {
        _fileName = fileName;
        _lineNumber = node.getLine();
        _columnNumber = node.getCharPositionInLine();
        _description = description;
    }

    public SourceException(SourceLocation location, String description) {
        _fileName = location.fileName;
        _lineNumber = location.lineNumber;
        _columnNumber = location.columnNumber;
        _description = description;
    }

    @Override
    public String getMessage() {
        return String.format("%s:%d:%d: %s", _fileName, _lineNumber, _columnNumber, _description);
    }

}
