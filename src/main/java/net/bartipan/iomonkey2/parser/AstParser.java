package net.bartipan.iomonkey2.parser;

import net.bartipan.iomonkey2.grammar.IOMonkey2Lexer;
import net.bartipan.iomonkey2.grammar.IOMonkey2Parser;
import net.bartipan.iomonkey2.model.*;

import org.antlr.runtime.ANTLRFileStream;
import org.antlr.runtime.ANTLRStringStream;
import org.antlr.runtime.CommonTokenStream;
import org.antlr.runtime.RecognitionException;
import org.antlr.runtime.tree.CommonTree;
import sun.net.www.ParseUtil;

import java.io.File;
import java.io.IOException;

/**
 * Created by honza on 9/16/14.
 */
public class AstParser implements IAstVisitor {
    private Unit _unit;
    private File _file;
    private String _fileName;

    public Unit parseUnit(File file) throws IOException, RecognitionException {
        _unit = new Unit();
        _fileName = file.getAbsolutePath();
        _file = file;

        IOMonkey2Lexer lexer = new IOMonkey2Lexer(new ANTLRFileStream(_fileName, "UTF-8"));
        CommonTokenStream tokenStream = new CommonTokenStream(lexer);
        IOMonkey2Parser parser = new IOMonkey2Parser(tokenStream);

        AstWalker.walk(_fileName, (CommonTree) parser.document().getTree(), this);

        return _unit;
    }

    public Unit parseUnit(String source) throws IOException, RecognitionException {
        _unit = new Unit();
        _fileName = "<source>";
        _file = null;

        IOMonkey2Lexer lexer = new IOMonkey2Lexer(new ANTLRStringStream(source));
        CommonTokenStream tokenStream = new CommonTokenStream(lexer);
        IOMonkey2Parser parser = new IOMonkey2Parser(tokenStream);

        AstWalker.walk(_fileName, (CommonTree) parser.document().getTree(), this);

        return _unit;
    }

    @Override
    public void visitPackage(CommonTree node) {
        _unit.name = ParserUtils.parseDotName(_fileName, (CommonTree)node.getChild(0));
        if (node.getChildCount() > 1) {
            _unit.docComment = ParserUtils.parseDocComment(_fileName, (CommonTree) node.getChild(1));
        }
    }

    @Override
    public void visitOptions(CommonTree node) {
        final int n = node.getChildCount();
        for (int i = 0; i < n; i++) {
            CommonTree cn = (CommonTree)node.getChild(i);
            _unit.options.add(ParserUtils.parseOption(_fileName, cn));
        }
    }

    @Override
    public void visitImport(CommonTree node) {
        _unit.imports.add(new Import(ParserUtils.parseDotName(_fileName, (CommonTree)node.getChild(0))));
    }

    @Override
    public void visitStruct(CommonTree node) {
        Struct s = ParserUtils.parseStruct(_fileName, node);

        // check for name conflict
        Element potentialConflict = _unit.findStruct(s.name);
        if (potentialConflict != null) {
            throw new NameConflictException(s.sourceLocation, "struct", s.name, potentialConflict.sourceLocation);
        }

        _unit.structs.add(s);
        s.unit = _unit;
    }

    @Override
    public void visitEnum(CommonTree node) {
        net.bartipan.iomonkey2.model.Enum e = ParserUtils.parseEnum(_fileName, node);

        // check for name conflict
        Element potentialConflict = _unit.findEnum(e.name);
        if (potentialConflict != null) {
            // check for name conflict
            throw new NameConflictException(e.sourceLocation, "enum", e.name, potentialConflict.sourceLocation);
        }
        _unit.enums.add(e);
    }
}
