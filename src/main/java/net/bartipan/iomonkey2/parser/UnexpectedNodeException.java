package net.bartipan.iomonkey2.parser;

import org.antlr.runtime.tree.CommonTree;

import java.util.Arrays;

/**
 * Created by honza on 9/16/14.
 */
public class UnexpectedNodeException extends RuntimeException {
    private CommonTree _node;
    private String _fileName;
    private String _expected;

    public UnexpectedNodeException(CommonTree node, String fileName) {
        _node = node;
        _fileName = fileName;
    }

    public UnexpectedNodeException(CommonTree node, String fileName, String expected) {
        _node = node;
        _fileName = fileName;
        _expected = expected;
    }

    @Override
    public String getMessage() {

        String location = String.format("(%d:%d)", _node.getLine(), _node.getCharPositionInLine());
        if (_fileName != null)
            location = _fileName + location;
        String message = location + " Unexpected node " + _node.getToken().toString();
        if (_expected != null)
            message += ", expecting " + _expected;
        return message;
    }

    private final static boolean contains(int val, int[] args) {
        for (int av : args) {
            if (av == val)
                return true;
        }

        return false;
    }

    public final static void expect(String fileName, CommonTree node, String expected, int... expectedTokenTypes) {
        if (!contains(node.getToken().getType(), expectedTokenTypes))
            throw new UnexpectedNodeException(node, fileName, expected);
    }
}
