package net.bartipan.iomonkey2.parser;

import org.antlr.runtime.tree.CommonTree;

/**
 * Created by honza on 9/16/14.
 */
public interface IAstVisitor {
    void visitPackage(CommonTree node);
    void visitOptions(CommonTree node);
    void visitImport(CommonTree node);
    void visitStruct(CommonTree node);
    void visitEnum(CommonTree node);
}
