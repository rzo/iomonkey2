package net.bartipan.iomonkey2;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by jbartipan on 28.01.16.
 */
public enum FormatVersion {
    V0, // first implementation
    V1; // + optional field size prefix for fwd compatibility of loading for optionals

    private static final Map<String,FormatVersion> _formatStrings;
    static {
        _formatStrings = new HashMap<>();
        _formatStrings.put("0", V0);
        _formatStrings.put("v0", V0);
        _formatStrings.put("1", V1);
        _formatStrings.put("v1", V1);
    }

    public static FormatVersion fromString(String val) {
        if (!_formatStrings.containsKey(val)) {
            throw new IllegalArgumentException(String.format("Unknown format string '%s' allowed values are: %s",
                    val, String.join(", ", _formatStrings.keySet())));
        }

        return _formatStrings.get(val);
    }

}
