package net.bartipan.iomonkey2;

import net.bartipan.iomonkey2.codegen.CodeGenFactory;
import net.bartipan.iomonkey2.codegen.ICodeGen;
import net.bartipan.iomonkey2.codegen.doc.DocCodeGen;
import net.bartipan.iomonkey2.codegen.cpp.CppCodeGen;
import net.bartipan.iomonkey2.codegen.cs.CsCodeGen;
import net.bartipan.iomonkey2.codegen.cs2j.Cs2JXmlCodeGen;
import net.bartipan.iomonkey2.codegen.dot.DotCodeGen;
import net.bartipan.iomonkey2.codegen.dummy.DummyPrint;
import net.bartipan.iomonkey2.codegen.java.JavaCodeGen;
import net.bartipan.iomonkey2.codegen.json.JsonCodeGen;
import net.bartipan.iomonkey2.codegen.py.PyCodeGen;
import net.bartipan.iomonkey2.linker.Linker;
import net.bartipan.iomonkey2.model.*;
import net.bartipan.iomonkey2.parser.UnexpectedNodeException;
import net.bartipan.iomonkey2.utils.PropertiesVariableProvider;
import net.bartipan.iomonkey2.utils.VariableProvider;
import org.antlr.runtime.*;
import org.apache.commons.cli.*;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.Properties;

/**
 * Created by honza on 9/15/14.
 */
public class Main {

    private Options _opts;
    private CommandLine _cmdLine;
    private File _outputDir;
    private Context _ctx;
    private String[] _unitSearchPaths;
    private String _codeGenName = "dummy";
    private int _skipFilePathComponents = 0;
    private String _snapshotSuffix = "";
    private Properties _properties;
    private VariableProvider _propertiesVariableProvider;

    private static Main _main;

    public static Main getInstance() {
        return _main;
    }

    public String getSnapshotSuffix() {
        return _snapshotSuffix;
    }

    private void initCodeGenFactory() {
        DummyPrint p = new DummyPrint();
        CodeGenFactory f = CodeGenFactory.getInstance();
        f.register("dummy", p);
        f.register("print", p);

        CsCodeGen cs = new CsCodeGen();
        f.register("cs", cs);
        f.register("C#", cs);

        PyCodeGen py = new PyCodeGen();
        f.register("py", py);
        f.register("python", py);

        JavaCodeGen java = new JavaCodeGen();
        f.register("java", java);

        CppCodeGen cpp = new CppCodeGen();
        f.register("C++", cpp);
        f.register("cpp", cpp);

        DotCodeGen dot = new DotCodeGen();
        f.register("dot", dot);
        f.register("uml", dot);

        Cs2JXmlCodeGen cs2j = new Cs2JXmlCodeGen();
        f.register("cs2j", cs2j);

        DocCodeGen doc = new DocCodeGen();
        f.register("doc", doc);

        JsonCodeGen json = new JsonCodeGen();
        f.register("json", json);
    }

    private void createOptions() {
        _opts = new Options();
        _opts.addOption(OptionBuilder.withArgName("package dir").hasArg()
                .withDescription("Add package dir to list of package library directories").create("L"));
        _opts.addOption(OptionBuilder.withArgName("output dir").hasArg()
                .withDescription("Set output directory").create("o"));
        _opts.addOption(OptionBuilder.withArgName("code-gen").hasArg()
                .withDescription("Set code-gen").create("x"));
        _opts.addOption(OptionBuilder.withArgName("num components").hasArg().withType(Number.class)
                .withDescription("Skip num components in output file path").withLongOpt("skip-components").create("s"));
        _opts.addOption(OptionBuilder.withDescription("List available code-gens").create("l"));
        _opts.addOption(OptionBuilder.withDescription("PrintHelp").create("h"));
        _opts.addOption(OptionBuilder.withDescription("Set snapshot suffix").hasArg().withArgName("suffix")
                .withLongOpt("snapshot-suffix").create('n'));
        _opts.addOption(OptionBuilder.withDescription("Sets format version").hasArg().create('v'));
        _opts.addOption(OptionBuilder.withDescription("Generates library registrator for all readers registration. " +
                "The value of registratorDirAndPackage parameter have to be in format <dir>:<package> e.g. iom2/game/model:game.model")
                .hasArg().withArgName("registratorDirAndPackage").create("r"));
        _opts.addOption(OptionBuilder.withArgName("key=value pair").hasArgs(2).withValueSeparator('=')
                                .withDescription("Define new property (key=value)").create("P"));
    }

    private void parseOpts(String[] args) throws ParseException {
        if (args.length > 1 && args[0].charAt(0) != '-') {
            _codeGenName = args[0];
            args = Arrays.copyOfRange(args, 1, args.length);
        }
        _cmdLine = new PosixParser().parse(_opts, args);
        if (_cmdLine.hasOption('v'))
            Config.FORMAT_VERSION = FormatVersion.fromString(_cmdLine.getOptionValue("v"));
    }

    private void processFile(String fileName, ICodeGen codeGen, File destDir) {
        File file = new File(fileName);
        _ctx = new Context();

        if (_unitSearchPaths != null)
            Collections.addAll(_ctx.unitSearchPaths, _unitSearchPaths);


        try {
            _ctx.loadMainUnit(file);
        } catch (IOException | RecognitionException | UnexpectedNodeException e) {
            e.printStackTrace();
        }

        Unit unit = _ctx.getMainUnit();

        if (unit != null) {

            Linker linker = new Linker(_ctx);
            linker.link();

            codeGen.generate(_ctx, unit, destDir, _skipFilePathComponents);

        }
    }

    private void run(String[] args) throws ParseException, IOException, RecognitionException {
        initCodeGenFactory();
        createOptions();
        parseOpts(args);

        boolean showHelp = _cmdLine.hasOption("h") || args.length == 0;
        boolean listCodeGens = _cmdLine.hasOption("l");

        if (_codeGenName == null || _codeGenName.isEmpty()) {
            showHelp = true;
            listCodeGens = true;
            System.out.println("Codegen not specified");
        }

        ICodeGen cg = CodeGenFactory.getInstance().get(_cmdLine.getOptionValue("x",_codeGenName));

        if (cg == null) {
            listCodeGens = true;
            System.out.printf("Unknown codegen '%s'\n", _codeGenName);
        }

        if (showHelp) {
            HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp("iomonkey2 [<codegen>] [<opts>] <structdef file> ...", _opts);
        }

        if (listCodeGens) {
            System.out.println("Available Code-Gens");
            CodeGenFactory f = CodeGenFactory.getInstance();
            for (String cgn : f.availableCodeGens()) {
                System.out.printf("- %s\t%s\n", cgn, f.get(cgn).description());
            }
        }

        if (showHelp || listCodeGens)
            return;

        _outputDir = new File(_cmdLine.getOptionValue('o', "."));
        _unitSearchPaths = _cmdLine.getOptionValues('L');
        if (_cmdLine.hasOption('s')) {
            //_skipFilePathComponents = ((Integer)_cmdLine.getParsedOptionValue("skip-components")).intValue();
            _skipFilePathComponents = Integer.parseInt(_cmdLine.getOptionValue('s'));
        }

        if (_cmdLine.hasOption('n')) {
            _snapshotSuffix = _cmdLine.getOptionValue('n');
        }
        _properties = _cmdLine.getOptionProperties("P");
        //System.out.println("properties: " + _properties);

        cg.prepare(_outputDir);

        for (String arg : _cmdLine.getArgs()) {
            try {
                processFile(arg, cg, _outputDir);
            } catch (Exception e) {
                System.err.printf("Exception %s while processing %s", e.getClass().getName(), arg);
                throw e;
            }
        }

        if (_cmdLine.hasOption('r')) {
            String dirAndPackage = _cmdLine.getOptionValue('r');
            String[] tokens = dirAndPackage.split(":");

            if (tokens.length != 2) {
                throw new IllegalArgumentException(String.format("Passed registrator dirAndPackage argument (%s) doesn't conform " +
                        "required format <dirPath>:<packageName> e.g. iom2/game/model:game.model", dirAndPackage));
            }

            File baseDir = new File(tokens[0]);

            if (!baseDir.exists() || !baseDir.isDirectory()) {
                throw new IllegalArgumentException(String.format("Passed path (to directory with source for which should " +
                        "be generated reader registrators) is invalid, the directory '%s' doesn't exists", tokens[0]));
            }

            DotName packageName = new DotName(tokens[1]);

            runRegistratorGen(cg, baseDir, packageName);

        }

        cg.finish(_outputDir);
    }

    private void runRegistratorGen(ICodeGen cg, File baseDir, DotName packageName) throws IOException, RecognitionException {
        RegistratorContext rctx = new RegistratorContext(baseDir);
        rctx.load();
        cg.generateRegistrator(rctx, packageName, _outputDir, _skipFilePathComponents);
    }

    public Properties getProperties() {
        return _properties;
    }

    public VariableProvider getPropertiesValueProvider() {
        if (_propertiesVariableProvider == null) {
            assert _properties != null;
            _propertiesVariableProvider = new PropertiesVariableProvider(_properties);
        }
        return _propertiesVariableProvider;
    }

    public static void main(String[] args) {
        try {
            _main = new Main();
            _main.run(args);
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(127);
        }
    }
}
