package net.bartipan.iomonkey2.codegen.cpp;

import net.bartipan.iomonkey2.Config;
import net.bartipan.iomonkey2.FormatVersion;
import net.bartipan.iomonkey2.codegen.CodeGenUtils;
import net.bartipan.iomonkey2.codegen.ICodeGen;
import net.bartipan.iomonkey2.model.*;
import org.stringtemplate.v4.STGroup;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * Created by honza on 9/28/14.
 */
public class CppCodeGen implements ICodeGen {
    @Override
    public void generate(Context ctx, Unit unit, File destDir, int skipFileComponents) {
        String structHTemplateFN = "/codegen/cpp/struct-legacy.h.stg";
        String structCppTemplateFN = "/codegen/cpp/struct-legacy.cpp.stg";

        if (Config.FORMAT_VERSION == FormatVersion.V1) {
            structHTemplateFN = "/codegen/cpp/struct.h.stg";
            structCppTemplateFN = "/codegen/cpp/struct.cpp.stg";
        }

        STGroup stgStructH = CodeGenUtils.openStg(structHTemplateFN);
        STGroup stgStructCpp = CodeGenUtils.openStg(structCppTemplateFN);
        STGroup stgEnum = CodeGenUtils.openStg("/codegen/cpp/enum.h.stg");

        String ns = unit.name.toString();
        File unitDestDir = unit.name.asCamelCaseFile(destDir, skipFileComponents);

        unitDestDir.mkdirs();

        for (Element s : unit.structs) {
            try {
                CodeGenUtils.generateFile(new File(unitDestDir, s.getCppName() + ".h"), stgStructH, "basicStructDocument",
                        "struct", s, "unit", unit);
                CodeGenUtils.generateFile(new File(unitDestDir, s.getCppName() + ".cpp"), stgStructCpp, "basicStructDocument",
                        "struct", s, "unit", unit);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        for (Element e : unit.enums) {
            try {
                CodeGenUtils.generateFile(new File(unitDestDir, e.name + ".h"), stgEnum, "enumDocument",
                        "e", e, "unit", unit);
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }

        if (!unit.linkInfo.getInheritanceNeededStructReferences().isEmpty()) {
            try {
                CodeGenUtils.generateFile(new File(unitDestDir, "IomReaderUtils.h"), "/codegen/cpp/readerUtils.h.stg",
                        "document", "unit", unit);
                CodeGenUtils.generateFile(new File(unitDestDir, "IomReaderUtils.cpp"), "/codegen/cpp/readerUtils.cpp.stg",
                        "document", "unit", unit);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public String description() {
        return "C++";
    }

    @Override
    public void prepare(File destDir) {

    }

    @Override
    public void finish(File destDir) {

    }

    @Override
    public void generateRegistrator(RegistratorContext ctx, DotName packageName, File destDir, int skipNameComponents) {
        STGroup stgH = CodeGenUtils.openStg("/codegen/cpp/registrator.h.stg");
        STGroup stgCpp = CodeGenUtils.openStg("/codegen/cpp/registrator.cpp.stg");
        File unitDir = packageName.asCamelCaseFile(destDir, skipNameComponents);
        File destFileH = new File(unitDir, "Registrator.h");
        File destFileCpp = new File(unitDir, "Registrator.cpp");
        try {
            List<Unit> units = ctx.getSortedUnits();
            CodeGenUtils.generateFile(destFileH, stgH, "document",
                    "units", units,
                    "namespace", packageName
            );
            CodeGenUtils.generateFile(destFileCpp, stgCpp, "document",
                    "units", units,
                    "namespace", packageName
            );
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
