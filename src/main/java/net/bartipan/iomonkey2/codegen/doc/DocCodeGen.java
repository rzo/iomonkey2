package net.bartipan.iomonkey2.codegen.doc;

import net.bartipan.iomonkey2.codegen.CodeGenUtils;
import net.bartipan.iomonkey2.codegen.ICodeGen;
import net.bartipan.iomonkey2.model.Context;
import net.bartipan.iomonkey2.model.DotName;
import net.bartipan.iomonkey2.model.RegistratorContext;
import net.bartipan.iomonkey2.model.Unit;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by jbartipan on 30.08.16.
 */
public class DocCodeGen implements ICodeGen {
    private List<Unit> _processedUnits = new ArrayList<>();

    @Override
    public void generate(Context ctx, Unit unit, File destDir, int skipFileComponents) {
        _processedUnits.add(unit);
        try {
            generateUnitPage(ctx, unit, destDir, skipFileComponents);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void prepare(File destDir) {
        if (!destDir.exists()) {
            destDir.mkdirs();
        }

        try {
            CodeGenUtils.generateFile(new File(destDir, "main.css"), "/codegen/doc/main.css.stg", "main");
        } catch (IOException e) {
            e.printStackTrace();
        }

        _processedUnits.clear();
    }

    @Override
    public void finish(File destDir) {
        String timestamp = new SimpleDateFormat("yyyy-MM-dd HH:mm").format(new Date());
        try {
            CodeGenUtils.generateFile(new File(destDir, "index.html"), "/codegen/doc/index.html.stg",
                    "main");
            CodeGenUtils.generateFile(new File(destDir, "packages.html"), "/codegen/doc/packages.html.stg",
                    "main", "units", _processedUnits, "timestamp", timestamp);
            CodeGenUtils.generateFile(new File(destDir, "overview.html"), "/codegen/doc/overview.html.stg",
                    "main", "units", _processedUnits, "timestamp", timestamp);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void generateUnitPage(Context ctx, Unit unit, File destDir, int skipFileComponents) throws IOException {
        File f = new File(destDir, unit.name.toString() + ".html");
        String timestamp = new SimpleDateFormat("yyyy-MM-dd HH:mm").format(new Date());
        CodeGenUtils.generateFile(f, "/codegen/doc/unit.html.stg", "main", "unit", unit, "timestamp", timestamp);
    }

    @Override
    public String description() {
        return "Struct Documentation in HTML";
    }

    @Override
    public void generateRegistrator(RegistratorContext ctx, DotName packageName, File destDir, int skipNameComponents) {

    }
}
