package net.bartipan.iomonkey2.codegen;

import org.stringtemplate.v4.AutoIndentWriter;
import org.stringtemplate.v4.ST;
import org.stringtemplate.v4.STGroup;
import org.stringtemplate.v4.STGroupFile;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringWriter;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

/**
 * Created by honza on 9/28/14.
 */
public class CodeGenUtils {
    public static String genTemplate(STGroup stg, String templName, Object... args) throws IOException {
        ST st = stg.getInstanceOf(templName);
        assert args.length % 2 == 0;
        for (int i = 0; i < args.length; i += 2) {
            st.add(args[i].toString(), args[i + 1]);
        }
        
        StringWriter sw = new StringWriter();
        st.write(new AutoIndentWriter(sw, "\n"));
        return sw.toString();
    }

    public static STGroup openStg(String jarPath) {
        URL url = CodeGenFactory.getInstance().getClass().getResource(jarPath);
        return new STGroupFile(url, "utf-8", '<', '>');
    }


    public static void generateFile(File dest, String stgPath, String templName, Object... templArgs) throws IOException {
        STGroup stg = openStg(stgPath);
        generateFile(dest, stg, templName, templArgs);

    }

    public static void generateFile(File dest, STGroup stg, String templName, Object... templArgs) throws IOException {
        String buf = genTemplate(stg, templName, templArgs);

        FileWriter fw = new FileWriter(dest);
        fw.write(buf);
        fw.close();
    }

    private static boolean isUpperUnderscoreCase(String name) {
        for (int i = 0; i < name.length(); i++) {
            char ch = name.charAt(i);
            if (!(Character.isUpperCase(ch) || ch == '_'))
                return false;
        }

        return true;
    }

    public static String[] nameComponents(String name) {
        List<String> ret = new ArrayList<String>();
        int compBegin = 0;

        Predicate<Character> predicate;
        int off = 0;

        if (isUpperUnderscoreCase(name)) {
            predicate = ch -> ch == '_';
            off = 1;
        } else
            predicate = ch -> Character.isUpperCase(ch);

        for (int i = 1; i < name.length(); i++) {
            if (predicate.test(name.charAt(i))) {
                ret.add(name.substring(compBegin, i));
                compBegin = i + off;
            }
        }

        ret.add(name.substring(compBegin));

        return ret.toArray(new String[]{});
    }

    public static String pascalComponent(String component) {
        return Character.toUpperCase(component.charAt(0)) + component.substring(1).toLowerCase();
    }

    public static String pascalCase(String name) {
        StringBuilder sb = new StringBuilder();
        for (String c : nameComponents(name)) {
            sb.append(pascalComponent(c));
        }
        return sb.toString();
    }

    public static String camelCase(String name) {
        StringBuilder sb = new StringBuilder();
        String[] comps = nameComponents(name);

        sb.append(comps[0].toLowerCase());

        for (int i = 1 ; i < comps.length; i++) {
            sb.append(pascalComponent(comps[i]));
        }
        return sb.toString();
    }

    public static String upperUnderscoreCase(String name) {
        StringBuilder sb = new StringBuilder();
        boolean first = true;
        for (String c : nameComponents(name)) {
            if (first)
                first = false;
            else
                sb.append('_');
            sb.append(c.toUpperCase());
        }
        return sb.toString();
    }

    public static String generateName(String name, NameCaseType nameCaseType) {
        switch (nameCaseType) {
            case CAMEL:
                return camelCase(name);
            case PASCAL:
                return pascalCase(name);
            case UPPER_UNDERSCORE:
                return upperUnderscoreCase(name);
            default:
                throw new UnsupportedOperationException("Unsupported NameCaseType " + nameCaseType);
        }
    }

}
