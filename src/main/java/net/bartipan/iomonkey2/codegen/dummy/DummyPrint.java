package net.bartipan.iomonkey2.codegen.dummy;

import net.bartipan.iomonkey2.codegen.ICodeGen;
import net.bartipan.iomonkey2.model.*;
import net.bartipan.iomonkey2.model.Enum;

import java.io.File;

/**
 * Created by honza on 9/23/14.
 */
public class DummyPrint implements ICodeGen {

    private String format(AttributeType at) {
        String ret = (at.scalarType == ScalarType.TypeId) ? at.typeId : at.scalarType.getCppType();

        if (at.arraySize == 0) {
            ret += "[]";
        } else if (at.arraySize > 0) {
            ret += String.format("[%d]", at.arraySize);
        }

        return ret;
    }

    private String format(Annotation ant) {
        String ret = String.format("@%s", ant.name);
        if (ant.parameters.size() > 0) {
            StringBuilder sb = new StringBuilder();
            sb.append("(");
            boolean first = true;
            for (Value v : ant.parameters) {
                if (first)
                    first = false;
                else
                    sb.append(", ");
                sb.append(v.getCsValue());
            }
            sb.append(")");
            ret += sb.toString();
        }

        return ret;
    }

    private void printStruct(Struct s) {
        for (Annotation ant : s.annotations) {
            System.out.println(format(ant));
        }
        System.out.println("struct " + s.name + ":");
        for (Attribute attr : s.attributes) {
            for (Annotation ant : attr.annotations) {
                System.out.println(" " + format(ant));
            }
            System.out.println(" " + attr.name + ": " + format(attr.type));
        }
    }

    private void printEnum(Enum e) {
        System.out.println("enum " + e.name);
        for (EnumConst c : e.constants) {
            System.out.println(" " + c.name + "=" + c.value);
        }

    }

    @Override
    public void generate(Context ctx, Unit unit, File destDir, int skipFileComponents) {
        System.out.println("Unit: " + unit.name);

        for (Struct s : unit.structs) {
            printStruct(s);
        }

        for (Enum e : unit.enums) {
            printEnum(e);
        }
    }

    @Override
    public String description() {
        return "Dummy printer";
    }

    @Override
    public void prepare(File destDir) {

    }

    @Override
    public void finish(File destDir) {

    }

    @Override
    public void generateRegistrator(RegistratorContext ctx, DotName packageName, File destDir, int skipNameComponents) {

    }
}
