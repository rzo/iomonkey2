package net.bartipan.iomonkey2.codegen.cs;

import net.bartipan.iomonkey2.Config;
import net.bartipan.iomonkey2.FormatVersion;
import net.bartipan.iomonkey2.codegen.CodeGenUtils;
import net.bartipan.iomonkey2.codegen.ICodeGen;
import net.bartipan.iomonkey2.codegen.NameCaseType;
import net.bartipan.iomonkey2.model.*;
import org.stringtemplate.v4.STGroup;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * Created by honza on 9/28/14.
 */
public class CsCodeGen implements ICodeGen {
    @Override
    public void generate(Context ctx, Unit unit, File destDir, int skipFileComponents) {
        String structStgFN = "/codegen/cs/struct-legacy.stg";

        if (Config.FORMAT_VERSION == FormatVersion.V1) {
            structStgFN = "/codegen/cs/struct.stg";
        }
        STGroup stg = CodeGenUtils.openStg(structStgFN);
        STGroup stgEnum = CodeGenUtils.openStg("/codegen/cs/enum.stg");

        //String ns = unit.name.toString();
        String pathPrefix = unit.getPathPrefix("cs");

        // handle skip components in prefix
        if ((!pathPrefix.isEmpty()) && skipFileComponents > 0) {
            String[] prefixComponents = pathPrefix.split(File.separator.equals("\\") ? "\\\\" : "/");

            int numberComponentsToSkip = Math.min(prefixComponents.length, skipFileComponents);

            if (prefixComponents.length == numberComponentsToSkip) {
                pathPrefix = "";
            } else {
                for (int i = numberComponentsToSkip; i < prefixComponents.length; i++) {
                    if (i == numberComponentsToSkip)
                        pathPrefix = prefixComponents[i];
                    else
                        pathPrefix = pathPrefix + "." + prefixComponents[i];
                }
            }

            skipFileComponents -= numberComponentsToSkip;
        }

        if (!pathPrefix.isEmpty())
            destDir = new File(destDir, pathPrefix);
        File unitDestDir = unit.name.asPascalCaseFile(destDir, skipFileComponents);
        String pathSuffix = unit.getPathSuffix(NameCaseType.PASCAL);
        if (!pathSuffix.isEmpty())
            unitDestDir = new File(unitDestDir, pathSuffix);

        unitDestDir.mkdirs();

        for (Element s : unit.structs) {
            try {
                CodeGenUtils.generateFile(new File(unitDestDir, s.name + ".cs"), stg, "basicStructDocument", "struct", s,
                        "unit", unit);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        for (Element e : unit.enums) {
            try {
                CodeGenUtils.generateFile(new File(unitDestDir, e.name + ".cs"), stgEnum, "enumDocument", "e", e, "unit", unit);
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }

        //if (!unit.linkInfo.getInheritanceNeededStructReferences().isEmpty()) {
            try {
                CodeGenUtils.generateFile(new File(unitDestDir, "IomReaderUtils.cs"), "/codegen/cs/readerUtils.stg",
                        "document", "unit", unit);
            } catch (IOException e) {
                e.printStackTrace();
            }
        //}
    }

    @Override
    public String description() {
        return "C#";
    }

    @Override
    public void prepare(File destDir) {

    }

    @Override
    public void finish(File destDir) {

    }

    @Override
    public void generateRegistrator(RegistratorContext ctx, DotName packageName, File destDir, int skipNameComponents) {
        STGroup stg = CodeGenUtils.openStg("/codegen/cs/registrator.stg");
        File destFile = destDir;

        for (String dc : packageName.components) {
            destFile = new File(destFile, dc);
        }
        destFile = new File(packageName.asPascalCaseFile(destDir, skipNameComponents), "Registrator.cs");
        try {
            CodeGenUtils.generateFile(destFile, stg, "document",
                    "units", ctx.getSortedUnits(),
                    "namespace", packageName.getPascalCase());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
