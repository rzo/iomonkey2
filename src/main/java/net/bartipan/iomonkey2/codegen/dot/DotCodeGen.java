package net.bartipan.iomonkey2.codegen.dot;

import net.bartipan.iomonkey2.codegen.CodeGenUtils;
import net.bartipan.iomonkey2.codegen.ICodeGen;
import net.bartipan.iomonkey2.model.Context;
import net.bartipan.iomonkey2.model.DotName;
import net.bartipan.iomonkey2.model.RegistratorContext;
import net.bartipan.iomonkey2.model.Unit;

import java.io.File;
import java.io.IOException;

/**
 * Created by jbartipan on 25.02.15.
 */
public class DotCodeGen implements ICodeGen {
    @Override
    public void generate(Context ctx, Unit unit, File destDir, int skipFileComponents) {
        if (!destDir.exists())
            destDir.mkdirs();

        File outFile = new File(destDir, unit.name.toString(".") + ".dot");
        try {
            CodeGenUtils.generateFile(outFile, "/codegen/dot/dot.stg", "document", "units", ctx.units.values() );
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String description() {
        return "Dot Class Graph";
    }

    @Override
    public void prepare(File destDir) {

    }

    @Override
    public void finish(File destDir) {

    }

    @Override
    public void generateRegistrator(RegistratorContext ctx, DotName packageName, File destDir, int skipNameComponents) {

    }
}
