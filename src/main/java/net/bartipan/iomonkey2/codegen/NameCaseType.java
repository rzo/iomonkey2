package net.bartipan.iomonkey2.codegen;

/**
 * Created by jbartipan on 19.05.15.
 */
public enum NameCaseType {
    PASCAL, CAMEL, UPPER_UNDERSCORE

}
