package net.bartipan.iomonkey2.codegen.cs2j;

import net.bartipan.iomonkey2.codegen.CodeGenUtils;
import net.bartipan.iomonkey2.codegen.ICodeGen;
import net.bartipan.iomonkey2.model.*;
import net.bartipan.iomonkey2.model.Enum;

import java.io.File;
import java.io.IOException;

/**
 * Created by jbartipan on 12.01.16.
 */
public class Cs2JXmlCodeGen implements ICodeGen {
    public void generate(Context ctx, Unit unit, File destDir, int skipFileComponents)
    {
        File outDir = new File(destDir, unit.name.getPascalCase(File.separator));
        if (!outDir.exists())
            outDir.mkdirs();

        try {
            for (Struct s : unit.structs) {
                File outFile = new File(outDir, s.getCsName() + ".xml");
                CodeGenUtils.generateFile(outFile, "/codegen/cs2j/xmlStruct.stg", "document", "s", s, "unit", unit);
            }
            for (Enum s : unit.enums) {
                File outFile = new File(outDir, s.getCsName() + ".xml");
                CodeGenUtils.generateFile(outFile, "/codegen/cs2j/xmlEnum.stg", "document", "s", s, "unit", unit);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String description()
    {
        return "cs2j XML transform files for iom2 struct allowing using cs2j and iom2 generated classes " +
                "to work seamlessly";
    }

    @Override
    public void prepare(File destDir) {

    }

    @Override
    public void finish(File destDir) {

    }

    @Override
    public void generateRegistrator(RegistratorContext ctx, DotName packageName, File destDir, int skipNameComponents) {

    }
}
