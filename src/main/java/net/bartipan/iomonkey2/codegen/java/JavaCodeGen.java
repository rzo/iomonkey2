package net.bartipan.iomonkey2.codegen.java;

import net.bartipan.iomonkey2.Config;
import net.bartipan.iomonkey2.FormatVersion;
import net.bartipan.iomonkey2.codegen.CodeGenUtils;
import net.bartipan.iomonkey2.codegen.ICodeGen;
import net.bartipan.iomonkey2.codegen.NameCaseType;
import net.bartipan.iomonkey2.model.*;
import org.stringtemplate.v4.STGroup;

import java.io.File;
import java.io.IOException;

/**
 * Created by jbartipan on 05.12.14.
 */
public class JavaCodeGen implements ICodeGen {
    @Override
    public void generate(Context ctx, Unit unit, File destDir, int skipFileComponents) {
        String structTemplateFN = "/codegen/java/struct-legacy.stg";
        if (Config.FORMAT_VERSION == FormatVersion.V1) {
            structTemplateFN = "/codegen/java/struct.stg";
        }
        STGroup stg = CodeGenUtils.openStg(structTemplateFN);
        STGroup stgEnum = CodeGenUtils.openStg("/codegen/java/enum.stg");

        String pathPrefix = unit.getPathPrefix("java");
        if (!pathPrefix.isEmpty())
            destDir = new File(destDir, pathPrefix);


        File unitDestDir = unit.name.asCamelCaseFile(destDir, skipFileComponents);

        String pathSuffix = unit.getPathSuffix(NameCaseType.CAMEL);
        if (!pathSuffix.isEmpty())
            unitDestDir = new File(unitDestDir, pathSuffix);


        unitDestDir.mkdirs();

        for (Element s : unit.structs) {
            try {
                CodeGenUtils.generateFile(new File(unitDestDir, s.name + ".java"), stg, "basicStructDocument", "struct", s,
                        "unit", unit);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        for (Element e : unit.enums) {
            try {
                CodeGenUtils.generateFile(new File(unitDestDir, e.name + ".java"), stgEnum, "enumDocument", "e", e, "unit", unit);
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }

        //if (!unit.linkInfo.getInheritanceNeededStructReferences().isEmpty()) {
            try {
                CodeGenUtils.generateFile(new File(unitDestDir, "IomReaderUtils.java"), "/codegen/java/readerUtils.stg",
                        "document", "unit", unit);
            } catch (IOException e) {
                e.printStackTrace();
            }
        //}
    }

    @Override
    public String description() {
        return "Java";
    }

    @Override
    public void prepare(File destDir) {

    }

    @Override
    public void finish(File destDir) {

    }

    @Override
    public void generateRegistrator(RegistratorContext ctx, DotName packageName, File destDir, int skipNameComponents) {
        STGroup stg = CodeGenUtils.openStg("/codegen/java/registrator.stg");
        File destFile = destDir;

        for (String dc : packageName.components) {
            destFile = new File(destFile, dc);
        }
        destFile = new File(packageName.asCamelCaseFile(destDir, skipNameComponents), "Registrator.java");
        try {
            CodeGenUtils.generateFile(destFile, stg, "document",
                    "units", ctx.getSortedUnits(),
                    "package", packageName.getCamelCase());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
