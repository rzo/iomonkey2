package net.bartipan.iomonkey2.codegen.py;

import net.bartipan.iomonkey2.codegen.CodeGenUtils;
import net.bartipan.iomonkey2.codegen.ICodeGen;
import net.bartipan.iomonkey2.model.Context;
import net.bartipan.iomonkey2.model.DotName;
import net.bartipan.iomonkey2.model.RegistratorContext;
import net.bartipan.iomonkey2.model.Unit;
import org.stringtemplate.v4.STGroup;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;

/**
 * Created by jbartipan on 07.11.14.
 */
public class PyCodeGen implements ICodeGen {
    @Override
    public void generate(Context ctx, Unit unit, File destDir, int skipFileComponents) {
        STGroup stg = CodeGenUtils.openStg("/codegen/py/module.stg");

        File unitFile = unit.name.asFile(destDir, ".py");
        File unitDestDir = unitFile.getParentFile();



        unitDestDir.mkdirs();
        try {
            ensurePackageDir(destDir, unitDestDir);
            ensureUtilPackage(destDir);
        } catch (IOException e) {
            System.err.println("Failed when creating output directory");
            e.printStackTrace();
            return;
        }

        try {
            CodeGenUtils.generateFile(unitFile, stg, "module", "unit", unit);
        } catch (IOException e) {
            e.printStackTrace();
        }

        /*
        if (!unit.linkInfo.getExtendedStructReferences().isEmpty()) {
            try {
                CodeGenUtils.generateFile(new File(unitDestDir, "IomReaderUtils.py"), "/codegen/py/readerUtils.stg",
                        "document", "unit", unit);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        */
    }


    private void ensurePackageDir(File parent, File packcageDir) throws IOException {
        File p = packcageDir.getAbsoluteFile();

        while (!p.getCanonicalFile().equals(parent.getCanonicalFile())) {
            File f = new File(p, "__init__.py");
            if (!f.exists()) {
                System.out.println("Creating " + f.toString());
                f.createNewFile();
            }
            p = p.getParentFile();
        }
    }

    private void ensureUtilPackage(File destDir) throws IOException {
        File destFile = new File(destDir, "iomonkey2" + File.separator + "__init__.py");

        /*if (!destFile.exists())*/ {
            Files.copy(getClass().getResourceAsStream("/codegen/py/iomonkey2/__init__.py"), destFile.toPath(),
                    StandardCopyOption.REPLACE_EXISTING);
        }
    }

    @Override
    public String description() {
        return "Python";
    }

    @Override
    public void prepare(File destDir) {

    }

    @Override
    public void finish(File destDir) {

    }

    @Override
    public void generateRegistrator(RegistratorContext ctx, DotName packageName, File destDir, int skipNameComponents) {

    }
}
