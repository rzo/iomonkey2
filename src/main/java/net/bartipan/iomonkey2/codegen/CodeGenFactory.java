package net.bartipan.iomonkey2.codegen;

import java.util.*;

/**
 * Created by honza on 9/23/14.
 */
public class CodeGenFactory {
    private static CodeGenFactory _instance = new CodeGenFactory();
    private Map<String, ICodeGen> _codeGens = new HashMap<String, ICodeGen>();

    public ICodeGen get(String name) {
        return _codeGens.get(name);
    }

    public void register(String name, ICodeGen codeGen) {
        _codeGens.put(name, codeGen);
    }

    public Set<String> availableCodeGens() {
        return _codeGens.keySet();
    }

    public static CodeGenFactory getInstance() {
        return _instance;
    }
}
