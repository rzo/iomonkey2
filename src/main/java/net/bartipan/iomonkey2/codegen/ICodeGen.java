package net.bartipan.iomonkey2.codegen;

import net.bartipan.iomonkey2.model.Context;
import net.bartipan.iomonkey2.model.DotName;
import net.bartipan.iomonkey2.model.RegistratorContext;
import net.bartipan.iomonkey2.model.Unit;

import java.io.File;

/**
 * Created by honza on 9/23/14.
 */
public interface ICodeGen {
    public void generate(Context ctx, Unit unit, File destDir, int skipFileComponents);
    public String description();
    public void prepare(File destDir);
    public void finish(File destDir);
    public void generateRegistrator(RegistratorContext ctx, DotName packageName, File destDir, int skipNameComponents);
}
