package net.bartipan.iomonkey2.utils;

import java.util.HashMap;

/**
 * Created by stetinapet on 17.1.17.
 */
public class HashMapVariableProvider extends HashMap<String, Object> implements VariableProvider {

    @Override
    public Object getValue(String name) {
        return super.get(name);
    }
}
