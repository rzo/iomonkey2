package net.bartipan.iomonkey2.utils;

import java.util.Properties;

/**
 * Created by stetinapet on 17.1.17.
 */
public class PropertiesVariableProvider implements VariableProvider {

    private Properties properties;

    public PropertiesVariableProvider(Properties properties) {
        this.properties = properties;
    }

    @Override
    public Object getValue(String name) {
        return properties.getProperty(name);
    }
}
