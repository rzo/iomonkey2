package net.bartipan.iomonkey2.utils;

/**
 * Created by stetinapet on 17.1.17.
 */
public interface VariableProvider {

    Object getValue(String name);

    VariableProvider JVM_PROPERTIES = new JvmPropertiesVariableProvider();
}
