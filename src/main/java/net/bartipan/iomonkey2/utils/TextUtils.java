package net.bartipan.iomonkey2.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by stetinapet on 17.1.17.
 */
public class TextUtils {

    private static Pattern VARIABLE_PATTERN = Pattern.compile("\\$\\{([^}]+)}");

    public static String replaceVariables(String input, VariableProvider variableProvider) {
        Matcher m = VARIABLE_PATTERN.matcher(input);
        StringBuffer buffer = new StringBuffer();
        while (m.find()) {
            String variableName = m.group(1);
            Object value = variableProvider.getValue(variableName);
            if (value != null) {
                m.appendReplacement(buffer, Matcher.quoteReplacement(value.toString()));
            }
        }
        m.appendTail(buffer);
        return buffer.toString();
    }

}
