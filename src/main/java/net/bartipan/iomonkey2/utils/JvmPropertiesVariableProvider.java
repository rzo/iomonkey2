package net.bartipan.iomonkey2.utils;

/**
 * Created by stetinapet on 17.1.17.
 */
public class JvmPropertiesVariableProvider implements VariableProvider {

    @Override
    public Object getValue(String name) {
        return System.getProperty(name);
    }
}
