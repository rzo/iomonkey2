package net.bartipan.iomonkey2.model;

import net.bartipan.iomonkey2.Config;

import java.nio.ByteBuffer;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

/**
 * Created by honza on 9/26/14.
 */
public class StructId {
    public byte[] id;

    public StructId(byte[] id) {
        this.id = Arrays.copyOf(id, Config.STRUCT_ID_LEN);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof StructId) {
            return Arrays.equals(((StructId) obj).id, id);
        }
        return super.equals(obj);
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(id);
    }

    public static StructId fromStruct(Element s) {
        String uniqueName = s.fullQualifiedName();

        byte[] digest = new byte[0];
        try {
            digest = MessageDigest.getInstance("MD5").digest(uniqueName.getBytes());
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        return new StructId(digest);

    }

    public long getLong() {
        ByteBuffer buf = ByteBuffer.allocate(Long.BYTES);
        buf.put(id);
        buf.flip();
        return buf.getLong();
    }
}
