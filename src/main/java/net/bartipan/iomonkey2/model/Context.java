package net.bartipan.iomonkey2.model;

import net.bartipan.iomonkey2.Config;
import net.bartipan.iomonkey2.parser.AstParser;
import org.antlr.runtime.RecognitionException;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by honza on 9/15/14.
 */
public class Context {
    private static AstParser _astParser = new AstParser();
    public String mainUnitName;
    public Map<String, Unit> units = new HashMap<String, Unit>();

    public List<String> unitSearchPaths = new ArrayList<String>();

    public List<Struct> rootStructs = new ArrayList<Struct>();

    public File findUnit(DotName unitName) {
        for (String baseDir : unitSearchPaths) {
            File f = unitName.asFile(new File(baseDir), Config.EXTENSION);
            if (f.exists())
                return f;
        }

        return null;
    }

    public Unit loadUnit(DotName unitName) throws IOException, RecognitionException {
        String un = unitName.toString();
        if (!units.containsKey(un)) {
            File unitFile = findUnit(unitName);

            if (unitFile == null) {
                // throw cannot
                throw new FileNotFoundException("Cannot find unit '" + un + "'" );
            }

            Unit unit = _astParser.parseUnit(unitFile);

            units.put(unit.name.toString(), unit);

            for (Import imp : unit.imports) {
                loadUnit(imp.unitName);
            }

            return unit;
        }

        return units.get(un);
    }

    public void loadMainUnit(File file) throws IOException, RecognitionException {
        Unit unit = _astParser.parseUnit(file);

        // check if unit package conforms directory structure
        String validFilePathSuffix = File.separator + unit.name.toString(File.separator) + Config.EXTENSION;
        String actualFilePath = file.getAbsolutePath();
        boolean validPath = actualFilePath.toLowerCase().endsWith(validFilePathSuffix.toLowerCase());
        if (!validPath) {
            System.err.printf("WARNING in %s: directory structure doesn't conform package name\n", actualFilePath);
            System.err.printf("The file should be located in directories like this: ...%s\n", validFilePathSuffix);
            System.err.printf("This is not fatal error, but likely imports won't work as the root path " +
                    "for other modules was computed: %s", actualFilePath.substring(actualFilePath.length() - validFilePathSuffix.length()));
        }

        // get package base directory
        File baseDir = file.getAbsoluteFile().getParentFile();
        int n = unit.name.components.length - 1;
        while (n-- > 0) baseDir = baseDir.getParentFile();

        unitSearchPaths.add(0, baseDir.getAbsolutePath());
        GlobalSettings.getInstance().loadFromDir(baseDir);

        mainUnitName = unit.name.toString();
        units.put(mainUnitName, unit);

        // load imported units
        for (Import imp : unit.imports) {
            imp.unit = loadUnit(imp.unitName);
        }
    }

    public void loadMainUnit(String source) throws IOException, RecognitionException {
        Unit unit = _astParser.parseUnit(source);

        mainUnitName = unit.name.toString();
        units.put(mainUnitName, unit);

        // load imported units
        for (Import imp : unit.imports) {
            loadUnit(imp.unitName);
        }
    }

    public Unit getMainUnit() {
        return units.get(mainUnitName);
    }

    @Override
    public String toString() {
        return String.format("<Context main: %s>", mainUnitName);
    }
}
