package net.bartipan.iomonkey2.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by honza on 9/26/14.
 */
public class StructLinkInfo {
    public StructId structId;
    public int requiredOffset;
    public int optionalOffset;

    public List<Attribute> requiredAttributes = new ArrayList<Attribute>();
    public List<Attribute> optionalAttributes = new ArrayList<Attribute>();
    public List<StructReference> references = new ArrayList<>();


    public boolean getHasOptionals() { return !optionalAttributes.isEmpty();}
    public boolean hasAnyReferences() {
        return !references.isEmpty();
    }
}
