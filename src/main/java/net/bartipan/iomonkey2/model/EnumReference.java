package net.bartipan.iomonkey2.model;

/**
 * Created by honza on 9/28/14.
 */
public class EnumReference {
    public Attribute ref;
    public Enum target;

    public EnumReference(Attribute ref, Enum target) {
        this.ref = ref;
        this.target = target;
    }
}
