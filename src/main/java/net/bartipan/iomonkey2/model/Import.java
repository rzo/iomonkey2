package net.bartipan.iomonkey2.model;

/**
 * Created by honza on 9/12/14.
 */
public class Import {
    public DotName unitName;
    public Unit unit;

    public Import() {

    }

    public Import(DotName name) {
        unitName = name;
    }

    @Override
    public String toString() {
        return String.format("<Import %s>", unitName);
    }
}
