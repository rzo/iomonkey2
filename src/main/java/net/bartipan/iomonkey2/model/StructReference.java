package net.bartipan.iomonkey2.model;

/**
 * Created by honza on 9/28/14.
 */
public class StructReference {
    public Attribute ref;
    public Struct target;

    public StructReference(Attribute ref, Struct target) {
        this.ref = ref;
        this.target = target;
    }
}
