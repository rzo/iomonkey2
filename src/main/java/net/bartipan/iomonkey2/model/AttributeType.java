package net.bartipan.iomonkey2.model;

import net.bartipan.iomonkey2.codegen.CodeGenUtils;

/**
 * Created by honza on 9/12/14.
 */

public class AttributeType {
    public ScalarType scalarType;
    public int arraySize = -1; // -1 - no array, 0 - dynamic array (List), >0 - fixed array
    public String[] arraySizeRef = null;
    public boolean nativeArray; // true if generated code should be array (java example: byte[] bytes)
    public String typeId; // typeId for scalarType == ScalarType.TypeId
    public Struct resolvedStruct;
    public Enum resolvedEnum;
    public MapDescriptor mapDescriptor; // if not null then this attribute will be rendered as Map instead of List

    @Override
    public String toString() {
        final String arr = arraySize < 0 ? "" : arraySize == 0 ? "[]" : String.format("[%d]", arraySize);
        final String type = (scalarType != ScalarType.TypeId) ? scalarType.toString() :
                (resolvedStruct != null ? resolvedStruct.name : resolvedEnum != null ? resolvedEnum.name : "<unknown>");

        return type + arr;
    }

    public boolean isPod() {
        return scalarType != ScalarType.TypeId || resolvedEnum != null;
    }

    public boolean isEnum() {
        return resolvedEnum != null;
    }

    public String getCsType(boolean frameworkType) {
        String ret = getCsScalarType(frameworkType);

        if (arraySize == 0) {
            if (isMap()) {
                ret = String.format("Dictionary<%s, %s>", mapDescriptor.keyAttribute.type.getCsScalarType(frameworkType), ret);
            } else if (isNativeArray()) {
                ret += "[]";
            } else {
                ret = String.format("List<%s>", ret);
            }
            if (frameworkType)
                ret = "System.Collections.Generic." + ret;
        } else if (arraySize > 0) {
            ret += "[]";
        }

        return ret;
    }

    public String getCsType() {
        return getCsType(false);
    }

    public String getCsFrameworkType() {
        return getCsType(true);
    }

    public String getCsEscapedFrameworkType() {
        return getCsType(true).replace("<", "*[").replace(">", "]*");
    }

    public String getJavaType() {
        try {
            String ret = getJavaScalarType();

            if (arraySize == 0) {
                if (isMap()) {
                    ret = String.format("java.util.Map<%s, %s>", mapDescriptor.keyAttribute.type.getJavaScalarType(false), ret);
                } else if (isNativeArray()) {
                    ret += "[]";
                } else {
                    ret = String.format("java.util.List<%s>", getJavaScalarType(false));
                }
            } else if (arraySize > 0) {
                ret += "[]";
            }

            return ret;
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    public final String getCppArrayType() {
    	if (isMap()) {
    		return String.format("std::unordered_map< %s, %s >", 
    				mapDescriptor.keyAttribute.type.getCppScalarType(), getCppScalarType());
    	} else if (isNativeArray()) {
            throw new UnsupportedOperationException("cpp native array not implemented yet");
        }
    	
    	
        return String.format("std::vector< %s >", getCppScalarType());
    }

    public final String getCppArrayAliasType() {
        String prefix = scalarType == ScalarType.TypeId
                ? CodeGenUtils.pascalCase(typeId)
                : scalarType.getArrayAliasTypePrefix();
        return prefix + (isMap() ? "Map" : "List");
    }

    public String getCppType() {
        String ret = getCppScalarType();

        if (arraySize == 0) {
            ret = getCppArrayAliasType();
        } else if (arraySize > 0) {
            //ret += "[]";
        }

        return ret;
    }

    public String getCppPostfix() {
        return (arraySize > 0) ? String.format("[%d]", arraySize) : "";
    }

    public String getJavaScalarType() {
        return getJavaScalarType(true);
    }

    public String getJavaScalarType(boolean primitive) {
        if (scalarType == ScalarType.TypeId) {
            return typeId;
        }

        return scalarType.getJavaType(primitive);
    }

    public String getJavaFullQualifiedScalarType() {
        if (resolvedStruct != null)
            return resolvedStruct.fullQualifiedJavaName();
        if (resolvedEnum != null)
            return resolvedEnum.fullQualifiedJavaName();
        return getJavaScalarType(false);
    }

    public String getCppScalarType() {
        final String scalarType;
        if (this.scalarType == ScalarType.TypeId) {
            if (resolvedEnum != null)
                scalarType = resolvedEnum.getCppFullQualifiedName();
            else
                scalarType = resolvedStruct.getCppFullQualifiedName();
        } else {
            scalarType = this.scalarType.getCppType();
        }
        final String scalarMod = resolvedStruct != null ? "*" : "";
        return scalarType + scalarMod;
    }

    public String getCsScalarType(boolean framworkType) {
        if (scalarType == ScalarType.TypeId) {
            if (framworkType) {
                Element resolved = resolvedStruct != null ? resolvedStruct : resolvedEnum;

                if (resolved == null) {
                    throw new IllegalArgumentException("Scalar type is not resolved, you can get framework type only " +
                            "after symbol resolution is done");
                }

                return resolved.getCsFullQualifiedName();

            } else {
                return typeId;
            }
        }

        return framworkType ? scalarType.getCsFrameworkType() : scalarType.getCsType();
    }

    public String getCsScalarType() {
        return getCsScalarType(false);
    }

    public String getCsCtor() {
        if (arraySize == 0) {
            if (isMap()) {
                boolean isKeyEnum = mapDescriptor.keyAttribute.type.resolvedEnum != null;
                String ctorParam = isKeyEnum ? ("new " + mapDescriptor.keyAttribute.type.resolvedEnum.getCsComparerName() + "()") : "";
                return String.format("new Dictionary<%s, %s>(%s)", mapDescriptor.keyAttribute.type.getCsScalarType(),
                                     getCsScalarType(), ctorParam);
            } else if (isNativeArray()) {
                return String.format("new %s[0]", getCsScalarType());
            } else {
                return String.format("new List<%s>()", getCsScalarType());
            }
        } else if (arraySize > 0) {
            return String.format("new %s[%d]", getCsScalarType(), arraySize);
        } else {
            return String.format("new %s()", getCsScalarType());
        }
    }

    public String getCsScalarCtor() {
        return String.format("new %s()", getCsScalarType());
    }

    public String getCppCtor() {
        if (arraySize == 0) {
            if (isMap()) {
                return String.format("new Dictionary<%s, %s>()", mapDescriptor.keyAttribute.type.getCppScalarType(), getCppScalarType());
            } else if (isNativeArray()) {
                throw new UnsupportedOperationException("cpp native array not implemented yet");
            } else {
                return String.format("new List<%s>()", getCppScalarType());
            }
        } else if (arraySize > 0) {
            return String.format("new %s[%d]", getCppScalarType(), arraySize);
        } else {
        	String scalarType = getCppScalarType().trim();
        	
        	while (scalarType.endsWith("*")) 
        		scalarType = scalarType.substring(0, scalarType.length() - 1).trim();
            return String.format("new %s()", scalarType);
        }
    }

    public String getCppScalarCtor() {
    	String scalarType = getCppScalarType().trim();
    	
    	while (scalarType.endsWith("*")) 
    		scalarType = scalarType.substring(0, scalarType.length() - 1).trim();
    	
        return String.format("new %s()", scalarType);
    }

    public String getJavaCtor() {
        if (arraySize == 0) {
            if (isMap()) {
                return String.format("new java.util.LinkedHashMap<%s, %s>()", mapDescriptor.keyAttribute.type.getJavaScalarType(false), getJavaScalarType());
            } else if (isNativeArray()) {
                return String.format("new %s[0]", getCsScalarType());
            } else {
                return String.format("new java.util.ArrayList<%s>()", getJavaScalarType(false));
            }
        } else if (arraySize > 0) {
            return String.format("new %s[%d]", getJavaScalarType(), arraySize);
        } else {
            return String.format("new %s()", getJavaScalarType());
        }
    }

    public String getJavaScalarCtor() {
        return String.format("new %s()", getJavaScalarType());
    }

    public boolean isDynamicArray() { return arraySize == 0; }
    public boolean isFixedArray() { return arraySize > 0; }
    public boolean isArray() { return arraySize >= 0; }
    public boolean isMap() { return mapDescriptor != null; }
    public boolean isNativeArray() { return nativeArray; }
    public boolean isNativeByteArray() {
        return isNativeArray() && scalarType == ScalarType.Byte;
    }

    public Attribute getMapKeyAttribute() {
        assert mapDescriptor != null;
        return mapDescriptor.keyAttribute;
    }

    public String getCsReadMethod() {
        String ret = scalarType.getCsReadMethod();
        if (ret == null) {
            assert resolvedEnum != null;
        }

        return ret;
    }

    public String getJavaReadMethod() {
        String ret = scalarType.getJavaReadMethod();
        if (ret == null) {
            assert resolvedEnum != null;
        }

        return ret;
    }

    public String getPyReadMethod() {
        String ret = scalarType.getPyReadMethod();
        if (ret == null) {
            assert resolvedEnum != null;
        }

        return ret;
    }

    public String getCsFixedArrayImplicitValue() {
        String scalarType = getCsScalarType();
        return getFixedArrayImplicitValue(scalarType);
    }

    public String getJavaFixedArrayImplicitValue() {
        String scalarType = getJavaScalarType();
        return getFixedArrayImplicitValue(scalarType);
    }

    private String getFixedArrayImplicitValue(String scalarTypeStr) {
        StringBuilder sb = new StringBuilder();
        sb.append("new ").append(scalarTypeStr).append("[]{");
        for (int i = 0; i < arraySize; i++) {
            if (i != 0)
                sb.append(", ");
            switch (scalarType) {
                case Int:
                case Short:
                case Byte:
                case Long:
                    sb.append("0");
                    break;

                case Float:
                    sb.append("0.0f");
                    break;

                case String:
                    sb.append("\"\"");
                    break;

                case TypeId:
                    if (resolvedEnum != null)
                         sb.append(resolvedEnum.getCsName()).append(".").append(resolvedEnum.constants.get(0).getCsName());
                    else
                        sb.append("null");
                    break;
            }
        }
        sb.append("}");

        return sb.toString();
    }

    public String getPyScalarInitValue() {
        StringBuilder sb = new StringBuilder();
        switch (scalarType) {
            case Long:
            case Int:
            case Short:
            case Byte:
                sb.append("0");
                break;

            case Float:
                sb.append("0.0");
                break;

            case String:
                sb.append("\"\"");
                break;

            case TypeId:
                if (resolvedEnum != null)
                    sb.append(resolvedEnum.getPyName()).append(".").append(resolvedEnum.constants.get(0).getPyName());
                else
                    sb.append(resolvedStruct.fullQualifiedName()).append("()");
                break;
        }

        return sb.toString();
    }

    public String getPyInitValue() {
        if (arraySize == 0)
            return "[]";

        final String scalarVal = getPyScalarInitValue();
        if (arraySize < 0)
            return scalarVal;

        StringBuilder sb = new StringBuilder();
        sb.append("[");
        for (int i = 0; i < arraySize; i++) {
            if (i > 0)
                sb.append(", ");
            sb.append(scalarVal);
        }
        sb.append("]");

        return sb.toString();
    }


    public String getPyScalarCtor() {
        return resolvedStruct.fullQualifiedName() + "()";
    }

    public String getDotType() {
        return getCsType().replace("<","&lt;").replace(">","&gt;");
    }

    public int getScalarSize() {
        switch (scalarType) {
            case Bool:
            case Byte:
                return 1;

            case Short:
                return 2;

            case Int:
            case Float:
                return 4;

            case Long:
                return 8;

            case String:
                return -1;

            case TypeId:
                return resolvedEnum != null ? 4 : -1;
        }

        return -1;
    }

}
