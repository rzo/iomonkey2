package net.bartipan.iomonkey2.model;

import net.bartipan.iomonkey2.Main;
import net.bartipan.iomonkey2.codegen.CodeGenUtils;
import net.bartipan.iomonkey2.codegen.NameCaseType;
import net.bartipan.iomonkey2.model.comparators.StructComparator;

import java.io.File;
import java.util.*;

/**
 * Created by honza on 9/12/14.
 */
public class Unit {
    public DotName name;
    public DocComment docComment = new DocComment();
    public List<Import> imports = new ArrayList<Import>();
    public List<Option> options = new ArrayList<Option>();
    public List<Struct> structs = new ArrayList<Struct>();
    public List<Enum> enums = new ArrayList<Enum>();

    public UnitLinkInfo linkInfo = new UnitLinkInfo();

    public boolean hasAnyImports() {
        return !imports.isEmpty();
    }

    public Option findOption(String optName) {
        for (Option opt : options) {
            if (opt.name.toString().equals(optName))
                return opt;
        }

        return null;
    }

    public String getOptionValue(String optName, String defaultValue) {
        Option opt = findOption(optName);
        return opt == null ? defaultValue : opt.value;
    }

    public String getPathPrefix(String lang) {
        return getOptionValue("packagePrefix." + lang,"").replace('.', File.separatorChar);
    }

    public String getPathSuffix(NameCaseType caseType) {
        String ret = "";
        String rawSuffix = Main.getInstance().getSnapshotSuffix();
        if (rawSuffix.isEmpty())
            return ret;

        DotName dotName = new DotName(rawSuffix);

        switch (caseType) {
            case PASCAL:
                ret = dotName.getPascalCase(File.separator);
                break;

            case CAMEL:
                ret = dotName.getCamelCase(File.separator);
                break;
        }

        return ret;
    }

    public Struct findStruct(String name) {
        for (Struct s : structs) {
            if (s.name.equals(name)) {
                return s;
            }
        }

        return null;
    }

    public Enum findEnum(String name) {
        for (Enum e : enums) {
            if (e.name.equals(name)) {
                return e;
            }
        }

        return null;
    }

    public String getCsName() {
        String prefix = "";
        String suffix = "";

        Option optPrefix = findOption("packagePrefix.cs");
        if (optPrefix != null) {
            prefix = optPrefix.value.trim();
            if (prefix.charAt(prefix.length() - 1) != '.')
                prefix += ".";
        }

        String snapshotSuffix = Main.getInstance().getSnapshotSuffix();
        if (!snapshotSuffix.isEmpty()) {
            suffix = "." + new DotName(snapshotSuffix).getPascalCase();
        }

        return prefix + name.getPascalCase() + suffix;
    }

    public String getPyName() {
        return name.getCamelCase(".");
    }

    public String getJavaName() {
        String prefix = "";
        String suffix = "";

        Option optPrefix = findOption("packagePrefix.java");
        if (optPrefix != null) {
            prefix = optPrefix.value.trim();
            if (prefix.charAt(prefix.length() - 1) != '.')
                prefix += ".";
        }

        String snapshotSuffix = Main.getInstance().getSnapshotSuffix();
        if (!snapshotSuffix.isEmpty()) {
            suffix = "." + new DotName(snapshotSuffix).getCamelCase(".");
        }

        return prefix + name.getCamelCase(".") + suffix;
    }

    public String getCppIncludePath() {
        return name.getCamelCase("/") + "/" + name + ".h";
    }

    public List<Struct> getNonAbstractExtendingStructs() {
        List<Struct> ret = new ArrayList<>();
        for (Struct s : structs) {
            if (!s.abstractStruct && s.resolvedSuperClass != null) {
                ret.add(s);
            }
        }
        return ret;
    }

    public List<Struct> getNonAbstractStructs() {
        List<Struct> ret = new ArrayList<>();
        for (Struct s : structs) {
            if (!s.abstractStruct) {
                ret.add(s);
            }
        }
        ret.sort(new StructComparator());
        return ret;
    }


    public List<Struct> getNonAbstractAndInheritanceNeededStructReferences() {
        List<Struct> ret = new ArrayList<>(getNonAbstractStructs());
        List<StructReference> refs = linkInfo.getInheritanceNeededStructReferences();
        for (StructReference sr : refs) {

            if (!sr.target.abstractStruct && !ret.contains(sr.target))
                ret.add(sr.target);

            List<Struct> allNonAbstractSubClasses = sr.target.getAllNonAbstractSubClasses();
            for (Struct s : allNonAbstractSubClasses) {
                if (!ret.contains(s)) {
                    ret.add(s);
                }
            }
        }
        ret.sort(new StructComparator());
        return ret;
    }


    private List<String> getCustomImports(String lang) {
        List<String> result = new ArrayList<>();
        String str = getOptionValue("customImports." + lang, null);
        if (str != null) {
            String[] tokens = str.split("\\s*,\\s*");
            for (String token : tokens) {
                if (!token.isEmpty()) {
                    result.add(token);
                }
            }
        }
        return result;
    }

    public List<String> getJavaCustomImports() {
        return getCustomImports("java");
    }

    public List<String> getCsCustomImports() {
        return getCustomImports("cs");
    }
}
