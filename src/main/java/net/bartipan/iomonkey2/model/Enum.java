package net.bartipan.iomonkey2.model;

import java.util.*;

/**
 * Created by honza on 9/12/14.
 */
public class Enum extends Element {
    public List<EnumConst> constants = new ArrayList<EnumConst>();

    public EnumLinkInfo linkInfo = new EnumLinkInfo();

    @Override
    public String toString() {
        return String.format("<enum %s>", name);
    }

    public String getCsComparerName() {
        return getCsName() + "Comparer";
    }

    public EnumConst findEnumConst(String name) {
        for (EnumConst ec : constants) {
            if (ec.name.equals(name))
                return ec;
        }

        return null;
    }

    public EnumConst getFirstConst() {
        return constants.get(0);
    }

}
