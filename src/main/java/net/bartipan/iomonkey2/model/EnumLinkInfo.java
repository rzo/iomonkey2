package net.bartipan.iomonkey2.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jbartipan on 30.08.16.
 */
public class EnumLinkInfo {
    public List<EnumReference> references = new ArrayList<>();

    public boolean hasAnyReferences() {
        return !references.isEmpty();
    }
}
