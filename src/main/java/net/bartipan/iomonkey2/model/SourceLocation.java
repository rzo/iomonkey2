package net.bartipan.iomonkey2.model;

import org.antlr.runtime.tree.CommonTree;

/**
 * Created by jbartipan on 30.10.14.
 */
public class SourceLocation {
    public String fileName;
    public int lineNumber;
    public int columnNumber;

    public SourceLocation(String fileName, int lineNumber, int columnNumber) {
        this.fileName = fileName;
        this.lineNumber = lineNumber;
        this.columnNumber = columnNumber;
    }

    public SourceLocation(String fileName, CommonTree node) {
        this.fileName = fileName;
        this.lineNumber = node.getLine();
        this.columnNumber = node.getCharPositionInLine();
    }

    @Override
    public String toString() {
        return String.format("%s(%d,%d)", fileName, lineNumber, columnNumber);
    }
}
