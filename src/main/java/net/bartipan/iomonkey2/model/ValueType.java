package net.bartipan.iomonkey2.model;

/**
 * Created by honza on 9/16/14.
 */
public enum ValueType {
    Bool, Int, Float, String, EnumConst, EnumConstRef, Null
}
