package net.bartipan.iomonkey2.model;

import net.bartipan.iomonkey2.Config;
import sun.util.resources.cldr.naq.CalendarData_naq_NA;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Created by jbartipan on 11.08.16.
 */
public class GlobalSettings {
    private static GlobalSettings _instance;
    private int _maxArraySize;

    public static GlobalSettings getInstance() {
        if (_instance == null)
            _instance = new GlobalSettings();
        return _instance;
    }

    private GlobalSettings() {
        _maxArraySize = Config.MAX_ARRAY_SIZE;
    }

    public void loadFromDir(File baseDir) throws FileNotFoundException {
        try {
            InputStream is = new FileInputStream(new File(baseDir, "global.properties"));
            Properties props = new Properties();
            props.load(is);
            _maxArraySize = Integer.parseInt(props.getProperty("maxArraySize", Integer.toString(Config.MAX_ARRAY_SIZE)));
        } catch (java.io.IOException e) {
            _maxArraySize = Config.MAX_ARRAY_SIZE;
        }
    }

    public int getMaxArraySize() {
        return _maxArraySize;
    }
}
