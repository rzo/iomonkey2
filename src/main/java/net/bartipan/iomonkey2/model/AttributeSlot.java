package net.bartipan.iomonkey2.model;

/**
 * Created by honza on 9/16/14.
 */
public class AttributeSlot {
    public boolean required = true;
    public int slot = -1; // -1 -> autocount

    public AttributeSlot() {

    }

    public AttributeSlot(boolean required, int slot) {
        this.required = required;
        this.slot = slot;
    }

}
