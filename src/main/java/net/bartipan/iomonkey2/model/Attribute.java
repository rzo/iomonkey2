package net.bartipan.iomonkey2.model;

import net.bartipan.iomonkey2.Config;
import net.bartipan.iomonkey2.codegen.CodeGenUtils;

import java.util.*;

import static net.bartipan.iomonkey2.model.AnnotationUtils.findAnnotation;

/**
 * Created by honza on 9/12/14.
 */
public class Attribute {
    public AttributeType type;
    public String name;
    public AttributeSlot slot = new AttributeSlot();
    public Value implicitValue;
    public List<Annotation> annotations = new ArrayList<Annotation>();
    public DocComment docComment = new DocComment();
    public Map<String, List<GeneratedAnnotation>> generatedLanguageAnnotations = new HashMap<>();
    public Struct struct;

    public SourceLocation sourceLocation;

    @Override
    public String toString() {
        return String.format("<attr %s: %s%s>", name, type.toString(),
                implicitValue == null ? "" : ("=" + implicitValue.value));
    }

    public final int getSlotIndex() {
        return slot.slot + (slot.required ? struct.getRequiredSlotOffset() : struct.getOptionalSlotOffset());
    }

    public final boolean isRequired() {
        return slot.required;
    }

    public final boolean isOptional() {
        return !slot.required;
    }

    public final boolean isNull() {

        return  // null set explicitly as implicit value
                (implicitValue != null && implicitValue.type == ValueType.Null)
                // or optional struct with no explicitly stated implicit value
                || (isOptional() && type.arraySize == 0 && type.resolvedStruct != null)
                // or abstract structs
                || (type.resolvedStruct != null && type.resolvedStruct.abstractStruct && type.arraySize == -1);
    }

    public final String getCsName() {
        return CodeGenUtils.pascalCase(name);
    }

    public final String getPyName() { return CodeGenUtils.camelCase(name); }

    public final String getCppName() { return CodeGenUtils.camelCase(name); }

    public final String getJavaName() { return CodeGenUtils.camelCase(name); }

    public final String getPyInitValue() {
        return implicitValue == null ? type.getPyInitValue() : implicitValue.getPyValue();
    }

    public final Value getFixedValue() {
        Annotation fixedAnnotation = findAnnotation(annotations, "Fixed");
        if (fixedAnnotation == null)
            return null;
        Value ret = fixedAnnotation.parameters.get(0);

        // if attr type is not enum but value is enum const, resolve the value to integer
        if (ret.type == ValueType.EnumConst && type.resolvedEnum != null) {
            ret = new Value(ValueType.Int, Integer.toString(ret.resolvedEnumConst.value));
        }

        return ret;
    }

    public final String getFixedValueJavaComparison() {
        Value v = getFixedValue();
        if (v.type == ValueType.String)
            return String.format("!%s.equals(%s)", getJavaName(), v.getJavaValue());
        else
            return String.format("%s != %s", getJavaName(), v.getJavaValue());
    }

    public final int getMaxArraySize() {
        Annotation annot = AnnotationUtils.findAnnotation(annotations, "MaxArraySize");
        return annot == null
                ? GlobalSettings.getInstance().getMaxArraySize()
                : Integer.parseInt(annot.parameters.get(0).value);
    }

    public List<GeneratedAnnotation> getJavaGeneratedAnnotations() {
        return getGeneratedAnnotations("java");
    }

    public List<GeneratedAnnotation> getCsGeneratedAnnotations() {
        return getGeneratedAnnotations("cs");
    }

    private List<GeneratedAnnotation> getGeneratedAnnotations(String lang) {
        List<GeneratedAnnotation> generatedAnnotations = generatedLanguageAnnotations.get(lang);
        if (generatedAnnotations != null) {
            return generatedAnnotations;
        } else {
            return Collections.emptyList();
        }
    }
}
