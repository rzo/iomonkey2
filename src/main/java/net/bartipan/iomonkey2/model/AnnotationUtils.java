package net.bartipan.iomonkey2.model;

import net.bartipan.iomonkey2.model.Annotation;

import java.util.List;

/**
 * Created by jbartipan on 10.03.15.
 */
public class AnnotationUtils {
    public static Annotation findAnnotation(List<Annotation> annotations, String name) {
        for (Annotation a : annotations) {
            if (a.name.equals(name)) {
                return a;
            }
        }

        return null;
    }
}
