package net.bartipan.iomonkey2.model;

/**
 * Created by jbartipan on 30.08.16.
 */
public final class DocComment {
    public String comment;

    public DocComment() {
        comment = null;
    }

    public DocComment(String comment) {
        this.comment = comment;
    }


    @Override
    public String toString() {
        return comment;
    }

    private String buildCommentString(String firstLine, String linePrefix, String lastLine) {
        if (comment == null || comment.isEmpty())
            return null;

        StringBuilder sb = new StringBuilder();
        String[] lines = comment.split("\n");

        sb.append(firstLine).append("\n");
        for (String line : lines) {
            sb.append(linePrefix).append(line).append("\n");
        }

        sb.append(lastLine)/*.append("\n")*/;
        return sb.toString();
    }

    public String getCs() {
        return buildCommentString(
                "/// <summary>",
                "/// ",
                "/// </summary>"
        );
    }

    public String getJava() {
        return buildCommentString(
                "/**",
                " * ",
                " */"
        );
    }

    public String getCpp() {
        return buildCommentString(
                "/**",
                " * ",
                " */"
        );
    }

    public String getHtml() {
        return comment;
    }

    public String getPlainText() {
        return comment;
    }

    public String getJsonText() {
        String plainText = getPlainText();
        return plainText
                .replace("\n","\\n")
                .replace("\"","\\\"");
    }
}
