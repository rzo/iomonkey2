package net.bartipan.iomonkey2.model;

/**
 * Created by honza on 9/16/14.
 */
public class Value {
    public ValueType type;
    public String value;
    public Element resolvedEnum;
    public EnumConst resolvedEnumConst;

    public Value(ValueType type, String value) {
        this.type = type;
        this.value = value;
    }

    @Override
    public String toString() {
        return String.format("<ImplValue %s: %s>", value, type.toString());
    }

    public String getCsValue() {
        if (type == ValueType.Null)
            return "null";

        if (type == ValueType.EnumConst)
            return String.format("%s.%s", resolvedEnum.getCsName(), resolvedEnumConst.getCsName());

        if (type == ValueType.EnumConstRef)
            return Integer.toString(resolvedEnumConst.value);

        if (type == ValueType.Float && value.indexOf('.') >= 0)
            return value + "f";

        return value;
    }

    public String getCppValue() {
        if (type == ValueType.Null)
            return "nullptr";

        if (type == ValueType.EnumConst)
            return String.format("%s::%s", resolvedEnum.getCppFullQualifiedName(), resolvedEnumConst.getCppName());

        if (type == ValueType.EnumConstRef)
            return Integer.toString(resolvedEnumConst.value);

        if (type == ValueType.String)
            return String.format("std::string(%s)", value);

        if (type == ValueType.Float && value.indexOf('.') >= 0)
            return value + "f";

        return value;
    }

    public String getJavaValue() {
        if (type == ValueType.Null)
            return "null";

        if (type == ValueType.EnumConst)
            return String.format("%s.%s", resolvedEnum.getJavaName(), resolvedEnumConst.getJavaName());

        if (type == ValueType.EnumConstRef)
            return Integer.toString(resolvedEnumConst.value);

        if (type == ValueType.Float && value.indexOf('.') >= 0)
            return value + "f";

        return value;
    }

    public String getPyValue() {
        if (type == ValueType.Null)
            return "None";

        if (type == ValueType.EnumConstRef)
            return Integer.toString(resolvedEnumConst.value);

        return type == ValueType.EnumConst
                ? String.format("%s.%s", resolvedEnum.getPyName(), resolvedEnumConst.getPyName()) : value;
    }

    public String getStringContent() {
        if (type != ValueType.String) {
            throw new UnsupportedOperationException("This can be used only on value type == String.");
        }
        return value.substring(1, value.length() - 1);
    }

    public String getHtmlValue() {
        if (type == ValueType.Null)
            return "null";

        if (type == ValueType.EnumConst)
            return String.format("%s.%s", resolvedEnum.name, resolvedEnumConst.name);

        if (type == ValueType.EnumConstRef)
            return String.format("%d (= %s.%s)", resolvedEnumConst.value, resolvedEnum.fullQualifiedName(),
                    resolvedEnumConst.name);

        if (type == ValueType.String)
            return value.replace("\"", "&quot;");

        return value;
    }
}
