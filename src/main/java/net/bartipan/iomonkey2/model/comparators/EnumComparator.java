package net.bartipan.iomonkey2.model.comparators;

import net.bartipan.iomonkey2.model.Enum;

import java.util.Comparator;

/**
 * Created by jbartipan on 22.02.16.
 */
public class EnumComparator implements Comparator<Enum> {
    @Override
    public int compare(Enum o1, Enum o2) {
        if (o1 == null)
            return o2 == null ? 0 : 1;
        return o1.name.compareTo(o2.name);
    }
}
