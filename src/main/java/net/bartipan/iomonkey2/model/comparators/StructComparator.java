package net.bartipan.iomonkey2.model.comparators;

import net.bartipan.iomonkey2.model.Struct;

import java.util.Comparator;

/**
 * Created by jbartipan on 22.02.16.
 */
public class StructComparator implements Comparator<Struct> {
    @Override
    public int compare(Struct o1, Struct o2) {
        if (o1 == null)
            return o2 == null ? 0 : 1;

        return Long.compare(o1.linkInfo.structId.getLong(), o2.linkInfo.structId.getLong());
    }
}

