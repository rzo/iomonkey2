package net.bartipan.iomonkey2.model.comparators;

import net.bartipan.iomonkey2.model.StructReference;

import java.util.Comparator;

/**
 * Created by jbartipan on 22.02.16.
 */
public class StructReferenceComparator implements Comparator<StructReference> {
    @Override
    public int compare(StructReference o1, StructReference o2){
        return (int) (o2.target.linkInfo.structId.getLong() - o1.target.linkInfo.structId.getLong());
    }
}
