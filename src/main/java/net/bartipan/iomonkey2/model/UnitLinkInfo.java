package net.bartipan.iomonkey2.model;

import net.bartipan.iomonkey2.model.comparators.EnumComparator;
import net.bartipan.iomonkey2.model.comparators.StructComparator;
import net.bartipan.iomonkey2.model.comparators.StructReferenceComparator;

import java.util.*;

/**
 * Created by honza on 9/28/14.
 */
public class UnitLinkInfo {
    public List<StructReference> structReferences = new ArrayList<StructReference>();
    public List<EnumReference> enumReferences = new ArrayList<EnumReference>();

    public List<StructReference> getExtendedStructReferences() {
        List<StructReference> ret = new ArrayList<StructReference>();;
        Set<String> addedStructNames = new HashSet<String>();
        for (StructReference sr : structReferences) {
            final String sn = sr.target.fullQualifiedName();
            if (sr.target.isExtended() && !addedStructNames.contains(sn)) {
                ret.add(sr);
                addedStructNames.add(sn);
            }
        }

        ret.sort(new StructReferenceComparator());

        return ret;
    }

    public List<StructReference> getInheritanceNeededStructReferences() {
        List<StructReference> ret = new ArrayList<StructReference>();;
        Set<String> addedStructNames = new HashSet<String>();
        for (StructReference sr : structReferences) {
            final String sn = sr.target.fullQualifiedName();
            if (sr.target.getNeedsSupportForInheritance() && !addedStructNames.contains(sn)) {
                ret.add(sr);
                addedStructNames.add(sn);
            }
        }

        ret.sort(new StructReferenceComparator());

        return ret;
    }

    public List<Struct> getReferencedStructs() {
        HashSet<Struct> structs = new HashSet<Struct>();
        for (StructReference sr : structReferences) {
            if (!structs.contains(sr.target))
                structs.add(sr.target);
        }

        ArrayList<Struct> ret = new ArrayList<Struct>(structs);
        ret.sort(new StructComparator());
        return ret;
    }

    public List<Enum> getReferencedEnums() {
        HashSet<Enum> enums = new HashSet<Enum>();
        for (EnumReference er : enumReferences) {
            if (!enums.contains(er.target))
                enums.add(er.target);
        }

        ArrayList<Enum> ret = new ArrayList<Enum>(enums);

        ret.sort(new EnumComparator());

        return ret;
    }

}
