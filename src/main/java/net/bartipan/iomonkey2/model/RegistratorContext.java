package net.bartipan.iomonkey2.model;

import net.bartipan.iomonkey2.Config;
import net.bartipan.iomonkey2.parser.AstParser;
import org.antlr.runtime.RecognitionException;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * Created by jbartipan on 06.01.17.
 */
public class RegistratorContext {
    private File _baseDir;
    private List<Unit> _units = new ArrayList<>();
    private AstParser _parser;

    public RegistratorContext(File baseDir) {
        _baseDir = baseDir;
        _parser = new AstParser();
    }

    public void load() throws IOException, RecognitionException {
        loadAllUnits(_baseDir);
    }

    private void loadAllUnits(File dir) throws IOException, RecognitionException {
        File[] files = dir.listFiles();
        for (File file : files) {
            if (file.isDirectory())
                loadAllUnits(file);
            else if (file.getName().endsWith(Config.EXTENSION))
                loadUnit(file);
        }
    }

    private void loadUnit(File unitFile) throws IOException, RecognitionException {
        _units.add(_parser.parseUnit(unitFile));
    }

    public List<Unit> getUnits() {
        return _units;
    }

    public List<Unit> getSortedUnits() {
        List<Unit> units = new ArrayList<Unit>(_units);
        units.sort(new Comparator<Unit>() {
            @Override
            public int compare(Unit o1, Unit o2) {
                return o1.name.toString().compareTo(o2.name.toString());
            }
        });
        return units;
    }
}
