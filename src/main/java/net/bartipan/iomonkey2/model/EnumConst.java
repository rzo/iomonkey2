package net.bartipan.iomonkey2.model;

import net.bartipan.iomonkey2.Main;
import net.bartipan.iomonkey2.codegen.CodeGenUtils;
import net.bartipan.iomonkey2.codegen.NameCaseType;

import java.util.Properties;

/**
 * Created by honza on 9/12/14.
 */
public class EnumConst {
    public String name;
    public int value;
    public DocComment docComment = new DocComment();

    public SourceLocation sourceLocation;

    public EnumConst(){

    }

    public EnumConst(String name, int value) {
        this.name = name;
        this.value = value;
    }

    @Override
    public String toString() {
        return String.format("%s=%d", name, value);
    }

    public String getCsName() { return getName(name, "cs"); }

    public String getPyName() { return getName(name, "python"); }

    public String getJavaName() { return getName(name, "java"); }

    public String getCppName() { return getName(name, "cpp"); }


    private String getName(String name, String lang) {
        NameCaseType nameCaseType = getCaseTypeOverride(lang);
        if (nameCaseType != null) {
            return CodeGenUtils.generateName(name, nameCaseType);
        } else {
            return CodeGenUtils.generateName(name, getDefaultNameCaseType(lang));
        }
    }

    private NameCaseType getDefaultNameCaseType(String lang) {
        switch (lang) {
            case "cs":
                return NameCaseType.PASCAL;
            case "python":
                return NameCaseType.UPPER_UNDERSCORE;
            case "java":
                return NameCaseType.UPPER_UNDERSCORE;
            case "cpp":
                return NameCaseType.UPPER_UNDERSCORE;
            default:
                throw new UnsupportedOperationException("Unsupported language " + lang);
        }
    }


    private NameCaseType getCaseTypeOverride(String lang) {
        Properties properties = Main.getInstance().getProperties();
        String key = lang + ".override.enumConst.nameCaseType";
        if (properties.containsKey(key)) {
            return NameCaseType.valueOf(properties.getProperty(key));
        } else {
            return null;
        }
    }
}
