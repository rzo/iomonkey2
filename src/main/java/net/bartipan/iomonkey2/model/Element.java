package net.bartipan.iomonkey2.model;

import java.util.ArrayList;
import java.util.List;

import net.bartipan.iomonkey2.codegen.CodeGenUtils;

public class Element {

	public Unit unit;
	public String name;
	public List<Annotation> annotations = new ArrayList<Annotation>();
	public DocComment docComment = new DocComment();
	public SourceLocation sourceLocation;

	public Element() {
		super();
	}

	public final String getFullQualifiedName() { return fullQualifiedName(); }

	public final String getCppIncludePath() {
	    return unit.name.getCamelCase("/") + "/" + getCppName() + ".h";
	}

	public final String getCppName() {
	    return CodeGenUtils.pascalCase(name);
	}

	public final String getCppFullQualifiedName() {
		return "::" + String.join("::", unit.name.components) + "::" + getCppName();
	}

	public final String fullQualifiedName() {
	    return unit.name.toString() + "." + name;
	}

	public final String getCsName() {
	    return CodeGenUtils.pascalCase(name);
	}

	public final String getPyName() {
	    return CodeGenUtils.pascalCase(name);
	}

	public final String getJavaName() {
	    return CodeGenUtils.pascalCase(name);
	}

	public final String getCamelName() {
        return CodeGenUtils.camelCase(name);
    }

	public final String fullQualifiedCsName() { return String.format("%s.%s", unit.getCsName(), getCsName()); }

	public final String fullQualifiedPyName() { return String.format("%s.%s", unit.getPyName(), getPyName()); }

	public final String fullQualifiedJavaName() { return String.format("%s.%s", unit.getJavaName(), getJavaName()); }

	public final String getJavaFullQualifiedName() { return fullQualifiedJavaName(); }
    public final String getCsFullQualifiedName()   { return fullQualifiedCsName(); }
    public final String getPyFullQualifiedName()   { return fullQualifiedPyName(); }
	public final Annotation getNamedAnnotation(String annotationName) {
        for (Annotation annotation : annotations) {
            if (annotation.name.equals(annotationName)) {
                return annotation;
            }
        }

        return null;
    }

}