package net.bartipan.iomonkey2.model;

import java.util.List;

/**
 * Created by jbartipan on 03.12.14.
 */
public class Annotation {
    public String name;
    public List<Value> parameters;

    public Annotation(String name, List<Value> parameters) {
        this.name = name;
        this.parameters = parameters;
    }
}
