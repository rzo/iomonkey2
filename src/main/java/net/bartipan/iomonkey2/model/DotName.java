package net.bartipan.iomonkey2.model;

import net.bartipan.iomonkey2.codegen.CodeGenUtils;

import java.io.File;

/**
 * Created by honza on 9/16/14.
 */
public class DotName {
    public String[] components;

    public DotName(String raw) {
        components = raw.split("\\.");
    }

    public DotName(String[] components) {
        this.components = components;
    }

    @Override
    public String toString() {
        return toString(".");
    }

    public String toString(String delim) {
        return String.join(delim, components);
    }

    public File asFile(File parent) {
        return new File(parent, toString(File.separator));
    }

    public File asFile(File parent, String ext) {
        return new File(parent, toString(File.separator) + ext);
    }

    public File asPascalCaseFile(File parent, int skipComponents) {
        return new File(parent, getPascalCase(File.separator, skipComponents));
    }

    public File asPascalCaseFile(File parent, String ext) {
        return new File(parent, getPascalCase(File.separator) + ext);
    }

    public String getPascalCase(String delim) {
        return getPascalCase(delim, 0);
    }

    public String getPascalCase(String delim, int startIdx) {
        String[] c = new String[components.length - startIdx];
        for (int i = startIdx; i < components.length; i++) {
            c[i - startIdx] = CodeGenUtils.pascalCase(components[i]);
        }
        return String.join(delim, c);
    }

    public String getPascalCase() {
        return getPascalCase(".");
    }

    public File asCamelCaseFile(File parent, int skipComponents) {
        return new File(parent, getCamelCase(File.separator, skipComponents));
    }

    public File asCamelCaseFile(File parent, String ext) {
        return new File(parent, getCamelCase(File.separator) + ext);
    }

    public String getCamelCase(String delim) {
        return getCamelCase(delim, 0);
    }

    public String getCamelCase(String delim, int startIdx) {
        String[] c = new String[components.length - startIdx];
        for (int i = startIdx; i < components.length; i++) {
            c[i - startIdx] = CodeGenUtils.camelCase(components[i]);
        }
        return String.join(delim, c);
    }

    public String getCamelCase() {
        return getCamelCase(".");
    }


    public String getLastComponent() { return components[components.length - 1]; }

    public String getCppNamespaceStart() {
        return "namespace " + getCamelCase(" { namespace ") + " {";
    }

    public String getCppNamespaceEnd() {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < components.length; i++) {
            sb.append('}');
        }
        return sb.toString();
    }

    public String getCppIncludePath() {
        return getCamelCase("/") + ".h";
    }

    public String getAsIdentifier(){
        return toString("_");
    }
}
