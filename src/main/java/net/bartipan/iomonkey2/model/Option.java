package net.bartipan.iomonkey2.model;

/**
 * Created by honza on 9/12/14.
 */
public class Option {
    public DotName name;
    public ValueType type;
    public String value;

    @Override
    public String toString() {
        return String.format("<Option %s=%s (%s)", name, value, type);
    }
}
