package net.bartipan.iomonkey2.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by stetinapet on 13.3.17.
 */
public class GeneratedAnnotation {
    public String name;
    public List<Value> arguments = new ArrayList<>();

    public String getJavaDefinition() {
        StringBuilder result = new StringBuilder("@" + name);
        if (arguments.size() > 0) {
            result.append("(");
            for (int i = 0; i < arguments.size(); i++) {
                Value value = arguments.get(i);
                if (value.type != ValueType.String) {
                    throw new UnsupportedOperationException("currently only string value is supported for generated annotation argument");
                }
                if (i > 0) {
                    result.append(", ");
                }
                result.append(value.getJavaValue());
            }
            result.append(")");
        }
        return result.toString();
    }
}
