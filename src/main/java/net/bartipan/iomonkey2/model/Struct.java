package net.bartipan.iomonkey2.model;

import net.bartipan.iomonkey2.model.comparators.StructComparator;

import java.util.*;

/**
 * Created by honza on 9/12/14.
 */
public class Struct extends Element {
    public boolean abstractStruct = false;
    public boolean finalStruct = false;
    public String superClassName;
    public List<Attribute> attributes = new ArrayList<Attribute>();
    public Struct resolvedSuperClass;
    public List<Struct> resolvedSubClasses = new ArrayList<Struct>();

    public StructLinkInfo linkInfo = new StructLinkInfo();

    @Override
    public String toString() {
        return String.format("<struct '%s'>", name.toString());
    }

    public final boolean isExtending() {
        return superClassName != null && !superClassName.isEmpty();
    }

    public final boolean isExtended() { return !resolvedSubClasses.isEmpty(); }

    public final boolean getNeedsSupportForInheritance() { return isExtended() || isExtending(); }

    public final List<Attribute> getDynamicArrayAttributes() {
        List<Attribute> ret = new ArrayList<Attribute>();
        for (Attribute attr : attributes) {
            if (attr.type.isDynamicArray())
                ret.add(attr);
        }

        return ret;
    }

    public final boolean isPod() {
        return (!isExtending()) && resolvedSubClasses.isEmpty();
    }

    public final boolean isRootClass() {
        return (!isExtending()) && !resolvedSubClasses.isEmpty();
    }

    public final int getRequiredSlotOffset() {
        return linkInfo == null ? 0 : linkInfo.requiredOffset;
    }

    public final int getOptionalSlotOffset() {
        return linkInfo == null ? 0 : linkInfo.optionalOffset;
    }

    public final int countRequiredAttributes() {
        int ret = 0;
        int maxSlot = 0;
        for (Attribute attr : attributes) {
            if (attr.isRequired()) {
                if (attr.slot != null && maxSlot < attr.slot.slot)
                    maxSlot = attr.slot.slot;
                ret++;
            }
        }

        return maxSlot + 1 > ret ? maxSlot + 1 : ret;
    }

    public final int countOptionalAttributes() {
        int ret = 0;
        int maxSlot = 0;
        for (Attribute attr : attributes) {
            if (attr.isOptional()) {
                if (attr.slot != null && maxSlot < attr.slot.slot)
                    maxSlot = attr.slot.slot;
                ret++;
            }
        }

        return maxSlot + 1 > ret ? maxSlot + 1: ret;
    }

    public final int nextFreeSlot(boolean required) {
        int idx = 0;
        boolean collide = true;

        while (collide) {
            collide = false;

            for (Attribute attr : attributes) {
                if (attr.slot == null || attr.slot.required != required)
                    continue;;

                int attrIdx = attr.slot == null ? -1 : attr.slot.slot;

                if (idx == attrIdx) {
                    collide = true;
                    idx++;
                    break;
                }
            }
        }

        return idx;
    }

    public final Attribute findAttribute(String name) {
        for (Attribute attr : attributes) {
            if (attr.name.equals(name)) {
                return attr;
            }
        }

        if (resolvedSuperClass != null)
            return resolvedSuperClass.findAttribute(name);

        return null;
    }

    public final List<Attribute> getAllAttributes() {
        ArrayList<Attribute> ret = new ArrayList<>(attributes);

        Struct s = resolvedSuperClass;
        while (s != null) {
            ret.addAll(s.attributes);

            s = s.resolvedSuperClass;
        }

        return ret;
    }

    public List<Struct> getAllSubClasses() {
        HashSet<Struct> uniqueSubClasses = new HashSet<Struct>();
        uniqueSubClasses.add(this);
        for (Struct sc : resolvedSubClasses) {
            uniqueSubClasses.addAll(sc.getAllSubClasses());
        }

        List<Struct> ret = new ArrayList<Struct>();
        ret.addAll(uniqueSubClasses);
        return ret;
    }

    public List<Struct> getAllNonAbstractSubClasses() {
        HashSet<Struct> uniqueSubClasses = new HashSet<Struct>();
        if (this.abstractStruct == false) {
            uniqueSubClasses.add(this);
        }
        for (Struct sc : resolvedSubClasses) {
            uniqueSubClasses.addAll(sc.getAllNonAbstractSubClasses());
        }

        List<Struct> ret = new ArrayList<Struct>();
        ret.addAll(uniqueSubClasses);

        ret.sort(new StructComparator());

        return ret;
    }

    public Element getUberSuperClass() {
        if (resolvedSuperClass == null)
            return null;

        Struct uber = resolvedSuperClass;
        while (uber.resolvedSuperClass != null)
            uber = uber.resolvedSuperClass;

        return uber;
    }

    public String getUberSuperClassName() {
        Element uber = getUberSuperClass();
        return uber == null ? null : uber.name;
    }
    
    public List<String> getCppReferencedElementsIncludePaths() {
    	List<String> ret = new ArrayList<String>();
    	
    	if (resolvedSuperClass != null) {
    		ret.add(resolvedSuperClass.getCppIncludePath());
    	}
    	
    	boolean includeIomReader = false;
    	
    	for (Attribute attr : attributes) {
    		if (attr.type.resolvedStruct != null) {
    			ret.add(attr.type.resolvedStruct.getCppIncludePath());
    		} else if (attr.type.resolvedEnum != null) {
    			ret.add(attr.type.resolvedEnum.getCppIncludePath());
    		}
    	}
    	
    	return ret;
    }

    public boolean getNeedsIomReaderUtils() {
        for (Attribute attr : attributes) {
            if (attr.type.resolvedStruct != null) {
                if (attr.type.resolvedStruct.getNeedsSupportForInheritance())
                    return true;

            }
        }
        return false;
    }
    
    public List<Attribute> getScalarStructRefAttributes() {
    	List<Attribute> ret = new ArrayList<>();
    	for (Attribute attr : attributes) {
    		if (attr.type.resolvedStruct != null && !attr.type.isArray())
    			ret.add(attr);
    	}
    	
    	return ret;
    }
    
    public boolean getHasScalarStructRefAttributes() {
    	return !getScalarStructRefAttributes().isEmpty();
    }

    public List<Attribute> getScalarNonAbstractStructRefAttributes() {
    	List<Attribute> ret = new ArrayList<>();
    	for (Attribute attr : attributes) {
    		if (attr.type.resolvedStruct != null && !attr.type.resolvedStruct.abstractStruct)
    			ret.add(attr);
    	}
    	
    	return ret;
    }

    public List<UmlAssociation> getUmlAssociations() {
        HashMap<Element, List<Attribute>> classMap = new HashMap<>();

        for (Attribute attr : attributes) {
            Element dest = attr.type.resolvedStruct;
            if (dest == null) dest = attr.type.resolvedEnum;
            if (dest != null)
            {
                //String destName = dest.fullQualifiedName();

                if (classMap.containsKey(dest))
                    classMap.get(dest).add(attr);
                else {
                    List<Attribute> attrs = new ArrayList<>();
                    attrs.add(attr);
                    classMap.put(dest, attrs);
                }
            }
        }

        ArrayList<UmlAssociation> ret = new ArrayList<>();
        for (Element e : classMap.keySet()) {
            List<Attribute> attrs = classMap.get(e);
            attrs.sort(new Comparator<Attribute>() {
                @Override
                public int compare(Attribute o1, Attribute o2) {
                    return o1.name.compareTo(o2.name);
                }
            });
            ret.add(new UmlAssociation(this, e, attrs));
        }

        ret.sort(new Comparator<UmlAssociation>() {
            @Override
            public int compare(UmlAssociation o1, UmlAssociation o2) {
                return o1.destination.fullQualifiedName().compareTo(o2.destination.fullQualifiedName());
            }
        });

        return ret;
    }

    public boolean isValueTypeRequested() {
        return getNamedAnnotation("ValueType") != null;
    }

    public boolean hasStructFixedArrayAttribute(boolean required) {
        List<Attribute> attrs = required ? linkInfo.requiredAttributes : linkInfo.optionalAttributes;
        for (Attribute a : attrs) {
            if (a.type.isFixedArray() && a.type.resolvedStruct != null)
                return true;
        }

        return false;
    }

    public boolean getHasRequiredStructFixedArrayAttribute() {
        return hasStructFixedArrayAttribute(true);
    }

    public boolean getHasOptionalStructFixedArrayAttribute() {
        return hasStructFixedArrayAttribute(false);
    }

    public List<Struct> getSuperClassChain() {
        List<Struct> ret = new ArrayList<>();

        Struct s = resolvedSuperClass;
        while (s != null) {
            ret.add(s);
            s = s.resolvedSuperClass;
        }

        return ret;
    }

    public List<Struct> getSuperClassChainIncludingThis() {
        List<Struct> ret = getSuperClassChain();
        ret.add(this);
        return ret;
    }

    public List<Struct> getNonAbstractSuperClassChainIncludingThis() {
        List<Struct> ret = getSuperClassChainIncludingThis();
        for (int i = ret.size() - 1; i >= 0; i--) {
            if (ret.get(i).abstractStruct)
                ret.remove(i);
        }
        return ret;
    }

    public boolean getHasOptionals() {
        Struct s = this;
        while (s != null) {
            if (s.linkInfo.getHasOptionals())
                return true;
            s = s.resolvedSuperClass;
        }
        return false;
    }

}
