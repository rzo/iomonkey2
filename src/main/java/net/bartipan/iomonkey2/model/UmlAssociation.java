package net.bartipan.iomonkey2.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jbartipan on 24.02.16.
 */
public class UmlAssociation {
    public Struct source;
    public Element destination;
    public List<Attribute> attributes = new ArrayList<>();

    public UmlAssociation() {
    }

    public UmlAssociation(Struct source, Element dest, List<Attribute> attributes) {
        this.source = source;
        this.destination = dest;
        this.attributes = attributes;
    }

}
