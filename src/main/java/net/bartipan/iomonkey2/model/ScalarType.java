package net.bartipan.iomonkey2.model;

public enum ScalarType {
    Bool, Byte, Short, Int, Long, Float, String, TypeId;

    public boolean isSimple() {
        return this != TypeId;
    }

    public String getCsType() {
        final String[] vals = {"bool", "byte", "short", "int", "long", "float", "string"};
        return vals[ordinal()];
    }

    public String getCsFrameworkType() {
        final String[] vals = {"System.Boolean", "System.Byte", "System.Int16", "System.Int32", "System.Int64",
                "System.Single", "System.String"};
        return vals[ordinal()];
    }

    public String getJavaType() {
        return getJavaType(true);
    }

    public String getJavaType(boolean primitive) {
        if (primitive) {
            final String[] vals = {"boolean", "byte", "short", "int", "long", "float", "String"};
            return vals[ordinal()];
        } else {
            final String[] vals = {"Boolean", "Byte", "Short", "Integer", "Long", "Float", "String"};
            return vals[ordinal()];
        }
    }

    public String getCppType() {
        final String[] vals = {"bool", "char", "short", "int", "long long", "float", "std::string"};
        return vals[ordinal()];
    }

    public String getArrayAliasTypePrefix() {
        final String[] vals = {"Bool", "Byte", "Short", "Int", "Long", "Float", "String"};
        return vals[ordinal()];
    }

    public String getCsReadMethod() {
        final String[] vals = {"ReadBoolean" ,"ReadByte", "ReadInt16", "ReadInt32", "ReadInt64", "ReadSingle", "ReadString"};
        return this == TypeId ? null : vals[ordinal()];
    }

    public String getJavaReadMethod() {
        final String[] vals = {"readBoolean", "readByte", "readShort", "readInt", "readLong", "readFloat", "readUTF"};
        return this == TypeId ? null : vals[ordinal()];
    }

    public String getJavaWriteMethod() {
        final String[] vals = {"writeBoolean","writeByte", "writeShort", "writeInt", "writeLong", "writeFloat", "writeUTF"};
        return this == TypeId ? null : vals[ordinal()];
    }

    public String getPyReadMethod() {
        final String[] vals = {"readBool","readByte", "readShort", "readInt", "writeLong", "readFloat", "readString"};
        return this == TypeId ? null : vals[ordinal()];
    }

    public boolean isString() {
        return this == String;
    }
}
