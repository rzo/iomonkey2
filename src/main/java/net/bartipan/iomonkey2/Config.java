package net.bartipan.iomonkey2;

import java.security.MessageDigest;

/**
 * Created by honza on 9/26/14.
 */
public class Config {
    public static final int STRUCT_ID_LEN = 8;
    public static final String EXTENSION = ".iom2";
    public static FormatVersion FORMAT_VERSION = FormatVersion.V0;
    public static int MAX_ARRAY_SIZE = 16384;
}
