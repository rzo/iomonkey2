package net.bartipan.iomonkey2.linker;

import net.bartipan.iomonkey2.model.*;
import net.bartipan.iomonkey2.model.Enum;

/**
 * Created by honza on 9/30/14.
 */
public class InvalidArraySizeValueException extends LinkerException {
    public InvalidArraySizeValueException(Attribute attr, Struct s, String reason) {
        super(String.format("Invalid value of array size of attribute %s in struct %s: %s",
                attr.name, s.name, reason));
    }
}
