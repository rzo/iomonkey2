package net.bartipan.iomonkey2.linker;

import net.bartipan.iomonkey2.model.*;
import net.bartipan.iomonkey2.model.Enum;
import net.bartipan.iomonkey2.parser.InvalidMapKeyException;
import net.bartipan.iomonkey2.parser.SourceException;

import java.util.*;

/**
 * Created by honza on 9/18/14.
 */
public class Linker {
    private Context _context;

    public Linker(Context ctx) {
        _context = ctx;
    }

    /*
    0. Resolve struct units
    1. Resolve super struct references.
    2. Resolve enum references.
    3. Compute slot numbers based on hierarchy.
     */

    private static final Struct findStruct(Unit u, String name) {
        for (Struct s : u.structs) {
            if (s.name.equals(name)) {
                return s;
            }
        }

        return null;
    }

    private void resolveElementUnits() {
        for (Unit u : _context.units.values()) {
            for (Element s : u.structs) {
                s.unit = u;
            }

            for (Element e : u.enums) {
                e.unit = u;
            }
        }
    }

    private Struct findStruct(String name, Unit localUnit) {
        if (localUnit != null) {
            for (Struct s : localUnit.structs) {
                if (s.name.equals(name)) {
                    return s;
                }
            }
        }

        for (Unit u : _context.units.values()) {
            if (u == localUnit)
                continue;

            for (Struct s : u.structs) {
                if (s.name.equals(name)) {
                    return s;
                }
            }
        }

        return null;
    }

    private Enum findEnum(String name, Unit localUnit) {
        if (localUnit != null) {
            for (Enum e : localUnit.enums) {
                if (e.name.equals(name)) {
                    return e;
                }
            }
        }

        for (Unit u : _context.units.values()) {
            if (localUnit == u)
                continue;

            for (Enum e : u.enums) {
                if (e.name.equals(name)) {
                    return e;
                }
            }
        }

        return null;
    }

    private void resolveStructSupers() {
        for (Unit u : _context.units.values()) {
            for (Struct s : u.structs) {
                if (s.superClassName != null && !s.superClassName.isEmpty()) {
                    Struct spr = findStruct(u, s.superClassName);
                    if (spr == null) {
                        spr = findStruct(s.superClassName, u);
                    }

                    if (spr == null) {
                        throw new UnresolvedSymbolException(s.superClassName, "struct " + s.name);
                    }

                    s.resolvedSuperClass = spr;
                    spr.resolvedSubClasses.add(s);
                }
            }
        }
    }

    private void resolveAttributeTypeArraySizes() {
        for (Unit u : _context.units.values()) {
            for (Struct s :u.structs) {
                for (Attribute a : s.attributes) {
                    if (a.type.arraySize == -2) {
                        String enumName = a.type.arraySizeRef[0];
                        Enum e = findEnum(enumName, u);

                        if (e == null) {
                            throw new InvalidArraySizeValueException(a, s, String.format("No such enum named '%s' found",
                                    enumName));
                        }

                        String enumAttr = a.type.arraySizeRef[1];
                        if (enumAttr.equals("count")) {
                            a.type.arraySize = e.constants.size();
                        } else {
                            EnumConst ec = e.findEnumConst(enumAttr);

                            if (ec == null) {
                                throw new InvalidArraySizeValueException(a, s, String.format(
                                        "No such attribute named '%s' in enum '%s' found", enumAttr, enumName));
                            }

                            a.type.arraySize = ec.value;
                        }
                        e.constants.size();
                    }
                }
            }
        }
    }
    private void resolveAttributeTypeIds() {
        for (Unit u : _context.units.values()) {
            for (Struct s :u.structs) {
                for (Attribute a : s.attributes) {
                    if (a.type.typeId != null && !a.type.typeId.isEmpty()) {
                        Struct rs = findStruct(a.type.typeId, u);

                        if (rs != null) {
                            a.type.resolvedStruct = rs;
                            StructReference ref = new StructReference(a, rs);
                            u.linkInfo.structReferences.add(ref);
                            rs.linkInfo.references.add(ref);
                        } else {
                            Enum re = findEnum(a.type.typeId, u);

                            if (re == null) {
                                throw new UnresolvedSymbolException(a.type.typeId,
                                        String.format("attribute %s in struct %s", a.name, s.name));
                            }

                            a.type.resolvedEnum = re;
                            EnumReference ref = new EnumReference(a, re);
                            u.linkInfo.enumReferences.add(ref);
                            re.linkInfo.references.add(ref);
                        }
                    }
                }
            }
        }
    }

    private void gatherRootStructs() {
        for (Unit u : _context.units.values()) {
            for (Struct s : u.structs) {
                if (s.isRootClass()) {
                    _context.rootStructs.add(s);
                    updateLinkInfoOffset(s, 0, 0);
                }
            }
        }
    }

    private void updateLinkInfoOffset(Struct s, int reqOff, int optOff) {
        s.linkInfo.requiredOffset = reqOff;
        s.linkInfo.optionalOffset = optOff;

        reqOff += s.countRequiredAttributes();
        optOff += s.countOptionalAttributes();

        for (Struct si : s.resolvedSubClasses) {
            updateLinkInfoOffset(si, reqOff, optOff);
        }
    }

    /*
    private void updateStructId(Struct s) {
        s.linkInfo.structId = StructId.fromStruct(s);

        for (Struct si : s.resolvedSubClasses) {
            updateStructId(si);
        }
    }
    */

    private void updateStructIds() {
        for (Unit u : _context.units.values()) {
            for (Struct s : u.structs) {
                s.linkInfo.structId = StructId.fromStruct(s);
            }
        }
    }

    private void updateStructSlots(Struct s) {
        Map<Integer, Attribute> reqAttrs = new HashMap<Integer, Attribute>();
        Map<Integer, Attribute> optAttrs = new HashMap<Integer, Attribute>();

        for (Attribute attr : s.attributes) {
            if (attr.slot == null) {
                attr.slot = new AttributeSlot();
            }

            if (attr.slot.slot < 0) {
                attr.slot.slot = s.nextFreeSlot(attr.isRequired());
            }

            (attr.slot.required ? reqAttrs : optAttrs).put(attr.slot.slot, attr);
        }

        SortedSet<Integer> reqIndices = new TreeSet<Integer>(reqAttrs.keySet());
        SortedSet<Integer> optIndices = new TreeSet<Integer>(optAttrs.keySet());

        for (int i : reqIndices) {
            s.linkInfo.requiredAttributes.add(reqAttrs.get(i));
        }

        for (int i : optIndices) {
            s.linkInfo.optionalAttributes.add(optAttrs.get(i));
        }
    }

    private void updateSlots() {
        for (Unit u : _context.units.values()) {
            for (Struct s : u.structs) {
                updateStructSlots(s);
            }
        }
    }

    private void resolveImplicitValues() {
        for (Unit u : _context.units.values()) {
            for (Struct s : u.structs) {
                for (Attribute a : s.attributes) {
                    if (a.implicitValue == null) {
                        a.implicitValue = a.getFixedValue();
                    }

                    if (a.isOptional() && (!a.type.isArray()) && a.implicitValue == null) {
                        switch (a.type.scalarType) {
                            case Byte:
                            case Int:
                            case Short:
                            case Long:
                                a.implicitValue = new Value(ValueType.Int, "0");
                                break;

                            case Float:
                                a.implicitValue = new Value(ValueType.Float, "0.0");
                                break;

                            case String:
                                a.implicitValue = new Value(ValueType.String, "\"\"");
                                break;

                            case TypeId:
                                if (a.type.resolvedEnum != null) {
                                    a.implicitValue = new Value(ValueType.EnumConst,
                                            a.type.resolvedEnum.constants.get(0).name);
                                }
                                break;

                            case Bool:
                                a.implicitValue = new Value(ValueType.Bool, "false");
                                break;

                            default:
                                assert(false);
                        }
                    } else if ((!a.type.isArray()) && a.type.scalarType == ScalarType.String && a.implicitValue == null) {
                        a.implicitValue = new Value(ValueType.String, "\"\"");
                    }

                    if (a.implicitValue == null && !a.type.isArray() && a.type.resolvedEnum != null) {
                        a.implicitValue = new Value(ValueType.EnumConst, a.type.resolvedEnum.constants.get(0).name);
                    }


                    if (a.implicitValue != null && a.implicitValue.type == ValueType.EnumConst) {
                        /*
                        a.implicitValue.resolvedEnum = a.type.resolvedEnum;
                        EnumConst ec = a.type.resolvedEnum.findEnumConst(a.implicitValue.value);

                        if (ec == null) {
                            throw new InvalidEnumConstImplicitValueException(a, a.type.resolvedEnum, a.implicitValue.value);
                        }

                        a.implicitValue.resolvedEnumConst = ec;
                        */
                        resolveValue(a.implicitValue, a, u);
                    }
                }
            }
        }
    }

    private void resolveAnnotationValues() {
        for (Unit u : _context.units.values()) {
            for (Struct s : u.structs) {
                resolveAnnotationsValues(s.annotations, u);
                for (Attribute a : s.attributes) {
                    resolveAnnotationsValues(a.annotations, u);
                }
            }
        }
    }

    private void resolveAnnotationsValues(List<Annotation> annots, Unit localUnit) {
        for (Annotation a : annots) {
            for (Value v : a.parameters) {
                resolveValue(v, null, localUnit);
            }
        }
    }

    private void resolveValue(Value val, Attribute attr, Unit localUnit) {
        switch (val.type) {
            case EnumConst: {
                val.resolvedEnum = attr.type.resolvedEnum;
                EnumConst ec = attr.type.resolvedEnum.findEnumConst(attr.implicitValue.value);
                if (ec == null) {
                    throw new InvalidEnumConstImplicitValueException(attr, attr.type.resolvedEnum, val.value);
                }
                val.resolvedEnumConst = ec;

                break;
            }

            case EnumConstRef: {
                String[] name = val.value.split("\\.");
                Enum e = findEnum(name[0], localUnit);
                if (e == null)
                    throw new InvalidEnumConstRefValueException(name[0], name[1]);
                EnumConst ec = e.findEnumConst(name[1]);
                if (ec == null)
                    throw new InvalidEnumConstRefValueException(name[0], name[1]);

                val.resolvedEnum = e;
                val.resolvedEnumConst = ec;
                break;
            }

        }
    }

    public void link() {
        resolveElementUnits();
        resolveStructSupers();
        updateSlots();
        resolveAttributeTypeIds();
        resolveAttributeTypeArraySizes();
        resolveImplicitValues();
        resolveAnnotationValues();
        gatherRootStructs();
        updateStructIds();
        validateNullImplicitValues();
        resolveAttributeAnnotations();
    }

    private void validateNullImplicitValues() {
        for (Unit u : _context.units.values()) {
            for (Struct s : u.structs) {
                // check if attributes with null implicit value are structs
                for (Attribute attr : s.attributes) {
                    if (attr.implicitValue != null && attr.implicitValue.type == ValueType.Null) {
                        if (attr.type.resolvedStruct == null) {
                            String msg = String.format("Null implicit value can be assigned only " +
                                    "to attributes of struct type, but attribute '%s' in structure '%s' " +
                                    "is of %s data-type", attr.name, s.fullQualifiedName(), attr.type.toString());

                            if (attr.type.resolvedEnum != null) {
                                msg += " (enum type)";
                            }

                            throw new SourceException(attr.sourceLocation, msg);
                        }
                    }
                }
            }
        }
    }

    private void resolveAttributeAnnotations() {
        for (Unit unit : _context.units.values()) {
            for (Struct struct : unit.structs) {
                for (Attribute attribute : struct.attributes) {
                    for (Annotation annotation : attribute.annotations) {
                        try {
                            resolveAttributeAnnotation(annotation, attribute);
                        } catch (Exception e) {
                            throw new RuntimeException("Failed to resolve annotation '" + annotation.name +
                                                               "' on attribute " + struct.name + "." + attribute.name +
                                                               ": " + e.getMessage(), e);
                        }
                    }
                }
            }
        }
    }

    private void resolveAttributeAnnotation(Annotation annotation, Attribute attribute) {
        if (annotation.name.equals("MapKeyName")) {
            if (!attribute.type.isDynamicArray()) {
                throw new InvalidMapKeyException("Only dynamic array can be annotated with MapKeyName");
            }
            if (attribute.type.resolvedStruct == null) {
                throw new InvalidMapKeyException("Only struct can be mapped by key");
            }
            Value value = annotation.parameters.get(0);
            Attribute keyAttribute = attribute.type.resolvedStruct.findAttribute(value.getStringContent());
            if (keyAttribute == null) {
                throw new InvalidMapKeyException("There is no attribute '" + value.value + "' in struct "
                                                         + attribute.type.resolvedStruct.name);
            }
            MapDescriptor mapDescriptor = new MapDescriptor();
            mapDescriptor.annotation = annotation;
            mapDescriptor.keyAttribute = keyAttribute;
            attribute.type.mapDescriptor = mapDescriptor;
        } else if (annotation.name.equals("Array")) {
            if (!attribute.type.isDynamicArray()) {
                throw new InvalidMapKeyException("Only dynamic array can be annotated with Array");
            }
            attribute.type.nativeArray = true;
        } else if (annotation.name.equals("JavaAnnot")) {
            resolveGeneratedAnnotation("java", annotation, attribute);
        } else if (annotation.name.equals("CSharpAnnot")) {
            resolveGeneratedAnnotation("cs", annotation, attribute);
        }
    }

    private void resolveGeneratedAnnotation(String language, Annotation annotation, Attribute attribute) {
        List<GeneratedAnnotation> generatedAnnotations = attribute.generatedLanguageAnnotations.get(language);
        if (generatedAnnotations == null) {
            generatedAnnotations = new ArrayList<>();
            attribute.generatedLanguageAnnotations.put(language, generatedAnnotations);
        }
        if (annotation.parameters.size() < 1) {
            throw new RuntimeException("At least one parameter with generated annotation's name is required.");
        }
        GeneratedAnnotation generatedAnnotation = new GeneratedAnnotation();
        generatedAnnotation.name = annotation.parameters.get(0).getStringContent();
        for (int i = 1; i < annotation.parameters.size(); i++) {
            generatedAnnotation.arguments.add(annotation.parameters.get(i));
        }
        generatedAnnotations.add(generatedAnnotation);
    }

}
