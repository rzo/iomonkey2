package net.bartipan.iomonkey2.linker;

/**
 * Created by honza on 9/18/14.
 */
public class LinkerException extends RuntimeException {
    public LinkerException() {
        super();
    }

    public LinkerException(String message) {
        super(message);
    }

    public LinkerException(String message, Throwable cause) {
        super(message, cause);
    }

    public LinkerException(Throwable cause) {
        super(cause);
    }

    protected LinkerException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
