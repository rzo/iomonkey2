package net.bartipan.iomonkey2.linker;

/**
 * Created by jbartipan on 12.10.16.
 */
public class InvalidEnumConstRefValueException extends LinkerException {
    private String _enumName;
    private String _enumConstName;

    public InvalidEnumConstRefValueException(String enumName, String constName) {
        super(String.format("Cannot find enum named '%s' or constant name '%s' within this enum", enumName, constName));
        _enumName = enumName;
        _enumConstName = constName;
    }

    public String getEnumName() {
        return _enumName;
    }

    public String getEnumConstName() {
        return _enumConstName;
    }
}
