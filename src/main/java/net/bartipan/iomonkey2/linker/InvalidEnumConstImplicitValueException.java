package net.bartipan.iomonkey2.linker;

import net.bartipan.iomonkey2.model.*;
import net.bartipan.iomonkey2.model.Enum;

/**
 * Created by honza on 9/30/14.
 */
public class InvalidEnumConstImplicitValueException extends LinkerException {
    private static String formatEnumConsts(Enum e) {
        StringBuilder sb = new StringBuilder();
        for (EnumConst ec : e.constants) {
            sb.append("- ").append(ec.name).append('\n');
        }
        return sb.toString();
    }
    public InvalidEnumConstImplicitValueException(Attribute attr, net.bartipan.iomonkey2.model.Enum e, String value) {
        super(String.format("Invalid Enum-Const implicit value '%s' of attribute %s.%s\n" +
                "Possible constants of enum %s are:\n%s\n",
                value, attr.struct.fullQualifiedName(), attr.name, e.fullQualifiedName(), formatEnumConsts(e)));
    }
}
