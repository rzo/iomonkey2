package net.bartipan.iomonkey2.linker;

/**
 * Created by honza on 9/18/14.
 */
public class UnresolvedSymbolException extends LinkerException {
    public String unresolvedSymbol;
    public String location;

    public UnresolvedSymbolException(String symbol, String location) {
        this.unresolvedSymbol = symbol;
        this.location = location;
    }

    @Override
    public String getMessage() {
        return String.format("Unresolved symbol '%s' in %s", unresolvedSymbol, location);
    }
}
