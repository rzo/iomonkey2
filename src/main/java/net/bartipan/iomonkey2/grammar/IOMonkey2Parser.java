// $ANTLR 3.5.1 /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g 2016-11-10 18:48:02

package net.bartipan.iomonkey2.grammar;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

import org.antlr.runtime.tree.*;


@SuppressWarnings("all")
public class IOMonkey2Parser extends Parser {
	public static final String[] tokenNames = new String[] {
		"<invalid>", "<EOR>", "<DOWN>", "<UP>", "ABSTRACT_STRUCT", "ANNOTATION", 
		"ANNOTATIONS", "BOOL_", "COMMENT", "DOCUMENT", "DOC_COMMENT", "DOC_COMMENT_SLASH", 
		"DOC_COMMENT_STAR", "DOTNAME", "ENUM", "ENUM_CONST", "ESC_SEQ", "EXPONENT", 
		"FLOAT", "HEX_DIGIT", "HEX_NUM", "ID", "IMPLICIT_VALUE", "IMPORT", "NULL", 
		"NUM", "OCTAL_ESC", "OPTION", "OPTIONS", "PACKAGE", "SIZE_INT", "SIZE_SYMBOL_REF", 
		"SLOT_OPTIONAL", "SLOT_REQUIRED", "SPLITTER_COMMENT", "STRING", "STRUCT", 
		"STRUCT_ATTR", "STRUCT_ATTR_SLOT", "STRUCT_SUPER", "TYPE_BOOL", "TYPE_BYTE", 
		"TYPE_DYNAMIC_ARRAY", "TYPE_FIXED_ARRAY", "TYPE_FLOAT", "TYPE_ID", "TYPE_INT", 
		"TYPE_LONG", "TYPE_SHORT", "TYPE_STRING", "UNICODE_ESC", "VALUE_BOOL", 
		"VALUE_ENUM_CONST", "VALUE_ENUM_CONST_REF", "VALUE_FLOAT", "VALUE_INT", 
		"VALUE_NULL", "VALUE_STRING", "WS", "'('", "')'", "'+'", "','", "'-'", 
		"'.'", "':'", "';'", "'='", "'@'", "'['", "']'", "'abstract'", "'bool'", 
		"'byte'", "'enum'", "'float'", "'import'", "'int'", "'long'", "'optional'", 
		"'options'", "'package'", "'required'", "'short'", "'string'", "'struct'", 
		"'{'", "'}'"
	};
	public static final int EOF=-1;
	public static final int T__59=59;
	public static final int T__60=60;
	public static final int T__61=61;
	public static final int T__62=62;
	public static final int T__63=63;
	public static final int T__64=64;
	public static final int T__65=65;
	public static final int T__66=66;
	public static final int T__67=67;
	public static final int T__68=68;
	public static final int T__69=69;
	public static final int T__70=70;
	public static final int T__71=71;
	public static final int T__72=72;
	public static final int T__73=73;
	public static final int T__74=74;
	public static final int T__75=75;
	public static final int T__76=76;
	public static final int T__77=77;
	public static final int T__78=78;
	public static final int T__79=79;
	public static final int T__80=80;
	public static final int T__81=81;
	public static final int T__82=82;
	public static final int T__83=83;
	public static final int T__84=84;
	public static final int T__85=85;
	public static final int T__86=86;
	public static final int T__87=87;
	public static final int ABSTRACT_STRUCT=4;
	public static final int ANNOTATION=5;
	public static final int ANNOTATIONS=6;
	public static final int BOOL_=7;
	public static final int COMMENT=8;
	public static final int DOCUMENT=9;
	public static final int DOC_COMMENT=10;
	public static final int DOC_COMMENT_SLASH=11;
	public static final int DOC_COMMENT_STAR=12;
	public static final int DOTNAME=13;
	public static final int ENUM=14;
	public static final int ENUM_CONST=15;
	public static final int ESC_SEQ=16;
	public static final int EXPONENT=17;
	public static final int FLOAT=18;
	public static final int HEX_DIGIT=19;
	public static final int HEX_NUM=20;
	public static final int ID=21;
	public static final int IMPLICIT_VALUE=22;
	public static final int IMPORT=23;
	public static final int NULL=24;
	public static final int NUM=25;
	public static final int OCTAL_ESC=26;
	public static final int OPTION=27;
	public static final int OPTIONS=28;
	public static final int PACKAGE=29;
	public static final int SIZE_INT=30;
	public static final int SIZE_SYMBOL_REF=31;
	public static final int SLOT_OPTIONAL=32;
	public static final int SLOT_REQUIRED=33;
	public static final int SPLITTER_COMMENT=34;
	public static final int STRING=35;
	public static final int STRUCT=36;
	public static final int STRUCT_ATTR=37;
	public static final int STRUCT_ATTR_SLOT=38;
	public static final int STRUCT_SUPER=39;
	public static final int TYPE_BOOL=40;
	public static final int TYPE_BYTE=41;
	public static final int TYPE_DYNAMIC_ARRAY=42;
	public static final int TYPE_FIXED_ARRAY=43;
	public static final int TYPE_FLOAT=44;
	public static final int TYPE_ID=45;
	public static final int TYPE_INT=46;
	public static final int TYPE_LONG=47;
	public static final int TYPE_SHORT=48;
	public static final int TYPE_STRING=49;
	public static final int UNICODE_ESC=50;
	public static final int VALUE_BOOL=51;
	public static final int VALUE_ENUM_CONST=52;
	public static final int VALUE_ENUM_CONST_REF=53;
	public static final int VALUE_FLOAT=54;
	public static final int VALUE_INT=55;
	public static final int VALUE_NULL=56;
	public static final int VALUE_STRING=57;
	public static final int WS=58;

	// delegates
	public Parser[] getDelegates() {
		return new Parser[] {};
	}

	// delegators


	public IOMonkey2Parser(TokenStream input) {
		this(input, new RecognizerSharedState());
	}
	public IOMonkey2Parser(TokenStream input, RecognizerSharedState state) {
		super(input, state);
	}

	protected TreeAdaptor adaptor = new CommonTreeAdaptor();

	public void setTreeAdaptor(TreeAdaptor adaptor) {
		this.adaptor = adaptor;
	}
	public TreeAdaptor getTreeAdaptor() {
		return adaptor;
	}
	@Override public String[] getTokenNames() { return IOMonkey2Parser.tokenNames; }
	@Override public String getGrammarFileName() { return "/Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g"; }


	public static class document_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "document"
	// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:76:1: document : packageDecl ( headerOp )* elementDefs -> ^( DOCUMENT packageDecl ( headerOp )* elementDefs ) ;
	public final IOMonkey2Parser.document_return document() throws RecognitionException {
		IOMonkey2Parser.document_return retval = new IOMonkey2Parser.document_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		ParserRuleReturnScope packageDecl1 =null;
		ParserRuleReturnScope headerOp2 =null;
		ParserRuleReturnScope elementDefs3 =null;

		RewriteRuleSubtreeStream stream_packageDecl=new RewriteRuleSubtreeStream(adaptor,"rule packageDecl");
		RewriteRuleSubtreeStream stream_headerOp=new RewriteRuleSubtreeStream(adaptor,"rule headerOp");
		RewriteRuleSubtreeStream stream_elementDefs=new RewriteRuleSubtreeStream(adaptor,"rule elementDefs");

		try {
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:77:2: ( packageDecl ( headerOp )* elementDefs -> ^( DOCUMENT packageDecl ( headerOp )* elementDefs ) )
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:77:4: packageDecl ( headerOp )* elementDefs
			{
			pushFollow(FOLLOW_packageDecl_in_document229);
			packageDecl1=packageDecl();
			state._fsp--;

			stream_packageDecl.add(packageDecl1.getTree());
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:77:16: ( headerOp )*
			loop1:
			while (true) {
				int alt1=2;
				int LA1_0 = input.LA(1);
				if ( (LA1_0==76||LA1_0==80) ) {
					alt1=1;
				}

				switch (alt1) {
				case 1 :
					// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:77:16: headerOp
					{
					pushFollow(FOLLOW_headerOp_in_document231);
					headerOp2=headerOp();
					state._fsp--;

					stream_headerOp.add(headerOp2.getTree());
					}
					break;

				default :
					break loop1;
				}
			}

			pushFollow(FOLLOW_elementDefs_in_document234);
			elementDefs3=elementDefs();
			state._fsp--;

			stream_elementDefs.add(elementDefs3.getTree());
			// AST REWRITE
			// elements: headerOp, packageDecl, elementDefs
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (Object)adaptor.nil();
			// 77:38: -> ^( DOCUMENT packageDecl ( headerOp )* elementDefs )
			{
				// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:77:41: ^( DOCUMENT packageDecl ( headerOp )* elementDefs )
				{
				Object root_1 = (Object)adaptor.nil();
				root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(DOCUMENT, "DOCUMENT"), root_1);
				adaptor.addChild(root_1, stream_packageDecl.nextTree());
				// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:77:64: ( headerOp )*
				while ( stream_headerOp.hasNext() ) {
					adaptor.addChild(root_1, stream_headerOp.nextTree());
				}
				stream_headerOp.reset();

				adaptor.addChild(root_1, stream_elementDefs.nextTree());
				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;

			}

			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "document"


	public static class packageDecl_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "packageDecl"
	// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:80:1: packageDecl : ( docComment )? 'package' dotName ';' -> ^( PACKAGE dotName ( docComment )? ) ;
	public final IOMonkey2Parser.packageDecl_return packageDecl() throws RecognitionException {
		IOMonkey2Parser.packageDecl_return retval = new IOMonkey2Parser.packageDecl_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token string_literal5=null;
		Token char_literal7=null;
		ParserRuleReturnScope docComment4 =null;
		ParserRuleReturnScope dotName6 =null;

		Object string_literal5_tree=null;
		Object char_literal7_tree=null;
		RewriteRuleTokenStream stream_66=new RewriteRuleTokenStream(adaptor,"token 66");
		RewriteRuleTokenStream stream_81=new RewriteRuleTokenStream(adaptor,"token 81");
		RewriteRuleSubtreeStream stream_dotName=new RewriteRuleSubtreeStream(adaptor,"rule dotName");
		RewriteRuleSubtreeStream stream_docComment=new RewriteRuleSubtreeStream(adaptor,"rule docComment");

		try {
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:81:2: ( ( docComment )? 'package' dotName ';' -> ^( PACKAGE dotName ( docComment )? ) )
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:81:4: ( docComment )? 'package' dotName ';'
			{
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:81:4: ( docComment )?
			int alt2=2;
			int LA2_0 = input.LA(1);
			if ( ((LA2_0 >= DOC_COMMENT_SLASH && LA2_0 <= DOC_COMMENT_STAR)) ) {
				alt2=1;
			}
			switch (alt2) {
				case 1 :
					// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:81:4: docComment
					{
					pushFollow(FOLLOW_docComment_in_packageDecl260);
					docComment4=docComment();
					state._fsp--;

					stream_docComment.add(docComment4.getTree());
					}
					break;

			}

			string_literal5=(Token)match(input,81,FOLLOW_81_in_packageDecl263);  
			stream_81.add(string_literal5);

			pushFollow(FOLLOW_dotName_in_packageDecl265);
			dotName6=dotName();
			state._fsp--;

			stream_dotName.add(dotName6.getTree());
			char_literal7=(Token)match(input,66,FOLLOW_66_in_packageDecl267);  
			stream_66.add(char_literal7);

			// AST REWRITE
			// elements: docComment, dotName
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (Object)adaptor.nil();
			// 81:38: -> ^( PACKAGE dotName ( docComment )? )
			{
				// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:81:41: ^( PACKAGE dotName ( docComment )? )
				{
				Object root_1 = (Object)adaptor.nil();
				root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(PACKAGE, "PACKAGE"), root_1);
				adaptor.addChild(root_1, stream_dotName.nextTree());
				// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:81:59: ( docComment )?
				if ( stream_docComment.hasNext() ) {
					adaptor.addChild(root_1, stream_docComment.nextTree());
				}
				stream_docComment.reset();

				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;

			}

			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "packageDecl"


	public static class dotName_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "dotName"
	// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:85:1: dotName : ID ( '.' ID )* -> ^( DOTNAME ID ( ID )* ) ;
	public final IOMonkey2Parser.dotName_return dotName() throws RecognitionException {
		IOMonkey2Parser.dotName_return retval = new IOMonkey2Parser.dotName_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token ID8=null;
		Token char_literal9=null;
		Token ID10=null;

		Object ID8_tree=null;
		Object char_literal9_tree=null;
		Object ID10_tree=null;
		RewriteRuleTokenStream stream_ID=new RewriteRuleTokenStream(adaptor,"token ID");
		RewriteRuleTokenStream stream_64=new RewriteRuleTokenStream(adaptor,"token 64");

		try {
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:86:2: ( ID ( '.' ID )* -> ^( DOTNAME ID ( ID )* ) )
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:86:4: ID ( '.' ID )*
			{
			ID8=(Token)match(input,ID,FOLLOW_ID_in_dotName291);  
			stream_ID.add(ID8);

			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:86:7: ( '.' ID )*
			loop3:
			while (true) {
				int alt3=2;
				int LA3_0 = input.LA(1);
				if ( (LA3_0==64) ) {
					alt3=1;
				}

				switch (alt3) {
				case 1 :
					// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:86:9: '.' ID
					{
					char_literal9=(Token)match(input,64,FOLLOW_64_in_dotName295);  
					stream_64.add(char_literal9);

					ID10=(Token)match(input,ID,FOLLOW_ID_in_dotName297);  
					stream_ID.add(ID10);

					}
					break;

				default :
					break loop3;
				}
			}

			// AST REWRITE
			// elements: ID, ID
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (Object)adaptor.nil();
			// 86:19: -> ^( DOTNAME ID ( ID )* )
			{
				// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:86:22: ^( DOTNAME ID ( ID )* )
				{
				Object root_1 = (Object)adaptor.nil();
				root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(DOTNAME, "DOTNAME"), root_1);
				adaptor.addChild(root_1, stream_ID.nextNode());
				// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:86:35: ( ID )*
				while ( stream_ID.hasNext() ) {
					adaptor.addChild(root_1, stream_ID.nextNode());
				}
				stream_ID.reset();

				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;

			}

			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "dotName"


	public static class headerOp_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "headerOp"
	// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:89:1: headerOp : ( importStat | optionsStat );
	public final IOMonkey2Parser.headerOp_return headerOp() throws RecognitionException {
		IOMonkey2Parser.headerOp_return retval = new IOMonkey2Parser.headerOp_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		ParserRuleReturnScope importStat11 =null;
		ParserRuleReturnScope optionsStat12 =null;


		try {
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:90:2: ( importStat | optionsStat )
			int alt4=2;
			int LA4_0 = input.LA(1);
			if ( (LA4_0==76) ) {
				alt4=1;
			}
			else if ( (LA4_0==80) ) {
				alt4=2;
			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 4, 0, input);
				throw nvae;
			}

			switch (alt4) {
				case 1 :
					// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:90:4: importStat
					{
					root_0 = (Object)adaptor.nil();


					pushFollow(FOLLOW_importStat_in_headerOp322);
					importStat11=importStat();
					state._fsp--;

					adaptor.addChild(root_0, importStat11.getTree());

					}
					break;
				case 2 :
					// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:91:4: optionsStat
					{
					root_0 = (Object)adaptor.nil();


					pushFollow(FOLLOW_optionsStat_in_headerOp327);
					optionsStat12=optionsStat();
					state._fsp--;

					adaptor.addChild(root_0, optionsStat12.getTree());

					}
					break;

			}
			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "headerOp"


	public static class importStat_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "importStat"
	// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:94:1: importStat : 'import' dotName ';' -> ^( IMPORT dotName ) ;
	public final IOMonkey2Parser.importStat_return importStat() throws RecognitionException {
		IOMonkey2Parser.importStat_return retval = new IOMonkey2Parser.importStat_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token string_literal13=null;
		Token char_literal15=null;
		ParserRuleReturnScope dotName14 =null;

		Object string_literal13_tree=null;
		Object char_literal15_tree=null;
		RewriteRuleTokenStream stream_66=new RewriteRuleTokenStream(adaptor,"token 66");
		RewriteRuleTokenStream stream_76=new RewriteRuleTokenStream(adaptor,"token 76");
		RewriteRuleSubtreeStream stream_dotName=new RewriteRuleSubtreeStream(adaptor,"rule dotName");

		try {
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:95:2: ( 'import' dotName ';' -> ^( IMPORT dotName ) )
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:95:4: 'import' dotName ';'
			{
			string_literal13=(Token)match(input,76,FOLLOW_76_in_importStat339);  
			stream_76.add(string_literal13);

			pushFollow(FOLLOW_dotName_in_importStat341);
			dotName14=dotName();
			state._fsp--;

			stream_dotName.add(dotName14.getTree());
			char_literal15=(Token)match(input,66,FOLLOW_66_in_importStat343);  
			stream_66.add(char_literal15);

			// AST REWRITE
			// elements: dotName
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (Object)adaptor.nil();
			// 95:25: -> ^( IMPORT dotName )
			{
				// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:95:28: ^( IMPORT dotName )
				{
				Object root_1 = (Object)adaptor.nil();
				root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(IMPORT, "IMPORT"), root_1);
				adaptor.addChild(root_1, stream_dotName.nextTree());
				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;

			}

			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "importStat"


	public static class optionsStat_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "optionsStat"
	// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:98:1: optionsStat : 'options' '{' ( optionStat ';' )+ '}' ( ';' )? -> ^( OPTIONS ( optionStat )+ ) ;
	public final IOMonkey2Parser.optionsStat_return optionsStat() throws RecognitionException {
		IOMonkey2Parser.optionsStat_return retval = new IOMonkey2Parser.optionsStat_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token string_literal16=null;
		Token char_literal17=null;
		Token char_literal19=null;
		Token char_literal20=null;
		Token char_literal21=null;
		ParserRuleReturnScope optionStat18 =null;

		Object string_literal16_tree=null;
		Object char_literal17_tree=null;
		Object char_literal19_tree=null;
		Object char_literal20_tree=null;
		Object char_literal21_tree=null;
		RewriteRuleTokenStream stream_66=new RewriteRuleTokenStream(adaptor,"token 66");
		RewriteRuleTokenStream stream_80=new RewriteRuleTokenStream(adaptor,"token 80");
		RewriteRuleTokenStream stream_86=new RewriteRuleTokenStream(adaptor,"token 86");
		RewriteRuleTokenStream stream_87=new RewriteRuleTokenStream(adaptor,"token 87");
		RewriteRuleSubtreeStream stream_optionStat=new RewriteRuleSubtreeStream(adaptor,"rule optionStat");

		try {
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:99:2: ( 'options' '{' ( optionStat ';' )+ '}' ( ';' )? -> ^( OPTIONS ( optionStat )+ ) )
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:99:4: 'options' '{' ( optionStat ';' )+ '}' ( ';' )?
			{
			string_literal16=(Token)match(input,80,FOLLOW_80_in_optionsStat363);  
			stream_80.add(string_literal16);

			char_literal17=(Token)match(input,86,FOLLOW_86_in_optionsStat365);  
			stream_86.add(char_literal17);

			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:99:18: ( optionStat ';' )+
			int cnt5=0;
			loop5:
			while (true) {
				int alt5=2;
				int LA5_0 = input.LA(1);
				if ( (LA5_0==ID) ) {
					alt5=1;
				}

				switch (alt5) {
				case 1 :
					// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:99:20: optionStat ';'
					{
					pushFollow(FOLLOW_optionStat_in_optionsStat369);
					optionStat18=optionStat();
					state._fsp--;

					stream_optionStat.add(optionStat18.getTree());
					char_literal19=(Token)match(input,66,FOLLOW_66_in_optionsStat371);  
					stream_66.add(char_literal19);

					}
					break;

				default :
					if ( cnt5 >= 1 ) break loop5;
					EarlyExitException eee = new EarlyExitException(5, input);
					throw eee;
				}
				cnt5++;
			}

			char_literal20=(Token)match(input,87,FOLLOW_87_in_optionsStat377);  
			stream_87.add(char_literal20);

			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:99:43: ( ';' )?
			int alt6=2;
			int LA6_0 = input.LA(1);
			if ( (LA6_0==66) ) {
				alt6=1;
			}
			switch (alt6) {
				case 1 :
					// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:99:43: ';'
					{
					char_literal21=(Token)match(input,66,FOLLOW_66_in_optionsStat379);  
					stream_66.add(char_literal21);

					}
					break;

			}

			// AST REWRITE
			// elements: optionStat
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (Object)adaptor.nil();
			// 99:49: -> ^( OPTIONS ( optionStat )+ )
			{
				// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:99:52: ^( OPTIONS ( optionStat )+ )
				{
				Object root_1 = (Object)adaptor.nil();
				root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(OPTIONS, "OPTIONS"), root_1);
				if ( !(stream_optionStat.hasNext()) ) {
					throw new RewriteEarlyExitException();
				}
				while ( stream_optionStat.hasNext() ) {
					adaptor.addChild(root_1, stream_optionStat.nextTree());
				}
				stream_optionStat.reset();

				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;

			}

			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "optionsStat"


	public static class optionStat_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "optionStat"
	// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:102:1: optionStat : dotName '=' optionValue -> ^( OPTION dotName optionValue ) ;
	public final IOMonkey2Parser.optionStat_return optionStat() throws RecognitionException {
		IOMonkey2Parser.optionStat_return retval = new IOMonkey2Parser.optionStat_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token char_literal23=null;
		ParserRuleReturnScope dotName22 =null;
		ParserRuleReturnScope optionValue24 =null;

		Object char_literal23_tree=null;
		RewriteRuleTokenStream stream_67=new RewriteRuleTokenStream(adaptor,"token 67");
		RewriteRuleSubtreeStream stream_dotName=new RewriteRuleSubtreeStream(adaptor,"rule dotName");
		RewriteRuleSubtreeStream stream_optionValue=new RewriteRuleSubtreeStream(adaptor,"rule optionValue");

		try {
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:103:2: ( dotName '=' optionValue -> ^( OPTION dotName optionValue ) )
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:103:4: dotName '=' optionValue
			{
			pushFollow(FOLLOW_dotName_in_optionStat402);
			dotName22=dotName();
			state._fsp--;

			stream_dotName.add(dotName22.getTree());
			char_literal23=(Token)match(input,67,FOLLOW_67_in_optionStat404);  
			stream_67.add(char_literal23);

			pushFollow(FOLLOW_optionValue_in_optionStat406);
			optionValue24=optionValue();
			state._fsp--;

			stream_optionValue.add(optionValue24.getTree());
			// AST REWRITE
			// elements: dotName, optionValue
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (Object)adaptor.nil();
			// 103:28: -> ^( OPTION dotName optionValue )
			{
				// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:103:31: ^( OPTION dotName optionValue )
				{
				Object root_1 = (Object)adaptor.nil();
				root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(OPTION, "OPTION"), root_1);
				adaptor.addChild(root_1, stream_dotName.nextTree());
				adaptor.addChild(root_1, stream_optionValue.nextTree());
				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;

			}

			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "optionStat"


	public static class optionValue_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "optionValue"
	// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:106:1: optionValue : ( stringValue | intValue );
	public final IOMonkey2Parser.optionValue_return optionValue() throws RecognitionException {
		IOMonkey2Parser.optionValue_return retval = new IOMonkey2Parser.optionValue_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		ParserRuleReturnScope stringValue25 =null;
		ParserRuleReturnScope intValue26 =null;


		try {
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:107:2: ( stringValue | intValue )
			int alt7=2;
			int LA7_0 = input.LA(1);
			if ( (LA7_0==STRING) ) {
				alt7=1;
			}
			else if ( (LA7_0==HEX_NUM||LA7_0==NUM||LA7_0==61||LA7_0==63) ) {
				alt7=2;
			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 7, 0, input);
				throw nvae;
			}

			switch (alt7) {
				case 1 :
					// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:107:4: stringValue
					{
					root_0 = (Object)adaptor.nil();


					pushFollow(FOLLOW_stringValue_in_optionValue428);
					stringValue25=stringValue();
					state._fsp--;

					adaptor.addChild(root_0, stringValue25.getTree());

					}
					break;
				case 2 :
					// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:107:18: intValue
					{
					root_0 = (Object)adaptor.nil();


					pushFollow(FOLLOW_intValue_in_optionValue432);
					intValue26=intValue();
					state._fsp--;

					adaptor.addChild(root_0, intValue26.getTree());

					}
					break;

			}
			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "optionValue"


	public static class elementDefs_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "elementDefs"
	// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:110:1: elementDefs : ( elementDef )* ;
	public final IOMonkey2Parser.elementDefs_return elementDefs() throws RecognitionException {
		IOMonkey2Parser.elementDefs_return retval = new IOMonkey2Parser.elementDefs_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		ParserRuleReturnScope elementDef27 =null;


		try {
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:111:2: ( ( elementDef )* )
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:111:4: ( elementDef )*
			{
			root_0 = (Object)adaptor.nil();


			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:111:4: ( elementDef )*
			loop8:
			while (true) {
				int alt8=2;
				int LA8_0 = input.LA(1);
				if ( ((LA8_0 >= DOC_COMMENT_SLASH && LA8_0 <= DOC_COMMENT_STAR)||LA8_0==68||LA8_0==71||LA8_0==74||LA8_0==85) ) {
					alt8=1;
				}

				switch (alt8) {
				case 1 :
					// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:111:4: elementDef
					{
					pushFollow(FOLLOW_elementDef_in_elementDefs446);
					elementDef27=elementDef();
					state._fsp--;

					adaptor.addChild(root_0, elementDef27.getTree());

					}
					break;

				default :
					break loop8;
				}
			}

			}

			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "elementDefs"


	public static class elementDef_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "elementDef"
	// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:114:1: elementDef : ( enumDef | structDef );
	public final IOMonkey2Parser.elementDef_return elementDef() throws RecognitionException {
		IOMonkey2Parser.elementDef_return retval = new IOMonkey2Parser.elementDef_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		ParserRuleReturnScope enumDef28 =null;
		ParserRuleReturnScope structDef29 =null;


		try {
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:115:2: ( enumDef | structDef )
			int alt9=2;
			alt9 = dfa9.predict(input);
			switch (alt9) {
				case 1 :
					// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:115:4: enumDef
					{
					root_0 = (Object)adaptor.nil();


					pushFollow(FOLLOW_enumDef_in_elementDef459);
					enumDef28=enumDef();
					state._fsp--;

					adaptor.addChild(root_0, enumDef28.getTree());

					}
					break;
				case 2 :
					// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:116:4: structDef
					{
					root_0 = (Object)adaptor.nil();


					pushFollow(FOLLOW_structDef_in_elementDef464);
					structDef29=structDef();
					state._fsp--;

					adaptor.addChild(root_0, structDef29.getTree());

					}
					break;

			}
			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "elementDef"


	public static class enumDef_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "enumDef"
	// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:119:1: enumDef : ( docComment )? 'enum' ID '{' enumConst ( ',' enumConst )* ( ',' )? '}' -> ^( ENUM ID ( docComment )? enumConst ( enumConst )* ) ;
	public final IOMonkey2Parser.enumDef_return enumDef() throws RecognitionException {
		IOMonkey2Parser.enumDef_return retval = new IOMonkey2Parser.enumDef_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token string_literal31=null;
		Token ID32=null;
		Token char_literal33=null;
		Token char_literal35=null;
		Token char_literal37=null;
		Token char_literal38=null;
		ParserRuleReturnScope docComment30 =null;
		ParserRuleReturnScope enumConst34 =null;
		ParserRuleReturnScope enumConst36 =null;

		Object string_literal31_tree=null;
		Object ID32_tree=null;
		Object char_literal33_tree=null;
		Object char_literal35_tree=null;
		Object char_literal37_tree=null;
		Object char_literal38_tree=null;
		RewriteRuleTokenStream stream_ID=new RewriteRuleTokenStream(adaptor,"token ID");
		RewriteRuleTokenStream stream_62=new RewriteRuleTokenStream(adaptor,"token 62");
		RewriteRuleTokenStream stream_74=new RewriteRuleTokenStream(adaptor,"token 74");
		RewriteRuleTokenStream stream_86=new RewriteRuleTokenStream(adaptor,"token 86");
		RewriteRuleTokenStream stream_87=new RewriteRuleTokenStream(adaptor,"token 87");
		RewriteRuleSubtreeStream stream_docComment=new RewriteRuleSubtreeStream(adaptor,"rule docComment");
		RewriteRuleSubtreeStream stream_enumConst=new RewriteRuleSubtreeStream(adaptor,"rule enumConst");

		try {
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:119:9: ( ( docComment )? 'enum' ID '{' enumConst ( ',' enumConst )* ( ',' )? '}' -> ^( ENUM ID ( docComment )? enumConst ( enumConst )* ) )
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:119:11: ( docComment )? 'enum' ID '{' enumConst ( ',' enumConst )* ( ',' )? '}'
			{
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:119:11: ( docComment )?
			int alt10=2;
			int LA10_0 = input.LA(1);
			if ( ((LA10_0 >= DOC_COMMENT_SLASH && LA10_0 <= DOC_COMMENT_STAR)) ) {
				alt10=1;
			}
			switch (alt10) {
				case 1 :
					// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:119:11: docComment
					{
					pushFollow(FOLLOW_docComment_in_enumDef475);
					docComment30=docComment();
					state._fsp--;

					stream_docComment.add(docComment30.getTree());
					}
					break;

			}

			string_literal31=(Token)match(input,74,FOLLOW_74_in_enumDef478);  
			stream_74.add(string_literal31);

			ID32=(Token)match(input,ID,FOLLOW_ID_in_enumDef480);  
			stream_ID.add(ID32);

			char_literal33=(Token)match(input,86,FOLLOW_86_in_enumDef482);  
			stream_86.add(char_literal33);

			pushFollow(FOLLOW_enumConst_in_enumDef484);
			enumConst34=enumConst();
			state._fsp--;

			stream_enumConst.add(enumConst34.getTree());
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:119:47: ( ',' enumConst )*
			loop11:
			while (true) {
				int alt11=2;
				int LA11_0 = input.LA(1);
				if ( (LA11_0==62) ) {
					int LA11_1 = input.LA(2);
					if ( ((LA11_1 >= DOC_COMMENT_SLASH && LA11_1 <= DOC_COMMENT_STAR)||LA11_1==ID) ) {
						alt11=1;
					}

				}

				switch (alt11) {
				case 1 :
					// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:119:49: ',' enumConst
					{
					char_literal35=(Token)match(input,62,FOLLOW_62_in_enumDef488);  
					stream_62.add(char_literal35);

					pushFollow(FOLLOW_enumConst_in_enumDef490);
					enumConst36=enumConst();
					state._fsp--;

					stream_enumConst.add(enumConst36.getTree());
					}
					break;

				default :
					break loop11;
				}
			}

			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:119:67: ( ',' )?
			int alt12=2;
			int LA12_0 = input.LA(1);
			if ( (LA12_0==62) ) {
				alt12=1;
			}
			switch (alt12) {
				case 1 :
					// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:119:67: ','
					{
					char_literal37=(Token)match(input,62,FOLLOW_62_in_enumDef496);  
					stream_62.add(char_literal37);

					}
					break;

			}

			char_literal38=(Token)match(input,87,FOLLOW_87_in_enumDef499);  
			stream_87.add(char_literal38);

			// AST REWRITE
			// elements: ID, enumConst, docComment, enumConst
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (Object)adaptor.nil();
			// 119:76: -> ^( ENUM ID ( docComment )? enumConst ( enumConst )* )
			{
				// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:119:79: ^( ENUM ID ( docComment )? enumConst ( enumConst )* )
				{
				Object root_1 = (Object)adaptor.nil();
				root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(ENUM, "ENUM"), root_1);
				adaptor.addChild(root_1, stream_ID.nextNode());
				// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:119:89: ( docComment )?
				if ( stream_docComment.hasNext() ) {
					adaptor.addChild(root_1, stream_docComment.nextTree());
				}
				stream_docComment.reset();

				adaptor.addChild(root_1, stream_enumConst.nextTree());
				// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:119:111: ( enumConst )*
				while ( stream_enumConst.hasNext() ) {
					adaptor.addChild(root_1, stream_enumConst.nextTree());
				}
				stream_enumConst.reset();

				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;

			}

			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "enumDef"


	public static class enumConst_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "enumConst"
	// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:122:1: enumConst : ( docComment )? ID '=' intValue -> ^( ENUM_CONST ID intValue ( docComment )? ) ;
	public final IOMonkey2Parser.enumConst_return enumConst() throws RecognitionException {
		IOMonkey2Parser.enumConst_return retval = new IOMonkey2Parser.enumConst_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token ID40=null;
		Token char_literal41=null;
		ParserRuleReturnScope docComment39 =null;
		ParserRuleReturnScope intValue42 =null;

		Object ID40_tree=null;
		Object char_literal41_tree=null;
		RewriteRuleTokenStream stream_67=new RewriteRuleTokenStream(adaptor,"token 67");
		RewriteRuleTokenStream stream_ID=new RewriteRuleTokenStream(adaptor,"token ID");
		RewriteRuleSubtreeStream stream_intValue=new RewriteRuleSubtreeStream(adaptor,"rule intValue");
		RewriteRuleSubtreeStream stream_docComment=new RewriteRuleSubtreeStream(adaptor,"rule docComment");

		try {
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:123:2: ( ( docComment )? ID '=' intValue -> ^( ENUM_CONST ID intValue ( docComment )? ) )
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:123:4: ( docComment )? ID '=' intValue
			{
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:123:4: ( docComment )?
			int alt13=2;
			int LA13_0 = input.LA(1);
			if ( ((LA13_0 >= DOC_COMMENT_SLASH && LA13_0 <= DOC_COMMENT_STAR)) ) {
				alt13=1;
			}
			switch (alt13) {
				case 1 :
					// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:123:4: docComment
					{
					pushFollow(FOLLOW_docComment_in_enumConst528);
					docComment39=docComment();
					state._fsp--;

					stream_docComment.add(docComment39.getTree());
					}
					break;

			}

			ID40=(Token)match(input,ID,FOLLOW_ID_in_enumConst531);  
			stream_ID.add(ID40);

			char_literal41=(Token)match(input,67,FOLLOW_67_in_enumConst533);  
			stream_67.add(char_literal41);

			pushFollow(FOLLOW_intValue_in_enumConst535);
			intValue42=intValue();
			state._fsp--;

			stream_intValue.add(intValue42.getTree());
			// AST REWRITE
			// elements: intValue, docComment, ID
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (Object)adaptor.nil();
			// 123:32: -> ^( ENUM_CONST ID intValue ( docComment )? )
			{
				// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:123:35: ^( ENUM_CONST ID intValue ( docComment )? )
				{
				Object root_1 = (Object)adaptor.nil();
				root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(ENUM_CONST, "ENUM_CONST"), root_1);
				adaptor.addChild(root_1, stream_ID.nextNode());
				adaptor.addChild(root_1, stream_intValue.nextTree());
				// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:123:60: ( docComment )?
				if ( stream_docComment.hasNext() ) {
					adaptor.addChild(root_1, stream_docComment.nextTree());
				}
				stream_docComment.reset();

				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;

			}

			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "enumConst"


	public static class structDef_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "structDef"
	// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:126:1: structDef : ( docComment )? ( annotations )? structHeaderDef ID ( structSuperDef )? '{' ( structItem )* '}' ( ';' )? -> ^( structHeaderDef ID ( structSuperDef )? ( annotations )? ( docComment )? ( structItem )* ) ;
	public final IOMonkey2Parser.structDef_return structDef() throws RecognitionException {
		IOMonkey2Parser.structDef_return retval = new IOMonkey2Parser.structDef_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token ID46=null;
		Token char_literal48=null;
		Token char_literal50=null;
		Token char_literal51=null;
		ParserRuleReturnScope docComment43 =null;
		ParserRuleReturnScope annotations44 =null;
		ParserRuleReturnScope structHeaderDef45 =null;
		ParserRuleReturnScope structSuperDef47 =null;
		ParserRuleReturnScope structItem49 =null;

		Object ID46_tree=null;
		Object char_literal48_tree=null;
		Object char_literal50_tree=null;
		Object char_literal51_tree=null;
		RewriteRuleTokenStream stream_66=new RewriteRuleTokenStream(adaptor,"token 66");
		RewriteRuleTokenStream stream_ID=new RewriteRuleTokenStream(adaptor,"token ID");
		RewriteRuleTokenStream stream_86=new RewriteRuleTokenStream(adaptor,"token 86");
		RewriteRuleTokenStream stream_87=new RewriteRuleTokenStream(adaptor,"token 87");
		RewriteRuleSubtreeStream stream_structSuperDef=new RewriteRuleSubtreeStream(adaptor,"rule structSuperDef");
		RewriteRuleSubtreeStream stream_docComment=new RewriteRuleSubtreeStream(adaptor,"rule docComment");
		RewriteRuleSubtreeStream stream_annotations=new RewriteRuleSubtreeStream(adaptor,"rule annotations");
		RewriteRuleSubtreeStream stream_structItem=new RewriteRuleSubtreeStream(adaptor,"rule structItem");
		RewriteRuleSubtreeStream stream_structHeaderDef=new RewriteRuleSubtreeStream(adaptor,"rule structHeaderDef");

		try {
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:127:2: ( ( docComment )? ( annotations )? structHeaderDef ID ( structSuperDef )? '{' ( structItem )* '}' ( ';' )? -> ^( structHeaderDef ID ( structSuperDef )? ( annotations )? ( docComment )? ( structItem )* ) )
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:127:5: ( docComment )? ( annotations )? structHeaderDef ID ( structSuperDef )? '{' ( structItem )* '}' ( ';' )?
			{
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:127:5: ( docComment )?
			int alt14=2;
			int LA14_0 = input.LA(1);
			if ( ((LA14_0 >= DOC_COMMENT_SLASH && LA14_0 <= DOC_COMMENT_STAR)) ) {
				alt14=1;
			}
			switch (alt14) {
				case 1 :
					// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:127:5: docComment
					{
					pushFollow(FOLLOW_docComment_in_structDef561);
					docComment43=docComment();
					state._fsp--;

					stream_docComment.add(docComment43.getTree());
					}
					break;

			}

			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:127:17: ( annotations )?
			int alt15=2;
			int LA15_0 = input.LA(1);
			if ( (LA15_0==68) ) {
				alt15=1;
			}
			switch (alt15) {
				case 1 :
					// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:127:17: annotations
					{
					pushFollow(FOLLOW_annotations_in_structDef564);
					annotations44=annotations();
					state._fsp--;

					stream_annotations.add(annotations44.getTree());
					}
					break;

			}

			pushFollow(FOLLOW_structHeaderDef_in_structDef567);
			structHeaderDef45=structHeaderDef();
			state._fsp--;

			stream_structHeaderDef.add(structHeaderDef45.getTree());
			ID46=(Token)match(input,ID,FOLLOW_ID_in_structDef569);  
			stream_ID.add(ID46);

			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:127:49: ( structSuperDef )?
			int alt16=2;
			int LA16_0 = input.LA(1);
			if ( (LA16_0==65) ) {
				alt16=1;
			}
			switch (alt16) {
				case 1 :
					// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:127:49: structSuperDef
					{
					pushFollow(FOLLOW_structSuperDef_in_structDef571);
					structSuperDef47=structSuperDef();
					state._fsp--;

					stream_structSuperDef.add(structSuperDef47.getTree());
					}
					break;

			}

			char_literal48=(Token)match(input,86,FOLLOW_86_in_structDef574);  
			stream_86.add(char_literal48);

			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:127:69: ( structItem )*
			loop17:
			while (true) {
				int alt17=2;
				int LA17_0 = input.LA(1);
				if ( ((LA17_0 >= DOC_COMMENT_SLASH && LA17_0 <= DOC_COMMENT_STAR)||LA17_0==ID||LA17_0==68||(LA17_0 >= 72 && LA17_0 <= 73)||LA17_0==75||(LA17_0 >= 77 && LA17_0 <= 79)||(LA17_0 >= 82 && LA17_0 <= 84)) ) {
					alt17=1;
				}

				switch (alt17) {
				case 1 :
					// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:127:69: structItem
					{
					pushFollow(FOLLOW_structItem_in_structDef576);
					structItem49=structItem();
					state._fsp--;

					stream_structItem.add(structItem49.getTree());
					}
					break;

				default :
					break loop17;
				}
			}

			char_literal50=(Token)match(input,87,FOLLOW_87_in_structDef580);  
			stream_87.add(char_literal50);

			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:127:86: ( ';' )?
			int alt18=2;
			int LA18_0 = input.LA(1);
			if ( (LA18_0==66) ) {
				alt18=1;
			}
			switch (alt18) {
				case 1 :
					// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:127:86: ';'
					{
					char_literal51=(Token)match(input,66,FOLLOW_66_in_structDef582);  
					stream_66.add(char_literal51);

					}
					break;

			}

			// AST REWRITE
			// elements: structItem, annotations, structHeaderDef, ID, docComment, structSuperDef
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (Object)adaptor.nil();
			// 128:2: -> ^( structHeaderDef ID ( structSuperDef )? ( annotations )? ( docComment )? ( structItem )* )
			{
				// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:128:5: ^( structHeaderDef ID ( structSuperDef )? ( annotations )? ( docComment )? ( structItem )* )
				{
				Object root_1 = (Object)adaptor.nil();
				root_1 = (Object)adaptor.becomeRoot(stream_structHeaderDef.nextNode(), root_1);
				adaptor.addChild(root_1, stream_ID.nextNode());
				// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:128:26: ( structSuperDef )?
				if ( stream_structSuperDef.hasNext() ) {
					adaptor.addChild(root_1, stream_structSuperDef.nextTree());
				}
				stream_structSuperDef.reset();

				// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:128:42: ( annotations )?
				if ( stream_annotations.hasNext() ) {
					adaptor.addChild(root_1, stream_annotations.nextTree());
				}
				stream_annotations.reset();

				// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:128:55: ( docComment )?
				if ( stream_docComment.hasNext() ) {
					adaptor.addChild(root_1, stream_docComment.nextTree());
				}
				stream_docComment.reset();

				// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:128:67: ( structItem )*
				while ( stream_structItem.hasNext() ) {
					adaptor.addChild(root_1, stream_structItem.nextTree());
				}
				stream_structItem.reset();

				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;

			}

			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "structDef"


	public static class structHeaderDef_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "structHeaderDef"
	// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:131:1: structHeaderDef : ( structHeader | abstractStructHeader );
	public final IOMonkey2Parser.structHeaderDef_return structHeaderDef() throws RecognitionException {
		IOMonkey2Parser.structHeaderDef_return retval = new IOMonkey2Parser.structHeaderDef_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		ParserRuleReturnScope structHeader52 =null;
		ParserRuleReturnScope abstractStructHeader53 =null;


		try {
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:132:2: ( structHeader | abstractStructHeader )
			int alt19=2;
			int LA19_0 = input.LA(1);
			if ( (LA19_0==85) ) {
				alt19=1;
			}
			else if ( (LA19_0==71) ) {
				alt19=2;
			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 19, 0, input);
				throw nvae;
			}

			switch (alt19) {
				case 1 :
					// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:132:4: structHeader
					{
					root_0 = (Object)adaptor.nil();


					pushFollow(FOLLOW_structHeader_in_structHeaderDef617);
					structHeader52=structHeader();
					state._fsp--;

					adaptor.addChild(root_0, structHeader52.getTree());

					}
					break;
				case 2 :
					// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:133:4: abstractStructHeader
					{
					root_0 = (Object)adaptor.nil();


					pushFollow(FOLLOW_abstractStructHeader_in_structHeaderDef622);
					abstractStructHeader53=abstractStructHeader();
					state._fsp--;

					adaptor.addChild(root_0, abstractStructHeader53.getTree());

					}
					break;

			}
			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "structHeaderDef"


	public static class structHeader_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "structHeader"
	// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:136:1: structHeader : 'struct' -> ^( STRUCT ) ;
	public final IOMonkey2Parser.structHeader_return structHeader() throws RecognitionException {
		IOMonkey2Parser.structHeader_return retval = new IOMonkey2Parser.structHeader_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token string_literal54=null;

		Object string_literal54_tree=null;
		RewriteRuleTokenStream stream_85=new RewriteRuleTokenStream(adaptor,"token 85");

		try {
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:137:2: ( 'struct' -> ^( STRUCT ) )
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:137:4: 'struct'
			{
			string_literal54=(Token)match(input,85,FOLLOW_85_in_structHeader635);  
			stream_85.add(string_literal54);

			// AST REWRITE
			// elements: 
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (Object)adaptor.nil();
			// 137:13: -> ^( STRUCT )
			{
				// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:137:16: ^( STRUCT )
				{
				Object root_1 = (Object)adaptor.nil();
				root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(STRUCT, "STRUCT"), root_1);
				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;

			}

			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "structHeader"


	public static class abstractStructHeader_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "abstractStructHeader"
	// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:140:1: abstractStructHeader : 'abstract' 'struct' -> ^( ABSTRACT_STRUCT ) ;
	public final IOMonkey2Parser.abstractStructHeader_return abstractStructHeader() throws RecognitionException {
		IOMonkey2Parser.abstractStructHeader_return retval = new IOMonkey2Parser.abstractStructHeader_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token string_literal55=null;
		Token string_literal56=null;

		Object string_literal55_tree=null;
		Object string_literal56_tree=null;
		RewriteRuleTokenStream stream_71=new RewriteRuleTokenStream(adaptor,"token 71");
		RewriteRuleTokenStream stream_85=new RewriteRuleTokenStream(adaptor,"token 85");

		try {
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:141:2: ( 'abstract' 'struct' -> ^( ABSTRACT_STRUCT ) )
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:141:4: 'abstract' 'struct'
			{
			string_literal55=(Token)match(input,71,FOLLOW_71_in_abstractStructHeader653);  
			stream_71.add(string_literal55);

			string_literal56=(Token)match(input,85,FOLLOW_85_in_abstractStructHeader655);  
			stream_85.add(string_literal56);

			// AST REWRITE
			// elements: 
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (Object)adaptor.nil();
			// 141:24: -> ^( ABSTRACT_STRUCT )
			{
				// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:141:27: ^( ABSTRACT_STRUCT )
				{
				Object root_1 = (Object)adaptor.nil();
				root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(ABSTRACT_STRUCT, "ABSTRACT_STRUCT"), root_1);
				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;

			}

			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "abstractStructHeader"


	public static class structSuperDef_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "structSuperDef"
	// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:144:1: structSuperDef : ':' ID -> ^( STRUCT_SUPER ID ) ;
	public final IOMonkey2Parser.structSuperDef_return structSuperDef() throws RecognitionException {
		IOMonkey2Parser.structSuperDef_return retval = new IOMonkey2Parser.structSuperDef_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token char_literal57=null;
		Token ID58=null;

		Object char_literal57_tree=null;
		Object ID58_tree=null;
		RewriteRuleTokenStream stream_ID=new RewriteRuleTokenStream(adaptor,"token ID");
		RewriteRuleTokenStream stream_65=new RewriteRuleTokenStream(adaptor,"token 65");

		try {
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:145:2: ( ':' ID -> ^( STRUCT_SUPER ID ) )
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:145:4: ':' ID
			{
			char_literal57=(Token)match(input,65,FOLLOW_65_in_structSuperDef673);  
			stream_65.add(char_literal57);

			ID58=(Token)match(input,ID,FOLLOW_ID_in_structSuperDef675);  
			stream_ID.add(ID58);

			// AST REWRITE
			// elements: ID
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (Object)adaptor.nil();
			// 145:11: -> ^( STRUCT_SUPER ID )
			{
				// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:145:14: ^( STRUCT_SUPER ID )
				{
				Object root_1 = (Object)adaptor.nil();
				root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(STRUCT_SUPER, "STRUCT_SUPER"), root_1);
				adaptor.addChild(root_1, stream_ID.nextNode());
				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;

			}

			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "structSuperDef"


	public static class structItem_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "structItem"
	// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:148:1: structItem : attribute ';' !;
	public final IOMonkey2Parser.structItem_return structItem() throws RecognitionException {
		IOMonkey2Parser.structItem_return retval = new IOMonkey2Parser.structItem_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token char_literal60=null;
		ParserRuleReturnScope attribute59 =null;

		Object char_literal60_tree=null;

		try {
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:149:2: ( attribute ';' !)
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:149:4: attribute ';' !
			{
			root_0 = (Object)adaptor.nil();


			pushFollow(FOLLOW_attribute_in_structItem695);
			attribute59=attribute();
			state._fsp--;

			adaptor.addChild(root_0, attribute59.getTree());

			char_literal60=(Token)match(input,66,FOLLOW_66_in_structItem697); 
			}

			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "structItem"


	public static class attribute_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "attribute"
	// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:152:1: attribute : ( docComment )? ( annotations )? ( attributeSlot )? attributeType ID ( attributeDefaultValueSet )? -> ^( STRUCT_ATTR ID attributeType ( attributeSlot )? ( attributeDefaultValueSet )? ( annotations )? ( docComment )? ) ;
	public final IOMonkey2Parser.attribute_return attribute() throws RecognitionException {
		IOMonkey2Parser.attribute_return retval = new IOMonkey2Parser.attribute_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token ID65=null;
		ParserRuleReturnScope docComment61 =null;
		ParserRuleReturnScope annotations62 =null;
		ParserRuleReturnScope attributeSlot63 =null;
		ParserRuleReturnScope attributeType64 =null;
		ParserRuleReturnScope attributeDefaultValueSet66 =null;

		Object ID65_tree=null;
		RewriteRuleTokenStream stream_ID=new RewriteRuleTokenStream(adaptor,"token ID");
		RewriteRuleSubtreeStream stream_attributeType=new RewriteRuleSubtreeStream(adaptor,"rule attributeType");
		RewriteRuleSubtreeStream stream_docComment=new RewriteRuleSubtreeStream(adaptor,"rule docComment");
		RewriteRuleSubtreeStream stream_annotations=new RewriteRuleSubtreeStream(adaptor,"rule annotations");
		RewriteRuleSubtreeStream stream_attributeDefaultValueSet=new RewriteRuleSubtreeStream(adaptor,"rule attributeDefaultValueSet");
		RewriteRuleSubtreeStream stream_attributeSlot=new RewriteRuleSubtreeStream(adaptor,"rule attributeSlot");

		try {
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:153:2: ( ( docComment )? ( annotations )? ( attributeSlot )? attributeType ID ( attributeDefaultValueSet )? -> ^( STRUCT_ATTR ID attributeType ( attributeSlot )? ( attributeDefaultValueSet )? ( annotations )? ( docComment )? ) )
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:153:4: ( docComment )? ( annotations )? ( attributeSlot )? attributeType ID ( attributeDefaultValueSet )?
			{
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:153:4: ( docComment )?
			int alt20=2;
			int LA20_0 = input.LA(1);
			if ( ((LA20_0 >= DOC_COMMENT_SLASH && LA20_0 <= DOC_COMMENT_STAR)) ) {
				alt20=1;
			}
			switch (alt20) {
				case 1 :
					// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:153:4: docComment
					{
					pushFollow(FOLLOW_docComment_in_attribute710);
					docComment61=docComment();
					state._fsp--;

					stream_docComment.add(docComment61.getTree());
					}
					break;

			}

			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:153:16: ( annotations )?
			int alt21=2;
			int LA21_0 = input.LA(1);
			if ( (LA21_0==68) ) {
				alt21=1;
			}
			switch (alt21) {
				case 1 :
					// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:153:16: annotations
					{
					pushFollow(FOLLOW_annotations_in_attribute713);
					annotations62=annotations();
					state._fsp--;

					stream_annotations.add(annotations62.getTree());
					}
					break;

			}

			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:153:29: ( attributeSlot )?
			int alt22=2;
			int LA22_0 = input.LA(1);
			if ( (LA22_0==79||LA22_0==82) ) {
				alt22=1;
			}
			switch (alt22) {
				case 1 :
					// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:153:29: attributeSlot
					{
					pushFollow(FOLLOW_attributeSlot_in_attribute716);
					attributeSlot63=attributeSlot();
					state._fsp--;

					stream_attributeSlot.add(attributeSlot63.getTree());
					}
					break;

			}

			pushFollow(FOLLOW_attributeType_in_attribute719);
			attributeType64=attributeType();
			state._fsp--;

			stream_attributeType.add(attributeType64.getTree());
			ID65=(Token)match(input,ID,FOLLOW_ID_in_attribute721);  
			stream_ID.add(ID65);

			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:153:61: ( attributeDefaultValueSet )?
			int alt23=2;
			int LA23_0 = input.LA(1);
			if ( (LA23_0==67) ) {
				alt23=1;
			}
			switch (alt23) {
				case 1 :
					// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:153:61: attributeDefaultValueSet
					{
					pushFollow(FOLLOW_attributeDefaultValueSet_in_attribute723);
					attributeDefaultValueSet66=attributeDefaultValueSet();
					state._fsp--;

					stream_attributeDefaultValueSet.add(attributeDefaultValueSet66.getTree());
					}
					break;

			}

			// AST REWRITE
			// elements: annotations, ID, attributeDefaultValueSet, docComment, attributeType, attributeSlot
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (Object)adaptor.nil();
			// 154:3: -> ^( STRUCT_ATTR ID attributeType ( attributeSlot )? ( attributeDefaultValueSet )? ( annotations )? ( docComment )? )
			{
				// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:154:6: ^( STRUCT_ATTR ID attributeType ( attributeSlot )? ( attributeDefaultValueSet )? ( annotations )? ( docComment )? )
				{
				Object root_1 = (Object)adaptor.nil();
				root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(STRUCT_ATTR, "STRUCT_ATTR"), root_1);
				adaptor.addChild(root_1, stream_ID.nextNode());
				adaptor.addChild(root_1, stream_attributeType.nextTree());
				// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:154:37: ( attributeSlot )?
				if ( stream_attributeSlot.hasNext() ) {
					adaptor.addChild(root_1, stream_attributeSlot.nextTree());
				}
				stream_attributeSlot.reset();

				// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:154:52: ( attributeDefaultValueSet )?
				if ( stream_attributeDefaultValueSet.hasNext() ) {
					adaptor.addChild(root_1, stream_attributeDefaultValueSet.nextTree());
				}
				stream_attributeDefaultValueSet.reset();

				// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:154:78: ( annotations )?
				if ( stream_annotations.hasNext() ) {
					adaptor.addChild(root_1, stream_annotations.nextTree());
				}
				stream_annotations.reset();

				// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:154:91: ( docComment )?
				if ( stream_docComment.hasNext() ) {
					adaptor.addChild(root_1, stream_docComment.nextTree());
				}
				stream_docComment.reset();

				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;

			}

			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "attribute"


	public static class attributeType_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "attributeType"
	// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:157:1: attributeType : ( scalarType | arrayType );
	public final IOMonkey2Parser.attributeType_return attributeType() throws RecognitionException {
		IOMonkey2Parser.attributeType_return retval = new IOMonkey2Parser.attributeType_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		ParserRuleReturnScope scalarType67 =null;
		ParserRuleReturnScope arrayType68 =null;


		try {
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:158:2: ( scalarType | arrayType )
			int alt24=2;
			switch ( input.LA(1) ) {
			case 77:
				{
				int LA24_1 = input.LA(2);
				if ( (LA24_1==ID) ) {
					alt24=1;
				}
				else if ( (LA24_1==69) ) {
					alt24=2;
				}

				else {
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 24, 1, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

				}
				break;
			case 83:
				{
				int LA24_2 = input.LA(2);
				if ( (LA24_2==ID) ) {
					alt24=1;
				}
				else if ( (LA24_2==69) ) {
					alt24=2;
				}

				else {
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 24, 2, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

				}
				break;
			case 73:
				{
				int LA24_3 = input.LA(2);
				if ( (LA24_3==ID) ) {
					alt24=1;
				}
				else if ( (LA24_3==69) ) {
					alt24=2;
				}

				else {
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 24, 3, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

				}
				break;
			case 78:
				{
				int LA24_4 = input.LA(2);
				if ( (LA24_4==ID) ) {
					alt24=1;
				}
				else if ( (LA24_4==69) ) {
					alt24=2;
				}

				else {
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 24, 4, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

				}
				break;
			case 84:
				{
				int LA24_5 = input.LA(2);
				if ( (LA24_5==ID) ) {
					alt24=1;
				}
				else if ( (LA24_5==69) ) {
					alt24=2;
				}

				else {
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 24, 5, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

				}
				break;
			case 75:
				{
				int LA24_6 = input.LA(2);
				if ( (LA24_6==ID) ) {
					alt24=1;
				}
				else if ( (LA24_6==69) ) {
					alt24=2;
				}

				else {
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 24, 6, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

				}
				break;
			case 72:
				{
				int LA24_7 = input.LA(2);
				if ( (LA24_7==ID) ) {
					alt24=1;
				}
				else if ( (LA24_7==69) ) {
					alt24=2;
				}

				else {
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 24, 7, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

				}
				break;
			case ID:
				{
				int LA24_8 = input.LA(2);
				if ( (LA24_8==ID) ) {
					alt24=1;
				}
				else if ( (LA24_8==69) ) {
					alt24=2;
				}

				else {
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 24, 8, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

				}
				break;
			default:
				NoViableAltException nvae =
					new NoViableAltException("", 24, 0, input);
				throw nvae;
			}
			switch (alt24) {
				case 1 :
					// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:158:4: scalarType
					{
					root_0 = (Object)adaptor.nil();


					pushFollow(FOLLOW_scalarType_in_attributeType761);
					scalarType67=scalarType();
					state._fsp--;

					adaptor.addChild(root_0, scalarType67.getTree());

					}
					break;
				case 2 :
					// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:159:4: arrayType
					{
					root_0 = (Object)adaptor.nil();


					pushFollow(FOLLOW_arrayType_in_attributeType766);
					arrayType68=arrayType();
					state._fsp--;

					adaptor.addChild(root_0, arrayType68.getTree());

					}
					break;

			}
			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "attributeType"


	public static class scalarType_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "scalarType"
	// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:162:1: scalarType : ( intType | shortType | byteType | longType | stringType | floatType | boolType | idType );
	public final IOMonkey2Parser.scalarType_return scalarType() throws RecognitionException {
		IOMonkey2Parser.scalarType_return retval = new IOMonkey2Parser.scalarType_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		ParserRuleReturnScope intType69 =null;
		ParserRuleReturnScope shortType70 =null;
		ParserRuleReturnScope byteType71 =null;
		ParserRuleReturnScope longType72 =null;
		ParserRuleReturnScope stringType73 =null;
		ParserRuleReturnScope floatType74 =null;
		ParserRuleReturnScope boolType75 =null;
		ParserRuleReturnScope idType76 =null;


		try {
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:163:2: ( intType | shortType | byteType | longType | stringType | floatType | boolType | idType )
			int alt25=8;
			switch ( input.LA(1) ) {
			case 77:
				{
				alt25=1;
				}
				break;
			case 83:
				{
				alt25=2;
				}
				break;
			case 73:
				{
				alt25=3;
				}
				break;
			case 78:
				{
				alt25=4;
				}
				break;
			case 84:
				{
				alt25=5;
				}
				break;
			case 75:
				{
				alt25=6;
				}
				break;
			case 72:
				{
				alt25=7;
				}
				break;
			case ID:
				{
				alt25=8;
				}
				break;
			default:
				NoViableAltException nvae =
					new NoViableAltException("", 25, 0, input);
				throw nvae;
			}
			switch (alt25) {
				case 1 :
					// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:163:4: intType
					{
					root_0 = (Object)adaptor.nil();


					pushFollow(FOLLOW_intType_in_scalarType778);
					intType69=intType();
					state._fsp--;

					adaptor.addChild(root_0, intType69.getTree());

					}
					break;
				case 2 :
					// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:164:4: shortType
					{
					root_0 = (Object)adaptor.nil();


					pushFollow(FOLLOW_shortType_in_scalarType783);
					shortType70=shortType();
					state._fsp--;

					adaptor.addChild(root_0, shortType70.getTree());

					}
					break;
				case 3 :
					// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:165:4: byteType
					{
					root_0 = (Object)adaptor.nil();


					pushFollow(FOLLOW_byteType_in_scalarType788);
					byteType71=byteType();
					state._fsp--;

					adaptor.addChild(root_0, byteType71.getTree());

					}
					break;
				case 4 :
					// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:166:4: longType
					{
					root_0 = (Object)adaptor.nil();


					pushFollow(FOLLOW_longType_in_scalarType793);
					longType72=longType();
					state._fsp--;

					adaptor.addChild(root_0, longType72.getTree());

					}
					break;
				case 5 :
					// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:167:4: stringType
					{
					root_0 = (Object)adaptor.nil();


					pushFollow(FOLLOW_stringType_in_scalarType798);
					stringType73=stringType();
					state._fsp--;

					adaptor.addChild(root_0, stringType73.getTree());

					}
					break;
				case 6 :
					// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:168:4: floatType
					{
					root_0 = (Object)adaptor.nil();


					pushFollow(FOLLOW_floatType_in_scalarType803);
					floatType74=floatType();
					state._fsp--;

					adaptor.addChild(root_0, floatType74.getTree());

					}
					break;
				case 7 :
					// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:169:4: boolType
					{
					root_0 = (Object)adaptor.nil();


					pushFollow(FOLLOW_boolType_in_scalarType808);
					boolType75=boolType();
					state._fsp--;

					adaptor.addChild(root_0, boolType75.getTree());

					}
					break;
				case 8 :
					// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:170:4: idType
					{
					root_0 = (Object)adaptor.nil();


					pushFollow(FOLLOW_idType_in_scalarType813);
					idType76=idType();
					state._fsp--;

					adaptor.addChild(root_0, idType76.getTree());

					}
					break;

			}
			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "scalarType"


	public static class intType_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "intType"
	// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:173:1: intType : 'int' -> TYPE_INT ;
	public final IOMonkey2Parser.intType_return intType() throws RecognitionException {
		IOMonkey2Parser.intType_return retval = new IOMonkey2Parser.intType_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token string_literal77=null;

		Object string_literal77_tree=null;
		RewriteRuleTokenStream stream_77=new RewriteRuleTokenStream(adaptor,"token 77");

		try {
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:173:9: ( 'int' -> TYPE_INT )
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:173:11: 'int'
			{
			string_literal77=(Token)match(input,77,FOLLOW_77_in_intType824);  
			stream_77.add(string_literal77);

			// AST REWRITE
			// elements: 
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (Object)adaptor.nil();
			// 173:17: -> TYPE_INT
			{
				adaptor.addChild(root_0, (Object)adaptor.create(TYPE_INT, "TYPE_INT"));
			}


			retval.tree = root_0;

			}

			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "intType"


	public static class shortType_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "shortType"
	// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:176:1: shortType : 'short' -> TYPE_SHORT ;
	public final IOMonkey2Parser.shortType_return shortType() throws RecognitionException {
		IOMonkey2Parser.shortType_return retval = new IOMonkey2Parser.shortType_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token string_literal78=null;

		Object string_literal78_tree=null;
		RewriteRuleTokenStream stream_83=new RewriteRuleTokenStream(adaptor,"token 83");

		try {
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:177:2: ( 'short' -> TYPE_SHORT )
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:177:4: 'short'
			{
			string_literal78=(Token)match(input,83,FOLLOW_83_in_shortType841);  
			stream_83.add(string_literal78);

			// AST REWRITE
			// elements: 
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (Object)adaptor.nil();
			// 177:12: -> TYPE_SHORT
			{
				adaptor.addChild(root_0, (Object)adaptor.create(TYPE_SHORT, "TYPE_SHORT"));
			}


			retval.tree = root_0;

			}

			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "shortType"


	public static class byteType_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "byteType"
	// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:180:1: byteType : 'byte' -> TYPE_BYTE ;
	public final IOMonkey2Parser.byteType_return byteType() throws RecognitionException {
		IOMonkey2Parser.byteType_return retval = new IOMonkey2Parser.byteType_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token string_literal79=null;

		Object string_literal79_tree=null;
		RewriteRuleTokenStream stream_73=new RewriteRuleTokenStream(adaptor,"token 73");

		try {
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:181:2: ( 'byte' -> TYPE_BYTE )
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:181:4: 'byte'
			{
			string_literal79=(Token)match(input,73,FOLLOW_73_in_byteType857);  
			stream_73.add(string_literal79);

			// AST REWRITE
			// elements: 
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (Object)adaptor.nil();
			// 181:11: -> TYPE_BYTE
			{
				adaptor.addChild(root_0, (Object)adaptor.create(TYPE_BYTE, "TYPE_BYTE"));
			}


			retval.tree = root_0;

			}

			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "byteType"


	public static class longType_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "longType"
	// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:184:1: longType : 'long' -> TYPE_LONG ;
	public final IOMonkey2Parser.longType_return longType() throws RecognitionException {
		IOMonkey2Parser.longType_return retval = new IOMonkey2Parser.longType_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token string_literal80=null;

		Object string_literal80_tree=null;
		RewriteRuleTokenStream stream_78=new RewriteRuleTokenStream(adaptor,"token 78");

		try {
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:184:9: ( 'long' -> TYPE_LONG )
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:184:11: 'long'
			{
			string_literal80=(Token)match(input,78,FOLLOW_78_in_longType871);  
			stream_78.add(string_literal80);

			// AST REWRITE
			// elements: 
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (Object)adaptor.nil();
			// 184:18: -> TYPE_LONG
			{
				adaptor.addChild(root_0, (Object)adaptor.create(TYPE_LONG, "TYPE_LONG"));
			}


			retval.tree = root_0;

			}

			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "longType"


	public static class stringType_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "stringType"
	// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:187:1: stringType : 'string' -> TYPE_STRING ;
	public final IOMonkey2Parser.stringType_return stringType() throws RecognitionException {
		IOMonkey2Parser.stringType_return retval = new IOMonkey2Parser.stringType_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token string_literal81=null;

		Object string_literal81_tree=null;
		RewriteRuleTokenStream stream_84=new RewriteRuleTokenStream(adaptor,"token 84");

		try {
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:188:2: ( 'string' -> TYPE_STRING )
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:188:4: 'string'
			{
			string_literal81=(Token)match(input,84,FOLLOW_84_in_stringType886);  
			stream_84.add(string_literal81);

			// AST REWRITE
			// elements: 
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (Object)adaptor.nil();
			// 188:13: -> TYPE_STRING
			{
				adaptor.addChild(root_0, (Object)adaptor.create(TYPE_STRING, "TYPE_STRING"));
			}


			retval.tree = root_0;

			}

			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "stringType"


	public static class floatType_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "floatType"
	// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:191:1: floatType : 'float' -> TYPE_FLOAT ;
	public final IOMonkey2Parser.floatType_return floatType() throws RecognitionException {
		IOMonkey2Parser.floatType_return retval = new IOMonkey2Parser.floatType_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token string_literal82=null;

		Object string_literal82_tree=null;
		RewriteRuleTokenStream stream_75=new RewriteRuleTokenStream(adaptor,"token 75");

		try {
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:192:2: ( 'float' -> TYPE_FLOAT )
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:192:4: 'float'
			{
			string_literal82=(Token)match(input,75,FOLLOW_75_in_floatType902);  
			stream_75.add(string_literal82);

			// AST REWRITE
			// elements: 
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (Object)adaptor.nil();
			// 192:12: -> TYPE_FLOAT
			{
				adaptor.addChild(root_0, (Object)adaptor.create(TYPE_FLOAT, "TYPE_FLOAT"));
			}


			retval.tree = root_0;

			}

			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "floatType"


	public static class boolType_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "boolType"
	// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:195:1: boolType : 'bool' -> TYPE_BOOL ;
	public final IOMonkey2Parser.boolType_return boolType() throws RecognitionException {
		IOMonkey2Parser.boolType_return retval = new IOMonkey2Parser.boolType_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token string_literal83=null;

		Object string_literal83_tree=null;
		RewriteRuleTokenStream stream_72=new RewriteRuleTokenStream(adaptor,"token 72");

		try {
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:196:2: ( 'bool' -> TYPE_BOOL )
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:196:4: 'bool'
			{
			string_literal83=(Token)match(input,72,FOLLOW_72_in_boolType918);  
			stream_72.add(string_literal83);

			// AST REWRITE
			// elements: 
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (Object)adaptor.nil();
			// 196:11: -> TYPE_BOOL
			{
				adaptor.addChild(root_0, (Object)adaptor.create(TYPE_BOOL, "TYPE_BOOL"));
			}


			retval.tree = root_0;

			}

			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "boolType"


	public static class idType_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "idType"
	// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:199:1: idType : ID -> ^( TYPE_ID ID ) ;
	public final IOMonkey2Parser.idType_return idType() throws RecognitionException {
		IOMonkey2Parser.idType_return retval = new IOMonkey2Parser.idType_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token ID84=null;

		Object ID84_tree=null;
		RewriteRuleTokenStream stream_ID=new RewriteRuleTokenStream(adaptor,"token ID");

		try {
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:199:8: ( ID -> ^( TYPE_ID ID ) )
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:199:10: ID
			{
			ID84=(Token)match(input,ID,FOLLOW_ID_in_idType933);  
			stream_ID.add(ID84);

			// AST REWRITE
			// elements: ID
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (Object)adaptor.nil();
			// 199:13: -> ^( TYPE_ID ID )
			{
				// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:199:16: ^( TYPE_ID ID )
				{
				Object root_1 = (Object)adaptor.nil();
				root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(TYPE_ID, "TYPE_ID"), root_1);
				adaptor.addChild(root_1, stream_ID.nextNode());
				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;

			}

			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "idType"


	public static class arrayType_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "arrayType"
	// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:203:1: arrayType : ( fixedSizeArrayType | dynamicArrayType );
	public final IOMonkey2Parser.arrayType_return arrayType() throws RecognitionException {
		IOMonkey2Parser.arrayType_return retval = new IOMonkey2Parser.arrayType_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		ParserRuleReturnScope fixedSizeArrayType85 =null;
		ParserRuleReturnScope dynamicArrayType86 =null;


		try {
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:204:2: ( fixedSizeArrayType | dynamicArrayType )
			int alt26=2;
			switch ( input.LA(1) ) {
			case 77:
				{
				int LA26_1 = input.LA(2);
				if ( (LA26_1==69) ) {
					int LA26_9 = input.LA(3);
					if ( (LA26_9==70) ) {
						alt26=2;
					}
					else if ( (LA26_9==ID||LA26_9==NUM) ) {
						alt26=1;
					}

					else {
						int nvaeMark = input.mark();
						try {
							for (int nvaeConsume = 0; nvaeConsume < 3 - 1; nvaeConsume++) {
								input.consume();
							}
							NoViableAltException nvae =
								new NoViableAltException("", 26, 9, input);
							throw nvae;
						} finally {
							input.rewind(nvaeMark);
						}
					}

				}

				else {
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 26, 1, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

				}
				break;
			case 83:
				{
				int LA26_2 = input.LA(2);
				if ( (LA26_2==69) ) {
					int LA26_9 = input.LA(3);
					if ( (LA26_9==70) ) {
						alt26=2;
					}
					else if ( (LA26_9==ID||LA26_9==NUM) ) {
						alt26=1;
					}

					else {
						int nvaeMark = input.mark();
						try {
							for (int nvaeConsume = 0; nvaeConsume < 3 - 1; nvaeConsume++) {
								input.consume();
							}
							NoViableAltException nvae =
								new NoViableAltException("", 26, 9, input);
							throw nvae;
						} finally {
							input.rewind(nvaeMark);
						}
					}

				}

				else {
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 26, 2, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

				}
				break;
			case 73:
				{
				int LA26_3 = input.LA(2);
				if ( (LA26_3==69) ) {
					int LA26_9 = input.LA(3);
					if ( (LA26_9==70) ) {
						alt26=2;
					}
					else if ( (LA26_9==ID||LA26_9==NUM) ) {
						alt26=1;
					}

					else {
						int nvaeMark = input.mark();
						try {
							for (int nvaeConsume = 0; nvaeConsume < 3 - 1; nvaeConsume++) {
								input.consume();
							}
							NoViableAltException nvae =
								new NoViableAltException("", 26, 9, input);
							throw nvae;
						} finally {
							input.rewind(nvaeMark);
						}
					}

				}

				else {
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 26, 3, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

				}
				break;
			case 78:
				{
				int LA26_4 = input.LA(2);
				if ( (LA26_4==69) ) {
					int LA26_9 = input.LA(3);
					if ( (LA26_9==70) ) {
						alt26=2;
					}
					else if ( (LA26_9==ID||LA26_9==NUM) ) {
						alt26=1;
					}

					else {
						int nvaeMark = input.mark();
						try {
							for (int nvaeConsume = 0; nvaeConsume < 3 - 1; nvaeConsume++) {
								input.consume();
							}
							NoViableAltException nvae =
								new NoViableAltException("", 26, 9, input);
							throw nvae;
						} finally {
							input.rewind(nvaeMark);
						}
					}

				}

				else {
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 26, 4, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

				}
				break;
			case 84:
				{
				int LA26_5 = input.LA(2);
				if ( (LA26_5==69) ) {
					int LA26_9 = input.LA(3);
					if ( (LA26_9==70) ) {
						alt26=2;
					}
					else if ( (LA26_9==ID||LA26_9==NUM) ) {
						alt26=1;
					}

					else {
						int nvaeMark = input.mark();
						try {
							for (int nvaeConsume = 0; nvaeConsume < 3 - 1; nvaeConsume++) {
								input.consume();
							}
							NoViableAltException nvae =
								new NoViableAltException("", 26, 9, input);
							throw nvae;
						} finally {
							input.rewind(nvaeMark);
						}
					}

				}

				else {
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 26, 5, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

				}
				break;
			case 75:
				{
				int LA26_6 = input.LA(2);
				if ( (LA26_6==69) ) {
					int LA26_9 = input.LA(3);
					if ( (LA26_9==70) ) {
						alt26=2;
					}
					else if ( (LA26_9==ID||LA26_9==NUM) ) {
						alt26=1;
					}

					else {
						int nvaeMark = input.mark();
						try {
							for (int nvaeConsume = 0; nvaeConsume < 3 - 1; nvaeConsume++) {
								input.consume();
							}
							NoViableAltException nvae =
								new NoViableAltException("", 26, 9, input);
							throw nvae;
						} finally {
							input.rewind(nvaeMark);
						}
					}

				}

				else {
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 26, 6, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

				}
				break;
			case 72:
				{
				int LA26_7 = input.LA(2);
				if ( (LA26_7==69) ) {
					int LA26_9 = input.LA(3);
					if ( (LA26_9==70) ) {
						alt26=2;
					}
					else if ( (LA26_9==ID||LA26_9==NUM) ) {
						alt26=1;
					}

					else {
						int nvaeMark = input.mark();
						try {
							for (int nvaeConsume = 0; nvaeConsume < 3 - 1; nvaeConsume++) {
								input.consume();
							}
							NoViableAltException nvae =
								new NoViableAltException("", 26, 9, input);
							throw nvae;
						} finally {
							input.rewind(nvaeMark);
						}
					}

				}

				else {
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 26, 7, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

				}
				break;
			case ID:
				{
				int LA26_8 = input.LA(2);
				if ( (LA26_8==69) ) {
					int LA26_9 = input.LA(3);
					if ( (LA26_9==70) ) {
						alt26=2;
					}
					else if ( (LA26_9==ID||LA26_9==NUM) ) {
						alt26=1;
					}

					else {
						int nvaeMark = input.mark();
						try {
							for (int nvaeConsume = 0; nvaeConsume < 3 - 1; nvaeConsume++) {
								input.consume();
							}
							NoViableAltException nvae =
								new NoViableAltException("", 26, 9, input);
							throw nvae;
						} finally {
							input.rewind(nvaeMark);
						}
					}

				}

				else {
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 26, 8, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

				}
				break;
			default:
				NoViableAltException nvae =
					new NoViableAltException("", 26, 0, input);
				throw nvae;
			}
			switch (alt26) {
				case 1 :
					// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:204:4: fixedSizeArrayType
					{
					root_0 = (Object)adaptor.nil();


					pushFollow(FOLLOW_fixedSizeArrayType_in_arrayType953);
					fixedSizeArrayType85=fixedSizeArrayType();
					state._fsp--;

					adaptor.addChild(root_0, fixedSizeArrayType85.getTree());

					}
					break;
				case 2 :
					// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:205:4: dynamicArrayType
					{
					root_0 = (Object)adaptor.nil();


					pushFollow(FOLLOW_dynamicArrayType_in_arrayType958);
					dynamicArrayType86=dynamicArrayType();
					state._fsp--;

					adaptor.addChild(root_0, dynamicArrayType86.getTree());

					}
					break;

			}
			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "arrayType"


	public static class fixedSizeArrayType_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "fixedSizeArrayType"
	// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:208:1: fixedSizeArrayType : scalarType '[' fixedArraySize ']' -> ^( TYPE_FIXED_ARRAY scalarType fixedArraySize ) ;
	public final IOMonkey2Parser.fixedSizeArrayType_return fixedSizeArrayType() throws RecognitionException {
		IOMonkey2Parser.fixedSizeArrayType_return retval = new IOMonkey2Parser.fixedSizeArrayType_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token char_literal88=null;
		Token char_literal90=null;
		ParserRuleReturnScope scalarType87 =null;
		ParserRuleReturnScope fixedArraySize89 =null;

		Object char_literal88_tree=null;
		Object char_literal90_tree=null;
		RewriteRuleTokenStream stream_69=new RewriteRuleTokenStream(adaptor,"token 69");
		RewriteRuleTokenStream stream_70=new RewriteRuleTokenStream(adaptor,"token 70");
		RewriteRuleSubtreeStream stream_fixedArraySize=new RewriteRuleSubtreeStream(adaptor,"rule fixedArraySize");
		RewriteRuleSubtreeStream stream_scalarType=new RewriteRuleSubtreeStream(adaptor,"rule scalarType");

		try {
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:209:2: ( scalarType '[' fixedArraySize ']' -> ^( TYPE_FIXED_ARRAY scalarType fixedArraySize ) )
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:209:4: scalarType '[' fixedArraySize ']'
			{
			pushFollow(FOLLOW_scalarType_in_fixedSizeArrayType970);
			scalarType87=scalarType();
			state._fsp--;

			stream_scalarType.add(scalarType87.getTree());
			char_literal88=(Token)match(input,69,FOLLOW_69_in_fixedSizeArrayType972);  
			stream_69.add(char_literal88);

			pushFollow(FOLLOW_fixedArraySize_in_fixedSizeArrayType974);
			fixedArraySize89=fixedArraySize();
			state._fsp--;

			stream_fixedArraySize.add(fixedArraySize89.getTree());
			char_literal90=(Token)match(input,70,FOLLOW_70_in_fixedSizeArrayType976);  
			stream_70.add(char_literal90);

			// AST REWRITE
			// elements: fixedArraySize, scalarType
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (Object)adaptor.nil();
			// 209:38: -> ^( TYPE_FIXED_ARRAY scalarType fixedArraySize )
			{
				// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:209:41: ^( TYPE_FIXED_ARRAY scalarType fixedArraySize )
				{
				Object root_1 = (Object)adaptor.nil();
				root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(TYPE_FIXED_ARRAY, "TYPE_FIXED_ARRAY"), root_1);
				adaptor.addChild(root_1, stream_scalarType.nextTree());
				adaptor.addChild(root_1, stream_fixedArraySize.nextTree());
				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;

			}

			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "fixedSizeArrayType"


	public static class fixedArraySize_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "fixedArraySize"
	// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:212:1: fixedArraySize : ( NUM -> ^( SIZE_INT NUM ) | ID '.' ID -> ^( SIZE_SYMBOL_REF ID ID ) );
	public final IOMonkey2Parser.fixedArraySize_return fixedArraySize() throws RecognitionException {
		IOMonkey2Parser.fixedArraySize_return retval = new IOMonkey2Parser.fixedArraySize_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token NUM91=null;
		Token ID92=null;
		Token char_literal93=null;
		Token ID94=null;

		Object NUM91_tree=null;
		Object ID92_tree=null;
		Object char_literal93_tree=null;
		Object ID94_tree=null;
		RewriteRuleTokenStream stream_NUM=new RewriteRuleTokenStream(adaptor,"token NUM");
		RewriteRuleTokenStream stream_ID=new RewriteRuleTokenStream(adaptor,"token ID");
		RewriteRuleTokenStream stream_64=new RewriteRuleTokenStream(adaptor,"token 64");

		try {
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:213:5: ( NUM -> ^( SIZE_INT NUM ) | ID '.' ID -> ^( SIZE_SYMBOL_REF ID ID ) )
			int alt27=2;
			int LA27_0 = input.LA(1);
			if ( (LA27_0==NUM) ) {
				alt27=1;
			}
			else if ( (LA27_0==ID) ) {
				alt27=2;
			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 27, 0, input);
				throw nvae;
			}

			switch (alt27) {
				case 1 :
					// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:213:9: NUM
					{
					NUM91=(Token)match(input,NUM,FOLLOW_NUM_in_fixedArraySize1002);  
					stream_NUM.add(NUM91);

					// AST REWRITE
					// elements: NUM
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 213:13: -> ^( SIZE_INT NUM )
					{
						// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:213:16: ^( SIZE_INT NUM )
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(SIZE_INT, "SIZE_INT"), root_1);
						adaptor.addChild(root_1, stream_NUM.nextNode());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;

					}
					break;
				case 2 :
					// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:214:9: ID '.' ID
					{
					ID92=(Token)match(input,ID,FOLLOW_ID_in_fixedArraySize1020);  
					stream_ID.add(ID92);

					char_literal93=(Token)match(input,64,FOLLOW_64_in_fixedArraySize1022);  
					stream_64.add(char_literal93);

					ID94=(Token)match(input,ID,FOLLOW_ID_in_fixedArraySize1024);  
					stream_ID.add(ID94);

					// AST REWRITE
					// elements: ID, ID
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 214:19: -> ^( SIZE_SYMBOL_REF ID ID )
					{
						// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:214:22: ^( SIZE_SYMBOL_REF ID ID )
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(SIZE_SYMBOL_REF, "SIZE_SYMBOL_REF"), root_1);
						adaptor.addChild(root_1, stream_ID.nextNode());
						adaptor.addChild(root_1, stream_ID.nextNode());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;

					}
					break;

			}
			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "fixedArraySize"


	public static class dynamicArrayType_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "dynamicArrayType"
	// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:217:1: dynamicArrayType : scalarType '[' ']' -> ^( TYPE_DYNAMIC_ARRAY scalarType ) ;
	public final IOMonkey2Parser.dynamicArrayType_return dynamicArrayType() throws RecognitionException {
		IOMonkey2Parser.dynamicArrayType_return retval = new IOMonkey2Parser.dynamicArrayType_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token char_literal96=null;
		Token char_literal97=null;
		ParserRuleReturnScope scalarType95 =null;

		Object char_literal96_tree=null;
		Object char_literal97_tree=null;
		RewriteRuleTokenStream stream_69=new RewriteRuleTokenStream(adaptor,"token 69");
		RewriteRuleTokenStream stream_70=new RewriteRuleTokenStream(adaptor,"token 70");
		RewriteRuleSubtreeStream stream_scalarType=new RewriteRuleSubtreeStream(adaptor,"rule scalarType");

		try {
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:218:2: ( scalarType '[' ']' -> ^( TYPE_DYNAMIC_ARRAY scalarType ) )
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:218:4: scalarType '[' ']'
			{
			pushFollow(FOLLOW_scalarType_in_dynamicArrayType1049);
			scalarType95=scalarType();
			state._fsp--;

			stream_scalarType.add(scalarType95.getTree());
			char_literal96=(Token)match(input,69,FOLLOW_69_in_dynamicArrayType1051);  
			stream_69.add(char_literal96);

			char_literal97=(Token)match(input,70,FOLLOW_70_in_dynamicArrayType1053);  
			stream_70.add(char_literal97);

			// AST REWRITE
			// elements: scalarType
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (Object)adaptor.nil();
			// 218:23: -> ^( TYPE_DYNAMIC_ARRAY scalarType )
			{
				// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:218:26: ^( TYPE_DYNAMIC_ARRAY scalarType )
				{
				Object root_1 = (Object)adaptor.nil();
				root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(TYPE_DYNAMIC_ARRAY, "TYPE_DYNAMIC_ARRAY"), root_1);
				adaptor.addChild(root_1, stream_scalarType.nextTree());
				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;

			}

			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "dynamicArrayType"


	public static class attributeSlot_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "attributeSlot"
	// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:221:1: attributeSlot : attributeSlotType ( '(' NUM ')' )? -> ^( attributeSlotType ( NUM )? ) ;
	public final IOMonkey2Parser.attributeSlot_return attributeSlot() throws RecognitionException {
		IOMonkey2Parser.attributeSlot_return retval = new IOMonkey2Parser.attributeSlot_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token char_literal99=null;
		Token NUM100=null;
		Token char_literal101=null;
		ParserRuleReturnScope attributeSlotType98 =null;

		Object char_literal99_tree=null;
		Object NUM100_tree=null;
		Object char_literal101_tree=null;
		RewriteRuleTokenStream stream_59=new RewriteRuleTokenStream(adaptor,"token 59");
		RewriteRuleTokenStream stream_NUM=new RewriteRuleTokenStream(adaptor,"token NUM");
		RewriteRuleTokenStream stream_60=new RewriteRuleTokenStream(adaptor,"token 60");
		RewriteRuleSubtreeStream stream_attributeSlotType=new RewriteRuleSubtreeStream(adaptor,"rule attributeSlotType");

		try {
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:222:2: ( attributeSlotType ( '(' NUM ')' )? -> ^( attributeSlotType ( NUM )? ) )
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:222:4: attributeSlotType ( '(' NUM ')' )?
			{
			pushFollow(FOLLOW_attributeSlotType_in_attributeSlot1073);
			attributeSlotType98=attributeSlotType();
			state._fsp--;

			stream_attributeSlotType.add(attributeSlotType98.getTree());
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:222:22: ( '(' NUM ')' )?
			int alt28=2;
			int LA28_0 = input.LA(1);
			if ( (LA28_0==59) ) {
				alt28=1;
			}
			switch (alt28) {
				case 1 :
					// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:222:24: '(' NUM ')'
					{
					char_literal99=(Token)match(input,59,FOLLOW_59_in_attributeSlot1077);  
					stream_59.add(char_literal99);

					NUM100=(Token)match(input,NUM,FOLLOW_NUM_in_attributeSlot1079);  
					stream_NUM.add(NUM100);

					char_literal101=(Token)match(input,60,FOLLOW_60_in_attributeSlot1081);  
					stream_60.add(char_literal101);

					}
					break;

			}

			// AST REWRITE
			// elements: NUM, attributeSlotType
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (Object)adaptor.nil();
			// 222:39: -> ^( attributeSlotType ( NUM )? )
			{
				// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:222:42: ^( attributeSlotType ( NUM )? )
				{
				Object root_1 = (Object)adaptor.nil();
				root_1 = (Object)adaptor.becomeRoot(stream_attributeSlotType.nextNode(), root_1);
				// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:222:62: ( NUM )?
				if ( stream_NUM.hasNext() ) {
					adaptor.addChild(root_1, stream_NUM.nextNode());
				}
				stream_NUM.reset();

				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;

			}

			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "attributeSlot"


	public static class attributeSlotType_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "attributeSlotType"
	// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:225:1: attributeSlotType : ( 'required' -> SLOT_REQUIRED | 'optional' -> SLOT_OPTIONAL );
	public final IOMonkey2Parser.attributeSlotType_return attributeSlotType() throws RecognitionException {
		IOMonkey2Parser.attributeSlotType_return retval = new IOMonkey2Parser.attributeSlotType_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token string_literal102=null;
		Token string_literal103=null;

		Object string_literal102_tree=null;
		Object string_literal103_tree=null;
		RewriteRuleTokenStream stream_79=new RewriteRuleTokenStream(adaptor,"token 79");
		RewriteRuleTokenStream stream_82=new RewriteRuleTokenStream(adaptor,"token 82");

		try {
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:226:2: ( 'required' -> SLOT_REQUIRED | 'optional' -> SLOT_OPTIONAL )
			int alt29=2;
			int LA29_0 = input.LA(1);
			if ( (LA29_0==82) ) {
				alt29=1;
			}
			else if ( (LA29_0==79) ) {
				alt29=2;
			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 29, 0, input);
				throw nvae;
			}

			switch (alt29) {
				case 1 :
					// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:226:4: 'required'
					{
					string_literal102=(Token)match(input,82,FOLLOW_82_in_attributeSlotType1105);  
					stream_82.add(string_literal102);

					// AST REWRITE
					// elements: 
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 226:15: -> SLOT_REQUIRED
					{
						adaptor.addChild(root_0, (Object)adaptor.create(SLOT_REQUIRED, "SLOT_REQUIRED"));
					}


					retval.tree = root_0;

					}
					break;
				case 2 :
					// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:227:4: 'optional'
					{
					string_literal103=(Token)match(input,79,FOLLOW_79_in_attributeSlotType1114);  
					stream_79.add(string_literal103);

					// AST REWRITE
					// elements: 
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 227:15: -> SLOT_OPTIONAL
					{
						adaptor.addChild(root_0, (Object)adaptor.create(SLOT_OPTIONAL, "SLOT_OPTIONAL"));
					}


					retval.tree = root_0;

					}
					break;

			}
			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "attributeSlotType"


	public static class attributeDefaultValueSet_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "attributeDefaultValueSet"
	// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:230:1: attributeDefaultValueSet : '=' attributeValue -> ^( IMPLICIT_VALUE attributeValue ) ;
	public final IOMonkey2Parser.attributeDefaultValueSet_return attributeDefaultValueSet() throws RecognitionException {
		IOMonkey2Parser.attributeDefaultValueSet_return retval = new IOMonkey2Parser.attributeDefaultValueSet_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token char_literal104=null;
		ParserRuleReturnScope attributeValue105 =null;

		Object char_literal104_tree=null;
		RewriteRuleTokenStream stream_67=new RewriteRuleTokenStream(adaptor,"token 67");
		RewriteRuleSubtreeStream stream_attributeValue=new RewriteRuleSubtreeStream(adaptor,"rule attributeValue");

		try {
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:231:2: ( '=' attributeValue -> ^( IMPLICIT_VALUE attributeValue ) )
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:231:4: '=' attributeValue
			{
			char_literal104=(Token)match(input,67,FOLLOW_67_in_attributeDefaultValueSet1130);  
			stream_67.add(char_literal104);

			pushFollow(FOLLOW_attributeValue_in_attributeDefaultValueSet1132);
			attributeValue105=attributeValue();
			state._fsp--;

			stream_attributeValue.add(attributeValue105.getTree());
			// AST REWRITE
			// elements: attributeValue
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (Object)adaptor.nil();
			// 231:23: -> ^( IMPLICIT_VALUE attributeValue )
			{
				// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:231:26: ^( IMPLICIT_VALUE attributeValue )
				{
				Object root_1 = (Object)adaptor.nil();
				root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(IMPLICIT_VALUE, "IMPLICIT_VALUE"), root_1);
				adaptor.addChild(root_1, stream_attributeValue.nextTree());
				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;

			}

			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "attributeDefaultValueSet"


	public static class attributeValue_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "attributeValue"
	// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:234:1: attributeValue : ( intValue | floatValue | stringValue | enumConstValue | nullValue | boolValue );
	public final IOMonkey2Parser.attributeValue_return attributeValue() throws RecognitionException {
		IOMonkey2Parser.attributeValue_return retval = new IOMonkey2Parser.attributeValue_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		ParserRuleReturnScope intValue106 =null;
		ParserRuleReturnScope floatValue107 =null;
		ParserRuleReturnScope stringValue108 =null;
		ParserRuleReturnScope enumConstValue109 =null;
		ParserRuleReturnScope nullValue110 =null;
		ParserRuleReturnScope boolValue111 =null;


		try {
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:235:2: ( intValue | floatValue | stringValue | enumConstValue | nullValue | boolValue )
			int alt30=6;
			switch ( input.LA(1) ) {
			case HEX_NUM:
			case NUM:
			case 61:
			case 63:
				{
				alt30=1;
				}
				break;
			case FLOAT:
				{
				alt30=2;
				}
				break;
			case STRING:
				{
				alt30=3;
				}
				break;
			case ID:
				{
				alt30=4;
				}
				break;
			case NULL:
				{
				alt30=5;
				}
				break;
			case BOOL_:
				{
				alt30=6;
				}
				break;
			default:
				NoViableAltException nvae =
					new NoViableAltException("", 30, 0, input);
				throw nvae;
			}
			switch (alt30) {
				case 1 :
					// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:235:4: intValue
					{
					root_0 = (Object)adaptor.nil();


					pushFollow(FOLLOW_intValue_in_attributeValue1152);
					intValue106=intValue();
					state._fsp--;

					adaptor.addChild(root_0, intValue106.getTree());

					}
					break;
				case 2 :
					// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:235:15: floatValue
					{
					root_0 = (Object)adaptor.nil();


					pushFollow(FOLLOW_floatValue_in_attributeValue1156);
					floatValue107=floatValue();
					state._fsp--;

					adaptor.addChild(root_0, floatValue107.getTree());

					}
					break;
				case 3 :
					// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:235:28: stringValue
					{
					root_0 = (Object)adaptor.nil();


					pushFollow(FOLLOW_stringValue_in_attributeValue1160);
					stringValue108=stringValue();
					state._fsp--;

					adaptor.addChild(root_0, stringValue108.getTree());

					}
					break;
				case 4 :
					// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:235:42: enumConstValue
					{
					root_0 = (Object)adaptor.nil();


					pushFollow(FOLLOW_enumConstValue_in_attributeValue1164);
					enumConstValue109=enumConstValue();
					state._fsp--;

					adaptor.addChild(root_0, enumConstValue109.getTree());

					}
					break;
				case 5 :
					// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:235:59: nullValue
					{
					root_0 = (Object)adaptor.nil();


					pushFollow(FOLLOW_nullValue_in_attributeValue1168);
					nullValue110=nullValue();
					state._fsp--;

					adaptor.addChild(root_0, nullValue110.getTree());

					}
					break;
				case 6 :
					// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:235:71: boolValue
					{
					root_0 = (Object)adaptor.nil();


					pushFollow(FOLLOW_boolValue_in_attributeValue1172);
					boolValue111=boolValue();
					state._fsp--;

					adaptor.addChild(root_0, boolValue111.getTree());

					}
					break;

			}
			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "attributeValue"


	public static class enumConstValue_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "enumConstValue"
	// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:238:1: enumConstValue : ID -> ^( VALUE_ENUM_CONST ID ) ;
	public final IOMonkey2Parser.enumConstValue_return enumConstValue() throws RecognitionException {
		IOMonkey2Parser.enumConstValue_return retval = new IOMonkey2Parser.enumConstValue_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token ID112=null;

		Object ID112_tree=null;
		RewriteRuleTokenStream stream_ID=new RewriteRuleTokenStream(adaptor,"token ID");

		try {
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:239:2: ( ID -> ^( VALUE_ENUM_CONST ID ) )
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:239:4: ID
			{
			ID112=(Token)match(input,ID,FOLLOW_ID_in_enumConstValue1183);  
			stream_ID.add(ID112);

			// AST REWRITE
			// elements: ID
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (Object)adaptor.nil();
			// 239:7: -> ^( VALUE_ENUM_CONST ID )
			{
				// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:239:10: ^( VALUE_ENUM_CONST ID )
				{
				Object root_1 = (Object)adaptor.nil();
				root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(VALUE_ENUM_CONST, "VALUE_ENUM_CONST"), root_1);
				adaptor.addChild(root_1, stream_ID.nextNode());
				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;

			}

			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "enumConstValue"


	public static class enumConstRefValue_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "enumConstRefValue"
	// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:242:1: enumConstRefValue : ID '.' ID -> ^( VALUE_ENUM_CONST_REF ID ID ) ;
	public final IOMonkey2Parser.enumConstRefValue_return enumConstRefValue() throws RecognitionException {
		IOMonkey2Parser.enumConstRefValue_return retval = new IOMonkey2Parser.enumConstRefValue_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token ID113=null;
		Token char_literal114=null;
		Token ID115=null;

		Object ID113_tree=null;
		Object char_literal114_tree=null;
		Object ID115_tree=null;
		RewriteRuleTokenStream stream_ID=new RewriteRuleTokenStream(adaptor,"token ID");
		RewriteRuleTokenStream stream_64=new RewriteRuleTokenStream(adaptor,"token 64");

		try {
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:243:2: ( ID '.' ID -> ^( VALUE_ENUM_CONST_REF ID ID ) )
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:243:4: ID '.' ID
			{
			ID113=(Token)match(input,ID,FOLLOW_ID_in_enumConstRefValue1203);  
			stream_ID.add(ID113);

			char_literal114=(Token)match(input,64,FOLLOW_64_in_enumConstRefValue1205);  
			stream_64.add(char_literal114);

			ID115=(Token)match(input,ID,FOLLOW_ID_in_enumConstRefValue1207);  
			stream_ID.add(ID115);

			// AST REWRITE
			// elements: ID, ID
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (Object)adaptor.nil();
			// 243:14: -> ^( VALUE_ENUM_CONST_REF ID ID )
			{
				// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:243:17: ^( VALUE_ENUM_CONST_REF ID ID )
				{
				Object root_1 = (Object)adaptor.nil();
				root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(VALUE_ENUM_CONST_REF, "VALUE_ENUM_CONST_REF"), root_1);
				adaptor.addChild(root_1, stream_ID.nextNode());
				adaptor.addChild(root_1, stream_ID.nextNode());
				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;

			}

			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "enumConstRefValue"


	public static class stringValue_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "stringValue"
	// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:246:1: stringValue : STRING -> ^( VALUE_STRING STRING ) ;
	public final IOMonkey2Parser.stringValue_return stringValue() throws RecognitionException {
		IOMonkey2Parser.stringValue_return retval = new IOMonkey2Parser.stringValue_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token STRING116=null;

		Object STRING116_tree=null;
		RewriteRuleTokenStream stream_STRING=new RewriteRuleTokenStream(adaptor,"token STRING");

		try {
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:247:2: ( STRING -> ^( VALUE_STRING STRING ) )
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:247:4: STRING
			{
			STRING116=(Token)match(input,STRING,FOLLOW_STRING_in_stringValue1230);  
			stream_STRING.add(STRING116);

			// AST REWRITE
			// elements: STRING
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (Object)adaptor.nil();
			// 247:11: -> ^( VALUE_STRING STRING )
			{
				// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:247:14: ^( VALUE_STRING STRING )
				{
				Object root_1 = (Object)adaptor.nil();
				root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(VALUE_STRING, "VALUE_STRING"), root_1);
				adaptor.addChild(root_1, stream_STRING.nextNode());
				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;

			}

			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "stringValue"


	public static class floatValue_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "floatValue"
	// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:250:1: floatValue : FLOAT -> ^( VALUE_FLOAT FLOAT ) ;
	public final IOMonkey2Parser.floatValue_return floatValue() throws RecognitionException {
		IOMonkey2Parser.floatValue_return retval = new IOMonkey2Parser.floatValue_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token FLOAT117=null;

		Object FLOAT117_tree=null;
		RewriteRuleTokenStream stream_FLOAT=new RewriteRuleTokenStream(adaptor,"token FLOAT");

		try {
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:251:2: ( FLOAT -> ^( VALUE_FLOAT FLOAT ) )
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:251:4: FLOAT
			{
			FLOAT117=(Token)match(input,FLOAT,FOLLOW_FLOAT_in_floatValue1251);  
			stream_FLOAT.add(FLOAT117);

			// AST REWRITE
			// elements: FLOAT
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (Object)adaptor.nil();
			// 251:10: -> ^( VALUE_FLOAT FLOAT )
			{
				// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:251:13: ^( VALUE_FLOAT FLOAT )
				{
				Object root_1 = (Object)adaptor.nil();
				root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(VALUE_FLOAT, "VALUE_FLOAT"), root_1);
				adaptor.addChild(root_1, stream_FLOAT.nextNode());
				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;

			}

			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "floatValue"


	public static class boolValue_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "boolValue"
	// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:254:1: boolValue : BOOL_ -> ^( VALUE_BOOL BOOL_ ) ;
	public final IOMonkey2Parser.boolValue_return boolValue() throws RecognitionException {
		IOMonkey2Parser.boolValue_return retval = new IOMonkey2Parser.boolValue_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token BOOL_118=null;

		Object BOOL_118_tree=null;
		RewriteRuleTokenStream stream_BOOL_=new RewriteRuleTokenStream(adaptor,"token BOOL_");

		try {
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:255:2: ( BOOL_ -> ^( VALUE_BOOL BOOL_ ) )
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:255:4: BOOL_
			{
			BOOL_118=(Token)match(input,BOOL_,FOLLOW_BOOL__in_boolValue1271);  
			stream_BOOL_.add(BOOL_118);

			// AST REWRITE
			// elements: BOOL_
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (Object)adaptor.nil();
			// 255:10: -> ^( VALUE_BOOL BOOL_ )
			{
				// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:255:13: ^( VALUE_BOOL BOOL_ )
				{
				Object root_1 = (Object)adaptor.nil();
				root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(VALUE_BOOL, "VALUE_BOOL"), root_1);
				adaptor.addChild(root_1, stream_BOOL_.nextNode());
				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;

			}

			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "boolValue"


	public static class intValue_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "intValue"
	// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:258:1: intValue : ( decimalIntValue -> ^( VALUE_INT decimalIntValue ) | hexadecimalIntValue -> ^( VALUE_INT hexadecimalIntValue ) );
	public final IOMonkey2Parser.intValue_return intValue() throws RecognitionException {
		IOMonkey2Parser.intValue_return retval = new IOMonkey2Parser.intValue_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		ParserRuleReturnScope decimalIntValue119 =null;
		ParserRuleReturnScope hexadecimalIntValue120 =null;

		RewriteRuleSubtreeStream stream_decimalIntValue=new RewriteRuleSubtreeStream(adaptor,"rule decimalIntValue");
		RewriteRuleSubtreeStream stream_hexadecimalIntValue=new RewriteRuleSubtreeStream(adaptor,"rule hexadecimalIntValue");

		try {
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:259:2: ( decimalIntValue -> ^( VALUE_INT decimalIntValue ) | hexadecimalIntValue -> ^( VALUE_INT hexadecimalIntValue ) )
			int alt31=2;
			switch ( input.LA(1) ) {
			case 61:
			case 63:
				{
				int LA31_1 = input.LA(2);
				if ( (LA31_1==NUM) ) {
					alt31=1;
				}
				else if ( (LA31_1==HEX_NUM) ) {
					alt31=2;
				}

				else {
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 31, 1, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

				}
				break;
			case NUM:
				{
				alt31=1;
				}
				break;
			case HEX_NUM:
				{
				alt31=2;
				}
				break;
			default:
				NoViableAltException nvae =
					new NoViableAltException("", 31, 0, input);
				throw nvae;
			}
			switch (alt31) {
				case 1 :
					// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:259:4: decimalIntValue
					{
					pushFollow(FOLLOW_decimalIntValue_in_intValue1290);
					decimalIntValue119=decimalIntValue();
					state._fsp--;

					stream_decimalIntValue.add(decimalIntValue119.getTree());
					// AST REWRITE
					// elements: decimalIntValue
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 259:24: -> ^( VALUE_INT decimalIntValue )
					{
						// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:259:27: ^( VALUE_INT decimalIntValue )
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(VALUE_INT, "VALUE_INT"), root_1);
						adaptor.addChild(root_1, stream_decimalIntValue.nextTree());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;

					}
					break;
				case 2 :
					// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:260:4: hexadecimalIntValue
					{
					pushFollow(FOLLOW_hexadecimalIntValue_in_intValue1307);
					hexadecimalIntValue120=hexadecimalIntValue();
					state._fsp--;

					stream_hexadecimalIntValue.add(hexadecimalIntValue120.getTree());
					// AST REWRITE
					// elements: hexadecimalIntValue
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (Object)adaptor.nil();
					// 260:24: -> ^( VALUE_INT hexadecimalIntValue )
					{
						// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:260:27: ^( VALUE_INT hexadecimalIntValue )
						{
						Object root_1 = (Object)adaptor.nil();
						root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(VALUE_INT, "VALUE_INT"), root_1);
						adaptor.addChild(root_1, stream_hexadecimalIntValue.nextTree());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;

					}
					break;

			}
			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "intValue"


	public static class decimalIntValue_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "decimalIntValue"
	// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:263:1: decimalIntValue : ( '+' | '-' )? NUM ;
	public final IOMonkey2Parser.decimalIntValue_return decimalIntValue() throws RecognitionException {
		IOMonkey2Parser.decimalIntValue_return retval = new IOMonkey2Parser.decimalIntValue_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token set121=null;
		Token NUM122=null;

		Object set121_tree=null;
		Object NUM122_tree=null;

		try {
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:264:2: ( ( '+' | '-' )? NUM )
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:264:4: ( '+' | '-' )? NUM
			{
			root_0 = (Object)adaptor.nil();


			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:264:4: ( '+' | '-' )?
			int alt32=2;
			int LA32_0 = input.LA(1);
			if ( (LA32_0==61||LA32_0==63) ) {
				alt32=1;
			}
			switch (alt32) {
				case 1 :
					// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:
					{
					set121=input.LT(1);
					if ( input.LA(1)==61||input.LA(1)==63 ) {
						input.consume();
						adaptor.addChild(root_0, (Object)adaptor.create(set121));
						state.errorRecovery=false;
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						throw mse;
					}
					}
					break;

			}

			NUM122=(Token)match(input,NUM,FOLLOW_NUM_in_decimalIntValue1337); 
			NUM122_tree = (Object)adaptor.create(NUM122);
			adaptor.addChild(root_0, NUM122_tree);

			}

			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "decimalIntValue"


	public static class hexadecimalIntValue_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "hexadecimalIntValue"
	// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:267:1: hexadecimalIntValue : ( '+' | '-' )? HEX_NUM ;
	public final IOMonkey2Parser.hexadecimalIntValue_return hexadecimalIntValue() throws RecognitionException {
		IOMonkey2Parser.hexadecimalIntValue_return retval = new IOMonkey2Parser.hexadecimalIntValue_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token set123=null;
		Token HEX_NUM124=null;

		Object set123_tree=null;
		Object HEX_NUM124_tree=null;

		try {
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:268:2: ( ( '+' | '-' )? HEX_NUM )
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:268:4: ( '+' | '-' )? HEX_NUM
			{
			root_0 = (Object)adaptor.nil();


			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:268:4: ( '+' | '-' )?
			int alt33=2;
			int LA33_0 = input.LA(1);
			if ( (LA33_0==61||LA33_0==63) ) {
				alt33=1;
			}
			switch (alt33) {
				case 1 :
					// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:
					{
					set123=input.LT(1);
					if ( input.LA(1)==61||input.LA(1)==63 ) {
						input.consume();
						adaptor.addChild(root_0, (Object)adaptor.create(set123));
						state.errorRecovery=false;
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						throw mse;
					}
					}
					break;

			}

			HEX_NUM124=(Token)match(input,HEX_NUM,FOLLOW_HEX_NUM_in_hexadecimalIntValue1358); 
			HEX_NUM124_tree = (Object)adaptor.create(HEX_NUM124);
			adaptor.addChild(root_0, HEX_NUM124_tree);

			}

			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "hexadecimalIntValue"


	public static class nullValue_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "nullValue"
	// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:271:1: nullValue : NULL -> ^( VALUE_NULL ) ;
	public final IOMonkey2Parser.nullValue_return nullValue() throws RecognitionException {
		IOMonkey2Parser.nullValue_return retval = new IOMonkey2Parser.nullValue_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token NULL125=null;

		Object NULL125_tree=null;
		RewriteRuleTokenStream stream_NULL=new RewriteRuleTokenStream(adaptor,"token NULL");

		try {
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:272:2: ( NULL -> ^( VALUE_NULL ) )
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:272:4: NULL
			{
			NULL125=(Token)match(input,NULL,FOLLOW_NULL_in_nullValue1370);  
			stream_NULL.add(NULL125);

			// AST REWRITE
			// elements: 
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (Object)adaptor.nil();
			// 272:9: -> ^( VALUE_NULL )
			{
				// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:272:12: ^( VALUE_NULL )
				{
				Object root_1 = (Object)adaptor.nil();
				root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(VALUE_NULL, "VALUE_NULL"), root_1);
				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;

			}

			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "nullValue"


	public static class annotations_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "annotations"
	// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:276:1: annotations : ( annotationDef )+ -> ^( ANNOTATIONS ( annotationDef )+ ) ;
	public final IOMonkey2Parser.annotations_return annotations() throws RecognitionException {
		IOMonkey2Parser.annotations_return retval = new IOMonkey2Parser.annotations_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		ParserRuleReturnScope annotationDef126 =null;

		RewriteRuleSubtreeStream stream_annotationDef=new RewriteRuleSubtreeStream(adaptor,"rule annotationDef");

		try {
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:277:2: ( ( annotationDef )+ -> ^( ANNOTATIONS ( annotationDef )+ ) )
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:277:4: ( annotationDef )+
			{
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:277:4: ( annotationDef )+
			int cnt34=0;
			loop34:
			while (true) {
				int alt34=2;
				int LA34_0 = input.LA(1);
				if ( (LA34_0==68) ) {
					alt34=1;
				}

				switch (alt34) {
				case 1 :
					// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:277:4: annotationDef
					{
					pushFollow(FOLLOW_annotationDef_in_annotations1391);
					annotationDef126=annotationDef();
					state._fsp--;

					stream_annotationDef.add(annotationDef126.getTree());
					}
					break;

				default :
					if ( cnt34 >= 1 ) break loop34;
					EarlyExitException eee = new EarlyExitException(34, input);
					throw eee;
				}
				cnt34++;
			}

			// AST REWRITE
			// elements: annotationDef
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (Object)adaptor.nil();
			// 277:19: -> ^( ANNOTATIONS ( annotationDef )+ )
			{
				// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:277:22: ^( ANNOTATIONS ( annotationDef )+ )
				{
				Object root_1 = (Object)adaptor.nil();
				root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(ANNOTATIONS, "ANNOTATIONS"), root_1);
				if ( !(stream_annotationDef.hasNext()) ) {
					throw new RewriteEarlyExitException();
				}
				while ( stream_annotationDef.hasNext() ) {
					adaptor.addChild(root_1, stream_annotationDef.nextTree());
				}
				stream_annotationDef.reset();

				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;

			}

			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "annotations"


	public static class annotationDef_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "annotationDef"
	// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:279:1: annotationDef : '@' ID ( '(' annotationParam ( ',' annotationParam )* ')' )? -> ^( ID ( annotationParam )* ) ;
	public final IOMonkey2Parser.annotationDef_return annotationDef() throws RecognitionException {
		IOMonkey2Parser.annotationDef_return retval = new IOMonkey2Parser.annotationDef_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token char_literal127=null;
		Token ID128=null;
		Token char_literal129=null;
		Token char_literal131=null;
		Token char_literal133=null;
		ParserRuleReturnScope annotationParam130 =null;
		ParserRuleReturnScope annotationParam132 =null;

		Object char_literal127_tree=null;
		Object ID128_tree=null;
		Object char_literal129_tree=null;
		Object char_literal131_tree=null;
		Object char_literal133_tree=null;
		RewriteRuleTokenStream stream_68=new RewriteRuleTokenStream(adaptor,"token 68");
		RewriteRuleTokenStream stream_59=new RewriteRuleTokenStream(adaptor,"token 59");
		RewriteRuleTokenStream stream_60=new RewriteRuleTokenStream(adaptor,"token 60");
		RewriteRuleTokenStream stream_ID=new RewriteRuleTokenStream(adaptor,"token ID");
		RewriteRuleTokenStream stream_62=new RewriteRuleTokenStream(adaptor,"token 62");
		RewriteRuleSubtreeStream stream_annotationParam=new RewriteRuleSubtreeStream(adaptor,"rule annotationParam");

		try {
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:280:2: ( '@' ID ( '(' annotationParam ( ',' annotationParam )* ')' )? -> ^( ID ( annotationParam )* ) )
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:280:4: '@' ID ( '(' annotationParam ( ',' annotationParam )* ')' )?
			{
			char_literal127=(Token)match(input,68,FOLLOW_68_in_annotationDef1411);  
			stream_68.add(char_literal127);

			ID128=(Token)match(input,ID,FOLLOW_ID_in_annotationDef1413);  
			stream_ID.add(ID128);

			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:280:11: ( '(' annotationParam ( ',' annotationParam )* ')' )?
			int alt36=2;
			int LA36_0 = input.LA(1);
			if ( (LA36_0==59) ) {
				alt36=1;
			}
			switch (alt36) {
				case 1 :
					// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:280:13: '(' annotationParam ( ',' annotationParam )* ')'
					{
					char_literal129=(Token)match(input,59,FOLLOW_59_in_annotationDef1417);  
					stream_59.add(char_literal129);

					pushFollow(FOLLOW_annotationParam_in_annotationDef1419);
					annotationParam130=annotationParam();
					state._fsp--;

					stream_annotationParam.add(annotationParam130.getTree());
					// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:280:33: ( ',' annotationParam )*
					loop35:
					while (true) {
						int alt35=2;
						int LA35_0 = input.LA(1);
						if ( (LA35_0==62) ) {
							alt35=1;
						}

						switch (alt35) {
						case 1 :
							// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:280:34: ',' annotationParam
							{
							char_literal131=(Token)match(input,62,FOLLOW_62_in_annotationDef1422);  
							stream_62.add(char_literal131);

							pushFollow(FOLLOW_annotationParam_in_annotationDef1424);
							annotationParam132=annotationParam();
							state._fsp--;

							stream_annotationParam.add(annotationParam132.getTree());
							}
							break;

						default :
							break loop35;
						}
					}

					char_literal133=(Token)match(input,60,FOLLOW_60_in_annotationDef1431);  
					stream_60.add(char_literal133);

					}
					break;

			}

			// AST REWRITE
			// elements: ID, annotationParam
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (Object)adaptor.nil();
			// 280:67: -> ^( ID ( annotationParam )* )
			{
				// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:280:70: ^( ID ( annotationParam )* )
				{
				Object root_1 = (Object)adaptor.nil();
				root_1 = (Object)adaptor.becomeRoot(stream_ID.nextNode(), root_1);
				// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:280:75: ( annotationParam )*
				while ( stream_annotationParam.hasNext() ) {
					adaptor.addChild(root_1, stream_annotationParam.nextTree());
				}
				stream_annotationParam.reset();

				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;

			}

			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "annotationDef"


	public static class annotationParam_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "annotationParam"
	// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:283:1: annotationParam : ( intValue | floatValue | stringValue | enumConstRefValue );
	public final IOMonkey2Parser.annotationParam_return annotationParam() throws RecognitionException {
		IOMonkey2Parser.annotationParam_return retval = new IOMonkey2Parser.annotationParam_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		ParserRuleReturnScope intValue134 =null;
		ParserRuleReturnScope floatValue135 =null;
		ParserRuleReturnScope stringValue136 =null;
		ParserRuleReturnScope enumConstRefValue137 =null;


		try {
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:284:2: ( intValue | floatValue | stringValue | enumConstRefValue )
			int alt37=4;
			switch ( input.LA(1) ) {
			case HEX_NUM:
			case NUM:
			case 61:
			case 63:
				{
				alt37=1;
				}
				break;
			case FLOAT:
				{
				alt37=2;
				}
				break;
			case STRING:
				{
				alt37=3;
				}
				break;
			case ID:
				{
				alt37=4;
				}
				break;
			default:
				NoViableAltException nvae =
					new NoViableAltException("", 37, 0, input);
				throw nvae;
			}
			switch (alt37) {
				case 1 :
					// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:284:4: intValue
					{
					root_0 = (Object)adaptor.nil();


					pushFollow(FOLLOW_intValue_in_annotationParam1456);
					intValue134=intValue();
					state._fsp--;

					adaptor.addChild(root_0, intValue134.getTree());

					}
					break;
				case 2 :
					// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:285:4: floatValue
					{
					root_0 = (Object)adaptor.nil();


					pushFollow(FOLLOW_floatValue_in_annotationParam1461);
					floatValue135=floatValue();
					state._fsp--;

					adaptor.addChild(root_0, floatValue135.getTree());

					}
					break;
				case 3 :
					// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:286:4: stringValue
					{
					root_0 = (Object)adaptor.nil();


					pushFollow(FOLLOW_stringValue_in_annotationParam1466);
					stringValue136=stringValue();
					state._fsp--;

					adaptor.addChild(root_0, stringValue136.getTree());

					}
					break;
				case 4 :
					// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:287:4: enumConstRefValue
					{
					root_0 = (Object)adaptor.nil();


					pushFollow(FOLLOW_enumConstRefValue_in_annotationParam1471);
					enumConstRefValue137=enumConstRefValue();
					state._fsp--;

					adaptor.addChild(root_0, enumConstRefValue137.getTree());

					}
					break;

			}
			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "annotationParam"


	public static class docComment_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "docComment"
	// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:290:1: docComment : ( docCommentSlashBlock | docCommentStartBlock );
	public final IOMonkey2Parser.docComment_return docComment() throws RecognitionException {
		IOMonkey2Parser.docComment_return retval = new IOMonkey2Parser.docComment_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		ParserRuleReturnScope docCommentSlashBlock138 =null;
		ParserRuleReturnScope docCommentStartBlock139 =null;


		try {
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:291:2: ( docCommentSlashBlock | docCommentStartBlock )
			int alt38=2;
			int LA38_0 = input.LA(1);
			if ( (LA38_0==DOC_COMMENT_SLASH) ) {
				alt38=1;
			}
			else if ( (LA38_0==DOC_COMMENT_STAR) ) {
				alt38=2;
			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 38, 0, input);
				throw nvae;
			}

			switch (alt38) {
				case 1 :
					// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:291:4: docCommentSlashBlock
					{
					root_0 = (Object)adaptor.nil();


					pushFollow(FOLLOW_docCommentSlashBlock_in_docComment1483);
					docCommentSlashBlock138=docCommentSlashBlock();
					state._fsp--;

					adaptor.addChild(root_0, docCommentSlashBlock138.getTree());

					}
					break;
				case 2 :
					// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:292:4: docCommentStartBlock
					{
					root_0 = (Object)adaptor.nil();


					pushFollow(FOLLOW_docCommentStartBlock_in_docComment1488);
					docCommentStartBlock139=docCommentStartBlock();
					state._fsp--;

					adaptor.addChild(root_0, docCommentStartBlock139.getTree());

					}
					break;

			}
			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "docComment"


	public static class docCommentSlashBlock_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "docCommentSlashBlock"
	// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:295:1: docCommentSlashBlock : ( DOC_COMMENT_SLASH )+ -> ^( DOC_COMMENT ( DOC_COMMENT_SLASH )+ ) ;
	public final IOMonkey2Parser.docCommentSlashBlock_return docCommentSlashBlock() throws RecognitionException {
		IOMonkey2Parser.docCommentSlashBlock_return retval = new IOMonkey2Parser.docCommentSlashBlock_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token DOC_COMMENT_SLASH140=null;

		Object DOC_COMMENT_SLASH140_tree=null;
		RewriteRuleTokenStream stream_DOC_COMMENT_SLASH=new RewriteRuleTokenStream(adaptor,"token DOC_COMMENT_SLASH");

		try {
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:296:2: ( ( DOC_COMMENT_SLASH )+ -> ^( DOC_COMMENT ( DOC_COMMENT_SLASH )+ ) )
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:296:4: ( DOC_COMMENT_SLASH )+
			{
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:296:4: ( DOC_COMMENT_SLASH )+
			int cnt39=0;
			loop39:
			while (true) {
				int alt39=2;
				int LA39_0 = input.LA(1);
				if ( (LA39_0==DOC_COMMENT_SLASH) ) {
					alt39=1;
				}

				switch (alt39) {
				case 1 :
					// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:296:4: DOC_COMMENT_SLASH
					{
					DOC_COMMENT_SLASH140=(Token)match(input,DOC_COMMENT_SLASH,FOLLOW_DOC_COMMENT_SLASH_in_docCommentSlashBlock1500);  
					stream_DOC_COMMENT_SLASH.add(DOC_COMMENT_SLASH140);

					}
					break;

				default :
					if ( cnt39 >= 1 ) break loop39;
					EarlyExitException eee = new EarlyExitException(39, input);
					throw eee;
				}
				cnt39++;
			}

			// AST REWRITE
			// elements: DOC_COMMENT_SLASH
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (Object)adaptor.nil();
			// 296:23: -> ^( DOC_COMMENT ( DOC_COMMENT_SLASH )+ )
			{
				// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:296:26: ^( DOC_COMMENT ( DOC_COMMENT_SLASH )+ )
				{
				Object root_1 = (Object)adaptor.nil();
				root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(DOC_COMMENT, "DOC_COMMENT"), root_1);
				if ( !(stream_DOC_COMMENT_SLASH.hasNext()) ) {
					throw new RewriteEarlyExitException();
				}
				while ( stream_DOC_COMMENT_SLASH.hasNext() ) {
					adaptor.addChild(root_1, stream_DOC_COMMENT_SLASH.nextNode());
				}
				stream_DOC_COMMENT_SLASH.reset();

				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;

			}

			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "docCommentSlashBlock"


	public static class docCommentStartBlock_return extends ParserRuleReturnScope {
		Object tree;
		@Override
		public Object getTree() { return tree; }
	};


	// $ANTLR start "docCommentStartBlock"
	// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:299:1: docCommentStartBlock : DOC_COMMENT_STAR -> ^( DOC_COMMENT DOC_COMMENT_STAR ) ;
	public final IOMonkey2Parser.docCommentStartBlock_return docCommentStartBlock() throws RecognitionException {
		IOMonkey2Parser.docCommentStartBlock_return retval = new IOMonkey2Parser.docCommentStartBlock_return();
		retval.start = input.LT(1);

		Object root_0 = null;

		Token DOC_COMMENT_STAR141=null;

		Object DOC_COMMENT_STAR141_tree=null;
		RewriteRuleTokenStream stream_DOC_COMMENT_STAR=new RewriteRuleTokenStream(adaptor,"token DOC_COMMENT_STAR");

		try {
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:300:2: ( DOC_COMMENT_STAR -> ^( DOC_COMMENT DOC_COMMENT_STAR ) )
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:300:4: DOC_COMMENT_STAR
			{
			DOC_COMMENT_STAR141=(Token)match(input,DOC_COMMENT_STAR,FOLLOW_DOC_COMMENT_STAR_in_docCommentStartBlock1522);  
			stream_DOC_COMMENT_STAR.add(DOC_COMMENT_STAR141);

			// AST REWRITE
			// elements: DOC_COMMENT_STAR
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (Object)adaptor.nil();
			// 300:21: -> ^( DOC_COMMENT DOC_COMMENT_STAR )
			{
				// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:300:24: ^( DOC_COMMENT DOC_COMMENT_STAR )
				{
				Object root_1 = (Object)adaptor.nil();
				root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(DOC_COMMENT, "DOC_COMMENT"), root_1);
				adaptor.addChild(root_1, stream_DOC_COMMENT_STAR.nextNode());
				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;

			}

			retval.stop = input.LT(-1);

			retval.tree = (Object)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "docCommentStartBlock"

	// Delegated rules


	protected DFA9 dfa9 = new DFA9(this);
	static final String DFA9_eotS =
		"\5\uffff";
	static final String DFA9_eofS =
		"\5\uffff";
	static final String DFA9_minS =
		"\2\13\1\104\2\uffff";
	static final String DFA9_maxS =
		"\3\125\2\uffff";
	static final String DFA9_acceptS =
		"\3\uffff\1\1\1\2";
	static final String DFA9_specialS =
		"\5\uffff}>";
	static final String[] DFA9_transitionS = {
			"\1\1\1\2\67\uffff\1\4\2\uffff\1\4\2\uffff\1\3\12\uffff\1\4",
			"\1\1\70\uffff\1\4\2\uffff\1\4\2\uffff\1\3\12\uffff\1\4",
			"\1\4\2\uffff\1\4\2\uffff\1\3\12\uffff\1\4",
			"",
			""
	};

	static final short[] DFA9_eot = DFA.unpackEncodedString(DFA9_eotS);
	static final short[] DFA9_eof = DFA.unpackEncodedString(DFA9_eofS);
	static final char[] DFA9_min = DFA.unpackEncodedStringToUnsignedChars(DFA9_minS);
	static final char[] DFA9_max = DFA.unpackEncodedStringToUnsignedChars(DFA9_maxS);
	static final short[] DFA9_accept = DFA.unpackEncodedString(DFA9_acceptS);
	static final short[] DFA9_special = DFA.unpackEncodedString(DFA9_specialS);
	static final short[][] DFA9_transition;

	static {
		int numStates = DFA9_transitionS.length;
		DFA9_transition = new short[numStates][];
		for (int i=0; i<numStates; i++) {
			DFA9_transition[i] = DFA.unpackEncodedString(DFA9_transitionS[i]);
		}
	}

	protected class DFA9 extends DFA {

		public DFA9(BaseRecognizer recognizer) {
			this.recognizer = recognizer;
			this.decisionNumber = 9;
			this.eot = DFA9_eot;
			this.eof = DFA9_eof;
			this.min = DFA9_min;
			this.max = DFA9_max;
			this.accept = DFA9_accept;
			this.special = DFA9_special;
			this.transition = DFA9_transition;
		}
		@Override
		public String getDescription() {
			return "114:1: elementDef : ( enumDef | structDef );";
		}
	}

	public static final BitSet FOLLOW_packageDecl_in_document229 = new BitSet(new long[]{0x0000000000001800L,0x0000000000211490L});
	public static final BitSet FOLLOW_headerOp_in_document231 = new BitSet(new long[]{0x0000000000001800L,0x0000000000211490L});
	public static final BitSet FOLLOW_elementDefs_in_document234 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_docComment_in_packageDecl260 = new BitSet(new long[]{0x0000000000000000L,0x0000000000020000L});
	public static final BitSet FOLLOW_81_in_packageDecl263 = new BitSet(new long[]{0x0000000000200000L});
	public static final BitSet FOLLOW_dotName_in_packageDecl265 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000004L});
	public static final BitSet FOLLOW_66_in_packageDecl267 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_ID_in_dotName291 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000001L});
	public static final BitSet FOLLOW_64_in_dotName295 = new BitSet(new long[]{0x0000000000200000L});
	public static final BitSet FOLLOW_ID_in_dotName297 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000001L});
	public static final BitSet FOLLOW_importStat_in_headerOp322 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_optionsStat_in_headerOp327 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_76_in_importStat339 = new BitSet(new long[]{0x0000000000200000L});
	public static final BitSet FOLLOW_dotName_in_importStat341 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000004L});
	public static final BitSet FOLLOW_66_in_importStat343 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_80_in_optionsStat363 = new BitSet(new long[]{0x0000000000000000L,0x0000000000400000L});
	public static final BitSet FOLLOW_86_in_optionsStat365 = new BitSet(new long[]{0x0000000000200000L});
	public static final BitSet FOLLOW_optionStat_in_optionsStat369 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000004L});
	public static final BitSet FOLLOW_66_in_optionsStat371 = new BitSet(new long[]{0x0000000000200000L,0x0000000000800000L});
	public static final BitSet FOLLOW_87_in_optionsStat377 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000004L});
	public static final BitSet FOLLOW_66_in_optionsStat379 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_dotName_in_optionStat402 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000008L});
	public static final BitSet FOLLOW_67_in_optionStat404 = new BitSet(new long[]{0xA000000802100000L});
	public static final BitSet FOLLOW_optionValue_in_optionStat406 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_stringValue_in_optionValue428 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_intValue_in_optionValue432 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_elementDef_in_elementDefs446 = new BitSet(new long[]{0x0000000000001802L,0x0000000000200490L});
	public static final BitSet FOLLOW_enumDef_in_elementDef459 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_structDef_in_elementDef464 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_docComment_in_enumDef475 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000400L});
	public static final BitSet FOLLOW_74_in_enumDef478 = new BitSet(new long[]{0x0000000000200000L});
	public static final BitSet FOLLOW_ID_in_enumDef480 = new BitSet(new long[]{0x0000000000000000L,0x0000000000400000L});
	public static final BitSet FOLLOW_86_in_enumDef482 = new BitSet(new long[]{0x0000000000201800L});
	public static final BitSet FOLLOW_enumConst_in_enumDef484 = new BitSet(new long[]{0x4000000000000000L,0x0000000000800000L});
	public static final BitSet FOLLOW_62_in_enumDef488 = new BitSet(new long[]{0x0000000000201800L});
	public static final BitSet FOLLOW_enumConst_in_enumDef490 = new BitSet(new long[]{0x4000000000000000L,0x0000000000800000L});
	public static final BitSet FOLLOW_62_in_enumDef496 = new BitSet(new long[]{0x0000000000000000L,0x0000000000800000L});
	public static final BitSet FOLLOW_87_in_enumDef499 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_docComment_in_enumConst528 = new BitSet(new long[]{0x0000000000200000L});
	public static final BitSet FOLLOW_ID_in_enumConst531 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000008L});
	public static final BitSet FOLLOW_67_in_enumConst533 = new BitSet(new long[]{0xA000000002100000L});
	public static final BitSet FOLLOW_intValue_in_enumConst535 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_docComment_in_structDef561 = new BitSet(new long[]{0x0000000000000000L,0x0000000000200090L});
	public static final BitSet FOLLOW_annotations_in_structDef564 = new BitSet(new long[]{0x0000000000000000L,0x0000000000200080L});
	public static final BitSet FOLLOW_structHeaderDef_in_structDef567 = new BitSet(new long[]{0x0000000000200000L});
	public static final BitSet FOLLOW_ID_in_structDef569 = new BitSet(new long[]{0x0000000000000000L,0x0000000000400002L});
	public static final BitSet FOLLOW_structSuperDef_in_structDef571 = new BitSet(new long[]{0x0000000000000000L,0x0000000000400000L});
	public static final BitSet FOLLOW_86_in_structDef574 = new BitSet(new long[]{0x0000000000201800L,0x00000000009CEB10L});
	public static final BitSet FOLLOW_structItem_in_structDef576 = new BitSet(new long[]{0x0000000000201800L,0x00000000009CEB10L});
	public static final BitSet FOLLOW_87_in_structDef580 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000004L});
	public static final BitSet FOLLOW_66_in_structDef582 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_structHeader_in_structHeaderDef617 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_abstractStructHeader_in_structHeaderDef622 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_85_in_structHeader635 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_71_in_abstractStructHeader653 = new BitSet(new long[]{0x0000000000000000L,0x0000000000200000L});
	public static final BitSet FOLLOW_85_in_abstractStructHeader655 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_65_in_structSuperDef673 = new BitSet(new long[]{0x0000000000200000L});
	public static final BitSet FOLLOW_ID_in_structSuperDef675 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_attribute_in_structItem695 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000004L});
	public static final BitSet FOLLOW_66_in_structItem697 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_docComment_in_attribute710 = new BitSet(new long[]{0x0000000000200000L,0x00000000001CEB10L});
	public static final BitSet FOLLOW_annotations_in_attribute713 = new BitSet(new long[]{0x0000000000200000L,0x00000000001CEB00L});
	public static final BitSet FOLLOW_attributeSlot_in_attribute716 = new BitSet(new long[]{0x0000000000200000L,0x0000000000186B00L});
	public static final BitSet FOLLOW_attributeType_in_attribute719 = new BitSet(new long[]{0x0000000000200000L});
	public static final BitSet FOLLOW_ID_in_attribute721 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000008L});
	public static final BitSet FOLLOW_attributeDefaultValueSet_in_attribute723 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_scalarType_in_attributeType761 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_arrayType_in_attributeType766 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_intType_in_scalarType778 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_shortType_in_scalarType783 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_byteType_in_scalarType788 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_longType_in_scalarType793 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_stringType_in_scalarType798 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_floatType_in_scalarType803 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_boolType_in_scalarType808 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_idType_in_scalarType813 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_77_in_intType824 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_83_in_shortType841 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_73_in_byteType857 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_78_in_longType871 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_84_in_stringType886 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_75_in_floatType902 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_72_in_boolType918 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_ID_in_idType933 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_fixedSizeArrayType_in_arrayType953 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_dynamicArrayType_in_arrayType958 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_scalarType_in_fixedSizeArrayType970 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000020L});
	public static final BitSet FOLLOW_69_in_fixedSizeArrayType972 = new BitSet(new long[]{0x0000000002200000L});
	public static final BitSet FOLLOW_fixedArraySize_in_fixedSizeArrayType974 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000040L});
	public static final BitSet FOLLOW_70_in_fixedSizeArrayType976 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_NUM_in_fixedArraySize1002 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_ID_in_fixedArraySize1020 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000001L});
	public static final BitSet FOLLOW_64_in_fixedArraySize1022 = new BitSet(new long[]{0x0000000000200000L});
	public static final BitSet FOLLOW_ID_in_fixedArraySize1024 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_scalarType_in_dynamicArrayType1049 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000020L});
	public static final BitSet FOLLOW_69_in_dynamicArrayType1051 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000040L});
	public static final BitSet FOLLOW_70_in_dynamicArrayType1053 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_attributeSlotType_in_attributeSlot1073 = new BitSet(new long[]{0x0800000000000002L});
	public static final BitSet FOLLOW_59_in_attributeSlot1077 = new BitSet(new long[]{0x0000000002000000L});
	public static final BitSet FOLLOW_NUM_in_attributeSlot1079 = new BitSet(new long[]{0x1000000000000000L});
	public static final BitSet FOLLOW_60_in_attributeSlot1081 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_82_in_attributeSlotType1105 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_79_in_attributeSlotType1114 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_67_in_attributeDefaultValueSet1130 = new BitSet(new long[]{0xA000000803340080L});
	public static final BitSet FOLLOW_attributeValue_in_attributeDefaultValueSet1132 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_intValue_in_attributeValue1152 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_floatValue_in_attributeValue1156 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_stringValue_in_attributeValue1160 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_enumConstValue_in_attributeValue1164 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_nullValue_in_attributeValue1168 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_boolValue_in_attributeValue1172 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_ID_in_enumConstValue1183 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_ID_in_enumConstRefValue1203 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000001L});
	public static final BitSet FOLLOW_64_in_enumConstRefValue1205 = new BitSet(new long[]{0x0000000000200000L});
	public static final BitSet FOLLOW_ID_in_enumConstRefValue1207 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_STRING_in_stringValue1230 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_FLOAT_in_floatValue1251 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_BOOL__in_boolValue1271 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_decimalIntValue_in_intValue1290 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_hexadecimalIntValue_in_intValue1307 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_NUM_in_decimalIntValue1337 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_HEX_NUM_in_hexadecimalIntValue1358 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_NULL_in_nullValue1370 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_annotationDef_in_annotations1391 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000010L});
	public static final BitSet FOLLOW_68_in_annotationDef1411 = new BitSet(new long[]{0x0000000000200000L});
	public static final BitSet FOLLOW_ID_in_annotationDef1413 = new BitSet(new long[]{0x0800000000000002L});
	public static final BitSet FOLLOW_59_in_annotationDef1417 = new BitSet(new long[]{0xA000000802340000L});
	public static final BitSet FOLLOW_annotationParam_in_annotationDef1419 = new BitSet(new long[]{0x5000000000000000L});
	public static final BitSet FOLLOW_62_in_annotationDef1422 = new BitSet(new long[]{0xA000000802340000L});
	public static final BitSet FOLLOW_annotationParam_in_annotationDef1424 = new BitSet(new long[]{0x5000000000000000L});
	public static final BitSet FOLLOW_60_in_annotationDef1431 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_intValue_in_annotationParam1456 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_floatValue_in_annotationParam1461 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_stringValue_in_annotationParam1466 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_enumConstRefValue_in_annotationParam1471 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_docCommentSlashBlock_in_docComment1483 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_docCommentStartBlock_in_docComment1488 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_DOC_COMMENT_SLASH_in_docCommentSlashBlock1500 = new BitSet(new long[]{0x0000000000000802L});
	public static final BitSet FOLLOW_DOC_COMMENT_STAR_in_docCommentStartBlock1522 = new BitSet(new long[]{0x0000000000000002L});
}
