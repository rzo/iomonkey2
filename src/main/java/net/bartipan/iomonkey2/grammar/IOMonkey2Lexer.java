// $ANTLR 3.5.1 /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g 2016-11-10 18:48:02

package net.bartipan.iomonkey2.grammar;


import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class IOMonkey2Lexer extends Lexer {
	public static final int EOF=-1;
	public static final int T__59=59;
	public static final int T__60=60;
	public static final int T__61=61;
	public static final int T__62=62;
	public static final int T__63=63;
	public static final int T__64=64;
	public static final int T__65=65;
	public static final int T__66=66;
	public static final int T__67=67;
	public static final int T__68=68;
	public static final int T__69=69;
	public static final int T__70=70;
	public static final int T__71=71;
	public static final int T__72=72;
	public static final int T__73=73;
	public static final int T__74=74;
	public static final int T__75=75;
	public static final int T__76=76;
	public static final int T__77=77;
	public static final int T__78=78;
	public static final int T__79=79;
	public static final int T__80=80;
	public static final int T__81=81;
	public static final int T__82=82;
	public static final int T__83=83;
	public static final int T__84=84;
	public static final int T__85=85;
	public static final int T__86=86;
	public static final int T__87=87;
	public static final int ABSTRACT_STRUCT=4;
	public static final int ANNOTATION=5;
	public static final int ANNOTATIONS=6;
	public static final int BOOL_=7;
	public static final int COMMENT=8;
	public static final int DOCUMENT=9;
	public static final int DOC_COMMENT=10;
	public static final int DOC_COMMENT_SLASH=11;
	public static final int DOC_COMMENT_STAR=12;
	public static final int DOTNAME=13;
	public static final int ENUM=14;
	public static final int ENUM_CONST=15;
	public static final int ESC_SEQ=16;
	public static final int EXPONENT=17;
	public static final int FLOAT=18;
	public static final int HEX_DIGIT=19;
	public static final int HEX_NUM=20;
	public static final int ID=21;
	public static final int IMPLICIT_VALUE=22;
	public static final int IMPORT=23;
	public static final int NULL=24;
	public static final int NUM=25;
	public static final int OCTAL_ESC=26;
	public static final int OPTION=27;
	public static final int OPTIONS=28;
	public static final int PACKAGE=29;
	public static final int SIZE_INT=30;
	public static final int SIZE_SYMBOL_REF=31;
	public static final int SLOT_OPTIONAL=32;
	public static final int SLOT_REQUIRED=33;
	public static final int SPLITTER_COMMENT=34;
	public static final int STRING=35;
	public static final int STRUCT=36;
	public static final int STRUCT_ATTR=37;
	public static final int STRUCT_ATTR_SLOT=38;
	public static final int STRUCT_SUPER=39;
	public static final int TYPE_BOOL=40;
	public static final int TYPE_BYTE=41;
	public static final int TYPE_DYNAMIC_ARRAY=42;
	public static final int TYPE_FIXED_ARRAY=43;
	public static final int TYPE_FLOAT=44;
	public static final int TYPE_ID=45;
	public static final int TYPE_INT=46;
	public static final int TYPE_LONG=47;
	public static final int TYPE_SHORT=48;
	public static final int TYPE_STRING=49;
	public static final int UNICODE_ESC=50;
	public static final int VALUE_BOOL=51;
	public static final int VALUE_ENUM_CONST=52;
	public static final int VALUE_ENUM_CONST_REF=53;
	public static final int VALUE_FLOAT=54;
	public static final int VALUE_INT=55;
	public static final int VALUE_NULL=56;
	public static final int VALUE_STRING=57;
	public static final int WS=58;

	// delegates
	// delegators
	public Lexer[] getDelegates() {
		return new Lexer[] {};
	}

	public IOMonkey2Lexer() {} 
	public IOMonkey2Lexer(CharStream input) {
		this(input, new RecognizerSharedState());
	}
	public IOMonkey2Lexer(CharStream input, RecognizerSharedState state) {
		super(input,state);
	}
	@Override public String getGrammarFileName() { return "/Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g"; }

	// $ANTLR start "T__59"
	public final void mT__59() throws RecognitionException {
		try {
			int _type = T__59;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:11:7: ( '(' )
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:11:9: '('
			{
			match('('); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__59"

	// $ANTLR start "T__60"
	public final void mT__60() throws RecognitionException {
		try {
			int _type = T__60;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:12:7: ( ')' )
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:12:9: ')'
			{
			match(')'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__60"

	// $ANTLR start "T__61"
	public final void mT__61() throws RecognitionException {
		try {
			int _type = T__61;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:13:7: ( '+' )
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:13:9: '+'
			{
			match('+'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__61"

	// $ANTLR start "T__62"
	public final void mT__62() throws RecognitionException {
		try {
			int _type = T__62;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:14:7: ( ',' )
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:14:9: ','
			{
			match(','); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__62"

	// $ANTLR start "T__63"
	public final void mT__63() throws RecognitionException {
		try {
			int _type = T__63;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:15:7: ( '-' )
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:15:9: '-'
			{
			match('-'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__63"

	// $ANTLR start "T__64"
	public final void mT__64() throws RecognitionException {
		try {
			int _type = T__64;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:16:7: ( '.' )
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:16:9: '.'
			{
			match('.'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__64"

	// $ANTLR start "T__65"
	public final void mT__65() throws RecognitionException {
		try {
			int _type = T__65;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:17:7: ( ':' )
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:17:9: ':'
			{
			match(':'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__65"

	// $ANTLR start "T__66"
	public final void mT__66() throws RecognitionException {
		try {
			int _type = T__66;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:18:7: ( ';' )
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:18:9: ';'
			{
			match(';'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__66"

	// $ANTLR start "T__67"
	public final void mT__67() throws RecognitionException {
		try {
			int _type = T__67;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:19:7: ( '=' )
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:19:9: '='
			{
			match('='); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__67"

	// $ANTLR start "T__68"
	public final void mT__68() throws RecognitionException {
		try {
			int _type = T__68;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:20:7: ( '@' )
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:20:9: '@'
			{
			match('@'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__68"

	// $ANTLR start "T__69"
	public final void mT__69() throws RecognitionException {
		try {
			int _type = T__69;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:21:7: ( '[' )
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:21:9: '['
			{
			match('['); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__69"

	// $ANTLR start "T__70"
	public final void mT__70() throws RecognitionException {
		try {
			int _type = T__70;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:22:7: ( ']' )
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:22:9: ']'
			{
			match(']'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__70"

	// $ANTLR start "T__71"
	public final void mT__71() throws RecognitionException {
		try {
			int _type = T__71;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:23:7: ( 'abstract' )
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:23:9: 'abstract'
			{
			match("abstract"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__71"

	// $ANTLR start "T__72"
	public final void mT__72() throws RecognitionException {
		try {
			int _type = T__72;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:24:7: ( 'bool' )
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:24:9: 'bool'
			{
			match("bool"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__72"

	// $ANTLR start "T__73"
	public final void mT__73() throws RecognitionException {
		try {
			int _type = T__73;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:25:7: ( 'byte' )
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:25:9: 'byte'
			{
			match("byte"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__73"

	// $ANTLR start "T__74"
	public final void mT__74() throws RecognitionException {
		try {
			int _type = T__74;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:26:7: ( 'enum' )
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:26:9: 'enum'
			{
			match("enum"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__74"

	// $ANTLR start "T__75"
	public final void mT__75() throws RecognitionException {
		try {
			int _type = T__75;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:27:7: ( 'float' )
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:27:9: 'float'
			{
			match("float"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__75"

	// $ANTLR start "T__76"
	public final void mT__76() throws RecognitionException {
		try {
			int _type = T__76;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:28:7: ( 'import' )
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:28:9: 'import'
			{
			match("import"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__76"

	// $ANTLR start "T__77"
	public final void mT__77() throws RecognitionException {
		try {
			int _type = T__77;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:29:7: ( 'int' )
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:29:9: 'int'
			{
			match("int"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__77"

	// $ANTLR start "T__78"
	public final void mT__78() throws RecognitionException {
		try {
			int _type = T__78;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:30:7: ( 'long' )
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:30:9: 'long'
			{
			match("long"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__78"

	// $ANTLR start "T__79"
	public final void mT__79() throws RecognitionException {
		try {
			int _type = T__79;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:31:7: ( 'optional' )
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:31:9: 'optional'
			{
			match("optional"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__79"

	// $ANTLR start "T__80"
	public final void mT__80() throws RecognitionException {
		try {
			int _type = T__80;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:32:7: ( 'options' )
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:32:9: 'options'
			{
			match("options"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__80"

	// $ANTLR start "T__81"
	public final void mT__81() throws RecognitionException {
		try {
			int _type = T__81;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:33:7: ( 'package' )
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:33:9: 'package'
			{
			match("package"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__81"

	// $ANTLR start "T__82"
	public final void mT__82() throws RecognitionException {
		try {
			int _type = T__82;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:34:7: ( 'required' )
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:34:9: 'required'
			{
			match("required"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__82"

	// $ANTLR start "T__83"
	public final void mT__83() throws RecognitionException {
		try {
			int _type = T__83;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:35:7: ( 'short' )
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:35:9: 'short'
			{
			match("short"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__83"

	// $ANTLR start "T__84"
	public final void mT__84() throws RecognitionException {
		try {
			int _type = T__84;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:36:7: ( 'string' )
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:36:9: 'string'
			{
			match("string"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__84"

	// $ANTLR start "T__85"
	public final void mT__85() throws RecognitionException {
		try {
			int _type = T__85;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:37:7: ( 'struct' )
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:37:9: 'struct'
			{
			match("struct"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__85"

	// $ANTLR start "T__86"
	public final void mT__86() throws RecognitionException {
		try {
			int _type = T__86;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:38:7: ( '{' )
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:38:9: '{'
			{
			match('{'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__86"

	// $ANTLR start "T__87"
	public final void mT__87() throws RecognitionException {
		try {
			int _type = T__87;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:39:7: ( '}' )
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:39:9: '}'
			{
			match('}'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__87"

	// $ANTLR start "BOOL_"
	public final void mBOOL_() throws RecognitionException {
		try {
			int _type = BOOL_;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:303:6: ( 'true' | 'false' )
			int alt1=2;
			int LA1_0 = input.LA(1);
			if ( (LA1_0=='t') ) {
				alt1=1;
			}
			else if ( (LA1_0=='f') ) {
				alt1=2;
			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 1, 0, input);
				throw nvae;
			}

			switch (alt1) {
				case 1 :
					// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:303:8: 'true'
					{
					match("true"); 

					}
					break;
				case 2 :
					// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:304:4: 'false'
					{
					match("false"); 

					}
					break;

			}
			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "BOOL_"

	// $ANTLR start "NULL"
	public final void mNULL() throws RecognitionException {
		try {
			int _type = NULL;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:307:7: ( 'null' )
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:307:9: 'null'
			{
			match("null"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "NULL"

	// $ANTLR start "ID"
	public final void mID() throws RecognitionException {
		try {
			int _type = ID;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:310:5: ( ( 'a' .. 'z' | 'A' .. 'Z' | '_' ) ( 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' | '_' )* )
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:310:7: ( 'a' .. 'z' | 'A' .. 'Z' | '_' ) ( 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' | '_' )*
			{
			if ( (input.LA(1) >= 'A' && input.LA(1) <= 'Z')||input.LA(1)=='_'||(input.LA(1) >= 'a' && input.LA(1) <= 'z') ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:310:31: ( 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' | '_' )*
			loop2:
			while (true) {
				int alt2=2;
				int LA2_0 = input.LA(1);
				if ( ((LA2_0 >= '0' && LA2_0 <= '9')||(LA2_0 >= 'A' && LA2_0 <= 'Z')||LA2_0=='_'||(LA2_0 >= 'a' && LA2_0 <= 'z')) ) {
					alt2=1;
				}

				switch (alt2) {
				case 1 :
					// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:
					{
					if ( (input.LA(1) >= '0' && input.LA(1) <= '9')||(input.LA(1) >= 'A' && input.LA(1) <= 'Z')||input.LA(1)=='_'||(input.LA(1) >= 'a' && input.LA(1) <= 'z') ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

				default :
					break loop2;
				}
			}

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "ID"

	// $ANTLR start "NUM"
	public final void mNUM() throws RecognitionException {
		try {
			int _type = NUM;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:313:5: ( ( '0' .. '9' )+ )
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:313:7: ( '0' .. '9' )+
			{
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:313:7: ( '0' .. '9' )+
			int cnt3=0;
			loop3:
			while (true) {
				int alt3=2;
				int LA3_0 = input.LA(1);
				if ( ((LA3_0 >= '0' && LA3_0 <= '9')) ) {
					alt3=1;
				}

				switch (alt3) {
				case 1 :
					// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:
					{
					if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

				default :
					if ( cnt3 >= 1 ) break loop3;
					EarlyExitException eee = new EarlyExitException(3, input);
					throw eee;
				}
				cnt3++;
			}

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "NUM"

	// $ANTLR start "HEX_NUM"
	public final void mHEX_NUM() throws RecognitionException {
		try {
			int _type = HEX_NUM;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:316:9: ( '0x' ( HEX_DIGIT )+ )
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:316:11: '0x' ( HEX_DIGIT )+
			{
			match("0x"); 

			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:316:16: ( HEX_DIGIT )+
			int cnt4=0;
			loop4:
			while (true) {
				int alt4=2;
				int LA4_0 = input.LA(1);
				if ( ((LA4_0 >= '0' && LA4_0 <= '9')||(LA4_0 >= 'A' && LA4_0 <= 'F')||(LA4_0 >= 'a' && LA4_0 <= 'f')) ) {
					alt4=1;
				}

				switch (alt4) {
				case 1 :
					// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:
					{
					if ( (input.LA(1) >= '0' && input.LA(1) <= '9')||(input.LA(1) >= 'A' && input.LA(1) <= 'F')||(input.LA(1) >= 'a' && input.LA(1) <= 'f') ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

				default :
					if ( cnt4 >= 1 ) break loop4;
					EarlyExitException eee = new EarlyExitException(4, input);
					throw eee;
				}
				cnt4++;
			}

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "HEX_NUM"

	// $ANTLR start "FLOAT"
	public final void mFLOAT() throws RecognitionException {
		try {
			int _type = FLOAT;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:320:5: ( ( '0' .. '9' )+ '.' ( '0' .. '9' )* ( EXPONENT )? | '.' ( '0' .. '9' )+ ( EXPONENT )? | ( '0' .. '9' )+ EXPONENT )
			int alt11=3;
			alt11 = dfa11.predict(input);
			switch (alt11) {
				case 1 :
					// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:320:9: ( '0' .. '9' )+ '.' ( '0' .. '9' )* ( EXPONENT )?
					{
					// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:320:9: ( '0' .. '9' )+
					int cnt5=0;
					loop5:
					while (true) {
						int alt5=2;
						int LA5_0 = input.LA(1);
						if ( ((LA5_0 >= '0' && LA5_0 <= '9')) ) {
							alt5=1;
						}

						switch (alt5) {
						case 1 :
							// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:
							{
							if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
								input.consume();
							}
							else {
								MismatchedSetException mse = new MismatchedSetException(null,input);
								recover(mse);
								throw mse;
							}
							}
							break;

						default :
							if ( cnt5 >= 1 ) break loop5;
							EarlyExitException eee = new EarlyExitException(5, input);
							throw eee;
						}
						cnt5++;
					}

					match('.'); 
					// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:320:25: ( '0' .. '9' )*
					loop6:
					while (true) {
						int alt6=2;
						int LA6_0 = input.LA(1);
						if ( ((LA6_0 >= '0' && LA6_0 <= '9')) ) {
							alt6=1;
						}

						switch (alt6) {
						case 1 :
							// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:
							{
							if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
								input.consume();
							}
							else {
								MismatchedSetException mse = new MismatchedSetException(null,input);
								recover(mse);
								throw mse;
							}
							}
							break;

						default :
							break loop6;
						}
					}

					// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:320:37: ( EXPONENT )?
					int alt7=2;
					int LA7_0 = input.LA(1);
					if ( (LA7_0=='E'||LA7_0=='e') ) {
						alt7=1;
					}
					switch (alt7) {
						case 1 :
							// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:320:37: EXPONENT
							{
							mEXPONENT(); 

							}
							break;

					}

					}
					break;
				case 2 :
					// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:321:9: '.' ( '0' .. '9' )+ ( EXPONENT )?
					{
					match('.'); 
					// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:321:13: ( '0' .. '9' )+
					int cnt8=0;
					loop8:
					while (true) {
						int alt8=2;
						int LA8_0 = input.LA(1);
						if ( ((LA8_0 >= '0' && LA8_0 <= '9')) ) {
							alt8=1;
						}

						switch (alt8) {
						case 1 :
							// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:
							{
							if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
								input.consume();
							}
							else {
								MismatchedSetException mse = new MismatchedSetException(null,input);
								recover(mse);
								throw mse;
							}
							}
							break;

						default :
							if ( cnt8 >= 1 ) break loop8;
							EarlyExitException eee = new EarlyExitException(8, input);
							throw eee;
						}
						cnt8++;
					}

					// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:321:25: ( EXPONENT )?
					int alt9=2;
					int LA9_0 = input.LA(1);
					if ( (LA9_0=='E'||LA9_0=='e') ) {
						alt9=1;
					}
					switch (alt9) {
						case 1 :
							// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:321:25: EXPONENT
							{
							mEXPONENT(); 

							}
							break;

					}

					}
					break;
				case 3 :
					// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:322:9: ( '0' .. '9' )+ EXPONENT
					{
					// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:322:9: ( '0' .. '9' )+
					int cnt10=0;
					loop10:
					while (true) {
						int alt10=2;
						int LA10_0 = input.LA(1);
						if ( ((LA10_0 >= '0' && LA10_0 <= '9')) ) {
							alt10=1;
						}

						switch (alt10) {
						case 1 :
							// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:
							{
							if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
								input.consume();
							}
							else {
								MismatchedSetException mse = new MismatchedSetException(null,input);
								recover(mse);
								throw mse;
							}
							}
							break;

						default :
							if ( cnt10 >= 1 ) break loop10;
							EarlyExitException eee = new EarlyExitException(10, input);
							throw eee;
						}
						cnt10++;
					}

					mEXPONENT(); 

					}
					break;

			}
			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "FLOAT"

	// $ANTLR start "SPLITTER_COMMENT"
	public final void mSPLITTER_COMMENT() throws RecognitionException {
		try {
			int _type = SPLITTER_COMMENT;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:327:2: ( '////' (~ ( '\\n' | '\\r' ) )* ( '\\r' )? '\\n' )
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:327:4: '////' (~ ( '\\n' | '\\r' ) )* ( '\\r' )? '\\n'
			{
			match("////"); 

			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:327:11: (~ ( '\\n' | '\\r' ) )*
			loop12:
			while (true) {
				int alt12=2;
				int LA12_0 = input.LA(1);
				if ( ((LA12_0 >= '\u0000' && LA12_0 <= '\t')||(LA12_0 >= '\u000B' && LA12_0 <= '\f')||(LA12_0 >= '\u000E' && LA12_0 <= '\uFFFF')) ) {
					alt12=1;
				}

				switch (alt12) {
				case 1 :
					// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:
					{
					if ( (input.LA(1) >= '\u0000' && input.LA(1) <= '\t')||(input.LA(1) >= '\u000B' && input.LA(1) <= '\f')||(input.LA(1) >= '\u000E' && input.LA(1) <= '\uFFFF') ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

				default :
					break loop12;
				}
			}

			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:327:25: ( '\\r' )?
			int alt13=2;
			int LA13_0 = input.LA(1);
			if ( (LA13_0=='\r') ) {
				alt13=1;
			}
			switch (alt13) {
				case 1 :
					// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:327:25: '\\r'
					{
					match('\r'); 
					}
					break;

			}

			match('\n'); 
			_channel=HIDDEN;
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "SPLITTER_COMMENT"

	// $ANTLR start "DOC_COMMENT_SLASH"
	public final void mDOC_COMMENT_SLASH() throws RecognitionException {
		try {
			int _type = DOC_COMMENT_SLASH;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:331:2: ( '///' (~ ( '\\n' | '\\r' ) )* ( '\\r' )? '\\n' )
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:331:4: '///' (~ ( '\\n' | '\\r' ) )* ( '\\r' )? '\\n'
			{
			match("///"); 

			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:331:10: (~ ( '\\n' | '\\r' ) )*
			loop14:
			while (true) {
				int alt14=2;
				int LA14_0 = input.LA(1);
				if ( ((LA14_0 >= '\u0000' && LA14_0 <= '\t')||(LA14_0 >= '\u000B' && LA14_0 <= '\f')||(LA14_0 >= '\u000E' && LA14_0 <= '\uFFFF')) ) {
					alt14=1;
				}

				switch (alt14) {
				case 1 :
					// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:
					{
					if ( (input.LA(1) >= '\u0000' && input.LA(1) <= '\t')||(input.LA(1) >= '\u000B' && input.LA(1) <= '\f')||(input.LA(1) >= '\u000E' && input.LA(1) <= '\uFFFF') ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

				default :
					break loop14;
				}
			}

			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:331:24: ( '\\r' )?
			int alt15=2;
			int LA15_0 = input.LA(1);
			if ( (LA15_0=='\r') ) {
				alt15=1;
			}
			switch (alt15) {
				case 1 :
					// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:331:24: '\\r'
					{
					match('\r'); 
					}
					break;

			}

			match('\n'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "DOC_COMMENT_SLASH"

	// $ANTLR start "DOC_COMMENT_STAR"
	public final void mDOC_COMMENT_STAR() throws RecognitionException {
		try {
			int _type = DOC_COMMENT_STAR;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:334:2: (| '/**' ( options {greedy=false; } : . )* '*/' )
			int alt17=2;
			int LA17_0 = input.LA(1);
			if ( (LA17_0=='/') ) {
				alt17=2;
			}

			else {
				alt17=1;
			}

			switch (alt17) {
				case 1 :
					// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:335:2: 
					{
					}
					break;
				case 2 :
					// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:335:4: '/**' ( options {greedy=false; } : . )* '*/'
					{
					match("/**"); 

					// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:335:10: ( options {greedy=false; } : . )*
					loop16:
					while (true) {
						int alt16=2;
						int LA16_0 = input.LA(1);
						if ( (LA16_0=='*') ) {
							int LA16_1 = input.LA(2);
							if ( (LA16_1=='/') ) {
								alt16=2;
							}
							else if ( ((LA16_1 >= '\u0000' && LA16_1 <= '.')||(LA16_1 >= '0' && LA16_1 <= '\uFFFF')) ) {
								alt16=1;
							}

						}
						else if ( ((LA16_0 >= '\u0000' && LA16_0 <= ')')||(LA16_0 >= '+' && LA16_0 <= '\uFFFF')) ) {
							alt16=1;
						}

						switch (alt16) {
						case 1 :
							// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:335:38: .
							{
							matchAny(); 
							}
							break;

						default :
							break loop16;
						}
					}

					match("*/"); 

					}
					break;

			}
			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "DOC_COMMENT_STAR"

	// $ANTLR start "COMMENT"
	public final void mCOMMENT() throws RecognitionException {
		try {
			int _type = COMMENT;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:339:5: ( '//' (~ ( '\\n' | '\\r' ) )* ( '\\r' )? '\\n' | '/*' ( options {greedy=false; } : . )* '*/' )
			int alt21=2;
			int LA21_0 = input.LA(1);
			if ( (LA21_0=='/') ) {
				int LA21_1 = input.LA(2);
				if ( (LA21_1=='/') ) {
					alt21=1;
				}
				else if ( (LA21_1=='*') ) {
					alt21=2;
				}

				else {
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 21, 1, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 21, 0, input);
				throw nvae;
			}

			switch (alt21) {
				case 1 :
					// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:339:9: '//' (~ ( '\\n' | '\\r' ) )* ( '\\r' )? '\\n'
					{
					match("//"); 

					// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:339:14: (~ ( '\\n' | '\\r' ) )*
					loop18:
					while (true) {
						int alt18=2;
						int LA18_0 = input.LA(1);
						if ( ((LA18_0 >= '\u0000' && LA18_0 <= '\t')||(LA18_0 >= '\u000B' && LA18_0 <= '\f')||(LA18_0 >= '\u000E' && LA18_0 <= '\uFFFF')) ) {
							alt18=1;
						}

						switch (alt18) {
						case 1 :
							// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:
							{
							if ( (input.LA(1) >= '\u0000' && input.LA(1) <= '\t')||(input.LA(1) >= '\u000B' && input.LA(1) <= '\f')||(input.LA(1) >= '\u000E' && input.LA(1) <= '\uFFFF') ) {
								input.consume();
							}
							else {
								MismatchedSetException mse = new MismatchedSetException(null,input);
								recover(mse);
								throw mse;
							}
							}
							break;

						default :
							break loop18;
						}
					}

					// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:339:28: ( '\\r' )?
					int alt19=2;
					int LA19_0 = input.LA(1);
					if ( (LA19_0=='\r') ) {
						alt19=1;
					}
					switch (alt19) {
						case 1 :
							// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:339:28: '\\r'
							{
							match('\r'); 
							}
							break;

					}

					match('\n'); 
					_channel=HIDDEN;
					}
					break;
				case 2 :
					// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:340:9: '/*' ( options {greedy=false; } : . )* '*/'
					{
					match("/*"); 

					// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:340:14: ( options {greedy=false; } : . )*
					loop20:
					while (true) {
						int alt20=2;
						int LA20_0 = input.LA(1);
						if ( (LA20_0=='*') ) {
							int LA20_1 = input.LA(2);
							if ( (LA20_1=='/') ) {
								alt20=2;
							}
							else if ( ((LA20_1 >= '\u0000' && LA20_1 <= '.')||(LA20_1 >= '0' && LA20_1 <= '\uFFFF')) ) {
								alt20=1;
							}

						}
						else if ( ((LA20_0 >= '\u0000' && LA20_0 <= ')')||(LA20_0 >= '+' && LA20_0 <= '\uFFFF')) ) {
							alt20=1;
						}

						switch (alt20) {
						case 1 :
							// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:340:42: .
							{
							matchAny(); 
							}
							break;

						default :
							break loop20;
						}
					}

					match("*/"); 

					_channel=HIDDEN;
					}
					break;

			}
			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "COMMENT"

	// $ANTLR start "WS"
	public final void mWS() throws RecognitionException {
		try {
			int _type = WS;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:343:5: ( ( ' ' | '\\t' | '\\r' | '\\n' ) )
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:343:9: ( ' ' | '\\t' | '\\r' | '\\n' )
			{
			if ( (input.LA(1) >= '\t' && input.LA(1) <= '\n')||input.LA(1)=='\r'||input.LA(1)==' ' ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			_channel=HIDDEN;
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "WS"

	// $ANTLR start "STRING"
	public final void mSTRING() throws RecognitionException {
		try {
			int _type = STRING;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:351:5: ( '\"' ( ESC_SEQ |~ ( '\\\\' | '\"' ) )* '\"' )
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:351:8: '\"' ( ESC_SEQ |~ ( '\\\\' | '\"' ) )* '\"'
			{
			match('\"'); 
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:351:12: ( ESC_SEQ |~ ( '\\\\' | '\"' ) )*
			loop22:
			while (true) {
				int alt22=3;
				int LA22_0 = input.LA(1);
				if ( (LA22_0=='\\') ) {
					alt22=1;
				}
				else if ( ((LA22_0 >= '\u0000' && LA22_0 <= '!')||(LA22_0 >= '#' && LA22_0 <= '[')||(LA22_0 >= ']' && LA22_0 <= '\uFFFF')) ) {
					alt22=2;
				}

				switch (alt22) {
				case 1 :
					// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:351:14: ESC_SEQ
					{
					mESC_SEQ(); 

					}
					break;
				case 2 :
					// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:351:24: ~ ( '\\\\' | '\"' )
					{
					if ( (input.LA(1) >= '\u0000' && input.LA(1) <= '!')||(input.LA(1) >= '#' && input.LA(1) <= '[')||(input.LA(1) >= ']' && input.LA(1) <= '\uFFFF') ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

				default :
					break loop22;
				}
			}

			match('\"'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "STRING"

	// $ANTLR start "EXPONENT"
	public final void mEXPONENT() throws RecognitionException {
		try {
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:356:10: ( ( 'e' | 'E' ) ( '+' | '-' )? ( '0' .. '9' )+ )
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:356:12: ( 'e' | 'E' ) ( '+' | '-' )? ( '0' .. '9' )+
			{
			if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:356:22: ( '+' | '-' )?
			int alt23=2;
			int LA23_0 = input.LA(1);
			if ( (LA23_0=='+'||LA23_0=='-') ) {
				alt23=1;
			}
			switch (alt23) {
				case 1 :
					// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:
					{
					if ( input.LA(1)=='+'||input.LA(1)=='-' ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

			}

			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:356:33: ( '0' .. '9' )+
			int cnt24=0;
			loop24:
			while (true) {
				int alt24=2;
				int LA24_0 = input.LA(1);
				if ( ((LA24_0 >= '0' && LA24_0 <= '9')) ) {
					alt24=1;
				}

				switch (alt24) {
				case 1 :
					// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:
					{
					if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

				default :
					if ( cnt24 >= 1 ) break loop24;
					EarlyExitException eee = new EarlyExitException(24, input);
					throw eee;
				}
				cnt24++;
			}

			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "EXPONENT"

	// $ANTLR start "HEX_DIGIT"
	public final void mHEX_DIGIT() throws RecognitionException {
		try {
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:359:11: ( ( '0' .. '9' | 'a' .. 'f' | 'A' .. 'F' ) )
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:
			{
			if ( (input.LA(1) >= '0' && input.LA(1) <= '9')||(input.LA(1) >= 'A' && input.LA(1) <= 'F')||(input.LA(1) >= 'a' && input.LA(1) <= 'f') ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "HEX_DIGIT"

	// $ANTLR start "ESC_SEQ"
	public final void mESC_SEQ() throws RecognitionException {
		try {
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:363:5: ( '\\\\' ( 'b' | 't' | 'n' | 'f' | 'r' | '\\\"' | '\\'' | '\\\\' ) | UNICODE_ESC | OCTAL_ESC )
			int alt25=3;
			int LA25_0 = input.LA(1);
			if ( (LA25_0=='\\') ) {
				switch ( input.LA(2) ) {
				case '\"':
				case '\'':
				case '\\':
				case 'b':
				case 'f':
				case 'n':
				case 'r':
				case 't':
					{
					alt25=1;
					}
					break;
				case 'u':
					{
					alt25=2;
					}
					break;
				case '0':
				case '1':
				case '2':
				case '3':
				case '4':
				case '5':
				case '6':
				case '7':
					{
					alt25=3;
					}
					break;
				default:
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 25, 1, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}
			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 25, 0, input);
				throw nvae;
			}

			switch (alt25) {
				case 1 :
					// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:363:9: '\\\\' ( 'b' | 't' | 'n' | 'f' | 'r' | '\\\"' | '\\'' | '\\\\' )
					{
					match('\\'); 
					if ( input.LA(1)=='\"'||input.LA(1)=='\''||input.LA(1)=='\\'||input.LA(1)=='b'||input.LA(1)=='f'||input.LA(1)=='n'||input.LA(1)=='r'||input.LA(1)=='t' ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;
				case 2 :
					// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:364:9: UNICODE_ESC
					{
					mUNICODE_ESC(); 

					}
					break;
				case 3 :
					// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:365:9: OCTAL_ESC
					{
					mOCTAL_ESC(); 

					}
					break;

			}
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "ESC_SEQ"

	// $ANTLR start "OCTAL_ESC"
	public final void mOCTAL_ESC() throws RecognitionException {
		try {
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:370:5: ( '\\\\' ( '0' .. '3' ) ( '0' .. '7' ) ( '0' .. '7' ) | '\\\\' ( '0' .. '7' ) ( '0' .. '7' ) | '\\\\' ( '0' .. '7' ) )
			int alt26=3;
			int LA26_0 = input.LA(1);
			if ( (LA26_0=='\\') ) {
				int LA26_1 = input.LA(2);
				if ( ((LA26_1 >= '0' && LA26_1 <= '3')) ) {
					int LA26_2 = input.LA(3);
					if ( ((LA26_2 >= '0' && LA26_2 <= '7')) ) {
						int LA26_4 = input.LA(4);
						if ( ((LA26_4 >= '0' && LA26_4 <= '7')) ) {
							alt26=1;
						}

						else {
							alt26=2;
						}

					}

					else {
						alt26=3;
					}

				}
				else if ( ((LA26_1 >= '4' && LA26_1 <= '7')) ) {
					int LA26_3 = input.LA(3);
					if ( ((LA26_3 >= '0' && LA26_3 <= '7')) ) {
						alt26=2;
					}

					else {
						alt26=3;
					}

				}

				else {
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 26, 1, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 26, 0, input);
				throw nvae;
			}

			switch (alt26) {
				case 1 :
					// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:370:9: '\\\\' ( '0' .. '3' ) ( '0' .. '7' ) ( '0' .. '7' )
					{
					match('\\'); 
					if ( (input.LA(1) >= '0' && input.LA(1) <= '3') ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					if ( (input.LA(1) >= '0' && input.LA(1) <= '7') ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					if ( (input.LA(1) >= '0' && input.LA(1) <= '7') ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;
				case 2 :
					// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:371:9: '\\\\' ( '0' .. '7' ) ( '0' .. '7' )
					{
					match('\\'); 
					if ( (input.LA(1) >= '0' && input.LA(1) <= '7') ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					if ( (input.LA(1) >= '0' && input.LA(1) <= '7') ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;
				case 3 :
					// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:372:9: '\\\\' ( '0' .. '7' )
					{
					match('\\'); 
					if ( (input.LA(1) >= '0' && input.LA(1) <= '7') ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

			}
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "OCTAL_ESC"

	// $ANTLR start "UNICODE_ESC"
	public final void mUNICODE_ESC() throws RecognitionException {
		try {
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:377:5: ( '\\\\' 'u' HEX_DIGIT HEX_DIGIT HEX_DIGIT HEX_DIGIT )
			// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:377:9: '\\\\' 'u' HEX_DIGIT HEX_DIGIT HEX_DIGIT HEX_DIGIT
			{
			match('\\'); 
			match('u'); 
			mHEX_DIGIT(); 

			mHEX_DIGIT(); 

			mHEX_DIGIT(); 

			mHEX_DIGIT(); 

			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "UNICODE_ESC"

	@Override
	public void mTokens() throws RecognitionException {
		// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:1:8: ( T__59 | T__60 | T__61 | T__62 | T__63 | T__64 | T__65 | T__66 | T__67 | T__68 | T__69 | T__70 | T__71 | T__72 | T__73 | T__74 | T__75 | T__76 | T__77 | T__78 | T__79 | T__80 | T__81 | T__82 | T__83 | T__84 | T__85 | T__86 | T__87 | BOOL_ | NULL | ID | NUM | HEX_NUM | FLOAT | SPLITTER_COMMENT | DOC_COMMENT_SLASH | DOC_COMMENT_STAR | COMMENT | WS | STRING )
		int alt27=41;
		alt27 = dfa27.predict(input);
		switch (alt27) {
			case 1 :
				// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:1:10: T__59
				{
				mT__59(); 

				}
				break;
			case 2 :
				// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:1:16: T__60
				{
				mT__60(); 

				}
				break;
			case 3 :
				// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:1:22: T__61
				{
				mT__61(); 

				}
				break;
			case 4 :
				// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:1:28: T__62
				{
				mT__62(); 

				}
				break;
			case 5 :
				// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:1:34: T__63
				{
				mT__63(); 

				}
				break;
			case 6 :
				// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:1:40: T__64
				{
				mT__64(); 

				}
				break;
			case 7 :
				// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:1:46: T__65
				{
				mT__65(); 

				}
				break;
			case 8 :
				// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:1:52: T__66
				{
				mT__66(); 

				}
				break;
			case 9 :
				// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:1:58: T__67
				{
				mT__67(); 

				}
				break;
			case 10 :
				// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:1:64: T__68
				{
				mT__68(); 

				}
				break;
			case 11 :
				// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:1:70: T__69
				{
				mT__69(); 

				}
				break;
			case 12 :
				// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:1:76: T__70
				{
				mT__70(); 

				}
				break;
			case 13 :
				// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:1:82: T__71
				{
				mT__71(); 

				}
				break;
			case 14 :
				// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:1:88: T__72
				{
				mT__72(); 

				}
				break;
			case 15 :
				// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:1:94: T__73
				{
				mT__73(); 

				}
				break;
			case 16 :
				// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:1:100: T__74
				{
				mT__74(); 

				}
				break;
			case 17 :
				// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:1:106: T__75
				{
				mT__75(); 

				}
				break;
			case 18 :
				// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:1:112: T__76
				{
				mT__76(); 

				}
				break;
			case 19 :
				// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:1:118: T__77
				{
				mT__77(); 

				}
				break;
			case 20 :
				// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:1:124: T__78
				{
				mT__78(); 

				}
				break;
			case 21 :
				// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:1:130: T__79
				{
				mT__79(); 

				}
				break;
			case 22 :
				// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:1:136: T__80
				{
				mT__80(); 

				}
				break;
			case 23 :
				// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:1:142: T__81
				{
				mT__81(); 

				}
				break;
			case 24 :
				// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:1:148: T__82
				{
				mT__82(); 

				}
				break;
			case 25 :
				// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:1:154: T__83
				{
				mT__83(); 

				}
				break;
			case 26 :
				// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:1:160: T__84
				{
				mT__84(); 

				}
				break;
			case 27 :
				// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:1:166: T__85
				{
				mT__85(); 

				}
				break;
			case 28 :
				// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:1:172: T__86
				{
				mT__86(); 

				}
				break;
			case 29 :
				// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:1:178: T__87
				{
				mT__87(); 

				}
				break;
			case 30 :
				// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:1:184: BOOL_
				{
				mBOOL_(); 

				}
				break;
			case 31 :
				// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:1:190: NULL
				{
				mNULL(); 

				}
				break;
			case 32 :
				// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:1:195: ID
				{
				mID(); 

				}
				break;
			case 33 :
				// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:1:198: NUM
				{
				mNUM(); 

				}
				break;
			case 34 :
				// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:1:202: HEX_NUM
				{
				mHEX_NUM(); 

				}
				break;
			case 35 :
				// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:1:210: FLOAT
				{
				mFLOAT(); 

				}
				break;
			case 36 :
				// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:1:216: SPLITTER_COMMENT
				{
				mSPLITTER_COMMENT(); 

				}
				break;
			case 37 :
				// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:1:233: DOC_COMMENT_SLASH
				{
				mDOC_COMMENT_SLASH(); 

				}
				break;
			case 38 :
				// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:1:251: DOC_COMMENT_STAR
				{
				mDOC_COMMENT_STAR(); 

				}
				break;
			case 39 :
				// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:1:268: COMMENT
				{
				mCOMMENT(); 

				}
				break;
			case 40 :
				// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:1:276: WS
				{
				mWS(); 

				}
				break;
			case 41 :
				// /Users/jbartipan/work/iomonkey2/grammar/IOMonkey2.g:1:279: STRING
				{
				mSTRING(); 

				}
				break;

		}
	}


	protected DFA11 dfa11 = new DFA11(this);
	protected DFA27 dfa27 = new DFA27(this);
	static final String DFA11_eotS =
		"\5\uffff";
	static final String DFA11_eofS =
		"\5\uffff";
	static final String DFA11_minS =
		"\2\56\3\uffff";
	static final String DFA11_maxS =
		"\1\71\1\145\3\uffff";
	static final String DFA11_acceptS =
		"\2\uffff\1\2\1\1\1\3";
	static final String DFA11_specialS =
		"\5\uffff}>";
	static final String[] DFA11_transitionS = {
			"\1\2\1\uffff\12\1",
			"\1\3\1\uffff\12\1\13\uffff\1\4\37\uffff\1\4",
			"",
			"",
			""
	};

	static final short[] DFA11_eot = DFA.unpackEncodedString(DFA11_eotS);
	static final short[] DFA11_eof = DFA.unpackEncodedString(DFA11_eofS);
	static final char[] DFA11_min = DFA.unpackEncodedStringToUnsignedChars(DFA11_minS);
	static final char[] DFA11_max = DFA.unpackEncodedStringToUnsignedChars(DFA11_maxS);
	static final short[] DFA11_accept = DFA.unpackEncodedString(DFA11_acceptS);
	static final short[] DFA11_special = DFA.unpackEncodedString(DFA11_specialS);
	static final short[][] DFA11_transition;

	static {
		int numStates = DFA11_transitionS.length;
		DFA11_transition = new short[numStates][];
		for (int i=0; i<numStates; i++) {
			DFA11_transition[i] = DFA.unpackEncodedString(DFA11_transitionS[i]);
		}
	}

	protected class DFA11 extends DFA {

		public DFA11(BaseRecognizer recognizer) {
			this.recognizer = recognizer;
			this.decisionNumber = 11;
			this.eot = DFA11_eot;
			this.eof = DFA11_eof;
			this.min = DFA11_min;
			this.max = DFA11_max;
			this.accept = DFA11_accept;
			this.special = DFA11_special;
			this.transition = DFA11_transition;
		}
		@Override
		public String getDescription() {
			return "319:1: FLOAT : ( ( '0' .. '9' )+ '.' ( '0' .. '9' )* ( EXPONENT )? | '.' ( '0' .. '9' )+ ( EXPONENT )? | ( '0' .. '9' )+ EXPONENT );";
		}
	}

	static final String DFA27_eotS =
		"\1\37\5\uffff\1\42\6\uffff\12\33\2\uffff\2\33\1\uffff\2\65\6\uffff\20"+
		"\33\4\uffff\7\33\1\122\10\33\3\uffff\1\33\1\144\1\145\1\146\3\33\1\uffff"+
		"\1\152\6\33\1\161\1\162\4\uffff\1\111\2\uffff\1\33\3\uffff\1\171\1\161"+
		"\1\33\1\uffff\3\33\1\176\2\33\6\uffff\1\37\1\33\1\uffff\1\u0083\3\33\1"+
		"\uffff\1\u0088\1\u0089\1\uffff\1\33\1\uffff\1\33\1\u008c\1\u008d\1\33"+
		"\2\uffff\1\u008f\1\u0090\2\uffff\1\u0091\3\uffff";
	static final String DFA27_eofS =
		"\u0092\uffff";
	static final String DFA27_minS =
		"\1\11\5\uffff\1\60\6\uffff\1\142\1\157\1\156\1\141\1\155\1\157\1\160\1"+
		"\141\1\145\1\150\2\uffff\1\162\1\165\1\uffff\2\56\1\52\5\uffff\1\163\1"+
		"\157\1\164\1\165\1\157\1\154\1\160\1\164\1\156\1\164\1\143\1\161\1\157"+
		"\1\162\1\165\1\154\2\uffff\2\0\1\164\1\154\1\145\1\155\1\141\1\163\1\157"+
		"\1\60\1\147\1\151\1\153\1\165\1\162\1\151\1\145\1\154\1\0\1\uffff\1\0"+
		"\1\162\3\60\1\164\1\145\1\162\1\uffff\1\60\1\157\1\141\1\151\1\164\1\156"+
		"\1\143\2\60\2\0\1\12\1\uffff\3\0\1\141\3\uffff\2\60\1\164\1\uffff\1\156"+
		"\1\147\1\162\1\60\1\147\1\164\2\uffff\1\0\1\12\2\uffff\1\0\1\143\1\uffff"+
		"\1\60\1\141\2\145\1\uffff\2\60\1\uffff\1\164\1\uffff\1\154\2\60\1\144"+
		"\2\uffff\2\60\2\uffff\1\60\3\uffff";
	static final String DFA27_maxS =
		"\1\175\5\uffff\1\71\6\uffff\1\142\1\171\1\156\1\154\1\156\1\157\1\160"+
		"\1\141\1\145\1\164\2\uffff\1\162\1\165\1\uffff\1\170\1\145\1\57\5\uffff"+
		"\1\163\1\157\1\164\1\165\1\157\1\154\1\160\1\164\1\156\1\164\1\143\1\161"+
		"\1\157\1\162\1\165\1\154\2\uffff\2\uffff\1\164\1\154\1\145\1\155\1\141"+
		"\1\163\1\157\1\172\1\147\1\151\1\153\1\165\1\162\1\165\1\145\1\154\1\uffff"+
		"\1\uffff\1\uffff\1\162\3\172\1\164\1\145\1\162\1\uffff\1\172\1\157\1\141"+
		"\1\151\1\164\1\156\1\143\2\172\2\uffff\1\12\1\uffff\3\uffff\1\141\3\uffff"+
		"\2\172\1\164\1\uffff\1\156\1\147\1\162\1\172\1\147\1\164\2\uffff\1\uffff"+
		"\1\12\2\uffff\1\uffff\1\143\1\uffff\1\172\1\163\2\145\1\uffff\2\172\1"+
		"\uffff\1\164\1\uffff\1\154\2\172\1\144\2\uffff\2\172\2\uffff\1\172\3\uffff";
	static final String DFA27_acceptS =
		"\1\uffff\1\1\1\2\1\3\1\4\1\5\1\uffff\1\7\1\10\1\11\1\12\1\13\1\14\12\uffff"+
		"\1\34\1\35\2\uffff\1\40\3\uffff\1\46\1\50\1\51\1\6\1\43\20\uffff\1\42"+
		"\1\41\23\uffff\1\47\10\uffff\1\23\14\uffff\1\45\4\uffff\1\16\1\17\1\20"+
		"\3\uffff\1\24\6\uffff\1\36\1\37\2\uffff\1\44\1\45\2\uffff\1\21\4\uffff"+
		"\1\31\2\uffff\1\44\1\uffff\1\22\4\uffff\1\32\1\33\2\uffff\1\26\1\27\1"+
		"\uffff\1\15\1\25\1\30";
	static final String DFA27_specialS =
		"\66\uffff\1\4\1\5\20\uffff\1\3\1\uffff\1\0\21\uffff\1\10\1\7\2\uffff\1"+
		"\1\1\2\1\12\20\uffff\1\11\3\uffff\1\6\32\uffff}>";
	static final String[] DFA27_transitionS = {
			"\2\40\2\uffff\1\40\22\uffff\1\40\1\uffff\1\41\5\uffff\1\1\1\2\1\uffff"+
			"\1\3\1\4\1\5\1\6\1\36\1\34\11\35\1\7\1\10\1\uffff\1\11\2\uffff\1\12\32"+
			"\33\1\13\1\uffff\1\14\1\uffff\1\33\1\uffff\1\15\1\16\2\33\1\17\1\20\2"+
			"\33\1\21\2\33\1\22\1\33\1\32\1\23\1\24\1\33\1\25\1\26\1\31\6\33\1\27"+
			"\1\uffff\1\30",
			"",
			"",
			"",
			"",
			"",
			"\12\43",
			"",
			"",
			"",
			"",
			"",
			"",
			"\1\44",
			"\1\45\11\uffff\1\46",
			"\1\47",
			"\1\51\12\uffff\1\50",
			"\1\52\1\53",
			"\1\54",
			"\1\55",
			"\1\56",
			"\1\57",
			"\1\60\13\uffff\1\61",
			"",
			"",
			"\1\62",
			"\1\63",
			"",
			"\1\43\1\uffff\12\35\13\uffff\1\43\37\uffff\1\43\22\uffff\1\64",
			"\1\43\1\uffff\12\35\13\uffff\1\43\37\uffff\1\43",
			"\1\67\4\uffff\1\66",
			"",
			"",
			"",
			"",
			"",
			"\1\70",
			"\1\71",
			"\1\72",
			"\1\73",
			"\1\74",
			"\1\75",
			"\1\76",
			"\1\77",
			"\1\100",
			"\1\101",
			"\1\102",
			"\1\103",
			"\1\104",
			"\1\105",
			"\1\106",
			"\1\107",
			"",
			"",
			"\57\111\1\110\uffd0\111",
			"\52\111\1\112\uffd5\111",
			"\1\113",
			"\1\114",
			"\1\115",
			"\1\116",
			"\1\117",
			"\1\120",
			"\1\121",
			"\12\33\7\uffff\32\33\4\uffff\1\33\1\uffff\32\33",
			"\1\123",
			"\1\124",
			"\1\125",
			"\1\126",
			"\1\127",
			"\1\130\13\uffff\1\131",
			"\1\132",
			"\1\133",
			"\12\135\1\137\2\135\1\136\41\135\1\134\uffd0\135",
			"",
			"\52\142\1\141\4\142\1\140\uffd0\142",
			"\1\143",
			"\12\33\7\uffff\32\33\4\uffff\1\33\1\uffff\32\33",
			"\12\33\7\uffff\32\33\4\uffff\1\33\1\uffff\32\33",
			"\12\33\7\uffff\32\33\4\uffff\1\33\1\uffff\32\33",
			"\1\147",
			"\1\150",
			"\1\151",
			"",
			"\12\33\7\uffff\32\33\4\uffff\1\33\1\uffff\32\33",
			"\1\153",
			"\1\154",
			"\1\155",
			"\1\156",
			"\1\157",
			"\1\160",
			"\12\33\7\uffff\32\33\4\uffff\1\33\1\uffff\32\33",
			"\12\33\7\uffff\32\33\4\uffff\1\33\1\uffff\32\33",
			"\12\163\1\165\2\163\1\164\ufff2\163",
			"\12\135\1\137\2\135\1\136\ufff2\135",
			"\1\137",
			"",
			"\52\142\1\141\uffd5\142",
			"\52\142\1\141\4\142\1\167\uffd0\142",
			"\52\142\1\141\uffd5\142",
			"\1\170",
			"",
			"",
			"",
			"\12\33\7\uffff\32\33\4\uffff\1\33\1\uffff\32\33",
			"\12\33\7\uffff\32\33\4\uffff\1\33\1\uffff\32\33",
			"\1\172",
			"",
			"\1\173",
			"\1\174",
			"\1\175",
			"\12\33\7\uffff\32\33\4\uffff\1\33\1\uffff\32\33",
			"\1\177",
			"\1\u0080",
			"",
			"",
			"\12\163\1\165\2\163\1\164\ufff2\163",
			"\1\165",
			"",
			"",
			"\52\142\1\141\uffd5\142",
			"\1\u0082",
			"",
			"\12\33\7\uffff\32\33\4\uffff\1\33\1\uffff\32\33",
			"\1\u0084\21\uffff\1\u0085",
			"\1\u0086",
			"\1\u0087",
			"",
			"\12\33\7\uffff\32\33\4\uffff\1\33\1\uffff\32\33",
			"\12\33\7\uffff\32\33\4\uffff\1\33\1\uffff\32\33",
			"",
			"\1\u008a",
			"",
			"\1\u008b",
			"\12\33\7\uffff\32\33\4\uffff\1\33\1\uffff\32\33",
			"\12\33\7\uffff\32\33\4\uffff\1\33\1\uffff\32\33",
			"\1\u008e",
			"",
			"",
			"\12\33\7\uffff\32\33\4\uffff\1\33\1\uffff\32\33",
			"\12\33\7\uffff\32\33\4\uffff\1\33\1\uffff\32\33",
			"",
			"",
			"\12\33\7\uffff\32\33\4\uffff\1\33\1\uffff\32\33",
			"",
			"",
			""
	};

	static final short[] DFA27_eot = DFA.unpackEncodedString(DFA27_eotS);
	static final short[] DFA27_eof = DFA.unpackEncodedString(DFA27_eofS);
	static final char[] DFA27_min = DFA.unpackEncodedStringToUnsignedChars(DFA27_minS);
	static final char[] DFA27_max = DFA.unpackEncodedStringToUnsignedChars(DFA27_maxS);
	static final short[] DFA27_accept = DFA.unpackEncodedString(DFA27_acceptS);
	static final short[] DFA27_special = DFA.unpackEncodedString(DFA27_specialS);
	static final short[][] DFA27_transition;

	static {
		int numStates = DFA27_transitionS.length;
		DFA27_transition = new short[numStates][];
		for (int i=0; i<numStates; i++) {
			DFA27_transition[i] = DFA.unpackEncodedString(DFA27_transitionS[i]);
		}
	}

	protected class DFA27 extends DFA {

		public DFA27(BaseRecognizer recognizer) {
			this.recognizer = recognizer;
			this.decisionNumber = 27;
			this.eot = DFA27_eot;
			this.eof = DFA27_eof;
			this.min = DFA27_min;
			this.max = DFA27_max;
			this.accept = DFA27_accept;
			this.special = DFA27_special;
			this.transition = DFA27_transition;
		}
		@Override
		public String getDescription() {
			return "1:1: Tokens : ( T__59 | T__60 | T__61 | T__62 | T__63 | T__64 | T__65 | T__66 | T__67 | T__68 | T__69 | T__70 | T__71 | T__72 | T__73 | T__74 | T__75 | T__76 | T__77 | T__78 | T__79 | T__80 | T__81 | T__82 | T__83 | T__84 | T__85 | T__86 | T__87 | BOOL_ | NULL | ID | NUM | HEX_NUM | FLOAT | SPLITTER_COMMENT | DOC_COMMENT_SLASH | DOC_COMMENT_STAR | COMMENT | WS | STRING );";
		}
		@Override
		public int specialStateTransition(int s, IntStream _input) throws NoViableAltException {
			IntStream input = _input;
			int _s = s;
			switch ( s ) {
					case 0 : 
						int LA27_74 = input.LA(1);
						s = -1;
						if ( (LA27_74=='/') ) {s = 96;}
						else if ( (LA27_74=='*') ) {s = 97;}
						else if ( ((LA27_74 >= '\u0000' && LA27_74 <= ')')||(LA27_74 >= '+' && LA27_74 <= '.')||(LA27_74 >= '0' && LA27_74 <= '\uFFFF')) ) {s = 98;}
						if ( s>=0 ) return s;
						break;

					case 1 : 
						int LA27_96 = input.LA(1);
						s = -1;
						if ( (LA27_96=='*') ) {s = 97;}
						else if ( ((LA27_96 >= '\u0000' && LA27_96 <= ')')||(LA27_96 >= '+' && LA27_96 <= '\uFFFF')) ) {s = 98;}
						else s = 73;
						if ( s>=0 ) return s;
						break;

					case 2 : 
						int LA27_97 = input.LA(1);
						s = -1;
						if ( (LA27_97=='/') ) {s = 119;}
						else if ( (LA27_97=='*') ) {s = 97;}
						else if ( ((LA27_97 >= '\u0000' && LA27_97 <= ')')||(LA27_97 >= '+' && LA27_97 <= '.')||(LA27_97 >= '0' && LA27_97 <= '\uFFFF')) ) {s = 98;}
						if ( s>=0 ) return s;
						break;

					case 3 : 
						int LA27_72 = input.LA(1);
						s = -1;
						if ( (LA27_72=='/') ) {s = 92;}
						else if ( ((LA27_72 >= '\u0000' && LA27_72 <= '\t')||(LA27_72 >= '\u000B' && LA27_72 <= '\f')||(LA27_72 >= '\u000E' && LA27_72 <= '.')||(LA27_72 >= '0' && LA27_72 <= '\uFFFF')) ) {s = 93;}
						else if ( (LA27_72=='\r') ) {s = 94;}
						else if ( (LA27_72=='\n') ) {s = 95;}
						if ( s>=0 ) return s;
						break;

					case 4 : 
						int LA27_54 = input.LA(1);
						s = -1;
						if ( (LA27_54=='/') ) {s = 72;}
						else if ( ((LA27_54 >= '\u0000' && LA27_54 <= '.')||(LA27_54 >= '0' && LA27_54 <= '\uFFFF')) ) {s = 73;}
						if ( s>=0 ) return s;
						break;

					case 5 : 
						int LA27_55 = input.LA(1);
						s = -1;
						if ( (LA27_55=='*') ) {s = 74;}
						else if ( ((LA27_55 >= '\u0000' && LA27_55 <= ')')||(LA27_55 >= '+' && LA27_55 <= '\uFFFF')) ) {s = 73;}
						if ( s>=0 ) return s;
						break;

					case 6 : 
						int LA27_119 = input.LA(1);
						s = -1;
						if ( (LA27_119=='*') ) {s = 97;}
						else if ( ((LA27_119 >= '\u0000' && LA27_119 <= ')')||(LA27_119 >= '+' && LA27_119 <= '\uFFFF')) ) {s = 98;}
						else s = 31;
						if ( s>=0 ) return s;
						break;

					case 7 : 
						int LA27_93 = input.LA(1);
						s = -1;
						if ( (LA27_93=='\r') ) {s = 94;}
						else if ( (LA27_93=='\n') ) {s = 95;}
						else if ( ((LA27_93 >= '\u0000' && LA27_93 <= '\t')||(LA27_93 >= '\u000B' && LA27_93 <= '\f')||(LA27_93 >= '\u000E' && LA27_93 <= '\uFFFF')) ) {s = 93;}
						if ( s>=0 ) return s;
						break;

					case 8 : 
						int LA27_92 = input.LA(1);
						s = -1;
						if ( ((LA27_92 >= '\u0000' && LA27_92 <= '\t')||(LA27_92 >= '\u000B' && LA27_92 <= '\f')||(LA27_92 >= '\u000E' && LA27_92 <= '\uFFFF')) ) {s = 115;}
						else if ( (LA27_92=='\r') ) {s = 116;}
						else if ( (LA27_92=='\n') ) {s = 117;}
						if ( s>=0 ) return s;
						break;

					case 9 : 
						int LA27_115 = input.LA(1);
						s = -1;
						if ( (LA27_115=='\r') ) {s = 116;}
						else if ( (LA27_115=='\n') ) {s = 117;}
						else if ( ((LA27_115 >= '\u0000' && LA27_115 <= '\t')||(LA27_115 >= '\u000B' && LA27_115 <= '\f')||(LA27_115 >= '\u000E' && LA27_115 <= '\uFFFF')) ) {s = 115;}
						if ( s>=0 ) return s;
						break;

					case 10 : 
						int LA27_98 = input.LA(1);
						s = -1;
						if ( (LA27_98=='*') ) {s = 97;}
						else if ( ((LA27_98 >= '\u0000' && LA27_98 <= ')')||(LA27_98 >= '+' && LA27_98 <= '\uFFFF')) ) {s = 98;}
						if ( s>=0 ) return s;
						break;
			}
			NoViableAltException nvae =
				new NoViableAltException(getDescription(), 27, _s, input);
			error(nvae);
			throw nvae;
		}
	}

}
