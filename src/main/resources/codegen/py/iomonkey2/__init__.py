import struct


class StreamReader(object):
    def __init__(self, stream):
        self._fp = stream

    def _readAndUnpack(self, fmt, size):
        val, = struct.unpack(fmt, self._fp.read(size))
        return val

    def readByte(self):
        return self._readAndUnpack("b", 1)

    def readShort(self):
        return self._readAndUnpack("h", 2)

    def readInt(self):
        return self._readAndUnpack("i", 4)

    def readFloat(self):
        return self._readAndUnpack("f", 4)

    def readString(self):
        n = self._readAndUnpack("I", 4)
        return self._fp.read(n).decode("utf-8")


class StreamWriter(object):
    def __init__(self, stream):
        self._fp = stream

    def writeByte(self, val):
        self._fp.write(struct.pack("b", val))

    def writeShort(self, val):
        self._fp.write(struct.pack("h", val))

    def writeInt(self, val):
        self._fp.write(struct.pack("i", val))

    def writeFloat(self, val):
        self._fp.write(struct.pack("f", val))

    def writeString(self, val):
        buf = val.encode("utf-8")
        self._fp.write(struct.pack("I",len(buf)) + buf)