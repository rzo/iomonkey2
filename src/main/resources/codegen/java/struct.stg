group struct;


basicStructDocument(namespace, struct, unit) ::= <<
package <unit.javaName>;

import java.io.*;
import net.bartipan.iomonkey2.ILoadSave;
import net.bartipan.iomonkey2.FixedValueException;
import net.bartipan.iomonkey2.ExcessiveArraySizeException;
import net.bartipan.iomonkey2.LoadSaveHelper;

<unit.imports:importImport()>
<unit.javaCustomImports:customImport()>

<struct.docComment.java>
<if(struct.extending)>public <classSignature(struct)> <struct.name> extends <struct.resolvedSuperClass.name><\\>
<else>public <classSignature(struct)> <struct.name> implements ILoadSave, Serializable, Cloneable<\\>
<endif> {
    <struct.attributes:structAttrib()>

    <struct.linkInfo.optionalAttributes:implicitAttributeConst()>

    <structLoader(struct)>

    <structSaver(struct)>

    <structSaverSize(struct)>

<if(!struct.abstractStruct)>    
    public Object clone() {
        <struct.name> ret = new <struct.name>();

        <struct.allAttributes:cloneAttrib()>

        return ret;
    }
<elseif(!struct.extending)>
    public abstract Object clone();
<endif>

}

>>

classSignature(struct) ::= "<if(struct.abstractStruct)>abstract <endif>class"

implicitAttributeConst(attr) ::= <<
<if(attr.type.fixedArray)>
public static final <attr.type.javaType> <attr.javaName>Implicit = <attr.type.javaFixedArrayImplicitValue>;
<endif>
>>

importImport(imp) ::= <<
import <imp.unit.javaName>.*;

>>

customImport(imp) ::= <<
import <imp>;

>>

structAttrib(attr) ::= <<
<attr.docComment.java>
<attr.javaGeneratedAnnotations:generatedAnnotation()>
<if(attr.implicitValue)>
public <attr.type.javaType> <attr.javaName> = <attr.implicitValue.javaValue>;
<elseif(attr.type.array || attr.type.resolvedStruct)>
<if(attr.null)>
public <attr.type.javaType> <attr.javaName>;
<else>
public <attr.type.javaType> <attr.javaName> = <attr.type.javaCtor>;
<endif>
<else>
public <attr.type.javaType> <attr.javaName>;
<endif>

>>

generatedAnnotation(annotation) ::= <<
<annotation.javaDefinition>
>>

structLoader(struct) ::= <<
public void load(DataInput dis) throws IOException {
<if(struct.extended || struct.extending)>
	long signiture = dis.readLong();
	if (signiture != <struct.linkInfo.structId.long>L)
		throw new IOException("Expected signiture mismatched");

<endif>

	loadInnerData(dis);
}

public void loadInnerData(DataInput dis) throws IOException {
    // read required
    loadRequiredAttributes(dis);

<if(struct.hasOptionals)>
    // read optionals
    int numOpts = dis.readShort();
    while (numOpts-- > 0) {
        int optIdx = dis.readShort();
        int size  = dis.readInt();
        if (!loadOptionalAttribute(dis, optIdx)) {
            dis.skipBytes(size);
        }
    }
<endif>
}

protected void loadRequiredAttributes(DataInput dis) throws IOException {
<if(struct.extending)>
    super.loadRequiredAttributes(dis);

<endif>
<if(struct.hasRequiredStructFixedArrayAttribute)>
    int nullIdx;
    java.util.ArrayList\<Integer> nullIndices = new java.util.ArrayList\<Integer>();
<endif>

    <struct.linkInfo.requiredAttributes:reqAttrLoad()>

}

protected boolean loadOptionalAttribute(DataInput dis, int optIdx) throws IOException {
<if(struct.hasOptionalStructFixedArrayAttribute)>
    int nullIdx;
    java.util.ArrayList\<Integer> nullIndices = new java.util.ArrayList\<Integer>();
<endif>
<if(struct.hasOptionals)>
    switch (optIdx) {
    <struct.linkInfo.optionalAttributes:optAttrLoadCase()>

<if(struct.extending)>

    default:
        return super.loadOptionalAttribute(dis, optIdx);
<endif>

    }
<endif>

<if(!struct.extending || !struct.hasOptionals)>
    return false;
<endif>
}

>>

reqAttrLoad(attr) ::= <<
<if(attr.type.fixedArray)>
<fixedArrayAttrLoad(attr)>
<elseif(attr.type.dynamicArray)>
<dynamicArrayAttrLoad(attr)>
<else>
<scalarAttrLoad(attr)>
<endif>

>>

readAttr(attr) ::= <%
<if(attr.type.resolvedStruct && attr.type.resolvedStruct.needsSupportForInheritance)>
IomReaderUtils.read<attr.type.resolvedStruct.name>(dis)
<elseif(attr.type.resolvedEnum)>
<attr.type.resolvedEnum.javaName>.fromInt(dis.readInt())
<else>
dis.<attr.type.javaReadMethod>()
<endif>
%>

fixedArrayAttrLoad(attr) ::= <<
<if(attr.type.resolvedStruct)>
LoadSaveHelper.readNullIndices(dis, nullIndices);
nullIdx = 0;
<endif>

<if(attr.type.pod || (attr.type.resolvedStruct && attr.type.resolvedStruct.needsSupportForInheritance))>
for (int i = 0; i \< <attr.type.arraySize>; i++) {
<if(attr.type.resolvedStruct)>
    if (nullIdx \< nullIndices.size() && i == nullIndices.get(nullIdx)) {
        <attr.javaName>[i] = null;
        nullIdx++;
    } else

<endif>
    <attr.javaName>[i] = <readAttr(attr)>;
}
<else>
// non-POD read
<attr.javaName> = <attr.type.javaCtor>;
for (int i = 0; i \< <attr.type.arraySize>; i++) {
<if(attr.type.resolvedStruct)>
    if (nullIdx \< nullIndices.size() && i == nullIndices.get(nullIdx)) {
        <attr.javaName>[i] = null;
        nullIdx++;
    } else {
        <attr.javaName>[i] = <attr.type.javaScalarCtor>;
        <attr.javaName>[i].load(dis);
    }
<else>
    <attr.javaName>[i] = <attr.type.javaScalarCtor>;
    <attr.javaName>[i].load(dis);
<endif>
}
<endif>

>>

dynamicArrayAttrLoad(attr) ::= <<
{
    int size = dis.readInt();
    if (size > <attr.maxArraySize>)
        throw new ExcessiveArraySizeException(<attr.maxArraySize>, size);

<if(attr.type.nativeArray)>
    <attr.javaName> = new <attr.type.javaScalarType>[size];
<endif>
<if(attr.type.nativeByteArray)>
    dis.readFully(<attr.javaName>);
<else>
    for (int i = 0; i \< size; i++)
    {
<if(attr.type.pod || (attr.type.resolvedStruct && attr.type.resolvedStruct.needsSupportForInheritance))>
        <attr.type.javaScalarType> x = <readAttr(attr)>;
<else>
        // non-POD read
        <attr.type.javaScalarType> x = <attr.type.javaScalarCtor>;
        x.load(dis);
<endif>
<if(attr.type.map)>
        <attr.javaName>.put(x.<attr.type.mapDescriptor.keyAttribute.javaName>, x);
<elseif(attr.type.nativeArray)>
        <attr.javaName>[i] = x;
<else>
        <attr.javaName>.add(x);
<endif>
    }
<endif>
}

>>

scalarAttrLoad(attr) ::= <<
<if(attr.type.pod || (attr.type.resolvedStruct && attr.type.resolvedStruct.needsSupportForInheritance))>
<attr.javaName> = <readAttr(attr)>;
<else>
// non-POD read
<attr.javaName> = <attr.type.javaCtor>;
<attr.javaName>.load(dis);
<endif>

<fixedAttributeTest(attr)>
>>

optAttrLoadCase(attr) ::= <<
case <attr.slotIndex>:
<if(attr.type.fixedArray)>
    <fixedArrayAttrLoad(attr)>
<elseif(attr.type.dynamicArray)>
    <dynamicArrayAttrLoad(attr)>
<else>
    <scalarAttrLoad(attr)>
<endif>

    return true;


>>

fixedAttributeTest(attr) ::=<<
<if(attr.fixedValue)>
if (<attr.fixedValueJavaComparison>)
	throw new FixedValueException(<attr.fixedValue.javaValue>, <attr.javaName>, getClass());

<endif>
>>

structSaver(struct) ::= <<
public void save(DataOutput dos) throws IOException {
<if(struct.extended || struct.extending)>
    dos.writeLong(<struct.linkInfo.structId.long>L);

<endif>
    saveRequiredAttributes(dos);
    
<if(struct.hasOptionals)>
    short optCount = countOptionalAttributesToSave();
    dos.writeShort(optCount);
    saveOptionalAttributes(dos);

<endif>
}

protected void saveRequiredAttributes(DataOutput dos) throws IOException {
<if(struct.extending)>
    super.saveRequiredAttributes(dos);
    
<endif>    
    <struct.linkInfo.requiredAttributes:reqAttrSave()>

}


protected short countOptionalAttributesToSave() {
<if(struct.extending)>
    short optCount = super.countOptionalAttributesToSave();
<else>
    short optCount = 0;
<endif>
    
<if(struct.hasOptionals)>
    <struct.linkInfo.optionalAttributes:optAttrCount()>
<endif>
    
    return optCount;
}

protected void saveOptionalAttributes(DataOutput dos) throws IOException {
<if(struct.extending)>
    super.saveOptionalAttributes(dos);
    
<endif>

<if(struct.hasOptionals)>
    <struct.linkInfo.optionalAttributes:optAttrSave()>    
<endif>

}


>>

reqAttrSave(attr) ::= <<
<if(attr.type.fixedArray)>
<fixedArrayAttrSave(attr)>
<elseif(attr.type.dynamicArray)>
<dynamicArrayAttrSave(attr)>
<else>
<scalarAttrSave(attr)>
<endif>

>>



fixedArrayAttrSave(attr) ::= <<
<if(attr.type.resolvedStruct)>
LoadSaveHelper.writeNullIndices(dos, <attr.javaName>);

<endif>

for (int i = 0; i \< <attr.type.arraySize>; i++) {
<if(attr.type.enum)>
    dos.writeInt(<attr.javaName>[i].getValue());
<elseif(attr.type.pod)>
    dos.<attr.type.scalarType.javaWriteMethod>(<attr.javaName>[i]);
<else>
    if (<attr.javaName>[i] != null)
        <attr.javaName>[i].save(dos);
<endif>
}

>>

dynamicArrayAttrSave(attr) ::= <<
<if(attr.type.nativeArray)>
dos.writeInt(<attr.javaName>.length);
<else>
dos.writeInt(<attr.javaName>.size());
<endif>
<if(attr.type.nativeByteArray)>
dos.write(<attr.javaName>);
<else>
<if(attr.type.map)>
for (<attr.type.javaScalarType> x : <attr.javaName>.values())
<else>
for (<attr.type.javaScalarType> x : <attr.javaName>)
<endif>
<if(attr.type.enum)>
    dos.writeInt(x.getValue());
<elseif(attr.type.pod)>
    dos.<attr.type.scalarType.javaWriteMethod>(x);
<else>
    x.save(dos);
<endif>
<endif>
>>

scalarAttrSave(attr) ::= <<
<if(attr.type.pod)>
<if(attr.type.resolvedEnum)>
dos.writeInt(<attr.javaName>.getValue());
<else>
dos.<attr.type.scalarType.javaWriteMethod>(<attr.javaName>);
<endif>
<else>
<attr.javaName>.save(dos);
<endif>

>>

optAttrCount(attr) ::= <<
if (<optAttrWriteTest(attr)>) optCount++;

>>

optAttrSave(attr) ::= <<
if (<optAttrWriteTest(attr)>) {
    dos.writeShort(<attr.slotIndex>);

    int len = 0;
    <attrCalcSize(attr, "len")>
    dos.writeInt(len);

<if(attr.type.fixedArray)>
    <fixedArrayAttrSave(attr)>
<elseif(attr.type.dynamicArray)>
    <dynamicArrayAttrSave(attr)>
<else>
    <scalarAttrSave(attr)>
<endif>
}

>>

optAttrWriteTest(attr) ::= <%
<if(attr.type.dynamicArray)>
<attr.javaName>.size() > 0
<elseif(attr.type.fixedArray)>
!Arrays.equals(<attr.javaName>, <attr.javaName>Implicit)
<elseif(attr.null)>
<attr.javaName> != null
<else>
<attr.javaName> != <attr.implicitValue.javaValue>
<endif>
%>

cloneAttrib(attr) ::= <<
<if(attr.type.pod && !attr.type.array)>
ret.<attr.javaName> = <attr.javaName>;
<elseif(attr.type.dynamicArray)>
<if(!attr.type.nativeArray)>
ret.<attr.javaName> = <attr.type.javaCtor>;
<endif>
<if(attr.type.map)>
for (<attr.type.javaScalarType> v : <attr.javaName>.values()) {
	<attr.type.javaScalarType> x = v == null ? null : (<attr.type.javaScalarType>)v.clone();
	ret.<attr.javaName>.put(x.<attr.type.mapKeyAttribute.javaName>, x);
}
<elseif(attr.type.nativeArray)>
ret.<attr.javaName> = new <attr.type.javaScalarType>[<attr.javaName>.length];
for (int i = 0; i \< <attr.javaName>.length; i++)
{
<if(attr.type.pod)>
    ret.<attr.javaName>[i] = <attr.javaName>[i];
<else>
    ret.<attr.javaName>[i] = (<attr.type.javaScalarType>)<attr.javaName>[i].clone();
<endif>
}
<else>
for (<attr.type.javaScalarType> x : <attr.javaName>)
<if(attr.type.pod)>
    ret.<attr.javaName>.add(x);
<else>
    ret.<attr.javaName>.add(x == null ? null : (<attr.type.javaScalarType>)x.clone());
<endif>    
<endif>
<elseif(attr.type.fixedArray)>
ret.<attr.javaName> = <attr.type.javaCtor>;
for (int i = 0; i \< <attr.type.arraySize>; i++)
<if(attr.type.pod)>
    ret.<attr.javaName>[i] = <attr.javaName>[i];
<else>
    ret.<attr.javaName>[i] = <attr.javaName>[i] == null ? null : (<attr.type.javaScalarType>)<attr.javaName>[i].clone();
<endif>
<else>
ret.<attr.javaName> = <attr.javaName> == null ? null : (<attr.type.javaType>)<attr.javaName>.clone();
<endif>

>>



structSaverSize(struct) ::= <<
public long  calculateSize() {
    return calculateSize(true);
}

protected long calculateSize(boolean includeOptCount) {
    long ret = <if(struct.extending)>super.calculateSize(false)<else>0<endif>;

<if(struct.extended || struct.extending)>
    // class id
    if (includeOptCount) ret += 8;
<endif>

    // required
    <struct.linkInfo.requiredAttributes:attrCalcSize()>

<if(struct.hasOptionals)>
    // optionals
    if (includeOptCount)
        ret += 2;

    <struct.linkInfo.optionalAttributes:attrOptCalcSize()>
<endif>

    return ret;
}

>>

attrCalcSize(attr, dest="ret") ::= <<
<if(attr.type.dynamicArray)>
<dest> += 4;
<endif>
<if(attr.type.resolvedStruct)>
<if(attr.type.array)>
<if(attr.type.fixedArray)>
<dest> += 2; // null idx count (short)
<endif>

for (<attr.type.javaScalarType> x : <attr.javaName><if(attr.type.map)>.values()<endif>) {
<if(attr.type.fixedArray)>
    if (x != null)
        <dest> += (int)x.calculateSize();
    else
        <dest> += 4; // null idx (int)
<else>
    <dest> += x.calculateSize();
<endif>
}
<else><dest> += <attr.javaName>.calculateSize();
<endif>
<elseif(attr.type.scalarType.string)>
<if(attr.type.array)>
for (<attr.type.javaScalarType> x : <attr.javaName><if(attr.type.map)>.values()<endif>) {
    <dest> += LoadSaveHelper.computeStringStorage(x);
}
<else><dest> += <attrScalarSize(attr)>;
<endif>
<else>
<if(attr.type.array)><dest> += <attrArraySize(attr)> * <attrScalarSize(attr)>;
<else><dest> += <attrScalarSize(attr)>;
<endif>
<endif>

>>

attrScalarSize(attr) ::= "<if(attr.type.scalarType.string)>LoadSaveHelper.computeStringStorage(<attr.javaName>)<else><attr.type.scalarSize><endif>"

attrArraySize(attr) ::= "<if(attr.type.fixedArray)><attr.type.arraySize><elseif(attr.type.nativeArray)><attr.javaName>.length<else><attr.javaName>.size()<endif>"

attrOptCalcSize(attr) ::= <<
if (<optAttrWriteTest(attr)>) {
    ret += 2 + 4;
    <attrCalcSize(attr)>
}

>>
