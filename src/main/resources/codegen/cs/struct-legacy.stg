group struct;


basicStructDocument(namespace, struct, unit) ::= <<

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using IoMonkey;

<unit.imports:usingImport()>

namespace <unit.csName>
{
<if(struct.extending)>
    public <classSignature(struct)> <struct.name> : <struct.resolvedSuperClass.csFullQualifiedName><if(!struct.abstractStruct)>, ICloneable, IoMonkey.ILoadSave<endif>
<else>
    public <classSignature(struct)> <struct.name><if(!struct.abstractStruct)> : ICloneable, IoMonkey.ILoadSave<endif>
<endif>
    {
        <struct.attributes:structAttrib()>

        <struct.linkInfo.optionalAttributes:implicitAttributeConst()>

        <structLoader(struct)>

        <structSaver(struct)>

<if(!struct.abstractStruct)>    
	    public <possibleOverrideMethodMod(struct)>Object Clone()
	    {        
	        <struct.name> ret = new <struct.name>();

	    	<struct.allAttributes:cloneAttrib()>
	    	
	        return ret;
	    }
<elseif(!struct.extending)>
        public abstract Object Clone();
<endif>

    }
}

>>



classSignature(struct) ::= "<if(struct.abstractStruct)>abstract <endif>class"

implicitAttributeConst(attr) ::= <<
<if(attr.type.fixedArray)>
public static readonly <attr.type.csType> <attr.csName>Implicit = <attr.type.csFixedArrayImplicitValue>;
<endif>
>>

usingImport(imp) ::= <<
using <imp.unit.csName>;

>>

structAttrib(attr) ::= <<
<if(attr.implicitValue)>
public <attr.type.csType> <attr.csName> = <attr.implicitValue.csValue>;
<elseif(attr.type.array || attr.type.resolvedStruct)>
<if (attr.null)>
public <attr.type.csType> <attr.csName>;
<else>
public <attr.type.csType> <attr.csName> = <attr.type.csCtor>;
<endif>
<else>
public <attr.type.csType> <attr.csName>;
<endif>

>>

possibleOverrideMethodMod(struct) ::= "<if(struct.extending)>override <elseif(struct.extended)>virtual <endif>"

structLoader(struct) ::= <<
public <possibleOverrideMethodMod(struct)>void Load(BinaryReader reader)
{
<if(struct.extended || struct.extending)>
        long signiture = reader.ReadInt64();
        if (signiture != <struct.linkInfo.structId.long>L)
            throw new IOException("Struct signiture mismatched");

<endif>
    LoadInnerData(reader);
}
public <possibleOverrideMethodMod(struct)>void LoadInnerData(BinaryReader reader)
{    // read required
    LoadRequiredAttributes(reader);

<if(struct.hasOptionals)>
    // read optionals
    int numOpts = reader.ReadInt16();
    while (numOpts-- > 0)
    {
        int optIdx = reader.ReadInt16();
        LoadOptionalAttribute(reader, optIdx);
    }
<endif>
}

protected <possibleOverrideMethodMod(struct)>void LoadRequiredAttributes(BinaryReader reader)
{
<if(struct.extending)>
    base.LoadRequiredAttributes(reader);

<endif>

    <struct.linkInfo.requiredAttributes:reqAttrLoad()>

}

protected <possibleOverrideMethodMod(struct)>void LoadOptionalAttribute(BinaryReader reader, int optIdx)
{
<if(struct.hasOptionals)>
    switch (optIdx)
    {
    <struct.linkInfo.optionalAttributes:optAttrCase()>

<if(struct.extending)>

    default:
        base.LoadOptionalAttribute(reader, optIdx);
        
        break;
<endif>

    }
<endif>

}

>>

reqAttrLoad(attr) ::= <<
<if(attr.type.fixedArray)>
<fixedArrayAttrLoad(attr)>
<elseif(attr.type.dynamicArray)>
<dynamicArrayAttrLoad(attr)>
<else>
<scalarAttrLoad(attr)>
<endif>

>>

readAttr(attr) ::= <%
<if(attr.type.resolvedStruct && attr.type.resolvedStruct.needsSupportForInheritance)>
IomReaderUtils.Read<attr.type.resolvedStruct.name>(reader)
<elseif(attr.type.resolvedEnum)>
(<attr.type.resolvedEnum.csName>)reader.ReadInt32()
<else>
reader.<attr.type.csReadMethod>()
<endif>
%>

fixedArrayAttrLoad(attr) ::= <<
<if(attr.type.pod || (attr.type.resolvedStruct && attr.type.resolvedStruct.needsSupportForInheritance))>
for (int i = 0; i \< <attr.type.arraySize>; i++)
    <attr.csName>[i] = <readAttr(attr)>;
<else>
// non-POD read
<attr.csName> = <attr.type.csCtor>;
for (int i = 0; i \< <attr.type.arraySize>; i++)
{
    <attr.csName>[i] = <attr.type.csScalarCtor>;
    <attr.csName>[i].Load(reader);
}
<endif>

>>

dynamicArrayAttrLoad(attr) ::= <<
{
    int size = reader.ReadInt32();
    if (size > <attr.maxArraySize>)
        throw new ExcessiveArraySizeException(<attr.maxArraySize>, size);

<if(attr.type.nativeArray)>
    <attr.csName> = new <attr.type.csScalarType>[size];
<endif>
    for (int i = 0; i \< size; i++)
    {
<if(attr.type.pod || (attr.type.resolvedStruct && attr.type.resolvedStruct.needsSupportForInheritance))>
        <attr.type.csScalarType> x = <readAttr(attr)>;
<else>
        // non-POD read
        <attr.type.csScalarType> x = <attr.type.csScalarCtor>;
        x.Load(reader);
<endif>
<if(attr.type.map)>
        <attr.csName>.Add(x.<attr.type.mapDescriptor.keyAttribute.csName>, x);
<elseif(attr.type.nativeArray)>
        <attr.csName>[i] = x;
<else>
        <attr.csName>.Add(x);
<endif>

    }
}

>>

scalarAttrLoad(attr) ::= <<
<if(attr.type.pod || (attr.type.resolvedStruct && attr.type.resolvedStruct.needsSupportForInheritance))>
<attr.csName> = <readAttr(attr)>;
<else>
// non-POD read
<attr.csName> = <attr.type.csCtor>;
<attr.csName>.Load(reader);
<endif>

<fixedAttributeTest(attr)>
>>

optAttrCase(attr) ::= <<
case <attr.slotIndex>:
<if(attr.type.fixedArray)>
    <fixedArrayAttrLoad(attr)>
<elseif(attr.type.dynamicArray)>
    <dynamicArrayAttrLoad(attr)>
<else>
    <scalarAttrLoad(attr)>
<endif>

    break;


>>

fixedAttributeTest(attr) ::=<<
<if(attr.fixedValue)>
if (<attr.csName> != <attr.fixedValue.csValue>)
	throw new FixedValueException(<attr.fixedValue.csValue>, <attr.csName>, GetType());

<endif>
>>

structSaver(struct) ::= <<
public <possibleOverrideMethodMod(struct)>void Save(BinaryWriter writer)
{
<if(struct.extended || struct.extending)>
    writer.Write(<struct.linkInfo.structId.long>L);

<endif>
    SaveRequiredAttributes(writer);
    
<if(struct.hasOptionals)>
    short optCount = CountOptionalAttributesToSave();
    writer.Write(optCount);
    SaveOptionalAttributes(writer);

<endif>
}

protected <possibleOverrideMethodMod(struct)>void SaveRequiredAttributes(BinaryWriter writer)
{
<if(struct.extending)>
    base.SaveRequiredAttributes(writer);
    
<endif>    
    <struct.linkInfo.requiredAttributes:reqAttrSave()>

}


protected <possibleOverrideMethodMod(struct)>short CountOptionalAttributesToSave()
{
<if(struct.extending)>
    short optCount = base.CountOptionalAttributesToSave();
<else>
    short optCount = 0;
<endif>
    
<if(struct.hasOptionals)>
    <struct.linkInfo.optionalAttributes:optAttrCount()>
<endif>
    
    return optCount;
}

protected <possibleOverrideMethodMod(struct)>void SaveOptionalAttributes(BinaryWriter writer)
{
<if(struct.extending)>
    base.SaveOptionalAttributes(writer);
    
<endif>

<if(struct.hasOptionals)>
    <struct.linkInfo.optionalAttributes:optAttrSave()>    
<endif>

}


>>

reqAttrSave(attr) ::= <<
<if(attr.type.fixedArray)>
<fixedArrayAttrSave(attr)>
<elseif(attr.type.dynamicArray)>
<dynamicArrayAttrSave(attr)>
<else>
<scalarAttrSave(attr)>
<endif>

>>

writeAttr(attr) ::= <%
<if(attr.type.resolvedStruct && attr.type.resolvedStruct.extended)>
IomReaderUtils.Read<attr.type.resolvedStruct.name>(reader)
<elseif(attr.type.resolvedEnum)>
(<attr.type.resolvedEnum.csName>)reader.readInt()
<else>
reader.<attr.type.csReadMethod>()
<endif>
%>

fixedArrayAttrSave(attr) ::= <<

for (int i = 0; i \< <attr.type.arraySize>; i++)
{
<if(attr.type.pod)>
    writer.Write(<if(attr.type.enum)>(int)<endif><attr.csName>[i]);
<else>
    <attr.csName>[i].Save(writer);
<endif>
}

>>

dynamicArrayAttrSave(attr) ::= <<
<if(attr.type.nativeArray)>
writer.Write((int)<attr.csName>.Length);
<else>
writer.Write((int)<attr.csName>.Count);
<endif>
<if(attr.type.map)>
foreach(<attr.type.csScalarType> x in <attr.csName>.Values)
<else>
foreach(<attr.type.csScalarType> x in <attr.csName>)
<endif>
<if(attr.type.pod)>
    writer.Write(<if(attr.type.enum)>(int)<endif>x);
<else>
    x.Save(writer);
<endif>

>>

scalarAttrSave(attr) ::= <<
<if(attr.type.pod)>
writer.Write(<if(attr.type.enum)>(int)<endif><attr.csName>);
<else>
<attr.csName>.Save(writer);
<endif>

>>

optAttrCount(attr) ::= <<
if (<optAttrWriteTest(attr)>) optCount++;

>>

optAttrSave(attr) ::= <<
if (<optAttrWriteTest(attr)>)
{
    writer.Write((short)<attr.slotIndex>);
<if(attr.type.fixedArray)>
    <fixedArrayAttrSave(attr)>
<elseif(attr.type.dynamicArray)>
    <dynamicArrayAttrSave(attr)>
<else>
    <scalarAttrSave(attr)>
<endif>
}

>>

optAttrWriteTest(attr) ::= <%
<if(attr.type.dynamicArray)>
<attr.csName>.Count > 0
<elseif(attr.type.fixedArray)>
!<attr.csName>.SequenceEqual(<attr.csName>Implicit)
<elseif(attr.null)>
<attr.csName> != null
<else>
<attr.csName> != <attr.implicitValue.csValue>
<endif>
%>


cloneAttrib(attr) ::= <<
<if(attr.type.pod && !attr.type.array)>
ret.<attr.csName> = <attr.csName>;
<elseif(attr.type.dynamicArray)>
<if(!attr.type.nativeArray)>
ret.<attr.csName> = <attr.type.csCtor>;
<endif>
<if(attr.type.map)>
foreach (var kv in <attr.csName>)
{
	<attr.type.csScalarType> x = kv.Value == null ? null : (<attr.type.csScalarType>)kv.Value.Clone();
	ret.<attr.csName>.Add(x.<attr.type.mapKeyAttribute.csName>, x);
}
<elseif(attr.type.nativeArray)>
ret.<attr.csName> = new <attr.type.csScalarType>[<attr.csName>.Length];
for (int i = 0; i \< <attr.csName>.Length; i++)
{
<if(attr.type.pod)>
    ret.<attr.csName>[i] = <attr.csName>[i];
<else>
    ret.<attr.csName>[i] = (<attr.type.csScalarType>)<attr.csName>[i].Clone();
<endif>
}
<else>
foreach (var x in <attr.csName>)
<if(attr.type.pod)>
    ret.<attr.csName>.Add(x);
<else>
    ret.<attr.csName>.Add(x == null ? null : (<attr.type.csScalarType>)x.Clone());
<endif>    
<endif>
<elseif(attr.type.fixedArray)>
ret.<attr.csName> = <attr.type.csCtor>;
for (int i = 0; i \< <attr.type.arraySize>; i++)
<if(attr.type.pod)>
    ret.<attr.csName>[i] = <attr.csName>[i];
<else>
    ret.<attr.csName>[i] = <attr.csName>[i] == null ? null : (<attr.type.csScalarType>)<attr.csName>[i].Clone();
<endif>
<else>
ret.<attr.csName> = <attr.csName> == null ? null : (<attr.type.csType>)<attr.csName>.Clone();
<endif>

>>






