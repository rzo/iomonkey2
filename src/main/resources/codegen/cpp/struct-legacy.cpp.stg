group struct;


basicStructDocument(namespace, struct, unit) ::= <<


#include "<struct.cppIncludePath>"

<if(struct.needsIomReaderUtils)>#include "IomReaderUtils.h"
<endif>

<unit.name.cppNamespaceStart>

    <struct.linkInfo.optionalAttributes:implicitAttributeConst()>
    
    <structCtorDtor(struct)>

    <structLoader(struct)>

    <structSaver(struct)>

<unit.name.cppNamespaceEnd>
>>

implicitAttributeConst(attr) ::= <<
<if(attr.type.fixedArray)>
public static readonly <attr.type.csType> <attr.csName>Implicit = <attr.type.csFixedArrayImplicitValue>;
<endif>
>>

structCtorDtor(struct) ::= <<

<struct.cppName>::<struct.cppName>()
{
	<struct.attributes:implicitAttributeSet()>
}

<struct.cppName>::~<struct.cppName>()
{
	<struct.attributes:deleteAttr()>
}



>>

implicitAttributeSet(attr) ::= <<<if(attr.implicitValue)>
<attr.cppName> = <attr.implicitValue.cppValue>;

<endif>
>>


deleteAttr(attr) ::= <<<if (attr.type.dynamicArray)>
for (<attr.type.cppType>::iterator i = <attr.cppName>.begin(); i != <attr.cppName>.end(); ++i)
{
<if(attr.type.map)>
	delete i->second;
<else>
	delete *i;
<endif>	
}

<elseif (!attr.type.pod)>
if (<attr.cppName>)
	delete <attr.cppName>;

<endif>
>>


structAttrib(attr) ::= <<
<if(attr.implicitValue)>
public <attr.type.csType> <attr.csName> = <attr.implicitValue.csValue>;
<elseif(attr.type.array || attr.type.resolvedStruct)>
public <attr.type.csType> <attr.csName> = <attr.type.csCtor>;
<else>
public <attr.type.csType> <attr.csName>;
<endif>

>>

structLoader(struct) ::= <<
void <struct.cppName>::load(std::istream& is)
{
    // read required
    loadRequiredAttributes(is);

<if(struct.hasOptionals)>
    // read optionals
    short numOpts;
    iom::read(is, numOpts);
    while (numOpts--)
    {
        short optIdx;
        iom::read(is, optIdx);
        
        loadOptionalAttribute(is, optIdx);
    }
<endif>
}

void <struct.cppName>::loadRequiredAttributes(std::istream& is)
{
<if(struct.extending)>
    <struct.resolvedSuperClass.name>::loadRequiredAttributes(is);

<endif>

    <struct.linkInfo.requiredAttributes:reqAttrLoad()>

}

void <struct.cppName>::loadOptionalAttribute(std::istream& is, int optIdx)
{
<if(struct.hasOptionals)>
    switch (optIdx)
    {
    <struct.linkInfo.optionalAttributes:optAttrCase()>

<if(struct.extending)>

    default:
        <struct.resolvedSuperClass.name>::loadOptionalAttribute(is, optIdx);
        
        break;
<endif>

    }
<endif>

}

>>

reqAttrLoad(attr) ::= <<
<if(attr.type.fixedArray)>
<fixedArrayAttrLoad(attr)>
<elseif(attr.type.dynamicArray)>
<dynamicArrayAttrLoad(attr)>
<else>
<scalarAttrLoad(attr)>
<endif>

>>

readAttr(attr) ::= <%
<if(attr.type.resolvedStruct && attr.type.resolvedStruct.needsSupportForInheritance)>
IomReaderUtils::read<attr.type.resolvedStruct.cppName>(is)
<elseif(attr.type.resolvedEnum)>
<attr.type.resolvedEnum.cppName>(iom::read\<int>(is))
<else>
iom::read\<<attr.type.cppScalarType>>(is)
<endif>
%>

fixedArrayAttrLoad(attr) ::= <<
<if(attr.type.pod || (attr.type.resolvedStruct && attr.type.resolvedStruct.needsSupportForInheritance))>
for (int i = 0; i \< <attr.type.arraySize>; i++)
    <attr.cppName>[i] = <readAttr(attr)>;
<else>
// non-POD read
<attr.cppName> = <attr.type.cppCtor>;
for (int i = 0; i \< <attr.type.arraySize>; i++)
{
    <attr.cppName>[i] = <attr.type.cppScalarCtor>;
    <attr.cppName>[i]->load(is);
}
<endif>

>>

dynamicArrayAttrLoad(attr) ::= <<
{
    int size;
    iom::read(is, size);
    if (size > <attr.maxArraySize>)
        throw iom::ExcessiveArraySizeException(<attr.maxArraySize>, size);

    while (size--)
    {
<if(attr.type.pod || (attr.type.resolvedStruct && attr.type.resolvedStruct.needsSupportForInheritance))>
		<attr.type.cppScalarType> x = <readAttr(attr)>;
<else>
        // non-POD read
        <attr.type.cppScalarType> x = <attr.type.cppScalarCtor>;
        x->load(is);
<endif>
<if(attr.type.map)>
		<attr.cppName>[x-><attr.type.mapDescriptor.keyAttribute.cppName>] = x;
<else>
		<attr.cppName>.push_back(x);
<endif>

    }
}

>>

scalarAttrLoad(attr) ::= <<
<if(attr.type.pod || (attr.type.resolvedStruct && attr.type.resolvedStruct.needsSupportForInheritance))>
<attr.cppName> = <readAttr(attr)>;
<else>
// non-POD read
<attr.cppName> = <attr.type.cppCtor>;
<attr.cppName>->load(is);
<endif>
<fixedAttributeTest(attr)>

>>

fixedAttributeTest(attr) ::=<<
<if(attr.fixedValue)>
if (<attr.cppName> != <attr.fixedValue.cppValue>)
	throw iom::FixedValueException(<attr.fixedValue.cppValue>, <attr.cppName>);

<endif>
>>


optAttrCase(attr) ::= <<
case <attr.slotIndex>:
<if(attr.type.fixedArray)>
    <fixedArrayAttrLoad(attr)>
<elseif(attr.type.dynamicArray)>
    <dynamicArrayAttrLoad(attr)>
<else>
    <scalarAttrLoad(attr)>
<endif>

    break;


>>

structSaver(struct) ::= <<
void <struct.cppName>::save(std::ostream& os) const
{
<if(struct.extended || struct.extending)>
    iom::write(os, <struct.linkInfo.structId.long>L);

<endif>
    saveRequiredAttributes(os);
    
<if(struct.hasOptionals)>
    short optCount = countOptionalAttributesToSave();
    iom::write(os, optCount);
    saveOptionalAttributes(os);

<endif>
}

void <struct.cppName>::saveRequiredAttributes(std::ostream& os) const
{
<if(struct.extending)>
    <struct.resolvedSuperClass.name>::saveRequiredAttributes(os);
    
<endif>    
    <struct.linkInfo.requiredAttributes:reqAttrSave()>

}


short <struct.cppName>::countOptionalAttributesToSave() const
{
<if(struct.extending)>
    short optCount = <struct.resolvedSuperClass.cppFullQualifiedName>::countOptionalAttributesToSave();
<else>
    short optCount = 0;
<endif>
    
<if(struct.hasOptionals)>
    <struct.linkInfo.optionalAttributes:optAttrCount()>
<endif>
    
    return optCount;
}

void <struct.cppName>::saveOptionalAttributes(std::ostream& os) const
{
<if(struct.extending)>
    <struct.resolvedSuperClass.name>::saveOptionalAttributes(os);
    
<endif>

<if(struct.hasOptionals)>
    <struct.linkInfo.optionalAttributes:optAttrSave()>    
<endif>

}


>>

reqAttrSave(attr) ::= <<
<if(attr.type.fixedArray)>
<fixedArrayAttrSave(attr)>
<elseif(attr.type.dynamicArray)>
<dynamicArrayAttrSave(attr)>
<else>
<scalarAttrSave(attr)>
<endif>

>>

fixedArrayAttrSave(attr) ::= <<

for (int i = 0; i \< <attr.type.arraySize>; i++)
{
<if(attr.type.pod)>
    iom::write(os, <if(attr.type.enum)>(int)<endif><attr.cppName>[i]);
<else>
    <attr.cppName>[i]->save(os);
<endif>
}

>>

dynamicArrayAttrSave(attr) ::= <<
iom::write(os, (int)<attr.cppName>.size());
for (auto x : <attr.cppName>)
<if(attr.type.pod)>
    iom::write(os, <if(attr.type.enum)>(int)<endif>x);
<else>
    <if(attr.type.map)>x.second<else>x<endif>->save(os);
<endif>

>>

scalarAttrSave(attr) ::= <<
<if(attr.type.pod)>
<if(attr.type.resolvedEnum)>
iom::write(os, (int)<attr.cppName>);
<else>
iom::write(os, <attr.cppName>);
<endif>
<else>
<attr.cppName>->save(os);
<endif>

>>

optAttrCount(attr) ::= <<
if (<optAttrWriteTest(attr)>) optCount++;

>>

optAttrSave(attr) ::= <<
if (<optAttrWriteTest(attr)>)
{
    iom::write(os, (short)<attr.slotIndex>);
<if(attr.type.fixedArray)>
    <fixedArrayAttrSave(attr)>
<elseif(attr.type.dynamicArray)>
    <dynamicArrayAttrSave(attr)>
<else>
    <scalarAttrSave(attr)>
<endif>
}

>>

optAttrWriteTest(attr) ::= <%
<if(attr.type.dynamicArray)>
<attr.cppName>.size() > 0
<elseif(attr.type.fixedArray)>
!std::memcmp(<attr.cppName>, <attr.cppName>Implicit)
<else>
<attr.cppName> != <attr.implicitValue.cppValue>
<endif>
%>