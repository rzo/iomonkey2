package net.bartipan.iomonkey2.tests;

import net.bartipan.iomonkey2.linker.Linker;
import net.bartipan.iomonkey2.linker.UnresolvedSymbolException;
import net.bartipan.iomonkey2.model.*;
import net.bartipan.iomonkey2.model.Enum;
import net.bartipan.iomonkey2.parser.NameConflictException;
import org.antlr.runtime.RecognitionException;
import org.junit.Assert;

import java.io.IOException;
import java.util.List;

/**
 * Created by honza on 9/27/14.
 */
public class AssertUtils {
    public static void ensureUnitHasStructs(Unit unit, String... names) {
        for (String name : names) {
            Assert.assertNotNull(String.format("Struct '%s' not found in unit '%s'", name, unit.name.toString()),
                    unit.findStruct(name));
        }
    }

    public static void ensureUnitHasEnums(Unit unit, String... names) {
        for (String name : names) {
            Assert.assertNotNull(String.format("Enum '%s' not found in unit '%s'", name, unit.name.toString()),
                    unit.findEnum(name));
        }
    }

    public static void ensureStructExtends(Unit unit, String structName, String superStructName) {
        Struct s = unit.findStruct(structName);
        Assert.assertEquals(s.superClassName, superStructName);
    }

    public static void ensureStructIsExtended(Unit unit, String structName, String... subclassStructNames) {
        Struct s = unit.findStruct(structName);

        for (String subclassName : subclassStructNames) {
            boolean found = false;
            for (Element es : s.resolvedSubClasses) {
                if (es.name.equals(subclassName)) {
                    found = true;
                    break;
                }
            }

            Assert.assertTrue(
                    String.format("Struct '%s' is not extended by '%s'", s.fullQualifiedName(), subclassName),
                    found);
        }
    }

    public static void ensureStructEnumConsts(Unit unit, String enumName, String... enumConstNames) {
        Enum e = unit.findEnum(enumName);

        for (String ec : enumConstNames) {
            Assert.assertNotNull(String.format("Constant '%s' not found in enum '%s.%s'", ec, unit.name.toString(), enumName),
                    e.findEnumConst(ec));
        }
    }

    public static void ensureStructHasAttributes(Unit unit, String structName, String... attrNames) {
        Struct s = unit.findStruct(structName);
        for (String an : attrNames) {
            Assert.assertNotNull(String.format("Attribute '%s' not found in '%s'", an, s.fullQualifiedName()),
                    s.findAttribute(an));
        }
    }

    public static void ensureStructHasAttributes(Unit unit, String structName, boolean required, String... attrNames) {
        Struct s = unit.findStruct(structName);
        for (String an : attrNames) {
            final Attribute attribute = s.findAttribute(an);
            Assert.assertNotNull(String.format("Attribute '%s' not found in '%s'", an, s.fullQualifiedName()),
                    attribute);
            Assert.assertEquals(attribute.isRequired(), required);
        }
    }

    public static void ensureStructHasTypedAttributes(Unit unit, String structName, ScalarType type, int arraySize,
                                                      String... attrNames) {
        Struct s = unit.findStruct(structName);
        for (String an : attrNames) {
            final Attribute attr = s.findAttribute(an);
            Assert.assertEquals(attr.type.scalarType, type);
        }
    }

    public static void ensureStructAttributeOrder(Unit unit, String structName, boolean required,
                                                  String... attrNames) {
        Struct s = unit.findStruct(structName);
        List<Attribute> attrs = required ? s.linkInfo.requiredAttributes : s.linkInfo.optionalAttributes;

        Assert.assertEquals(attrs.size(), attrNames.length);
        for (int i = 0; i < attrNames.length; i++) {
            Attribute attr = attrs.get(i);
            Assert.assertEquals(attr.name, attrNames[i]);
        }
    }

    private static boolean unitContainsStructReference(Unit unit, String refAttrName, String targetStructName) {
        for (StructReference sr : unit.linkInfo.structReferences) {
            if (sr.ref.name.equals(refAttrName) && sr.target.name.equals(targetStructName))
                return true;
        }

        return false;
    }

    public static void ensureUnitHasStructReferences(Unit unit, String... names) {
        assert names.length % 2 == 0;
        for (int i = 0; i < names.length; i += 2) {
            final String refAttrName = names[i];
            final String targetStructName = names[i + 1];

            Assert.assertTrue(String.format("Reference to struct '%s' in attribute '%s'", targetStructName, refAttrName),
                    unitContainsStructReference(unit, refAttrName, targetStructName));
        }
    }

    private static boolean unitContainsEnumReference(Unit unit, String refAttrName, String targetEnumName) {
        for (EnumReference er : unit.linkInfo.enumReferences) {
            if (er.ref.name.equals(refAttrName) && er.target.name.equals(targetEnumName))
                return true;
        }

        return false;
    }

    public static void ensureUnitHasEnumReferences(Unit unit, String... names) {
        assert names.length % 2 == 0;
        for (int i = 0; i < names.length; i += 2) {
            final String refAttrName = names[i];
            final String targetEnumName = names[i + 1];

            Assert.assertTrue(String.format("Reference to enum '%s' in attribute '%s'", targetEnumName, refAttrName),
                    unitContainsEnumReference(unit, refAttrName, targetEnumName));
        }
    }

    public static void ensureUnresolvedException(String source) throws IOException, RecognitionException {
        Context ctx = new Context();
        ctx.loadMainUnit(source);

        Unit unit = ctx.getMainUnit();

        boolean failed = false;
        try {
            Linker linker = new Linker(ctx);
            linker.link();
        } catch (UnresolvedSymbolException e) {
            failed = true;
        }

        Assert.assertTrue("Linker have to fail.", failed);
    }

    public static void ensureParserException(String source, Class exceptionClass)  throws IOException, RecognitionException {
        Context ctx = new Context();

        boolean failed = false;
        Class caughtExceptionClass = null;
        try {
            ctx.loadMainUnit(source);
        } catch (Throwable e) {
            caughtExceptionClass = e.getClass();
            failed = caughtExceptionClass == exceptionClass;
        }

        Assert.assertTrue(
                caughtExceptionClass == null
                    ? String.format("Parser have to fail with %s", exceptionClass.getCanonicalName())
                    : String.format("Parser have to fail with %s but failed with %s", exceptionClass.getCanonicalName(),
                        caughtExceptionClass.getCanonicalName()),
                failed);
    }

    public static void ensureNameConflictException(String source) throws IOException, RecognitionException {
        ensureParserException(source, NameConflictException.class);
    }

}
