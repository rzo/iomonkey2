package net.bartipan.iomonkey2.tests.parser;

import net.bartipan.iomonkey2.codegen.CodeGenFactory;
import net.bartipan.iomonkey2.model.Context;
import net.bartipan.iomonkey2.model.Unit;
import net.bartipan.iomonkey2.tests.AssertUtils;
import org.antlr.runtime.RecognitionException;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

/**
 * Created by jbartipan on 04.12.14.
 */
public class CompleteGrammarTest {
    @Test
    public void testComplete() throws IOException, RecognitionException {
        InputStream is = getClass().getResourceAsStream("/complete.iom2");
        String source = "";
        byte[] buf = new byte[4096];

        int n;
        while ((n = is.read(buf)) > 0) {
            source += new String(buf, 0, n, "UTF-8");
        }
        Context ctx = new Context();
        ctx.loadMainUnit(source);
        Unit mainUnit = ctx.getMainUnit();
        AssertUtils.ensureUnitHasStructs(mainUnit,
                "Document",
                "Header",
                "Element",
                "TestAllScalarsElement",
                "TestAllFixedArrayElement",
                "TestAllDynamicArrayElement",
                "TestOptAllScalarsElement",
                "TestOptAllFixedArrayElement",
                "TestOptAllDynamicArrayElement",
                "TestNativeArrayElement");
    }
}
