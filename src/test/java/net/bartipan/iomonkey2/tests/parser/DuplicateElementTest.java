package net.bartipan.iomonkey2.tests.parser;

import net.bartipan.iomonkey2.model.Unit;
import net.bartipan.iomonkey2.parser.AstParser;
import net.bartipan.iomonkey2.tests.AssertUtils;
import org.antlr.runtime.RecognitionException;
import org.junit.Test;

import java.io.IOException;

/**
 * Created by jbartipan on 30.10.14.
 */
public class DuplicateElementTest {


    @Test
    public void duplicateStruct() throws IOException, RecognitionException {
        final String source = "package test;\n" +
                "struct Test {\n" +
                "  int answer = 42;\n" +
                "  int pi = 3.1415;\n" +
                "  string empty = \"\";\n" +
                "  Bool no = NO;\n" +
                "}\n" +
                "enum Bool { YES = 1, NO = 0 }\n" +
                "struct Test { }\n";

        AssertUtils.ensureNameConflictException(source);
    }

    @Test
    public void duplicateEnum() throws IOException, RecognitionException {
        final String source = "package test;\n" +
                "struct Test {\n" +
                "  int answer = 42;\n" +
                "  int pi = 3.1415;\n" +
                "  string empty = \"\";\n" +
                "  Bool no = NO;\n" +
                "}\n" +
                "enum Bool { YES = 1, NO = 0 }\n" +
                "enum Bool { No = 0, Yes = 1 }\n";

        AssertUtils.ensureNameConflictException(source);
    }

    @Test
    public void duplicateEnumConst() throws IOException, RecognitionException {
        final String source = "package test;\n" +
                "struct Test {\n" +
                "  int answer = 42;\n" +
                "  int pi = 3.1415;\n" +
                "  string empty = \"\";\n" +
                "  Bool no = NO;\n" +
                "}\n" +
                "enum Bool { No = 0, No = 1, Yes = 2 }\n";

        AssertUtils.ensureNameConflictException(source);
    }

    @Test
    public void duplicateAttribute() throws IOException, RecognitionException {
        final String source = "package test;\n" +
                "struct Test {\n" +
                "  int answer = 42;\n" +
                "  int answer = 3.1415;\n" +
                "  string empty = \"\";\n" +
                "  Bool no = NO;\n" +
                "}\n" +
                "enum Bool { NO = 0, YES = 1 }\n";

        AssertUtils.ensureNameConflictException(source);
    }
}
