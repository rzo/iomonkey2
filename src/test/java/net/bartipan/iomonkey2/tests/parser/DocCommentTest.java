package net.bartipan.iomonkey2.tests.parser;

import net.bartipan.iomonkey2.model.Attribute;
import net.bartipan.iomonkey2.model.Enum;
import net.bartipan.iomonkey2.model.Struct;
import net.bartipan.iomonkey2.model.Unit;
import net.bartipan.iomonkey2.parser.AstParser;
import org.antlr.runtime.RecognitionException;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;

/**
 * Created by jbartipan on 12.10.16.
 */
public class DocCommentTest {
    @Test
    public void testDocComment() throws IOException, RecognitionException {
        final String source = "package test.doccomment;\n" +
                "/////////////////////////////////\n" +
                "// This is just splitter\n" +
                "/// And this is doc-comment for struct\n" +
                "struct Doc\n" +
                "{\n" +
                "  /// This is comment for field\n"+
                "  int a\n;" +
                "}\n" +
                "/// This is multi line doc comment\n" +
                "/// for following enum\n" +
                "enum Type\n" +
                "{\n" +
                "   Alpha=1,Beta=2,Gamma=3\n" +
                "}\n" +
                "\n";
        AstParser parser = new AstParser();
        Unit unit = parser.parseUnit(source);
        Struct s = unit.structs.get(0);
        Assert.assertFalse("Struct doc-comment", s.docComment.comment.isEmpty());
        Attribute a = s.findAttribute("a");
        Assert.assertFalse("Attribute doc-comment", a.docComment.comment.isEmpty());
        Enum e = unit.enums.get(0);
        Assert.assertFalse("Enum doc-comment", e.docComment.comment.isEmpty());

    }
}
