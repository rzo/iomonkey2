package net.bartipan.iomonkey2.tests.parser;

import net.bartipan.iomonkey2.model.Attribute;
import net.bartipan.iomonkey2.model.Struct;
import net.bartipan.iomonkey2.model.Unit;
import net.bartipan.iomonkey2.model.ValueType;
import net.bartipan.iomonkey2.parser.AstParser;
import org.antlr.runtime.RecognitionException;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;

/**
 * Created by honza on 9/27/14.
 */
public class ImplicitAttributeTest {

    @Test
    public void testImplicitValues() throws IOException, RecognitionException {
        final String source = "package test;\n" +
                "struct Test {\n" +
                "  int answer = 42;\n" +
                "  int pi = 3.1415;\n" +
                "  string empty = \"\";\n" +
                "  Bool no = NO;\n" +
                "  T2 test = null;\n" +
                "}\n" +
                "enum Bool { YES = 1, NO = 0 }\n" +
                "struct T2 {}";

        AstParser parser = new AstParser();
        Unit unit = parser.parseUnit(source);
        Struct s = unit.findStruct("Test");
        Attribute answer = s.findAttribute("answer");
        Attribute pi = s.findAttribute("pi");
        Attribute empty = s.findAttribute("empty");
        Attribute no = s.findAttribute("no");
        Attribute test = s.findAttribute("test");

        Assert.assertEquals(answer.implicitValue.type, ValueType.Int);
        Assert.assertEquals(answer.implicitValue.value, "42");

        Assert.assertEquals(pi.implicitValue.type, ValueType.Float);
        Assert.assertEquals(pi.implicitValue.value, "3.1415");

        Assert.assertEquals(empty.implicitValue.type, ValueType.String);
        Assert.assertEquals(empty.implicitValue.value, "\"\"");

        Assert.assertEquals(no.implicitValue.type, ValueType.EnumConst);
        Assert.assertEquals(no.implicitValue.value, "NO");

        Assert.assertEquals(test.implicitValue.type, ValueType.Null);
        Assert.assertEquals(test.implicitValue.value, "");
    }
}
