package net.bartipan.iomonkey2.tests.parser;

import net.bartipan.iomonkey2.model.ScalarType;
import net.bartipan.iomonkey2.model.Unit;
import net.bartipan.iomonkey2.parser.AstParser;
import net.bartipan.iomonkey2.tests.AssertUtils;
import org.antlr.runtime.RecognitionException;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;

/**
 * Created by honza on 9/27/14.
 */
public class SimpleGrammarTest {
    @Test
    public void testComplexGrammar() throws IOException, RecognitionException {
        final String source = "package test;\n" +
                "struct Document {\n" +
                "  Header header;\n" +
                "  GroupElement root;" +
                "}\n" +
                "struct Header {\n" +
                "  byte[4] magic;\n" +
                "  byte majorVersion;\n" +
                "  byte minorVersion;\n" +
                "}\n" +
                "struct Element {\n" +
                "  string name;\n" +
                "}\n" +
                "struct GroupElement : Element {\n" +
                "  Element[] children;\n" +
                "}\n" +
                "struct ParagraphElement : Element {\n" +
                "  string styleName;\n" +
                "  string text;\n" +
                "  Alignment align;\n" +
                "}\n" +
                "struct ImageElement : Element {\n" +
                "  string imageUrl;\n" +
                "}\n" +
                "enum Alignment { LEFT = 0, RIGHT = 1, CENTER = 2, JUSTIFY = 3 }\n";

        AstParser parser = new AstParser();
        Unit unit  = parser.parseUnit(source);

        Assert.assertEquals(unit.name.toString(), "test");

        AssertUtils.ensureUnitHasEnums(unit, "Alignment");
        AssertUtils.ensureUnitHasStructs(unit, "Document", "Header", "Element", "GroupElement", "ParagraphElement",
                "ImageElement");

        AssertUtils.ensureStructExtends(unit, "GroupElement", "Element");
        AssertUtils.ensureStructExtends(unit, "ParagraphElement", "Element");
        AssertUtils.ensureStructExtends(unit, "ImageElement", "Element");

        AssertUtils.ensureStructHasAttributes(unit, "Document", "header", "root");
        AssertUtils.ensureStructHasAttributes(unit, "Header", "magic", "majorVersion", "minorVersion");
        AssertUtils.ensureStructHasAttributes(unit, "Element", "name");
        AssertUtils.ensureStructHasAttributes(unit, "GroupElement", "children");
        AssertUtils.ensureStructHasAttributes(unit, "ParagraphElement", "styleName", "text", "align");
        AssertUtils.ensureStructHasAttributes(unit, "ImageElement", "imageUrl");

    }

    @Test
    public void testSlots() throws IOException, RecognitionException {
        final String source = "package test;\n" +
                "struct Req{ required(0) byte a; required(2) short b; required int c; }\n" +
                "struct Opt{ optional(0) byte a; optional(2) short b; optional int c; }\n" +
                "struct Mixed { required int a; optional(1) Opt[] opts; }\n";

        AstParser parser = new AstParser();
        Unit unit = parser.parseUnit(source);

        Assert.assertEquals(unit.name.toString(), "test");
        AssertUtils.ensureUnitHasStructs(unit, "Req", "Opt");
        AssertUtils.ensureStructHasAttributes(unit, "Req", true, "a", "b", "c");
        AssertUtils.ensureStructHasAttributes(unit, "Opt", false, "a", "b", "c");
        AssertUtils.ensureStructHasAttributes(unit, "Mixed", true, "a");
        AssertUtils.ensureStructHasAttributes(unit, "Mixed", false, "opts");
    }

    @Test
    public void testAttributeTypes() throws IOException, RecognitionException {
        final String source = "package test;\n" +
                "struct ScalarTypes {\n" +
                "  byte  i8;\n" +
                "  short i16;\n" +
                "  int   i32;\n" +
                "  long  i64;\n" +
                "  float r32;\n" +
                "  string str;\n" +
                "  Enum enm;\n" +
                "  Vec2 vec;\n" +
                "}\n" +
                "enum Enum { YES=1, NO=0 }\n" +
                "struct Vec2 { float x; float y; }\n" +
                "struct DynArrayTypes {\n" +
                "  byte[]  i8;\n" +
                "  short[] i16;\n" +
                "  int[]   i32;\n" +
                "  long[]  i64;\n" +
                "  float[] r32;\n" +
                "  string[] str;\n" +
                "  Enum[] enm;\n" +
                "  Vec2[] vec;\n" +
                "}\n" +
                "struct FixedArrayTypes {\n" +
                "  byte[4]  i8;\n" +
                "  short[4] i16;\n" +
                "  int[4]   i32;\n" +
                "  long[4]  i64;\n" +
                "  float[8] r32;\n" +
                "  string[8] str;\n" +
                "  Enum[10] enm;\n" +
                "  Vec2[10] vec;\n" +
                "}\n" +
                "\n";

        AstParser parser = new AstParser();
        Unit unit = parser.parseUnit(source);

        AssertUtils.ensureStructHasTypedAttributes(unit, "ScalarTypes", ScalarType.Byte,  -1, "i8");
        AssertUtils.ensureStructHasTypedAttributes(unit, "ScalarTypes", ScalarType.Short,  -1, "i16");
        AssertUtils.ensureStructHasTypedAttributes(unit, "ScalarTypes", ScalarType.Int,  -1, "i32");
        AssertUtils.ensureStructHasTypedAttributes(unit, "ScalarTypes", ScalarType.Long,  -1, "i64");
        AssertUtils.ensureStructHasTypedAttributes(unit, "ScalarTypes", ScalarType.Float,  -1, "r32");
        AssertUtils.ensureStructHasTypedAttributes(unit, "ScalarTypes", ScalarType.String,  -1, "str");
        AssertUtils.ensureStructHasTypedAttributes(unit, "ScalarTypes", ScalarType.TypeId,  -1, "enm");
        AssertUtils.ensureStructHasTypedAttributes(unit, "ScalarTypes", ScalarType.TypeId,  -1, "vec");

        AssertUtils.ensureStructHasTypedAttributes(unit, "DynArrayTypes", ScalarType.Byte,  0, "i8");
        AssertUtils.ensureStructHasTypedAttributes(unit, "DynArrayTypes", ScalarType.Short,  0, "i16");
        AssertUtils.ensureStructHasTypedAttributes(unit, "DynArrayTypes", ScalarType.Int,  0, "i32");
        AssertUtils.ensureStructHasTypedAttributes(unit, "DynArrayTypes", ScalarType.Long,  0, "i64");
        AssertUtils.ensureStructHasTypedAttributes(unit, "DynArrayTypes", ScalarType.Float,  0, "r32");
        AssertUtils.ensureStructHasTypedAttributes(unit, "DynArrayTypes", ScalarType.String,  0, "str");
        AssertUtils.ensureStructHasTypedAttributes(unit, "DynArrayTypes", ScalarType.TypeId,  0, "enm");
        AssertUtils.ensureStructHasTypedAttributes(unit, "DynArrayTypes", ScalarType.TypeId,  0, "vec");

        AssertUtils.ensureStructHasTypedAttributes(unit, "FixedArrayTypes", ScalarType.Byte,  4, "i8");
        AssertUtils.ensureStructHasTypedAttributes(unit, "FixedArrayTypes", ScalarType.Short,  4, "i16");
        AssertUtils.ensureStructHasTypedAttributes(unit, "FixedArrayTypes", ScalarType.Int,  4, "i32");
        AssertUtils.ensureStructHasTypedAttributes(unit, "FixedArrayTypes", ScalarType.Long,  4, "i64");
        AssertUtils.ensureStructHasTypedAttributes(unit, "FixedArrayTypes", ScalarType.Float,  8, "r32");
        AssertUtils.ensureStructHasTypedAttributes(unit, "FixedArrayTypes", ScalarType.String,  8, "str");
        AssertUtils.ensureStructHasTypedAttributes(unit, "FixedArrayTypes", ScalarType.TypeId,  10, "enm");
        AssertUtils.ensureStructHasTypedAttributes(unit, "FixedArrayTypes", ScalarType.TypeId,  10, "vec");
    }

}
