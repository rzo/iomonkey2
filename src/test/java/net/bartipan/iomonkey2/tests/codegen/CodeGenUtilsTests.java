package net.bartipan.iomonkey2.tests.codegen;

import net.bartipan.iomonkey2.codegen.CodeGenUtils;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by honza on 10/11/14.
 */
public class CodeGenUtilsTests {
    @Test
    public void componentsTest() {
        String[] comps = CodeGenUtils.nameComponents("camelCase");
        Assert.assertEquals(2, comps.length);
        Assert.assertEquals("camel", comps[0]);
        Assert.assertEquals("Case", comps[1]);

        comps = CodeGenUtils.nameComponents("PascalCase");
        Assert.assertEquals(2, comps.length);
        Assert.assertEquals("Pascal", comps[0]);
        Assert.assertEquals("Case", comps[1]);

        comps = CodeGenUtils.nameComponents("UPPER_UNDERSCORE_CASE");
        Assert.assertEquals(3, comps.length);
        Assert.assertEquals("UPPER", comps[0]);
        Assert.assertEquals("UNDERSCORE", comps[1]);
        Assert.assertEquals("CASE", comps[2]);
    }

    @Test
    public void pascalCaseTest() {
        Assert.assertEquals("CamelCase", CodeGenUtils.pascalCase("camelCase"));
        Assert.assertEquals("PascalCase", CodeGenUtils.pascalCase("PascalCase"));
        Assert.assertEquals("UpperUnderscoreCase", CodeGenUtils.pascalCase("UPPER_UNDERSCORE_CASE"));
    }

    @Test
    public void camelCaseTest() {
        Assert.assertEquals("camelCase", CodeGenUtils.camelCase("camelCase"));
        Assert.assertEquals("pascalCase", CodeGenUtils.camelCase("PascalCase"));
        Assert.assertEquals("upperUnderscoreCase", CodeGenUtils.camelCase("UPPER_UNDERSCORE_CASE"));
    }

    @Test
    public void upperUnderscoreCaseTest() {
        Assert.assertEquals("CAMEL_CASE", CodeGenUtils.upperUnderscoreCase("camelCase"));
        Assert.assertEquals("PASCAL_CASE", CodeGenUtils.upperUnderscoreCase("PascalCase"));
        Assert.assertEquals("UPPER_UNDERSCORE_CASE", CodeGenUtils.upperUnderscoreCase("UPPER_UNDERSCORE_CASE"));
    }
}
