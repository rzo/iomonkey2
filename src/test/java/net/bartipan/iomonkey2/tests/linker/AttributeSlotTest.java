package net.bartipan.iomonkey2.tests.linker;

import net.bartipan.iomonkey2.linker.Linker;
import net.bartipan.iomonkey2.model.Context;
import net.bartipan.iomonkey2.model.Struct;
import net.bartipan.iomonkey2.model.Unit;
import net.bartipan.iomonkey2.parser.AstParser;
import org.antlr.runtime.RecognitionException;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;

/**
 * Created by honza on 9/28/14.
 */
public class AttributeSlotTest {
    @Test
    public void requiredAttributeTest() throws IOException, RecognitionException {
        final String source = "package test;\n" +
                "struct Test {\n" +
                "  required(3) byte r3;" +
                "  byte r0;\n" +
                "  required byte r1;\n" +
                "}\n";

        Context ctx = new Context();
        ctx.loadMainUnit(source);
        Unit unit = ctx.getMainUnit();
        Linker linker = new Linker(ctx);
        linker.link();

        Struct test = unit.findStruct("Test");
        Assert.assertTrue(test.findAttribute("r0").slot.required);
        Assert.assertTrue(test.findAttribute("r1").slot.required);
        Assert.assertTrue(test.findAttribute("r3").slot.required);
        Assert.assertEquals(0, test.findAttribute("r0").slot.slot);
        Assert.assertEquals(1, test.findAttribute("r1").slot.slot);
        Assert.assertEquals(3, test.findAttribute("r3").slot.slot);
    }

    @Test
    public void optionalAttributeTest() throws IOException, RecognitionException {
        final String source = "package test;\n" +
                "struct Test {\n" +
                "  optional(3) byte o3;" +
                "  optional byte o0;\n" +
                "}\n";

        Context ctx = new Context();
        ctx.loadMainUnit(source);
        Unit unit = ctx.getMainUnit();
        Linker linker = new Linker(ctx);
        linker.link();

        Struct test = unit.findStruct("Test");
        Assert.assertFalse(test.findAttribute("o0").slot.required);
        Assert.assertFalse(test.findAttribute("o3").slot.required);
        Assert.assertEquals(0, test.findAttribute("o0").slot.slot);
        Assert.assertEquals(3, test.findAttribute("o3").slot.slot);
    }

    @Test
    public void requiredAttributeInheritanceTest() throws IOException, RecognitionException {
        final String source = "package test;\n" +
                "struct Base { required byte x; }" +
                "struct Test : Base {\n" +
                "  required(3) byte r3;" +
                "  byte r0;\n" +
                "  required byte r1;\n" +
                "}\n";

        Context ctx = new Context();
        ctx.loadMainUnit(source);
        Unit unit = ctx.getMainUnit();
        Linker linker = new Linker(ctx);
        linker.link();

        Struct test = unit.findStruct("Test");
        Assert.assertTrue(test.findAttribute("r0").slot.required);
        Assert.assertTrue(test.findAttribute("r1").slot.required);
        Assert.assertTrue(test.findAttribute("r3").slot.required);
        Assert.assertEquals(0 + 1, test.findAttribute("r0").getSlotIndex());
        Assert.assertEquals(1 + 1, test.findAttribute("r1").getSlotIndex());
        Assert.assertEquals(3 + 1, test.findAttribute("r3").getSlotIndex());
    }

    @Test
    public void optionalAttributeInheritanceTest() throws IOException, RecognitionException {
        final String source = "package test;\n" +
                "struct Base { optional(0) byte x; }" +
                "struct Test : Base {\n" +
                "  optional(3) byte o3;" +
                "  optional byte o0;\n" +
                "}\n";

        Context ctx = new Context();
        ctx.loadMainUnit(source);
        Unit unit = ctx.getMainUnit();
        Linker linker = new Linker(ctx);
        linker.link();

        Struct test = unit.findStruct("Test");
        Assert.assertFalse(test.findAttribute("o0").slot.required);
        Assert.assertFalse(test.findAttribute("o3").slot.required);
        Assert.assertEquals(0 + 1, test.findAttribute("o0").getSlotIndex());
        Assert.assertEquals(3 + 1, test.findAttribute("o3").getSlotIndex());
    }

    @Test
    public void requiredAttributeInheritanceGapTest() throws IOException, RecognitionException {
        final String source = "package test;\n" +
                "struct Base { required(4) byte x; }" +
                "struct Test : Base {\n" +
                "  required(3) byte r3;" +
                "  byte r0;\n" +
                "  required byte r1;\n" +
                "}\n";

        Context ctx = new Context();
        ctx.loadMainUnit(source);
        Unit unit = ctx.getMainUnit();
        Linker linker = new Linker(ctx);
        linker.link();

        Struct test = unit.findStruct("Test");
        Assert.assertTrue(test.findAttribute("r0").slot.required);
        Assert.assertTrue(test.findAttribute("r1").slot.required);
        Assert.assertTrue(test.findAttribute("r3").slot.required);
        Assert.assertEquals(0 + 5, test.findAttribute("r0").getSlotIndex());
        Assert.assertEquals(1 + 5, test.findAttribute("r1").getSlotIndex());
        Assert.assertEquals(3 + 5, test.findAttribute("r3").getSlotIndex());
    }

    @Test
    public void optionalAttributeInheritanceGapTest() throws IOException, RecognitionException {
        final String source = "package test;\n" +
                "struct Base { optional(4) byte x; }" +
                "struct Test : Base {\n" +
                "  optional(3) byte o3;" +
                "  optional byte o0;\n" +
                "}\n";

        Context ctx = new Context();
        ctx.loadMainUnit(source);
        Unit unit = ctx.getMainUnit();
        Linker linker = new Linker(ctx);
        linker.link();

        Struct test = unit.findStruct("Test");
        Assert.assertFalse(test.findAttribute("o0").slot.required);
        Assert.assertFalse(test.findAttribute("o3").slot.required);
        Assert.assertEquals(0 + 5, test.findAttribute("o0").getSlotIndex());
        Assert.assertEquals(3 + 5, test.findAttribute("o3").getSlotIndex());
    }
}
