package net.bartipan.iomonkey2.tests.linker;

import net.bartipan.iomonkey2.linker.Linker;
import net.bartipan.iomonkey2.model.Context;
import net.bartipan.iomonkey2.model.Struct;
import net.bartipan.iomonkey2.model.Unit;
import net.bartipan.iomonkey2.tests.AssertUtils;
import org.antlr.runtime.RecognitionException;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;

/**
 * Created by honza on 9/27/14.
 */
public class LinkerTest {

    @Test
    public void testBasicInheritance() throws IOException, RecognitionException {
        final String source = "package test;\n" +
                "struct Base { string name; }\n" +
                "struct DerivedA : Base { float x; float y; }\n" +
                "struct DerivedB : Base { int x; int y; }\n";

        Context ctx = new Context();
        ctx.loadMainUnit(source);

        Unit unit = ctx.getMainUnit();

        Linker linker = new Linker(ctx);
        linker.link();

        AssertUtils.ensureStructExtends(unit, "DerivedA", "Base");
        AssertUtils.ensureStructExtends(unit, "DerivedB", "Base");
        AssertUtils.ensureStructIsExtended(unit, "Base", "DerivedA", "DerivedB");
    }

    @Test
    public void testInheritanceAttributeLookup() throws IOException, RecognitionException {
        final String source = "package test;\n" +
                "struct Base { string name; }\n" +
                "struct DerivedA : Base { float x; float y; }\n" +
                "struct DerivedB : Base { int x; int y; }\n";

        Context ctx = new Context();
        ctx.loadMainUnit(source);

        Unit unit = ctx.getMainUnit();

        Linker linker = new Linker(ctx);
        linker.link();

        Struct b = unit.findStruct("Base");
        Struct da = unit.findStruct("DerivedA");
        Struct db = unit.findStruct("DerivedB");

        Assert.assertNotNull(da.findAttribute("name"));
        Assert.assertNotNull(db.findAttribute("name"));
        Assert.assertNull(b.findAttribute("x"));
    }

    @Test
    public void testStructLinkInfoSlotOrder() throws IOException, RecognitionException {
        final String source = "package test;\n" +
                "struct Req { required int a; required(3) int c; required(1) int b; }\n" +
                "struct Opt { optional int a; optional(3) int c; optional(1) int b; }\n" +
                "struct Mixed {\n" +
                "  optional int oa; optional(3) int oc; optional(1) int ob;\n" +
                "  required int ra; required(3) int rc; required(1) int rb;\n" +
                "}\n" +
                "struct Mixed2 {\n" +
                "  optional int oa;\n" +
                "  required int ra;\n" +
                "  optional(1) int ob;\n" +
                "  required(3) int rc;\n" +
                "  optional(3) int oc;\n" +
                "  required(1) int rb;\n" +
                "}\n" +
                "\n";
        Context ctx = new Context();
        ctx.loadMainUnit(source);

        Unit unit = ctx.getMainUnit();

        Linker linker = new Linker(ctx);
        linker.link();

        AssertUtils.ensureStructAttributeOrder(unit, "Req", true, "a", "b", "c");
        AssertUtils.ensureStructAttributeOrder(unit, "Opt", false, "a", "b", "c");
        AssertUtils.ensureStructAttributeOrder(unit, "Mixed", true, "ra", "rb", "rc");
        AssertUtils.ensureStructAttributeOrder(unit, "Mixed", false, "oa", "ob", "oc");
        AssertUtils.ensureStructAttributeOrder(unit, "Mixed2", true, "ra", "rb", "rc");
        AssertUtils.ensureStructAttributeOrder(unit, "Mixed2", false, "oa", "ob", "oc");
    }

    @Test
    public void testStructInheritanceLinkInfoSlotOrder() throws IOException, RecognitionException {
        final String source = "package test;\n" +
                "struct Mixed {\n" +
                "  optional int oa; optional(2) int oc; optional(1) int ob;\n" +
                "  required int ra; required(2) int rc; required(1) int rb;\n" +
                "}\n" +
                "struct Mixed2 : Mixed {\n" +
                "  optional int od;\n" +
                "  required int rd;\n" +
                "  optional(1) int oe;\n" +
                "  required(2) int rf;\n" +
                "  optional(2) int of;\n" +
                "  required(1) int re;\n" +
                "}\n" +
                "\n";
        Context ctx = new Context();
        ctx.loadMainUnit(source);

        Unit unit = ctx.getMainUnit();

        Linker linker = new Linker(ctx);
        linker.link();

        AssertUtils.ensureStructAttributeOrder(unit, "Mixed", true, "ra", "rb", "rc");
        AssertUtils.ensureStructAttributeOrder(unit, "Mixed", false, "oa", "ob", "oc");
        AssertUtils.ensureStructAttributeOrder(unit, "Mixed2", true, "rd", "re", "rf");
        AssertUtils.ensureStructAttributeOrder(unit, "Mixed2", false, "od", "oe", "of");

        Struct m2 = unit.findStruct("Mixed2");
        Assert.assertEquals(3, m2.findAttribute("rd").getSlotIndex());
        Assert.assertEquals(3, m2.findAttribute("od").getSlotIndex());
        Assert.assertEquals(4, m2.findAttribute("re").getSlotIndex());
        Assert.assertEquals(4, m2.findAttribute("oe").getSlotIndex());
        Assert.assertEquals(5, m2.findAttribute("rf").getSlotIndex());
        Assert.assertEquals(5, m2.findAttribute("of").getSlotIndex());
    }
}
