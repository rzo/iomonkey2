package net.bartipan.iomonkey2.tests.linker;

import net.bartipan.iomonkey2.linker.Linker;
import net.bartipan.iomonkey2.model.Context;
import net.bartipan.iomonkey2.model.Unit;
import net.bartipan.iomonkey2.tests.AssertUtils;
import org.antlr.runtime.RecognitionException;
import org.junit.Test;

import java.io.IOException;

/**
 * Created by honza on 9/28/14.
 */
public class LinkerReferencesTest {

    @Test
    public void structReferencesTest() throws IOException, RecognitionException {
        final String source = "package test;\n" +
                "struct Document {\n" +
                "  Header header;\n" +
                "  Element root;\n" +
                "}\n" +
                "struct Header {\n" +
                "  byte[4] magic;\n" +
                "  short majorVer;\n" +
                "  short minorVer;\n" +
                "}\n" +
                "struct Element {\n" +
                "  Style style;\n" +
                "  string text;\n" +
                "}\n" +
                "struct Style {\n" +
                "  string font;\n" +
                "}\n";
        Context ctx = new Context();
        ctx.loadMainUnit(source);

        Unit unit = ctx.getMainUnit();

        Linker linker = new Linker(ctx);
        linker.link();

        AssertUtils.ensureUnitHasStructReferences(unit, "header", "Header", "root", "Element", "style", "Style");
    }

    @Test
    public void enumReferencesTest() throws IOException, RecognitionException {
        final String source = "package test;\n" +
                "struct Document {\n" +
                "  Header header;\n" +
                "  Element root;\n" +
                "}\n" +
                "struct Header {\n" +
                "  byte[4] magic;\n" +
                "  short majorVer;\n" +
                "  short minorVer;\n" +
                "  DocType docType;\n" +
                "}\n" +
                "struct Element {\n" +
                "  Style style;\n" +
                "  string text;\n" +
                "}\n" +
                "struct Style {\n" +
                "  string font;\n" +
                "  Alignment align;\n" +
                "}\n" +
                "enum DocType {\n" +
                "  NOTE=0, MEMO=1, ARTICLE=2" +
                "}\n" +
                "enum Alignment {\n" +
                "  LEFT = 0, RIGHT = 1, CENTER = 2, JUSTIFY = 3\n" +
                "}\n";
        Context ctx = new Context();
        ctx.loadMainUnit(source);

        Unit unit = ctx.getMainUnit();

        Linker linker = new Linker(ctx);
        linker.link();

        AssertUtils.ensureUnitHasStructReferences(unit, "header", "Header", "root", "Element", "style", "Style");
    }
}
