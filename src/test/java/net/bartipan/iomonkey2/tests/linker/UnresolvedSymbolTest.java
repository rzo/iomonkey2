package net.bartipan.iomonkey2.tests.linker;

import net.bartipan.iomonkey2.tests.AssertUtils;
import org.antlr.runtime.RecognitionException;
import org.junit.Test;

import java.io.IOException;

/**
 * Created by honza on 9/28/14.
 */
public class UnresolvedSymbolTest {

    @Test
    public void unresolvedAttributeTest() throws IOException, RecognitionException {
        final String source = "package test;\n" +
                "struct Node { NodeA nodeA; }\n";

        AssertUtils.ensureUnresolvedException(source);

        AssertUtils.ensureUnresolvedException("package test; struct Node { Missing[] missing; }");
        AssertUtils.ensureUnresolvedException("package test; struct Node { required Missing[] missing; }");
        AssertUtils.ensureUnresolvedException("package test; struct Node { required(0) Missing[] missing; }");
        AssertUtils.ensureUnresolvedException("package test; struct Node { optional(0) Missing[] missing; }");
    }

    @Test
    public void unresolvedSuperTest() throws IOException, RecognitionException {
        final String source = "package test;\n" +
                "struct Node : Base { int x; }\n";

        AssertUtils.ensureUnresolvedException(source);
    }
}
